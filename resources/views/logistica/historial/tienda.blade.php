@extends ('layouts.admin')
@section ('contenido')

<div>
  <h1 class="asd">Material N° {{$var}} - {{strtoupper($nombre_mat[0]->descrip_material)}}</h1>

  <div class="">
    <div class="row">
      <div class="col-md-2">
        <p>Fecha Inicial</p>
        <input type="date" id="fecha_inicial" class="form-control">
      </div>
      <div class="col-md-2">
        <p>Fecha Final</p>
        <input type="date" id="fecha_final" class="form-control ">
      </div>
    </div>
        <button id="filtrar" class="bttn-unite bttn-md bttn-warning  mr-sm-5" style="margin:10px">filtrar</button>
  </div>
</div>
  <hr class=" my-4">
<div class="float-left">
  <div class="input-group mb-5">
    <a href="" data-target="#modal-edit-{{$var}}" data-toggle="modal">
     <button class="bttn-unite bttn-md bttn-primary det mr-sm-2" >Cambiar Ubicacion</button></a>
  </div>
  @include('logistica.historial.tienda_modal')
</div>
<div class="float-right">
  <div class="input-group mb-5">
    <button class="bttn-unite bttn-md bttn-primary det mr-sm-2" >Ver todo</button>
    <button class="bttn-unite bttn-md bttn-primary ing mr-sm-2" >Ingresos</button>
    <button class="bttn-unite bttn-md bttn-primary sal mr-sm-2" >Salidas</button>
    <button class="bttn-unite bttn-md bttn-primary dev mr-sm-2" >Devoluciones</button>
  </div>
</div>
<div id="detalle">
  <div class="table-responsive" >
    <h2>Detalle Ingresos-Salidas-Devoluciones</h2>
    <table id="det" class="display table stacktable">
      <thead>
        <tr>
          <th>Fecha de ingreso</th>
          <th>Fecha de salida</th>
          <th>Fecha de devolucion</th>
          <th>Cantidad de ingreso</th>
          <th>Cantidad de Salida</th>
          <th>stock actualizado</th>
          <th>Personal Traslado</th>
          <th>Comentario</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
<div id="Ingreso" style="display:none">
  <div class="table-responsive" >
    <h2>Detalle Ingresos</h2>
    <table id="ing" class="display table stacktable">
      <thead>
        <tr>
          <th>Fecha de ingreso</th>
          <th>Fecha de salida</th>
          <th>Fecha de devolucion</th>
          <th>Cantidad de ingreso</th>
          <th>Cantidad de Salida</th>
          <th>stock actualizado</th>
          <th>Personal Traslado</th>
          <th>Comentario</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
<div id="Salidas" style="display:none">
  <div class="table-responsive" >
    <h2>Detalle Salidas</h2>
    <table id="sal" class="display table stacktable">
      <thead>
        <tr>
          <th>Fecha de ingreso</th>
          <th>Fecha de salida</th>
          <th>Fecha de devolucion</th>
          <th>Cantidad de ingreso</th>
          <th>Cantidad de Salida</th>
          <th>stock actualizado</th>
          <th>Personal Traslado</th>
          <th>Comentario</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
<div id="Devoluciones" style="display:none">
  <div class="table-responsive" >
    <h2>Detalle Devoluciones</h2>
    <table id="dev" class="table stacktable">
      <thead>
        <tr>
          <th>Fecha de ingreso</th>
          <th>Fecha de salida</th>
          <th>Fecha de devolucion</th>
          <th>Cantidad de ingreso</th>
          <th>Cantidad de Salida</th>
          <th>stock actualizado</th>
          <th>Personal Traslado</th>
          <th>Comentario</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
<a href="{!! url('logistica/tiendas') !!}" >
  <button class="bttn-unite bttn-md bttn-danger ">Atras</button></a>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

<script type="text/javascript">
  var det=$("#det").DataTable({"order": [[ 1, "asc" ]]});
  var ing=$("#ing").DataTable({"order": [[ 1, "asc" ]]});
  var sal=$("#sal").DataTable({"order": [[ 1, "asc" ]]});
  var dev=$("#dev").DataTable({"order": [[ 1, "asc" ]]});
  var data =<?php echo $detalle;?>;
  $("#filtrar").on('click',function(){
    var fecha_inicial=$("#fecha_inicial").val();
    var fecha_final=$("#fecha_final").val();
    if(fecha_inicial!="" && fecha_final!="" )
    {
      var fecha_ingreso
      var fecha_salida
      var fecha_devolucion
      det.rows().remove().draw();
      ing.rows().remove().draw();
      sal.rows().remove().draw();
      dev.rows().remove().draw();
      $.each(data,function(key,value){
        let fecha=""
        let estado=""
        if(value.fecha_ingreso!=null)
        {
          fecha=value.fecha_ingreso.split(" ");
          estado=0
        }
        if (value.fecha_salida!=null) {
          fecha=value.fecha_salida.split(" ");
          estado=1
        }
        if (value.fecha_devolucion!=null) {
          fecha=value.fecha_devolucion.split(" ");
          estado=2
        }
        if(fecha[0]>=fecha_inicial && fecha[0]<=fecha_final)
        {
          det.row.add( [
            value.fecha_ingreso,
            value.fecha_salida,
            value.fecha_devolucion,
            value.cantidad_ingresada,
            value.cantidad_salida,
            value.stock,
            value.trasladador_material,
            value.comentario_devolucion
          ] ).draw( false );
          switch (estado) {
            case 0:
            ing.row.add( [
              value.fecha_ingreso,
              value.fecha_salida,
              value.fecha_devolucion,
              value.cantidad_ingresada,
              value.cantidad_salida,
              value.stock,
              value.trasladador_material,
              value.comentario_devolucion
            ] ).draw( false );
            break;
            case 1:
            sal.row.add( [
              value.fecha_ingreso,
              value.fecha_salida,
              value.fecha_devolucion,
              value.cantidad_ingresada,
              value.cantidad_salida,
              value.stock,
              value.trasladador_material,
              value.comentario_devolucion
            ] ).draw( false );
            break;
            case 2:
            dev.row.add( [
              value.fecha_ingreso,
              value.fecha_salida,
              value.fecha_devolucion,
              value.cantidad_ingresada,
              value.cantidad_salida,
              value.stock,
              value.trasladador_material,
              value.comentario_devolucion
            ] ).draw( false );
            break;
            default:
          }
        }
      })
    }
    else {
      alert("Ingrese fechas correctas")
    }

  })

  $(".det").click(function(){
    $("#Ingreso").fadeOut()
    $("#Salidas").fadeOut()
    $("#Devoluciones").fadeOut()
    $("#detalle").fadeIn( )
  })
  $(".ing").click(function(){
    $("#detalle").fadeOut()
    $("#Salidas").fadeOut()
    $("#Devoluciones").fadeOut()
    $("#Ingreso").fadeIn( )
  })
  $(".sal").click(function(){
    $("#detalle").fadeOut()
    $("#Ingreso").fadeOut()
    $("#Devoluciones").fadeOut()
    $("#Salidas").fadeIn( )
  })
  $(".dev").click(function(){
    $("#detalle").fadeOut()
    $("#Salidas").fadeOut()
    $("#Ingreso").fadeOut()
    $("#Devoluciones").fadeIn( )
  })
</script>
@endsection
