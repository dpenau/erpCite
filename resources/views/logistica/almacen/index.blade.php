@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">
</div>
<div>
<h3 class="font-weight-bold">Listado de Almacenes
  <a href="almacen/create">
    <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nuevo Almacen</button></a></h3>
</div>
<div class="x_content table-responsive">
  <table id="example" class="display">
    <thead>
      <tr align="center">
        <th>Categoria del Almacen</th>
        <th>Nombre del Almacen</th>
        <th>Encargado del Almacen</th>
        <th>Ver mas</th>
        <th>Editar</th>
        <th>Eliminar</th>
      </tr>
    </thead>
    <tbody align="center">
      @foreach($almacen as $trab)
      @if($trab->estado_almacen==0)
      <tr class="bg-danger" align="center">
      @else
      <tr align="center">
      @endif
        <td>{{$trab->nom_categoria}}</td>
        <td>{{$trab->nom_almacen}}</td>
        <td>{{$trab->apellido_paterno." ".$trab->apellido_materno.", ".$trab->nombres}}</td>
        <td>
          <a href="" data-target="#modal-vermas-{{$trab->cod_almacen}}" data-toggle="modal">
           <button class="bttn-unite bttn-md bttn-primary"><i class="fas fa-eye"></i></button></a>
        </td>
        <td>
          <a href="" data-target="#modal-edit-{{$trab->cod_almacen}}" data-toggle="modal">
           <button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-edit"></i></button></a>
        </td>
        @if($trab->estado_almacen==1)
        <td>
          <a href="" data-target="#modal-eliminar-{{$trab->cod_almacen}}" data-toggle="modal">
           <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
        </td>
        @else
        <td>
          <a href="" data-target="#modal-activar-{{$trab->cod_almacen}}" data-toggle="modal">
           <button class="bttn-unite bttn-md bttn-success"><i class="fas fa-check-circle"></i></button></a>
        </td>
        @endif
      </tr>
      @include('logistica.almacen.modaleditar')
      @include('logistica.almacen.modalactivar')
      @include('logistica.almacen.eliminar')
      @include('logistica.almacen.vermas')
      @endforeach
    </tbody>
  </table>
</div>
@endsection
