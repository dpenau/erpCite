<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$trab->cod_almacen}}">
	{{Form::Open(array('action'=>array('AlmacenController@update',$trab->cod_almacen
  ),'method'=>'patch'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Editar Almacen</h4>
			</div>
			<div class="modal-body">
				<input type="text" style="display:none" name="cod_almacen_editar" value="{{$trab->cod_almacen}}">
        <div class="row">
          <div class="form-group  col-md-12">
            <label for="">Encargado</label>
            <select name="encargado" class="custom-select" required>
              <option value="" disabled>Encargado</option>
              @foreach ($trabajadores as $dist)
                @if($dist->DNI_trabajador==$trab->DNI_trabajador)
                  <option value="{{$trab->DNI_trabajador}}"  selected>{{$trab->apellido_paterno." ".$trab->apellido_materno.", ".$trab->nombres}}</option>
                @else
                  <option value="{{$dist->DNI_trabajador}}" >{{$dist->apellido_paterno." ".$dist->apellido_materno.", ".$dist->nombres}}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="form-group  ">
          <label for="total_orden_compra">Nombre del almacen:</label>
           <input type="text" class="form-control" maxlength="100" name="nombre" value="{{$trab->nom_almacen}}">
        </div>
        <div class="row">
          <div class="form-group  col-md-4">
            <label for="">Categoria</label>
            <select required="required" name="prioridad" class="custom-select">
              <option value="" selected disabled >Categoria</option>
              @foreach ($categorias as $dist)
                @if($dist->cod_categoria==$trab->cod_categoria)
                  <option value="{{$trab->cod_categoria}}"  selected>{{$trab->nom_categoria}}</option>
                @else
                <option value="{{$dist->cod_categoria}}" >{{$dist->nom_categoria}}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>
       
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
