<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
    id="modal-edit-{{ $pro->RUC_proveedor }}">
    {{ Form::Open(['action' => ['ProveedorController@update', $pro->RUC_proveedor], 'method' => 'patch']) }}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Editar Proveedor</h4>
            </div>
            <div class="modal-body">
                <input type="text" style="display:none" name="ruc_editar" value="{{ $pro->RUC_proveedor }}">
                <div class="row">
                    <div class="form-group  col-md-12 col-xs-12">
                        <select required="required" id="sub-categoria" name="tipo_proveedor" class="custom-select">
							<option value=""  disabled>Tipo de Proveedor</option>
							@foreach($subcategoria as $sub)
								@if($sub->cod_subcategoria == $pro->cod_subcategoria)
									<option value="{{$sub->cod_subcategoria}}" selected>{{$sub->nom_categoria}}-{{$sub->nom_subcategoria}}</option>
								@else
									<option value="{{$sub->cod_subcategoria}}">{{$sub->nom_categoria}}-{{$sub->nom_subcategoria}}</option>
								@endif
							@endforeach
						</select>

                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-12   col-xs-12">
                        <label for="total_orden_compra">Razon Social:</label>
                        <input maxlength="50" required="required" type="text" value="{{ $pro->nom_proveedor }}"
                            name="nomb_proveedor" required="required" class="form-control ">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-6   col-xs-12">
                        <label for="total_orden_compra">Direccion 1:</label>
                        <textarea class="form-control" maxlength="100" rows="3" name="dir_proveedor"
                            required="required">{{ $pro->direc_proveedor }}</textarea>
                    </div>
                    <div class="form-group  col-md-6   col-xs-12">
                        <label for="total_orden_compra">Direccion 2 (Tienda):</label>
                        <textarea class="form-control" maxlength="100" rows="3"
                            name="tienda_proveedor">{{ $pro->direc_tienda }}</textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-6   col-xs-12">
                        <label for="total_orden_compra">Telefono del proveedor:</label>
                        <input onkeypress="return isNumberKey(event)" value="{{ $pro->cel_proveedor }}" maxlength="9"
                            type="text" name="tele_proveedor" required="required" class="form-control ">
                    </div>
                    <div class="form-group  col-md-6   col-xs-12">
                        <label for="total_orden_compra">Correo:</label>
                        <input type="email" name="correo_proveedor" value="{{ $pro->correo_proveedor }}"
                            maxlength="50" class="form-control ">
                    </div>

                </div>
                <div class="row">
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Nombre de contacto 1:</label>
                        <input type="text" name="nom_contacto" value="{{ $pro->nomb_contacto }}" maxlength="100"
                            class="form-control ">
                    </div>
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Telefono de contacto 1:</label>
                        <input onkeypress="return isNumberKey(event)" value="{{ $pro->telefono_contacto }}"
                            maxlength="9" type="text" name="tel_contacto" class="form-control ">
                    </div>
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Correo de contacto 1:</label>
                        <input type="email" name="correo_contacto" value="{{ $pro->correo_contacto }}" maxlength="50"
                            class="form-control ">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Nombre de contacto 2:</label>
                        <input type="text" name="nom_contacto2" value="{{ $pro->nomb_contacto2 }}" maxlength="100"
                            class="form-control ">
                    </div>
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Telefono de contacto 2:</label>
                        <input onkeypress="return isNumberKey(event)" value="{{ $pro->telefono_contacto2 }}"
                            maxlength="9" type="text" name="tel_contacto2" class="form-control ">
                    </div>
                    <div class="form-group  col-md-4   col-xs-12">
                        <label for="total_orden_compra">Correo de contacto 2:</label>
                        <input type="email" name="correo_contacto2" value="{{ $pro->correo_contacto2 }}"
                            maxlength="50" class="form-control ">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
    {{ Form::Close() }}
</div>

	
