@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">

</div>
<div>
<h3 class="font-weight-bold">Indicadores de Proveedor</h3>
</div>
<div class="x_content table-responsive">
    <div row>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Proveedor:</label>
                <select class="custom-select" name="" id="proveedores">
                    <option value="" selected disabled>Seleccione un proveedor</option>
                    @foreach($proveedor as $prov)
                        <option value="{{$prov->RUC_proveedor}}">{{$prov->nom_proveedor}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div align="center">
            <h3 id="cantidad">Cantidad de ordenes en el año del proveedor </h3>
            <select name="" id="año_total">
                <option value="">Seleccione un año</option>
                <option value="2018">2018</option>
                <option value="2019">2019</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
            </select>
            <div id="id_grafico_cantidad">
              <canvas id="Pastel_cantidad"></canvas>
            </div>
    </div>
    <br>
    <div align="center">
            <h3>Compras de todos los proveedores</h3>
            <h1>Proximamente</h1>
            <div id="id_grafico_compras">
              <canvas id="tablas_compras"></canvas>
            </div>
    </div>
    <br>
    <div align="center">
        <h2 id="ultimos">Datos de los ultimos 3 meses en el {{date("Y")}} del proveedor </h2>
    </div>
    <div>
        <h4 id="total">Total de Ordenes de compra hace 3 Meses:  </h4>
        <h4 id="tiempo">Ordenes de Compra entregadas a tiempo:  </h4>
        <h4 id="destiempo">Ordenes de Compra entregadas a destiempo:  </h4>
        <h4 id="porcentaje">Porcentaje de entrega a tiempo:  </h4>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script><script>
  
  $(document).ready( function () {
    $("#año_total").change(()=>{
        if($("#proveedores").val()!=null)
        {
            var ruc=$("#proveedores").val();
            var año=$("#año_total").val();
            var cadena=ruc+"_"+año
            $.ajax({
                type:"get",
                url:"../reporte/obt_todo/"+cadena,
                success:function (data){
                    grafico(data);
                }
            })
        }
    })
    $("#proveedores").change(()=>{
        var data="{{date('Y')}}"
        $("#cantidad").text("Cantidad de ordenes en el año del proveedor "+$("#proveedores option:selected").text())
        $("#ultimos").text("Datos de los ultimos 3 meses en el "+data+" del proveedor "+$("#proveedores option:selected").text())
        var ruc=$("#proveedores").val();
        let html="<canvas id='Pastel_cantidad' ></canvas>";
        $("#id_grafico_cantidad").empty();
        $("#id_grafico_cantidad").append(html);
        $("#año_total").val("")
        $.ajax({
        type:"get",
        url:"../reporte/obt_datos/"+ruc,
        success:function (data){
            datos(data)
        }
      })
    })
    function datos(data)
    {
        var total=0;
        $.each(data,(i,val)=>{
            if(val.fecha_deposito!=null)
            {   
                var fecha_deposito=new Date(val.fecha_deposito);
                var fecha_orden=new Date(val.fecha_entrega);
                var fecha_deposito_string=fecha_deposito.getFullYear()+"/"+fecha_deposito.getMonth()+"/"+fecha_deposito.getDate();
                var fecha_orden_string=fecha_orden.getFullYear()+"/"+fecha_orden.getMonth()+"/"+fecha_orden.getDate();
                var restar=val.fecha_pago;
                if(fecha_deposito_string<=fecha_orden_string)
                {
                    total=total+1
                }
            }
        })
        let tamaño_datos=data.length;
        let porcentaje=(total*100)/tamaño_datos;
        porcentaje=porcentaje.toFixed(0)
        let desti=tamaño_datos-total
        $("#total").text("Total de Ordenes de compra hace 3 Meses:  "+tamaño_datos)
        $("#tiempo").text("Ordenes de Compra entregadas a tiempo:  "+total)
        $("#destiempo").text("Ordenes de Compra entregadas a destiempo:  "+desti)
        $("#porcentaje").text("Porcentaje de entrega a tiempo:  "+porcentaje+"%")
    }
    function grafico(data)
    {
        let totales=[0,0,0,0,0,0,0,0,0,0,0,0]
        $.each(data,(i,val)=>{
            let fecha_orden=new Date(val.fecha_orden_compra);
            let fecha_orden_string=fecha_orden.getMonth();
            switch(fecha_orden_string)
            {
                case 0:totales[0]=totales[0]+1
                break;
                case 1:totales[1]=totales[1]+1
                break;
                case 2:totales[2]=totales[2]+1
                break;
                case 3:totales[3]=totales[3]+1
                break;
                case 4:totales[4]=totales[4]+1
                break;
                case 5:totales[5]=totales[5]+1
                break;
                case 6:totales[6]=totales[6]+1
                break;
                case 7:totales[7]=totales[7]+1
                break;
                case 8:totales[8]=totales[8]+1
                break;
                case 9:totales[9]=totales[9]+1
                break;
                case 10:totales[10]=totales[10]+1
                break;
                case 11:totales[11]=totales[11]+1
                break;
            }
        })
        $.each(totales,(i,value)=>{
            console.log(value)
        })
      let html="<canvas id='Pastel_cantidad' ></canvas>";
      $("#id_grafico_cantidad").empty();
      $("#id_grafico_cantidad").append(html);
      var ctx = document.getElementById('Pastel_cantidad');
      var myChart = new Chart(ctx, {
          type: 'line',
          data: {
              labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
              datasets: [{
                  label: 'Ordenes de Compra',
                  data: totales,
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)'
                  ],
                  borderWidth: 1
              }
            ]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    }
  })
</script>
@endsection
