<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1"
            id="modal-del-nuevo-{{$op->cod_ficha_m}}">
          
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    
                        <input type="hidden"  name="material_ficha" value="10">
                        <input type="hidden"  name="cod_combina" value="{{$listaComb[0]->cod_combinacion}}">
                        <h4 class="modal-title" id="titulo_elim">Eliminar Material de Ficha de Producto</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro que desea eliminar este material? {{$op->cod_ficha_m}}</p>
                        <input type="text" style="display:none" name="codigo_material" id="codigo_material"
                            value="{{$op->cod_ficha_m}}">
                        <input type="text" style="display:none" name="estado" value="0">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="bttn-unite bttn-md bttn-primary " name="guardar_materiales" value="55">Confirmar</button>
                        <button type="button" class="bttn-unite bttn-md bttn-danger"
                            data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
            
        </div>