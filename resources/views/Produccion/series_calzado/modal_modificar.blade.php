<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-update-{{$op->cod_serie}}">
{{Form::Open(array('action'=>array('SeriesCalzadoController@update',$op->cod_serie),'method'=>'patch'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
          <h4 class="modal-title">Modificar Serie: {{$op->nombre_serie}}</h4>
			</div>
			<div class="modal-body">
        <div class="x_content">
                      <div class="row ">
                      	<div class="col-sm-12" align="center">
                                Serie
                              <input readonly maxlength="50" required="required" type="text" id="ruc" required="required" name="cod_serie_mod" class="form-control col-md-4" placeholder="Nombre de Linea*" value="{{$op->cod_serie}}">
                          </div>
                              <div class="col-sm-12" align="center">
                                                          Nombre de Serie
                              <input maxlength="50" required="required" type="text" id="ruc" required="required" name="nombre_serie_mod" class="form-control col-sm-4" placeholder="Nombre de Linea*" value="{{$op->nombre_serie}}">
                          </div>

                          <div class="col-sm-12" align="center">
                               Talla Inicial <input maxlength="50" required="required" type="text" id="ruc" required="required" name="tallaInicial" class="form-control col-sm-4" placeholder="Nombre de Linea*" value="{{$op->tallaInicial}}">
                          </div>
                          <div class="col-sm-12" align="center">
                                                      Talla Final
                              <input maxlength="50" required="required" type="text" id="ruc" required="required" name="tallaFinal" class="form-control col-sm-4" placeholder="Nombre de Linea*" value="{{$op->tallaFinal}}">
                          </div>

                      </div>




      </div>
      <div class="modal-footer">
      <button type="submit" class="bttn-unite bttn-md bttn-primary">Confirmar</button>
        <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
      </div>
			</div>

		</div>
	</div>
	{{Form::Close()}}

</div>
