@extends ('layouts.admin')
@section ('contenido')
<!--
   Desarrollado por
   Jesús Albino Calderón
   México
   ITSZO && UNSA
-->

<link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
<div>
    <h1 class="font-weight-bold">Listado de Ordenes de Producción
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="/Produccion/orden_produccion/create/{{$id_pedido}}"
    ><button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Nueva Orden de Produccion</button></a>
    </h1>
    <hr>
    <h5>Código de Orden de Pedido: {{$id_pedido}}
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

    Cliente: <!--{#{$Cliente}}--> {{$Cliente}}


    </h5>

</div>
<div id="tablas">
    <hr>
    <table id="example" class="display">
        <thead>
            <tr>
                <th>Código</th>

                <th>Fecha</th>
                <th>Estado</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody font-align="center">
            @foreach($ordenProducciones as $ordenProduccion) <tr>
                <td>{{$ordenProduccion->codigo_orden_pedido_produccion}}</td>

                <!--preguntar cual fecha es
                 por  pruebas se puso la fecha de entrega-->
                <td>{{$ordenProduccion->fecha_creacion}}</td>
                @if($ordenProduccion->estado_orden_pedido_produccion==0)
                <td>Normal</td>
                @else
                <td>Urgente</td>
                @endif

                <td>
                    <button class="bttn-unite bttn-md bttn-primary"> <i class="far fa-edit"></i> </button>

                    <a href="/Produccion/orden_produccion/ficha_de_orden_de_produccion/{{$ordenProduccion->codigo_orden_pedido_produccion}}">
                        <button class="bttn-unite bttn-md bttn-warning "><i class="fas fa-eye"></i></button>
                    </a>
                </td>

                <!--td>
                    <button  class="bttn-unite bttn-md btn-outline-secondary"><i class="fa fa-download"></i></button>

                </td-->
            </tr>
            @endforeach
        </tbody>
    </table>
    <a href="/Ventas/pedidos"><button class="bttn-unite bttn-md bttn-danger col-md-2 col-md-offset-5">Atras</button></a>




    @endsection
