<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-delete-{{$trab->DNI_trabajador}}">
	{{Form::Open(array('action'=>array('TrabajadorController@destroy',$trab->DNI_trabajador
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Desactivar Trabajador</h4>
			</div>
			<div class="modal-body">
				<input type="text" style="display:none" name="cod_dni_eliminar" value="{{$trab->DNI_trabajador}}">
				<input type="text" style="display:none" name="accion" value="0">
        <p>¿Está seguro que desea desactivar este personal?</p>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
