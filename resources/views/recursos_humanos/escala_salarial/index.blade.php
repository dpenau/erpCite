@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">

</div>
<div>
<h3 class="font-weight-bold">Escala Salarial<a href="escala_salarial/create">
  <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Registrar en Planilla</button></a>
</h3>
</div>

<div class="x_content table-responsive">
  <table id="example" class="display" style=" width:100%">
    <thead>
      <tr>
        <th>Trabajador</th>
        <th>Sueldo Mensual </th>
        <th>Sueldo Semanal</th>
        <th>Lunes</th>
        <th>Martes</th>
        <th>Miercoles</th>
        <th>Jueves</th>
        <th>Viernes</th>
        <th>Sabado</th>
        <th>Regimen Laboral</th>
        <th>Tipo de Seguro</th>
        <th>Tasa de Beneficio Anual</th>
        <th>Editar</th>
      </tr>
    </thead>
    <tbody>
      @if(count($hora)>4)
        <?php  $lunes=$martes=$miercoles=$jueves=$viernes=$sabado=""?>
        @for($i=0;$i< sizeof($hora);$i++)
          @if ($hora[$i]->dia=="Lu")
            <?php $lunes=$hora[$i]->hora;
              list($hour, $minute) = explode(':', $lunes);
              $lunes=$hour.".".$minute;
            ?>
          @endif
          @if ($hora[$i]->dia=="Ma")
            <?php $martes=$hora[$i]->hora;
            list($hour, $minute) = explode(':', $martes);
            $martes=$hour.".".$minute;
            ?>
          @endif
          @if ($hora[$i]->dia=="Mi")
            <?php $miercoles=$hora[$i]->hora;
            list($hour, $minute) = explode(':', $miercoles);
            $miercoles=$hour.".".$minute;
            ?>
          @endif
          @if ($hora[$i]->dia=="Ju")
            <?php $jueves=$hora[$i]->hora;
            list($hour, $minute) = explode(':', $jueves);
            $jueves=$hour.".".$minute;
            ?>
          @endif
          @if ($hora[$i]->dia=="Vi")
            <?php $viernes=$hora[$i]->hora;
            list($hour, $minute) = explode(':', $viernes);
            $viernes=$hour.".".$minute;
            ?>
          @endif
          @if ($hora[$i]->dia=="Sa")
            <?php $sabado=$hora[$i]->hora;
            list($hour, $minute) = explode(':', $sabado);
            $sabado=$hour.".".$minute;
            ?>
          @endif
        @endfor
        @foreach($escala as $esc)
          @if($esc->estado==0)
          <tr class="bg-danger">
          @endif
          <td>{{$esc->apellido_paterno}} {{$esc->apellido_materno}}, {{$esc->nombres}}</td>
          <td>S/.{{number_format($esc->sueldo_mensual,2)}}</td>
          <td>S/.{{ number_format($esc->sueldo_mensual/30*7,2)}}</td>
          <td>S/.{{number_format((($esc->sueldo_mensual/30*7)*$lunes)/$horas_totales,2)}}</td>
          <td>S/.{{number_format((($esc->sueldo_mensual/30*7)*$martes)/$horas_totales,2)}}</td>
          <td>S/.{{number_format((($esc->sueldo_mensual/30*7)*$miercoles)/$horas_totales,2)}}</td>
          <td>S/.{{number_format((($esc->sueldo_mensual/30*7)*$jueves)/$horas_totales,2)}}</td>
          <td>S/.{{number_format((($esc->sueldo_mensual/30*7)*$viernes)/$horas_totales,2)}}</td>
          <td>S/.{{number_format((($esc->sueldo_mensual/30*7)*$sabado)/$horas_totales,2)}}</td>
          <td>{{$esc->descrip_regimen_laboral}}</td>
          @if($esc->tipo_seguro==1)
          <td>SIS</td>
          @else
          <td>ESSALUD</td>
          @endif
          <td>{{$esc->tasa_beneficio}}%</td>
          <td>
            <a href="" data-target="#modal-edit-{{$esc->cod_trabajador}}" data-toggle="modal">
             <button class="bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button></a>
          </td>
        </tr>
        @include('recursos_humanos.escala_salarial.modal')
        @endforeach
      @endif
    </tbody>
  </table>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready( function () {
} );
</script>
@endsection
