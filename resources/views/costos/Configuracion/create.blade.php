@extends ('layouts.admin')
@section ('contenido')

 {!!Form::open(array('url'=>'Configuracion/Costos_Directos/create','method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}
@if(count($datos)>0)

    <div style="text-align:center;">
      <input type="text" style="display:none" name="accion" value="1">
	<table  class="paleBlueRows" style="margin: 0 auto;">
<thead>
<tr>
<th colspan="12"><h4>DATOS GENERALES</h4></th>
</tr>
</thead>
@foreach($datos as $dat)
  <tr>
    <td><h6>Repartición de Utilidades:</h6></td>
    <td><select name="Repartición" required='required' id="sel2"  class="custom-select">
        <option value="" selected disabled>Seleccionar</option>
        @foreach($reparticion as $rep)
          @if($dat->cod_retencion_utilidades==$rep->cod_retencion_utilidades)
            <option value="{{$rep->cod_retencion_utilidades}}" selected>{{$rep->descrip_retencion_utilidades}}</option>
          @else
            <option value="{{$rep->cod_retencion_utilidades}}">{{$rep->descrip_retencion_utilidades}}</option>
          @endif
        @endforeach
        </select></td>
  </tr>
  <tr>
    <td><h6>TCEA de Capital:</h6></td>
    <td><input type="text"maxlength="3" name="TCEACA" onkeypress="return isNumberKey(event)" value="{{$dat->TCEA_capital}}"   required> %  &nbsp&nbsp  &nbsp &nbsp </td>
  </tr>
  <tr>
    <td><h6>Lead Time:</h6></td>
    <td><input type="text" name="leadtime" onkeypress="return isNumberKey(event)" value="{{$dat->lead_time}}"  required> dias &nbsp &nbsp</td>
  </tr>
  <tr>
    <td><h6>% Reproceso:</h6></td>
    <td><input type="text" maxlength="3"name="reproceso" onkeypress="return isNumberKey(event)"  value="{{$dat->porcentaje_reproceso}}"  required > % &nbsp&nbsp &nbsp &nbsp</td>
  </tr>
  <tr>
    <td><h6>TCEA Crédito:</h6></td>
    <td><input type="text" maxlength="3"name="TCEACR" onkeypress="return isNumberKey(event)" value="{{$dat->TCEA_credito}}"  required> %  &nbsp&nbsp &nbsp &nbsp  </td>
  </tr>
  <tr>
    <td><h6>% Comisión Ventas</h6></td>
    <td><input type="text" maxlength="3"name="comision" onkeypress="return isNumberKey(event)" value="{{$dat->porcentaje_comision_ventas}}"  required> %  &nbsp &nbsp&nbsp&nbsp</td>
  </tr>
  <tr>
    <td><h6>Producción Mensual Promedio:</h6></td>
    <td style="display:none"><input type="text" name="prodmensual_base" value="{{$dat->produccion_promedio}}"></td>
    <td><input type="text" name="prodmensual" onkeypress="return isNumberKey(event)" value="{{$dat->produccion_promedio}}"   required>  pares</td>
  </tr>
  <tr>
    <td><h6>Produccion Total Anual {{date('Y')}}</h6></td>
    <td><input type="text" name="totalprodanual" onkeypress="return isNumberKey(event)" value="{{$dat->total_produccion_anual}}"   required>  pares</td>
  </tr>
  <tr>
    <td><h6>Política de Desarrollo de Prototipo:</h6></td>
    <td><input type="text" name="pdp"onkeypress="return isNumberKey(event)" value="{{$dat->politica_desarrollo_producto}}"  required>  Meses</td>
  </tr>
  <tr>
    <td><h6>Política de Desarrollo de Hormas:</h6></td>
    <td><input type="text" name="pdh" onkeypress="return isNumberKey(event)" value="{{$dat->politica_desarrollo_horma}}" required>  Meses</td>
  </tr>
  <tr>
    <td><h6>Política de Desarrollo de Troqueles:</h6></td>
    <td><input type="text" name="pdt" onkeypress="return isNumberKey(event)" value="{{$dat->politica_desarrollo_troqueles}}"  required>  Meses</td>
  </tr>
</table>
<br>
<p><span class="bg-danger text-white">*Al actualizar la produccion mensual promedio debe actualizar el costeo del modelo y la formulacion de precio de venta*</span></p>

<button type="submit" align="center"class="bttn-unite bttn-md bttn-primary ">Guardar</button>
@endforeach
@else
<input type="text" style="display:none" name="accion" value="0">
<table  class="paleBlueRows" style="margin: 0 auto;">
<thead>
<tr>
<th colspan="12"><h4>DATOS GENERALES</h4></th>
</tr>
</thead>
<tr>
  <td><h6>Repartición de Utilidades:</h6></td>
  <td><select name="Repartición" required='required' id="sel2"  class="custom-select">
      <option value="" selected disabled>Seleccionar</option>
      @foreach($reparticion as $rep)
          <option value="{{$rep->cod_retencion_utilidades}}">{{$rep->descrip_retencion_utilidades}}</option>
      @endforeach
      </select></td>
</tr>
<tr>
  <td><h6>TCEA de Capital:</h6></td>
  <td><input type="text"maxlength="3" name="TCEACA" onkeypress="return isNumberKey(event)"    required> %  &nbsp&nbsp  &nbsp &nbsp </td>
</tr>
<tr>
  <td><h6>Lead Time:</h6></td>
  <td><input type="text" name="leadtime" onkeypress="return isNumberKey(event)"   required> dias &nbsp &nbsp</td>
</tr>
<tr>
  <td><h6>% Reproceso:</h6></td>
  <td><input type="text" maxlength="3"name="reproceso" onkeypress="return isNumberKey(event)"    required > % &nbsp&nbsp &nbsp &nbsp</td>
</tr>
<tr>
  <td><h6>TCEA Crédito:</h6></td>
  <td><input type="text" maxlength="3"name="TCEACR" onkeypress="return isNumberKey(event)"   required> %  &nbsp&nbsp &nbsp &nbsp  </td>
</tr>
<tr>
  <td><h6>% Comisión Ventas</h6></td>
  <td><input type="text" maxlength="3"name="comision" onkeypress="return isNumberKey(event)"  required> %  &nbsp &nbsp&nbsp&nbsp</td>
</tr>
<tr>
  <td><h6>Producción Mensual Promedio:</h6></td>
  <td><input type="text" name="prodmensual" onkeypress="return isNumberKey(event)"    required>  pares</td>
</tr>
<tr>
  <td><h6>Política de Desarrollo de Prototipo:</h6></td>
  <td><input type="text" name="pdp"onkeypress="return isNumberKey(event)"  required>  meses</td>
</tr>
<tr>
  <td><h6>Política de Desarrollo de Hormas:</h6></td>
  <td><input type="text" name="pdh" onkeypress="return isNumberKey(event)"  required>  meses</td>
</tr>
<tr>
  <td><h6>Política de Desarrollo de Troqueles:</h6></td>
  <td><input type="text" name="pdt" onkeypress="return isNumberKey(event)"   required>  meses</td>
</tr>
</table>
<button type="submit" align="center"class="bttn-unite bttn-md bttn-primary ">Guardar</button>


    </div>
@endif
{!!Form::close()!!}




<style type="text/css">table.paleBlueRows {
  font-family: "Times New Roman", Times, serif;
  border: 1px solid #FFFFFF;
  width: 650px;
  height: 200px;
  text-align: center;
  border-collapse: collapse;

}
table.paleBlueRows td, table.paleBlueRows th {
  border: 1px solid #FFFFFF;
  padding: 3px 2px;
}
table.paleBlueRows tbody td {
  font-size: 13px;
}
table.paleBlueRows tr:nth-child(even) {
  background: #D0E4F5;
}
table.paleBlueRows thead {
  background: #0B6FA4;
  border-bottom: 5px solid #FFFFFF;
}
table.paleBlueRows thead th {
  font-size: 17px;
  font-weight: bold;
  color: #FFFFFF;
  text-align: center;
  border-left: 2px solid #FFFFFF;
}
table.paleBlueRows thead th:first-child {
  border-left: none;
}

table.paleBlueRows tfoot {
  font-size: 14px;
  font-weight: bold;
  color: #333333;
  background: #D0E4F5;
  border-top: 3px solid #444444;
}
table.paleBlueRows tfoot td {
  font-size: 14px;
}
</style>

@endsection
