<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-eliminar-{{$matsu->codigo_suministro}}">
	{{Form::Open(array('action'=>array('MaterialSuministroController@destroy',$matsu->codigo_suministro
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Eliminar Material Indirecto o Suministro</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea eliminar</p>
        <input type="text" style="display:none" name="emailmatesu" value="{{$matsu->codigo_suministro}}">
        <input type="text" style="display:none" name="estado_suministro" value="0">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-slant bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-slant bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
