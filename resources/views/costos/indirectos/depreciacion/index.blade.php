@extends ('layouts.admin')
@section('contenido')
<div class="preloader">

</div>
<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
            <div class="x_title ">
                <h1 class="d-inline font-weight-bold ">Gasto de Depreciación y Mantenimiento </h1>
                <!--<button type="submit" class="bttn-unite bttn-md bttn-success" target="_blank" id="sub"><i class="fas fa-plus"></i></button>-->
                <a href='depreciacion/create'>
                <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Activo</button></a>
                <div class="clearfix"></div>
            </div>

            <div class="x_content table-responsive">
                <table id="example" class="display">
                    <thead>
                        <tr>
                            <th>Área</th>
                            <th>Descripción del Activo</th>
                            <th>Año de Compra</th>
                            <th>Valor Unitario</th>
                            <th>Número de Activos</th>
                            <th>Depreciación Mensual</th>
                            <th>Estado</th>
                            <th>Gasto por Mantenimiento</th>
                            <th>Frecuencia Anual</th>
                            <th>Gasto Mensual</th>
                            <th>Ver mas</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($depreciacion as $dep)
                        <tr>
                            <td>{{$dep->descrip_area}}</td>
                            <td>{{$dep->descrip_activo}}</td>
                            <td>{{$dep->anio_compra}}</td>
                            <td>{{$dep->valor_unitario_sin_IGV}}</td>
                            <td>{{$dep->cantidad}}</td>

                            <?php $anio= date('Y')-($dep->anio_compra-$dep->anio_uso);?>
                            @if($anio<=$dep->anios_vida_util)
                            <td class="depreciacion">{{$dep->depreciacion_mensual}}</td>
                            <td>ACTIVO</td>
                            @else
                            <td >0</td>
                            <td>DEPRECIADO</td>
                            @endif
                            <td >{{$dep->gasto_mantenimiento}}</td>
                            <td>{{$dep->frecuencia_mantenimiento_anual}}</td>
                            <td class="gasto">{{$dep->gasto_mensual_depreciacion}}</td>

                            <td>
                                <a href="" data-target="#modal-vermas-{{$dep->cod_activo}}" data-toggle="modal">
                                <button class="bttn-unite bttn-md bttn-warning "><i class="fas fa-search"></i></button></a>
                            </td>

                            <td>
                                <a href="" data-target="#modal-eliminar-{{$dep->cod_activo}}" data-toggle="modal">
                                <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                            </td>
                        </tr>
                        @include('costos.indirectos.depreciacion.eliminar')
                        @include('costos.indirectos.depreciacion.vermas')
                        @endforeach
                    </tbody>
                </table>
                <h4 id="cdp">Costo Total de Depreciación</h4>
                <h4 id="cmant">Costo Total de Mantenimiento</h4>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
          var gasto=$("td[class~='gasto']");
          var depreciacion=$("td[class~='depreciacion']");
          var sumagasto=0.00;
          var sumadepreciacion=0.00;
          for(var i=0;i<gasto.length;i++)
          {
            var num=parseFloat(gasto[i].innerText)
            sumagasto=sumagasto+num;
          }
          for(var i=0;i<depreciacion.length;i++)
          {
            var num=parseFloat(depreciacion[i].innerText)
            sumadepreciacion=sumadepreciacion+num;
          }
          $("#cdp").html("Costo Total de Depreciación S/."+ sumadepreciacion.toFixed(6));
          $("#cmant").html("Costo Total de Mantenimiento S/."+ sumagasto.toFixed(6));
        });
    </script>

@endsection
