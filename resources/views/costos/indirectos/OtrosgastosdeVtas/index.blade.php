@extends ('layouts.admin')
@section('contenido')
<div class="preloader">

</div>
<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
            <div class="x_title ">
                <h1 class="d-inline font-weight-bold ">Gastos de Ventas y Marketing</h1>
                    <div class="list-group d-inline">
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 active ml-4" id="list-home-list" data-toggle="list" href="#list-Prototipo" role="tab" aria-controls="home">Sueldo ventas y Marketing</a>
                        <a class="bttn-slant bttn-md bttn-primary col-md-2 col-md-offset-5 ml-2" id="list-profile-list" data-toggle="list" href="#list-Hormas" role="tab" aria-controls="profile">Otros Gastos de Ventas</a>
                    </div>
                    <div class="clearfix"></div>
            </div>


            <div class="row">
            <div class="col-12">
                <div class="tab-content mt-4" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-Prototipo" role="tabpanel" aria-labelledby="list-home-list">
                <h2 class="d-inline ">Sueldo ventas y Marketing</h2>
                <a href="OtrosgastosdeVtas/crear_sueldo">
                    <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Sueldo ventas y Marketing</button>
                </a>
                <div class="x_content table-responsive">
                    <table id="example" class="display">
                    <thead>
                        <tr>
                          <th></th>
                        <th>Descripcion del puesto</th>
                        <th>Sueldo Mensual en Planilla</th>
                        <th>Beneficio Social en Planilla</th>
                        <th>Otros Sueldos Mensuales</th>
                        <th>Gasto Mensual</th>
                        <th>Fecha de Realizacion</th>
                        <th>Eliminar</th>
                        </tr>
                    </thead>
                      <tbody>
                        @foreach($sueldo as $mano)
                        <tr>
                          <td>
                            @if($mano->estado==2)
                              <div class="bg-info" style="height:20px; width:10px"></div>
                            @endif
                          </td>
                          <td>{{$mano->puesto}}</td>
                          <td>{{$mano->sueldo_mensual}}</td>
                          <td>{{$mano->beneficios}}</td>
                          <td>{{$mano->otros}}</td>
                          <td class="pgasto">{{$mano->gasto_mensual}}</td>
                          <td>{{$mano->fecha_creacion}}</td>
                          <td>
                              <a href="" data-target="#modal-eliminar-{{$mano->id_gastos_sueldos}}" data-toggle="modal">
                                  <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                          </td>
                        </tr>
                        @include('costos.indirectos.OtrosgastosdeVtas.modal')
                        @endforeach
                      </tbody>
                    </table>
                    <h4 id="ctp">Costo Total de Sueldo ventas y Marketing</h4>
                </div>
                </div>


                <div class="tab-pane fade" id="list-Hormas" role="tabpanel" aria-labelledby="list-profile-list">
                <h2 class="d-inline ">Otros Gastos de Ventas</h2>

                <a href="OtrosgastosdeVtas/crear_otro">
                    <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Otros Gastos de Ventas</button>
                </a>
                <div class="x_content table-responsive">
                    <table id="example2" class="display">
                    <thead>
                        <tr>
                        <th>Descripción de Gasto</th>
                        <th>Gasto Unitario</th>
                        <th>Gasto Mensual</th>
                        <th>Fecha de Creacion</th>
                        <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($otro as $ot)
                      <tr>
                        <td>{{$ot->descripcion}}</td>
                        <td>{{$ot->gasto}}</td>
                        <td class="hgasto">{{$ot->gasto}}</td>
                        <td>{{$ot->fecha_creacion}}</td>
                        <td>
                            <a href="" data-target="#modal-eliminar-{{$ot->id_otros}}" data-toggle="modal">
                                <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                        </td>
                      </tr>
                      @include('costos.indirectos.OtrosgastosdeVtas.modal_eliminar')
                      @endforeach
                    </tbody>

                    </table>
                    <h4 id="cth">Otros Gastos de Ventas</h4>
                </div>
                </div>

            </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
      var ptotal=$("td[class~='pgasto']");
      var htotales=$("td[class~='hgasto']");
      var suma=0.00;
      var sumah=0.00;
      for(var i=0;i<ptotal.length;i++)
      {
        var num=parseFloat(ptotal[i].innerText)
        suma=suma+num;
      }
      for(var i=0;i<htotales.length;i++)
      {
        var num=parseFloat(htotales[i].innerText)
        sumah=sumah+num;
      }
      $("#ctp").html("Costo Total de Sueldo ventas y Marketing S/."+ suma.toFixed(6))
      $("#cth").html("Costo Total de Otros Gastos de Ventas S/."+ sumah.toFixed(6))
    </script>

@endsection
