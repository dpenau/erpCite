@extends ('layouts.admin')
@section('contenido')
    <div class="preloader">
    </div>

    <div class="contenedor-tabs">
        <ul class="ul-tab">
            <li name="bloqueA" class="li-tab tabSelected" onclick="openCity('bloqueA')"><a href="#"> Materiales Indirectos y
                    Suministros</a>
            </li>
            <li name="bloqueB" class="li-tab" onclick="openCity('bloqueB')"><a href="#"> Mano de Obra Indirecta</a>
            </li>
            <li name="bloqueC" class="li-tab" onclick="openCity('bloqueC')"><a href="#"> Depreciación y
                    Mantenimiento</a></li>
            <li name="bloqueD" class="li-tab" onclick="openCity('bloqueD')"><a href="#"> Servicios Básicos</a></li>
            <li name="bloqueE" class="li-tab" onclick="openCity('bloqueE')"><a href="#"> Desarrollo de Producto</a>
            </li>
        </ul>
        <div class="subcontenedor-tabs">
            <div id="bloqueA" class="bloqueA bloque">
                @include('costos.indirectos.costos_operacion.material_directo.index')
            </div>
            <div id="bloqueB" class="bloqueB bloque" style="display:none">
                @include('costos.indirectos.costos_operacion.mano_obra.index')
            </div>
            <div id="bloqueC" class="bloqueC bloque" style="display:none">
                @include('costos.indirectos.costos_operacion.depreciacion.index')
            </div>
            <div id="bloqueD" class="bloqueD bloque" style="display:none">
                @include('costos.indirectos.costos_operacion.servicio_basico.index')
            </div>
            <div id="bloqueE" class="bloqueE bloque" style="display:none">
                @include('costos.indirectos.costos_operacion.desarrollo_producto.index')
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script type="text/javascript" language="javascript" class="init">
        function openCity(cityName) {
            var rows = document.getElementsByClassName("li-tab");
            for (let row of rows) {
                row.classList.remove("tabSelected")
            }
            var i;
            var x = document.getElementsByClassName("bloque");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            document.getElementById(cityName).style.display = "block";
            var rows = document.getElementsByName(cityName);
            rows[0].classList.add("tabSelected")
        }
        $(document).ready(function() {
            $('#material_directo').DataTable({
                'columnDefs': [{
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });
            $('#mano_obra').DataTable({
                'columnDefs': [{
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });
            $('#depreciacion').DataTable({
                'columnDefs': [{
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });

            $('#desarrollo_producto-material').DataTable({
                'columnDefs': [{
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });
            $('#desarrollo_producto-procesos').DataTable({
                'columnDefs': [{
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });
            $('#desarrollo_producto-servicios').DataTable({
                'columnDefs': [{
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });
            $('#desarrollo_producto-hormas').DataTable({
                'columnDefs': [{
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });
            $('#desarrollo_producto-troqueles').DataTable({
                'columnDefs': [{
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });
        })
    </script>
@endsection
