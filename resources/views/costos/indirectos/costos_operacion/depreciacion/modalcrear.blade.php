<div id="modal-create-depreciacion" class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog"
    tabindex="-1">
    <form method="POST" action="{{ url('configuracion/merma/create') }}">
        <!-- CSRF Token -->
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Activo</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-12 ">
                            <label>Depreciación</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12 ">
                            <label for="proceso">Proceso:</label>
                            <select name="proceso_id" class="custom-select">
                                <option value="" selected disabled>Procesos</option>
                                @foreach ($procesos as $proceso)
                                    <option value="{{ $proceso->cod_proceso }}">
                                        {{ $proceso->nombre }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-12 ">
                            <label for="total_orden_compra">Descripcion de Activo:</label>
                            <input type="text" class="form-control" maxlength="100" name="porcentaje_merma" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="tipo_merma">Tipo de Activo:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Años de:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 ">
                            <label for="tipo_merma">Tipo de Adquisición:</label>
                            <div class="form-group row">
                                <div class="form-group col-md-6 ">
                                    <label><input type="checkbox" id="cbox1" value="first_checkbox"> Nuevo</label>
                                </div>
                                <div class="form-group col-md-6 ">
                                    <label><input type="checkbox" id="cbox1" value="first_checkbox">
                                        Repotenciado</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="tipo_merma">Años de Uso Antes:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Años de Compra:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="tipo_merma">Costo Unitario:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Numero de:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="tipo_merma">Años Depreciados:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Costo Depreciación:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12 ">
                            <label>Mantenimiento</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 ">
                            <label for="tipo_merma">Mantenimiento:</label>
                            <div class="form-group row">
                                <div class="form-group col-md-6 ">
                                    <label><input type="checkbox" id="cbox1" value="first_checkbox"> Mantenimiento</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="tipo_merma">Costo por:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Frecuencia Anual:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="tipo_merma">Número de:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Costo Mensual:</label>
                            <input type="text" class="form-control" maxlength="100" name="tipo_merma" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Guardar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </form>

</div>
