<div id="modal-create-mano" class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1">
    <form method="POST" action="{{ url('configuracion/merma/create') }}">
        <!-- CSRF Token -->
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nueva Mano de Obra Directa</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-12 ">
                            <label for="proceso">Proceso:</label>
                            <select name="proceso_id" class="custom-select" required>
                                <option value="" selected disabled>Procesos</option>
                                @foreach ($procesos as $proceso)
                                    <option value="{{ $proceso->cod_proceso }}">
                                        {{ $proceso->nombre }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-12 ">
                            <label for="total_orden_compra">Descripcion del Puesto:</label>
                            <input type="text" class="form-control" maxlength="100" name="porcentaje_merma" value="" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12 ">
                            <label for="tipo_merma">Tipo de Sueldo:</label>
                            <div class="form-group row">
                                <div class="form-group col-md-6 ">
                                    <label><input type="checkbox" id="cbox1" value="first_checkbox"> Planilla</label>
                                </div>
                                <div class="form-group col-md-6 ">
                                    <label><input type="checkbox" id="cbox1" value="first_checkbox"> Otros Sueldos</label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="tipo_merma">Sueldo Mensual Planilla:</label>
                            <div class=" input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">S/.</span>
                                </div>
                                <input type="number" onkeypress="" id="costo_mensual" name="costo_mensual"
                                    class="form-control" step="0.0001" min="0" value="" readonly required>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Beneficio Social:</label>
                            <div class=" input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">S/.</span>
                                </div>
                                <input type="number" onkeypress="" id="beneficio_social" name="beneficio_social"
                                    class="form-control" step="0.0001" min="0" value="" readonly required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="tipo_merma">Otros Sueldos:</label>
                            <div class=" input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">S/.</span>
                                </div>
                                <input type="number" onkeypress="" id="otros_sueldos" name="otros_sueldos"
                                    class="form-control" step="0.0001" min="0" value="" readonly required>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Costo Mensual:</label>
                            <div class=" input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">S/.</span>
                                </div>
                                <input type="number" onkeypress="" id="costo_mensual" name="costo_mensual"
                                    class="form-control" step="0.0001" min="0" value="" readonly required>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </form>

</div>
