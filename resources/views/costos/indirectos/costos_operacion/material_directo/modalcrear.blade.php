<div id="modal-create-material" class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1">
    <form method="POST" action="{{ url('costos/indirectos/operacion/materialindirecto/create') }}">
        <!-- CSRF Token -->
        @csrf
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo Material Indirecto y Suministro</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6 ">
                            <label for="proceso_id">Proceso:</label>
                            <select name="proceso_id" class="custom-select" required>
                                <option value="" selected disabled>Procesos</option>
                                @foreach ($procesos as $proceso)
                                    <option value="{{ $proceso->cod_proceso }}">
                                        {{ $proceso->nombre }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tipo_material_id">Tipo de Material</label>
                            <select name="tipo_material_id" class="custom-select" required>
                                <option value="" selected disabled>Tipo Material</option>
                                @foreach ($tipo_materiales_indirectos as $tipo_material_indirecto)
                                    <option value="{{ $tipo_material_indirecto->id }}">
                                        {{ $tipo_material_indirecto->descripcion }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-12 ">
                            <label for="descripcion">Descripcion de Material:</label>
                            <input type="text" class="form-control" name="descripcion" value="" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 ">
                            <label for="unidad_compra_id">Unidad de Compra:</label>
                            <select name="unidad_compra_id" class="custom-select" required>
                                <option value="" selected disabled>Seleccione</option>
                                @foreach ($unidad_compras as $unidad_compra)
                                    <option value="{{ $unidad_compra->cod_unidad_medida }}">
                                        {{ $unidad_compra->descrip_unidad_medida }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="costo_unitario_create">Costo Unitario</label>
                            <div class=" input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">S/.</span>
                                </div>
                                <input type="number" onkeypress="" id="costo_unitario_create" name="costo_unitario"
                                    class="form-control" step="0.0001" min="0" value="0" onchange="costoMensualCreate(event)" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="consumo_create">Consumo:</label>
                            <input type="number" id="consumo_create" class="form-control" name="consumo" step="0.0001" min="0" value="0" onchange="costoMensualCreate(event)" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="meses_duracion_create">Meses de Duración</label>
                            <div class=" input-group">
                                <input type="number" onkeypress="" id="meses_duracion_create" name="meses_duracion"
                                    class="form-control" step="0.0001" min="0" value="0" onchange="costoMensualCreate(event)" required>
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">mes</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="costo_mensual_create">Costo Mensual</label>
                            <div class=" input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">S/.</span>
                                </div>
                                <input type="number" onkeypress="" id="costo_mensual_create" name="costo_mensual"
                                    class="form-control" value="" readonly required>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Guardar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </form>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {

        })
        let costoMensualCreate = (event) => {
            let costo_unitario = $("#costo_unitario_create").val();
            let meses_duracion = $("#meses_duracion_create").val();
            let consumo = $("#consumo_create").val();
            console.log(costo_unitario)
            console.log(meses_duracion)
            console.log(consumo)
            let resultado = calculoCostoMensualCreate(costo_unitario, meses_duracion, consumo);
            console.log(resultado)
            $("#costo_mensual_create").val(resultado);

        }

        let calculoCostoMensualCreate = (cu, md, con) => {
            return (cu * (con / md));
        }

    </script>
</div>
