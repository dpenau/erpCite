<div id="modal-editar-desarrollo-material-{{ $item->id }}" class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1">
    <form method="POST" action="{{ url('costos/indirectos/operacion/desarrollo/materialupdate/'.$item->id) }}">
        <!-- CSRF Token -->
        @csrf
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar Material</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6 ">
                            <label for="proceso_id">Proceso:</label>
                            <select name="proceso_id" class="custom-select" required>
                                <option value="" selected disabled>Procesos</option>
                                <option value="{{ $item->proceso_id }}" selected>
                                    {{ $item->proceso->nombre }}</option>
                                @foreach ($procesos as $proceso)
                                    @if ($proceso->cod_proceso != $item->proceso_id)
                                        <option value="{{ $proceso->cod_proceso }}">
                                            {{ $proceso->nombre }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6 ">
                            <label for="numero_prototipo">N° de Prototipos:</label>
                            <input type="text" class="form-control" name="numero_prototipo" value="{{ $item->numero_prototipo }}" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-12 ">
                            <label for="descripcion_material">Descripcion del Material:</label>
                            <input type="text" class="form-control" name="descripcion_material" value="{{ $item->descripcion_material }}" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="costo_total">Costo Total:</label>
                            <input type="number" step="0.0001" min="0" class="form-control" name="costo_total" value="{{ $item->costo_total }}" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="importe">Importe por:</label>
                            <input type="number" step="0.0001" min="0" class="form-control" name="importe" value="{{ $item->importe }}" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary ">Guardar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </form>

</div>
