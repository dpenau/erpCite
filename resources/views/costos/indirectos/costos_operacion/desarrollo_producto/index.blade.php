<div class="pb-3">
    <h3 class="font-weight-bold">Servicios Básicos

    </h3>
</div>
<div class="x_content table-responsive pt-3">
    <h5 class="pb-3"> Desarrollo del Producto - Materiales
        <a href="" data-target="#modal-create-desarrollo-material" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Material</button></a>
    </h5>
    <table id="desarrollo_producto-material" class="display">
        <thead>
            <tr>
                <th>Proceso/Área</th>
                <th>Descripcion del Material</th>
                <th>N° de Prototipos al mes </th>
                <th>Costo Total</th>
                <th>Importe por Prototipo</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($desarrollo_materiales as $item)
                <tr>
                    <td>{{ $item->proceso->nombre }}</td>
                    <td>{{ $item->descripcion_material }}</td>
                    <td>{{ $item->numero_prototipo }}</td>
                    <td>{{ $item->costo_total }}</td>
                    <td>{{ $item->importe }}</td>
                    <td>
                        <a href="" data-target="#modal-editar-desarrollo-material-{{ $item->id }}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-edit"></i></button></a>
                    </td>
                    <td>
                        <a href="" data-target="#modal-delete-desarrollo-material-{{ $item->id }}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                    </td>
                </tr>
                @include('costos.indirectos.costos_operacion.desarrollo_producto.modaleditarmaterial')
            @endforeach
        </tbody>
    </table>
    <hr class="my-2">
    <h5 class="pb-3"> Desarrollo del Producto - Procesos Productivos
        <a href="" data-target="#modal-create-desarrollo-proceso" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Proceso Productivo</button></a>
    </h5>
    <table id="desarrollo_producto-procesos" class="display">
        <thead>
            <tr>
                <th>Proceso/Área</th>
                <th>Descripcion del Material</th>
                <th>N° de Prototipos al mes </th>
                <th>Costo Total</th>
                <th>Importe por Prototipo</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($procesos_productivos as $item)
                <tr>
                    <td>{{ $item->proceso->nombre }}</td>
                    <td>{{ $item->descripcion_material }}</td>
                    <td>{{ $item->numero_prototipo }}</td>
                    <td>{{ $item->costo_total }}</td>
                    <td>{{ $item->importe }}</td>
                    <td>
                        <a href="" data-target="#modal-editar-servicio-{{ $item->id }}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-edit"></i></button></a>
                    </td>
                    <td>
                        <a href="" data-target="#modal-eliminar-servicio-{{ $item->id }}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <hr class="my-2">
    <h5 class="pb-3">Desarrollo del Producto - Servicios
        <a href="" data-target="#modal-create-desarrollo-servicio" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Servicio</button></a>
    </h5>
    <table id="desarrollo_producto-servicios" class="display">
        <thead>
            <tr>
                <th>Proceso/Área</th>
                <th>Descripcion del Material</th>
                <th>Costo Total</th>
                <th>Importe por Prototipo</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($desarrollo_servicios as $item)
                <tr>
                    <td>{{ $item->proceso->nombre }}</td>
                    <td>{{ $item->descripcion_material }}</td>
                    <td>{{ $item->costo_total }}</td>
                    <td>{{ $item->importe }}</td>
                    <td>
                        <a href="" data-target="#modal-editar-servicio-{{ $item->id }}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-edit"></i></button></a>
                    </td>
                    <td>
                        <a href="" data-target="#modal-eliminar-servicio-{{ $item->id }}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <hr class="my-2">
    <h5 class="pb-3">Desarrollo del Producto - Hormas
        <a href="" data-target="#modal-create-desarrollo-horma" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Operación</button></a>
    </h5>
    <table id="desarrollo_producto-hormas" class="display">
        <thead>
            <tr>
                <th>Proceso/Área</th>
                <th>Descripcion del Material</th>
                <th>Cantidad</th>
                <th>Costo Unitario</th>
                <th>Importe por Prototipo</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($desarrollo_hormas as $item)
                <tr>
                    <td>{{ $item->proceso->nombre }}</td>
                    <td>{{ $item->descripcion_material }}</td>
                    <td>{{ $item->cantidad }}</td>
                    <td>{{ $item->costo_unitario }}</td>
                    <td>{{ $item->importe }}</td>
                    <td>
                        <a href="" data-target="#modal-editar-servicio-{{ $item->id }}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-edit"></i></button></a>
                    </td>
                    <td>
                        <a href="" data-target="#modal-eliminar-servicio-{{ $item->id }}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <hr class="my-2">
    <h5 class="pb-3">Desarrollo del Producto - Troqueles
        <a href="" data-target="#modal-create-desarrollo-troquel" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Operación</button></a>
    </h5>
    <table id="desarrollo_producto-troqueles" class="display">
        <thead>
            <tr>
                <th>N°</th>
                <th>Proceso/Área</th>
                <th>Descripcion del Material</th>
                <th>N° de Prototipos al mes </th>
                <th>Costo Total</th>
                <th>Importe por Prototipo</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($desarrollo_hormas as $item)
                <tr>
                    <td>{{ $item->proceso->nombre }}</td>
                    <td>{{ $item->descripcion_material }}</td>
                    <td>{{ $item->cantidad }}</td>
                    <td>{{ $item->costo_unitario }}</td>
                    <td>{{ $item->importe }}</td>
                    <td>
                        <a href="" data-target="#modal-editar-servicio-{{ $item->id }}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-edit"></i></button></a>
                    </td>
                    <td>
                        <a href="" data-target="#modal-eliminar-servicio-{{ $item->id }}" data-toggle="modal">
                            <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @include('costos.indirectos.costos_operacion.desarrollo_producto.modalcrearhormas')
    @include('costos.indirectos.costos_operacion.desarrollo_producto.modalcrearmaterial')
    @include('costos.indirectos.costos_operacion.desarrollo_producto.modalcrearproceso')
    @include('costos.indirectos.costos_operacion.desarrollo_producto.modalcrearservicio')
    @include('costos.indirectos.costos_operacion.desarrollo_producto.modalcreartroqueles')
</div>
