<div class="pb-3">
    <h3 class="font-weight-bold">Servicios Básicos
        <a href="" data-target="#modal-create-servicio" data-toggle="modal">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5"> Agregar Servicio Básico</button></a>
        <a href="#">
            <button class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Reporte</button></a>

    </h3>
</div>
<div class="x_content table-responsive pt-3">
    <table id="servicio_basico" class="display">
        <thead>
            <tr>
                <th>Proceso/Área</th>
                <th>Descripción del Servicio</th>
                <th>Costo Mensual</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody name="servicio_basico_tbody" id="servicio_basico_tbody">

        </tbody>
    </table>
    @include('costos.indirectos.costos_operacion.servicio_basico.modaleditar')
    @include('costos.indirectos.costos_operacion.servicio_basico.modalcrear')
    @include('costos.indirectos.costos_operacion.servicio_basico.modaleliminar')
    <script type="text/javascript">
        var servicios_basicos;
        var servicio_basico= null;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function getAllServiciosBasicos() {
            $.ajax({
                type: 'GET',
                url: "{{ url('costos/indirectos/operacion/serviciobasico/get') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(data) {
                    servicios_basicos = data.servicios_basicos;
                    console.log(data.servicios_basicos)
                    $('#servicio_basico_tbody').empty();
                    $.each(data.servicios_basicos, function(index, servicio_basico) {
                        tableServicioBasico.row.add([
                            servicio_basico.descripcion,
                            servicio_basico.costo_mensual,
                            "<a></a>",
                            "<a></a>"
                        ]).draw();

                       /*$('#servicio_basico_tbody').append('<tr><td>' + servicio_basico.proceso.nombre +
                            '</td><td>' + servicio_basico.descripcion + '</td><td>' +
                            servicio_basico.costo_mensual + '</td>' +
                            '<td><a href="" onclick="setModalEditarServicioBasico('+servicio_basico.id+')" data-target="#modal-editar-servicio" data-toggle="modal"><button class="bttn-unite bttn-md bttn-warning"><i class="fas fa-edit"></i></button></a></td>' +
                            '<td><a href="" onclick="setModalDeleteServicioBasico('+servicio_basico.id+')" data-target="#modal-eliminar-servicio" data-toggle="modal"><button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a></td>' +
                            '</tr>');*/
                    });
                }
            })
        }

        function createServicioBasico() {
            let proceso_id = $('#proceso_id').val();
            let descripcion = $('#descripcion').val();
            var costomensual = $('#costomensual').val();

            console.log(costomensual)
            $.ajax({
                type: 'POST',
                url: "{{ url('costos/indirectos/operacion/serviciobasico/create/') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    proceso_id: proceso_id,
                    descripcion: descripcion,
                    costo_mensual: costomensual
                },
                success: function() {
                    getAllServiciosBasicos();
                }
            })
        }

        let setModalEditarServicioBasico = (event) => {
            console.log(event)
            servicio_basico = servicios_basicos.find(_servicio_basico => _servicio_basico.id == event);
            console.log(servicio_basico)
            $("#proceso_id").val(servicio_basico.proceso_id);
            $('#descripcionedit').val(servicio_basico.descripcion);
            $('#costomensualedit').val(servicio_basico.costo_mensual);
        }

        function editServicioBasico() {
            console.log(servicios_basicos);
            let id = $('#idserviciobasico').val();
            let proceso_id = $('#proceso_id').val();
            let descripcion = $('#descripcionedit').val();
            var costomensual = $('#costomensualedit').val();
            console.log(proceso_id);
            $.ajax({
                type: 'PUT',
                url: `{{ url('costos/indirectos/operacion/serviciobasico/update/${servicio_basico.id}') }}`,
                data: {
                    "_token": "{{ csrf_token() }}",
                    proceso_id: proceso_id,
                    descripcion: descripcion,
                    costo_mensual: costomensual
                },
                success: function(data) {
                    console.log(data.resp);
                    getAllServiciosBasicos();
                }
            })
        }
        let setModalDeleteServicioBasico = (event) => {
            console.log(event)
            servicio_basico = servicios_basicos.find(_servicio_basico => _servicio_basico.id == event);
        }
        function deleteServicioBasico() {

            $.ajax({
                type: 'DELETE',
                url: `{{ url('costos/indirectos/operacion/serviciobasico/delete/${servicio_basico.id}') }}`,
                data: {
                    "_token": "{{ csrf_token() }}",
                },
                success: function(data) {
                    console.log(data.resp);
                    getAllServiciosBasicos();
                }
            })
        }

        $(document).ready(function() {
            var tableServicioBasico = $('#servicio_basico').DataTable({
                'columnDefs': [{
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                }
            });
            getAllServiciosBasicos();

        })
    </script>
</div>
