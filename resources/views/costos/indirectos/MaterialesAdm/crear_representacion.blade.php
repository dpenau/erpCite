@extends ('layouts.admin')
@section('contenido')
<div class="content">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">Nuevo Gasto de Representacion</h2>
                <div class="clearfix"></div>
            </div>
            @if (count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
                </ul>
            </div>
            @endif
            {!!Form::open(array('url'=>'costos_indirectos/MaterialesAdm/guardar_representacion','method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}
            <div class="row">
                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Descripcion de Gasto:</h6>
                    <input type="text"  name="descripcion" maxlength="250" required="required" class="form-control ">
                    <h6 class="my-3">Gasto:</h6>
                    <input type="text" onkeypress="return isNumberKey(event)" name="gasto"  required="required" class="form-control ">
                </div>

                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Fecha de Creacion</h6>
                    <input type="date" required class="form-control" name="fecha_creacion">
                </div>
            </div>
            <div class="form-group my-5">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5 mr-2">Guardar</button>
                    <a href="."><button type="button" class="bttn-unite bttn-md bttn-danger col-md-2 col-md-offset-5">Cancelar</button></a>
                </div>
            </div>
                    {!!Form::close()!!}
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function()
    {
      $("#valor1").focusout(function(){
          var valor1=$("#valor1").val()
          var beneficio=$("#beneficio").val()
            var total=0.00;
            total=total+(parseFloat(valor1).toFixed(6)*(parseFloat(beneficio).toFixed(6)/100));
            $("#valor2").val(total.toFixed(6))
            $("#ben_soci").val(total.toFixed(6))
      });
      $("#valor3").focusout(function(){
          var valor1=$("#valor1").val()
          var valor2=$("#valor2").val()
          var valor3=$("#valor3").val()
          var beneficio=$("#beneficio").val()
            var total=0.00;
            total=total+(parseFloat(valor1)+parseFloat(valor2)+parseFloat(valor3));
            $("#gasto_mensual1").val(total.toFixed(6))
      });
    });
</script>
@endsection
