<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-vermas-{{$op->id_gastos_sueldos}}">
        <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-header">
                                <h4 class="modal-title">DATOS: </h4>
                        </div>
                        <div class="modal-body">
                <h5>Área: {{$op->area}}</h5>
                <h5>Puesto: {{$op->puesto}}</h5>
                <h5>DNI Trabajador: {{$op->dni_trabajador}}</h5>
                <h5>Nombre Trabajador: {{$op->nombre_trabajador}}</h5>
                <h5>Sueldo Mensual: {{$op->sueldo_mensual}}</h5>
                <h5>Beneficios: {{$op->beneficios}}</h5>
                <h5>Otros: {{$op->otros}}</h5>
                <h5>Gasto Mensual: {{$op->gasto_mensual}}</h5>


                        </div>
                </div>
        </div>
</div>