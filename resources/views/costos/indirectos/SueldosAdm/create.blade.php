@extends ('layouts.admin')
@section ('contenido')
    <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold">Nuevo Sueldo Administrativo</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif
          {!!Form::open(array('url'=>'costos_indirectos/SueldosAdm','method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}
                 <div class="row">
                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Area:</h6>
                    <select required='required' name="area"  class="custom-select">
                        @foreach ($area as $cat)
                          <option value="{{$cat->descrip_area}}" >{{$cat->descrip_area}}</option>
                        @endforeach
                    </select>
                    <h6 class="my-3">Descripcion del Puesto:</h6>
                     <select required='required' name="puesto" "  class="custom-select">
                        <option value="" selected disabled>Puesto</option>
                        @foreach ($puesto as $cat)
                          <option value="{{$cat->descrip_puesto}}" >{{$cat->descrip_puesto}}</option>
                        @endforeach
                      </select>

                    <h6 class="my-3">DNI de Trabajador:</h6>
                    <input type="text" id="" name="dni_trabajador" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Nombre de Trabajador:</h6>
                    <input type="text" id="" name="nombre_trabajador" maxlength="70" required="required" class="form-control ">

                </div>

                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Sueldo Mensual en Planilla:</h6>
                        <input type="text" id="sueldo_mensual1" name="sueldo_mensual" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Beneficios Sociales:</h6>
                        <input type="text" id="beneficios1" name="beneficios" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Otros Sueldos Mensuales:</h6>
                        <input type="text" id="otros1" name="otros" maxlength="70" required="required" class="form-control ">

                    <h6 class="my-3">Gasto Mensual:</h6>
                        <input readonly type="text" id="gasto_mensual1" name="gasto_mensual" maxlength="70" required="required" class="form-control ">

                </div>
            </div>
            <div class="form-group my-5">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <button type="submit" class="bttn-slant bttn-md bttn-success col-md-2 col-md-offset-5 mr-2">Guardar</button>
                    <a href=".">  
                    <button type="" class="bttn-slant bttn-md bttn-danger col-md-2 col-md-offset-5">Cancelar</input>
                    </a>
                </div>
            </div>
          {!!Form::close()!!}
          
        </div>
      </div>
    </div>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liareas').addClass("active");
</script>
@endpush
@endsection
