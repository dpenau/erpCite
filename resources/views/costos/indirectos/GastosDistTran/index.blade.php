@extends ('layouts.admin')
@section('contenido')

<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
        <div class="x_panel">
            <div class="x_title ">
                <h1 class="d-inline font-weight-bold ">Gastos de Distribucion y Transporte</h1>
                <a href='GastosDistTran/create'>
                <button type="submit" class="bttn-unite bttn-md bttn-success ml-2" target="_blank" id="sub">Nuevo Gasto</button></a>
                <div class="clearfix"></div>
            </div>
            <div class="row">
            <div class="col-12">
                <div class="tab-content mt-4" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-Prototipo" role="tabpanel" aria-labelledby="list-home-list">
                <div class="x_content table-responsive">
                    <table id="example" class="display">
                    <thead>
                        <tr>
                        <th>Descripcion del Gasto</th>
                        <th>Gasto Mensual</th>
                        <th>Fecha de Creacion</th>
                        <th>Eliminar</th>
                        </tr>
                    </thead>

                        <tbody>
                         @foreach($distribucion as $op)
                            <tr>
                                <td>{{$op->descripcion}}</td>
                                <td class="gasto">{{$op->gasto_mensual}}</td>
                                <td>{{$op->fecha_creacion}}</td>

                                <td>
                                      <a href="" data-target="#modal-delete-{{$op->id_gastoDistr}}" data-toggle="modal">

                                    <button class="bttn-unite bttn-md bttn-danger"><i class="fas fa-trash-alt"></i></button></a>
                                </td>
                            </tr>
                                  @include('costos.indirectos.GastosDistTran.modal')

                           @endforeach
                        </tbody>
                    </table>
                    <h4 id="cmant">Costo Total de Distribucion y Transporte</h4>
                </div>
                </div>
            </div>
          </div>
      </div>
      </div>
      </div>
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
      var gasto=$("td[class~='gasto']");
      var sumagasto=0.00;
      for(var i=0;i<gasto.length;i++)
      {
        var num=parseFloat(gasto[i].innerText)
        sumagasto=sumagasto+num;
      }
      $("#cmant").html("Costo Total de Distribucion y Transporte S/."+ sumagasto.toFixed(6));
    });
</script>
@endsection
