<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-vermas-{{$serviba->cod_gasto_servicio_basico}}">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">{{$serviba->descrip_servicio_basico}}</h4>
			</div>
			<div class="modal-body">
                <h5>Área: {{$serviba->descrip_area}}</h5>
                <h5>Gasto Mensual: S/.{{$serviba->gasto_mensual}}</h5>
			</div>
		</div>
	</div>
</div>