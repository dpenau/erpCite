@extends ('layouts.admin')
@section('contenido')
<div class="content">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">Gasto de Servicios Basicos</h2>
                <div class="clearfix"></div>
            </div>
            @if (count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
                </ul>
            </div>
            @endif
            {!!Form::open(array('url'=>'costo/indirectos/servicio_basico','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
            <div class="row">
                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Area:</h6>
                    <select required='required' id="categoria" name="cod_area" class="custom-select">
                        <option value="" selected disabled>Area</option>
                        @foreach ($area as $area)
                            <option value="{{$area->cod_area}}" >{{$area->descrip_area}}</option>
                        @endforeach
                    </select>

                    <h6 class="my-3">Descripcion del Servicio:</h6>
                        <input type="text" id="" name="descrip_servicio_basico" maxlength="70" required="required" class="form-control ">
                </div>

                <div class="form-group  col-md-4 col-md-offset-4 col-xs-12">
                    <h6 class="my-3">Gasto Mensual:</h6>
                        <input type="text" id="" name="gasto_mensual" maxlength="70" required="required" class="form-control ">
                    <h6 class="my-3">Fecha de Gasto</h6>
                        <input type="date" required class="form-control" name="fecha_creacion">
                </div>
                {!!Form::close()!!}
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5 mr-2">Guardar</button>
                    <a href="{{ url('costo/indirectos/servicio_basico') }}" ><button type="button" class="bttn-unite bttn-md bttn-danger col-md-2 col-md-offset-5 mr-2">Cancelar</button></a>
                </div>
            </div>

        </div>
    </div>
</div>




@endsection
