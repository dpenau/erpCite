<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-eliminar-{{$serviba->cod_gasto_servicio_basico}}">
	{{Form::Open(array('action'=>array('ServicioBasicoController@destroy',$serviba->cod_gasto_servicio_basico
  ),'method'=>'delete'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Eliminar Registro</h4>
			</div>
			<div class="modal-body">
				<p>Confirme si desea eliminar</p>
        <input type="text" style="display:none" name="email" value="{{$serviba->cod_gasto_servicio_basico}}">
        <input type="text" style="display:none" name="estado_gasto_basico" value="0">
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}

</div>
