@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">
</div>
<div class="left">
  <img src="../photo/{{$empresa[0]->imagen}}" width="200px" height="100px" alt="">
</div>
<div class="row">
<h1 class="font-weight-bold col-md-6 offset-md-3">Formulacion de precio de venta</h1>
</div>
@if(count($generales)!=0)
<div class="x_content">
  <div class="row">
    <div class="col-md-8" style="border:1px solid">
      <div style="text-align: center;">
        <h4>Datos Generales</h4>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="">
            <label for="">Empresa:</label>
            <input type="text" name="" disabled class="form-control" value="{{$empresa[0]->razon_social}}">
          </div>
          <div class="">
            <label for="">Marca:</label>
            <input type="text" name="" class="form-control" value="">
          </div>
          <div class="">
            <label for="">Linea:</label>
            <input type="text" name="" id="linea" disabled class="form-control" value="">
          </div>
          <div class="">
            <label for="">Cod. Modelo:</label>
            <select id="modelos" class="form-control" name="">
              <option value="" selected>Cod. Modelo:</option>
              @foreach($modelos as $mod)
                <option value="{{$mod->codigo_modelo}}+{{$mod->codigo_serie}}">{{$mod->codigo_modelo}}-{{$mod->nombre_serie}}</option>
              @endforeach
            </select>
          </div>
          {!!Form::open(array('url'=>'costo/precio_venta/guardar_precio/create','method'=>'POST','autocomplete'=>'off'))!!}
          {{Form::token()}}
          <div class="">
            <label for="">Cod. Articulo:</label>
            <select id="articulo" class="form-control" name="articulos" required>

            </select>
          </div>
          <div class="">
            <label for="">Descripcion:</label>
            <input type="text" name="" id="descripcion" disabled class="form-control" value="">
          </div>
        </div>
        <div class="col-md-6">
          <div class="">
            <label for="">Serie:</label>
            <input type="text" name="" id="serie" disabled class="form-control" value="">
          </div>
          <div class="">
            <label for="">Tallas:</label>
            <input type="text" name="" id="talla" disabled class="form-control" value="">
          </div>
          <div class="">
            <label for="">Talla Base:</label>
            <input type="text" name="" id="talla_base" disabled class="form-control" value="">
          </div>
          <hr class=" my-4">
          <div class="" style="text-align: center;">
            <img id="fotos" width="150px" src="" alt="">
            <input type="text"  style="display:none" id="imagen" value="">
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4" style="border:1px solid">
      <div style="text-align: center;">
        <h4>Norma Etiquetado</h4>
      </div>
      <div class="">
        <label for="">Capellada:</label>
        <input type="text" id="capellada" name="" disabled class="form-control" value="">
      </div>
      <div class="">
        <label for="">Forro:</label>
        <input type="text" id="forro" name="" disabled class="form-control" value="">
      </div>
      <div class="">
        <label for="">Piso:</label>
        <input type="text" id="piso" name="" disabled class="form-control" value="">
      </div>
      <div class="">
        <label for="">Plantilla:</label>
        <input type="text" id="plantilla" name="" disabled class="form-control" value="">
      </div>
      <hr class=" my-4">
      <div class="">
        <label for="">Cantidad a cotizar (Pares):</label>
        <input type="text" name="" id="cantidad" class="form-control" value="2">
      </div>
    </div>
  </div>
  <div id="buscando" align="center" style="display:none; margin:40px;">
    <div class="alert alert-success" role="alert">
      Obteniendo Datos...
    </div>
  </div>
  <hr class=" my-4"><hr class=" my-4">
  <div style="text-align: center;">
      <h2>Costos Directos de Fabricacion</h2>
  </div>
  <hr class=" my-4">
  <hr class=" my-4">
  <div >
    <div class="table-responsive">
      <table class="table table-hover table-striped">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Materiales Directos</th>
            <th scope="col">S/. Par</th>
            <th scope="col">Cotizacion</th>
            <th scope="col">%</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">Corte</th>
            <td id="m_corte"></td>
            <td id="i_m_corte"></td>
            <td id="p_m_corte"></td>
          </tr>
          <tr>
            <th scope="row">Habilitado</th>
            <td id="m_habilitado"></td>
            <td id="i_m_habilitado"></td>
            <td id="p_m_habilitado"></td>
          </tr>
          <tr>
            <th scope="row">Aparado</th>
            <td id="m_aparado"></td>
            <td id="i_m_aparado"></td>
            <td id="p_m_aparado"></td>
          </tr>
          <tr>
            <th scope="row">Alistado</th>
            <td id="m_alistado"></td>
            <td id="i_m_alistado"></td>
            <td id="p_m_alistado"></td>
          </tr>
          <tr>
            <th scope="row">Montaje</th>
            <td id="m_montaje"></td>
            <td id="i_m_montaje"></td>
            <td id="p_m_montaje"></td>
          </tr>
          <tr>
            <th scope="row">Acabado</th>
            <td id="m_acabado"></td>
            <td id="i_m_acabado"></td>
            <td id="p_m_acabado"></td>
          </tr>
          <tfoot class="table-success">
            <th >Total Materiales Directos</th>
            <th id="t_material"></th>
            <th id="m_total" colspan="2"></th>
          </tfoot>
        </tbody>
      </table>
    </div>
    <hr class=" my-4">
    <hr class=" my-4">
    <div class="table-responsive">
      <table class="table table-hover table-striped">
      <thead class="thead-dark">
        <tr>
          <th scope="col">Mano de Obra Directos</th>
          <th scope="col">S/. Par</th>
          <th scope="col">Cotizacion</th>
          <th scope="col">%</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">Corte</th>
          <td id="o_corte"></td>
          <td id="i_o_corte"></td>
          <td id="p_o_corte"></td>
        </tr>
        <tr>
          <th scope="row">Habilitado</th>
          <td id="o_habilitado"></td>
          <td id="i_o_habilitado"></td>
          <td id="p_o_habilitado"></td>
        </tr>
        <tr>
          <th scope="row">Aparado</th>
          <td id="o_aparado"></td>
          <td id="i_o_aparado"></td>
          <td id="p_o_aparado"></td>
        </tr>
        <tr>
          <th scope="row">Alistado</th>
          <td id="o_alistado"></td>
          <td id="i_o_alistado"></td>
          <td id="p_o_alistado"></td>
        </tr>
        <tr>
          <th scope="row">Montaje</th>
          <td id="o_montaje"></td>
          <td id="i_o_montaje"></td>
          <td id="p_o_montaje"></td>
        </tr>
        <tr>
          <th scope="row">Acabado</th>
          <td id="o_acabado"></td>
          <td id="i_o_acabado"></td>
          <td id="p_o_acabado"></td>
        </tr>
      </tbody>
      <tfoot class="table-success">
        <th>Total Mano de Obra Directa</th>
        <th id="t_mano"></th>
        <th id="o_total" colspan="2"></th>
      </tfoot>
    </table>
    </div>
  </div>
  <hr class=" my-4"><hr class=" my-4">
  <div style="text-align: center;">
      <h2>Costos Indirectos de Fabricacion</h2>
  </div>
  <hr class=" my-4"><hr class=" my-4">
  <div >
    <div class="table-responsive">
      <?php $t_produccion=0;?>
      <table class="table table-hover table-striped">
      <thead class="thead-dark">
        <tr>
          <th scope="col">Gastos de Produccion</th>
          <th scope="col">Gasto</th>
          <th scope="col">Tipo Distribucion</th>
          <th scope="col">Distribucion</th>
          <th scope="col">S/.Par</th>
          <th scope="col">Cotizacion</th>
          <th scope="col">%</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">Materiales Indirectos y Suministros</th>
          <td>{{$suministros}}</td>
          <td>Mensual</td>
          <td>{{$generales[0]->produccion_promedio}}</td>
          <td class="g_produccion">{{$suministros/$generales[0]->produccion_promedio}}</td>
          <td class="c_produccion"></td>
          <td class="p_produccion"></td>
        </tr>
        <tr>
          <th scope="row">Mano de Obra Indirecta</th>
          <td>{{$mobra}}</td>
          <td>Mensual</td>
          <td>{{$generales[0]->produccion_promedio}}</td>
          <td class="g_produccion">{{$mobra/$generales[0]->produccion_promedio}}</td>
          <td class="c_produccion"></td>
          <td class="p_produccion"></td>
        </tr>
        <tr>
          <th scope="row">Depreciacion de Activos</th>
          <?php
          $g_depreciacion=0;$g_mantenimiento=0;?>
          @foreach($depreciacion as $dep)
            <?php $anio= date('Y')-($dep->anio_compra-$dep->anio_uso);?>
            @if($anio<=$dep->anios_vida_util)
            <?php
            $g_depreciacion=$g_depreciacion+$dep->depreciacion_mensual;
            ?>
            @endif
            <?php $g_mantenimiento=$g_mantenimiento+$dep->gasto_mensual_depreciacion;?>
          @endforeach
          <td>{{$g_depreciacion}}</td>
          <td>Mensual</td>
          <td>{{$generales[0]->produccion_promedio}}</td>
          <td class="g_produccion">{{$g_depreciacion/$generales[0]->produccion_promedio}}</td>
          <td class="c_produccion"></td>
          <td class="p_produccion"></td>
        </tr>
        <tr>
          <th scope="row">Mantenimiento</th>
          <td>{{$g_mantenimiento}}</td>
          <td>Mensual</td>
          <td>{{$generales[0]->produccion_promedio}}</td>
          <td class="g_produccion">{{$g_mantenimiento/$generales[0]->produccion_promedio}}</td>
          <td class="c_produccion"></td>
          <td class="p_produccion"></td>
        </tr>
        <tr>
          <th scope="row">Servicios Basicos</th>
          <td>{{$servicio}}</td>
          <td>Mensual</td>
          <td>{{$generales[0]->produccion_promedio}}</td>
          <td class="g_produccion">{{$servicio/$generales[0]->produccion_promedio}}</td>
          <td class="c_produccion"></td>
          <td class="p_produccion"></td>
        </tr>
        <tr>
          <th scope="row">Desarrollo de Producto</th>
        </tr>
        <tr>
          <th scope="row">Desarrollo de Prototipo</th>
          <td>{{$prototipo}}</td>
          <td>Mensual</td>
          <td>{{$generales[0]->politica_desarrollo_producto}}</td>
          <td class="g_produccion">{{($prototipo/$generales[0]->politica_desarrollo_producto)/$generales[0]->produccion_promedio}}</td>
          <td class="c_produccion"></td>
          <td class="p_produccion"></td>
        </tr>
        <tr>
          <th scope="row">Desarrollo de Hormas</th>
          <td>{{$hormas}}</td>
          <td>Mensual</td>
          <td>{{$generales[0]->politica_desarrollo_horma}}</td>
          <td class="g_produccion">{{($hormas/$generales[0]->politica_desarrollo_horma)/$generales[0]->produccion_promedio}}</td>
          <td class="c_produccion"></td>
          <td class="p_produccion"></td>
        </tr>
        <tr>
          <th scope="row">Desarrollo de Troqueles</th>
          <td>{{$troqueles}}</td>
          <td>Mensual</td>
          <td>{{$generales[0]->politica_desarrollo_troqueles}}</td>
          <td class="g_produccion">{{($troqueles/$generales[0]->politica_desarrollo_troqueles)/$generales[0]->produccion_promedio}}</td>
          <td class="c_produccion"></td>
          <td class="p_produccion"></td>
        </tr>
      </tbody>
      <tfoot class="table-success">
        <th colspan="4">Total de Gastos de Produccion</th>
        <th id="t_produccion" ></th>
        <th colspan="2" id="t_c_produccion"></th>
      </tfoot>
    </table>
    </div>
  </div>
  <hr class=" my-4"><hr class=" my-4">
  <div >
    <div class="table-responsive">
      <table class="table table-hover table-striped">
      <thead class="thead-dark">
        <tr>
          <th scope="col">Gastos de Operacion</th>
          <th scope="col">Gasto</th>
          <th scope="col">Tipo Distribucion</th>
          <th scope="col">Distribucion</th>
          <th scope="col">S/.Par</th>
          <th scope="col">Cotizacion</th>
          <th scope="col">%</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">Gastos Administrativos</th>
          <td>{{$administracion}}</td>
          <td>Mensuales</td>
          <td>{{$generales[0]->produccion_promedio}}</td>
          <td class="g_operativo">{{$administracion/$generales[0]->produccion_promedio}}</td>
          <td class="c_operativo"></td>
          <td class="p_operativo"></td>
        </tr>
        <tr>
          <th scope="row">Gastos de Distribucion y Transporte</th>
          <td>{{$distribucion}}</td>
          <td>Mensuales</td>
          <td>{{$generales[0]->produccion_promedio}}</td>
          <td class="g_operativo">{{$distribucion/$generales[0]->produccion_promedio}}</td>
          <td class="c_operativo"></td>
          <td class="p_operativo"></td>
        </tr>
        <tr>
          <th scope="row">Gastos de Ventas y Marketing</th>
          <td>{{$marketing}}</td>
          <td>Mensuales</td>
          <td>{{$generales[0]->produccion_promedio}}</td>
          <td class="g_operativo">{{$marketing/$generales[0]->produccion_promedio}}</td>
          <td class="c_operativo"></td>
          <td class="p_operativo"></td>
        </tr>
        <tr>
          <th scope="row">Gastos Financieros</th>
          <?php $prest=0;?>
          @foreach($prestamo as $pres)
            <?php
            $date1 = new DateTime($pres->fecha_prestamo);
            $date2 = new DateTime();
            $diff = $date1->diff($date2);
            ?>
            @if($pres->meses_pago-$diff->m > 0)
            <?php
              $prest=$prest+($pres->intereses/$pres->meses_pago);
            ?>
            @endif
          @endforeach
          <td>{{$prest+$financiero}}</td>
          <td>Mensual</td>
          <td>{{$generales[0]->produccion_promedio}}</td>
          <td class="g_operativo">{{($prest+$financiero)/$generales[0]->produccion_promedio}}</td>
          <td class="c_operativo"></td>
          <td class="p_operativo"></td>
        </tr>
        <tr>
          <th scope="row">Gastos Externos</th>
          <td>{{$externo}}</td>
          <td>Mensual</td>
          <td>{{$generales[0]->produccion_promedio}}</td>
          <td class="g_operativo">{{$externo/$generales[0]->produccion_promedio}}</td>
          <td class="c_operativo"></td>
          <td class="p_operativo"></td>
        </tr>
      </tbody>
      <tfoot class="table-success">
        <th colspan="4">Total de Gastos Operativos</th>
        <th id="t_operativo"></th>
        <th colspan="2" id="t_c_operativo"></th>
      </tfoot>
    </table>
    </div>
  </div>
  <hr class=" my-4"><hr class=" my-4">
  <div >
    <div class="table-responsive">
      <table class="table table-hover table-striped">
      <thead class="thead-dark">
        <tr>
          <th scope="col">Otros costos indirectos de fabricacion</th>
          <th scope="col">Porcentaje %</th>
          <th scope="col">S/.Par</th>
          <th scope="col">Cotizacion</th>
          <th scope="col">%</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">Costo por comision de ventas</th>
          <td class="otros">{{$generales[0]->porcentaje_comision_ventas}}</td>
          <td class="g_otros"></td>
          <td class="c_otros"></td>
          <td class="p_otros"></td>
        </tr>
        <tr>
          <th scope="row">Costo por Reproceso</th>
          <td class="otros">{{$generales[0]->porcentaje_reproceso}}</td>
          <td class="g_otros"></td>
          <td class="c_otros"></td>
          <td class="p_otros"></td>
        </tr>
      </tbody>
      <tfoot class="table-success">
        <th colspan="2">Total de Otros Costos</th>
        <th id="t_otros"></th>
        <th colspan="3" id="t_c_otros"></th>
      </tfoot>
    </table>
    </div>
  </div>
  <hr class=" my-4">
  <div class="row">
    <div class="col-md-3">
      <h4>Inversion por par:S/.</h4>
      <h4>Inversion de cotizacion:S/.</h4>

    </div>
    <div class="col-md-3">
      <h4 id="inversion_par"></h4>
      <input type="text" style="display:none" id="inversion_par_input" name="costo">
      <h4 id="inversion_cotizacion"></h4>
    </div>
    <div class="col-md-3">
      <h4>Reparticion de Utilidades %</h4>
      <h4>Impuesto a la renta {{$empresa[0]->descrip_regimen_renta}}(%) Afecto a {{$empresa[0]->afecta_a}}</h4>
    </div>
    <div class="col-md-3">
      @if($empresa[0]->cod_regimen_renta!=1)
      <h4 id="utilidades">{{$generales[0]->porcentaje_retencion}}</h4>
      @else
      <h4 id="utilidades">0</h4>
      @endif
      <h4 id="renta">{{$empresa[0]->porcentaje}}</h4>
    </div>
  </div>

  <hr class=" my-4">
  <div style="text-align: center;">
      <h2>Resultado de costos de modelo</h2>
      <div class="row">
        <div class="col-md-6">
          <div class="table-responsive">
            <table class="table table-hover table-striped">
            <tbody>
              <tr>
                <th scope="row">Valor de Venta por par</th>
                <td><input onkeypress="return isNumberKey(event)" class="form-control" id="valor_venta"></input></td>
              </tr>
              <tr>
                <th scope="row">Inversion por Par</th>
                <td class="resultado"></td>
              </tr>
              <tr>
                <th scope="row">Utilidad Bruta</th>
                <td class="resultado"></td>
              </tr>
              <tr>
                <th scope="row">Reparticion de Utilidades</th>
                <td class="resultado"></td>
              </tr>
              <tr>
                <th scope="row">Util. Antes de Imp. Renta</th>
                <td class="resultado"></td>
              </tr>
              <tr>
                <th scope="row">Impuesto a la Renta</th>
                <td class="resultado"></td>
              </tr>
              <tr>
                <th scope="row">Utilidad Neta</th>
                <td class="resultado"></td>
                <td style="display:none"><input type="text"  id="resultado_input" name="utilidad" value=""></td>
              </tr>
              <tr>
                <th scope="row">Precio + IGV</th>
                <td class="resultado"></td>
                <td style="display:none"><input type="text"  id="precio_f" name="precio_final" value=""></td>
              </tr>
            </tbody>
          </table>
          <div class="">
            <button type="submit"  class="bttn-unite bttn-md bttn-success" name="button">Guardar Precio</button>
          </div>
          {!!Form::close()!!}
          </div>
        </div>
        <div class="col-md-6">
          <div class="table-responsive">
            <table class="table table-hover table-striped">
            <tbody>
              <tr>
                <th scope="row">Valor de Venta por cotizacion</th>
                <td><input align="center" onkeypress="return isNumberKey(event)" class="form-control" id="valor_venta_cotizacion"></input></td>
              </tr>
              <tr>
                <th scope="row">Inversion de Cotizacion</th>
                <td class="resultado_cotizacion"></td>
              </tr>
              <tr>
                <th scope="row">Utilidad Bruta</th>
                <td class="resultado_cotizacion"></td>
              </tr>
              <tr>
                <th scope="row">Reparticion de Utilidades</th>
                <td class="resultado_cotizacion"></td>
              </tr>
              <tr>
                <th scope="row">Util. Antes de Imp. Renta</th>
                <td class="resultado_cotizacion"></td>
              </tr>
              <tr>
                <th scope="row">Impuesto a la Renta</th>
                <td class="resultado_cotizacion"></td>
              </tr>
              <tr>
                <th scope="row">Utilidad Neta</th>
                <td class="resultado_cotizacion"></td>
              </tr>
              <tr>
                <th scope="row">Precio + IGV</th>
                <td class="resultado_cotizacion"></td>
              </tr>
            </tbody>
          </table>
          </div>
        </div>
      </div>
      <hr class=" my-4"><hr class=" my-4">
      <div class="row">
        <div class="col-md-6">
          <div align="center">
            <h3>Costo de Materiales</h3>
          </div>
          <div id="id_mats">
            <canvas id="myChart"></canvas>
          </div>
        </div>

      </div>
      <hr class=" my-4"><hr class=" my-4">
      <div class="row">
        <div class="col-md-6">
          <div align="center">
            <h3>Resultado de Costo por Par</h3>
            <div id="id_grafico">
              <canvas id="Pastel_Par" ></canvas>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div align="center">
            <h3>Resultado de Costo de Cotizacion</h3>
            <div id="id_grafico_cotizacion">
              <canvas id="Pastel_Cotizacion" ></canvas>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
@else
<div class="x_content">
  <p class="text-white bg-danger">Registre primero su configuracion inicial<a class="text-white" href="{{url('Configuracion/Costos_Directos')}}"> Click Aqui</a></p>
</div>
@endif
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
<script type="text/javascript">
  $(document).ready(function(){

    cotizacion_produccion();
    cotizacion_operativo();
    $("#modelos").change(function(){
      limpiar()
      $("#buscando").toggle("fast");
      var cod=$("#modelos").val();
      $.ajax({
        url: "../costo/precio_venta/obt_articulo/"+cod,
        success: function(html){
          $("#buscando").toggle("fast");
          $("#articulo").empty();
          var opciones="<option selected disabled>Seleccion un Articulo</option>"
          for(var i=0; i<html.length; i++)
          {
            var codigo=html[i].codigo_modelo;
            var codigo_serie_modelo=html[i].codigo;
            opciones+="<option value='"+codigo+"+"+codigo_serie_modelo+"'>"+codigo+"</option>"
          }
          $("#articulo").append(opciones);
        }
      });
    })
    $("#articulo").change(function(){
      $("#buscando").toggle("fast");
      var cod=$("#articulo").val();
      $.ajax({
        url: "../costo/precio_venta/obt_datos/"+cod,
        success: function(html){
          $("#buscando").toggle("fast");
          var dat=JSON.parse(html)
          var ruc="{{$empresa[0]->RUC_empresa}}"
          $("#linea").val(dat["datos"][0].nombre_linea)
          $("#descripcion").val(dat["datos"][0].descripcion)
          $("#serie").val(dat["datos"][0].nombre_serie)
          $("#talla").val(dat["datos"][0].tallaInicial+"-"+dat["datos"][0].tallaFinal)
          $("#talla_base").val(((parseFloat(dat["datos"][0].tallaFinal)+parseFloat(dat["datos"][0].tallaInicial))/2).toFixed(0));
          $("#capellada").val(dat["datos"][0].descrip_capellada)
          $("#forro").val(dat["datos"][0].descrip_forro)
          $("#piso").val(dat["datos"][0].descrip_piso)
          $("#plantilla").val(dat["datos"][0].descrip_plantilla)
          $("#fotos").attr("src","../photo/modelos/"+ruc+"/"+dat["datos"][0].imagen);
          var corte=habilitado=aparado=alistado=montaje=acabado=0;
          for(var i=0; i < dat["datos2"].length;i++)
          {
            switch (dat["datos2"][i].descrip_area) {
              case "P-Corte":
                corte+=dat["datos2"][i].total;
                break;
              case "P-Habilitado":
              habilitado+=dat["datos2"][i].total;
              break;
              case "P-Aparado":
              aparado+=dat["datos2"][i].total;
              break;
              case "P-Alistado":
              alistado+=dat["datos2"][i].total;
              break;
              case "P-Montaje":
              montaje+=dat["datos2"][i].total;
              break;
              case "P-Acabado":
              acabado+=dat["datos2"][i].total;
              break;
            }
          }
          $("#m_corte").text(corte)
          $("#m_habilitado").text(habilitado)
          $("#m_aparado").text(aparado)
          $("#m_alistado").text(alistado)
          $("#m_montaje").text(montaje)
          $("#m_acabado").text(acabado)
          cotizacion_material(corte,habilitado,aparado,alistado,montaje,acabado);
          corte=habilitado=aparado=alistado=montaje=acabado=0;
          for(var i=0; i < dat["datos3"].length;i++)
          {
            switch (dat["datos3"][i].descrip_area) {
              case "P-Corte":
                corte+=dat["datos3"][i].costo_por_par;
                break;
              case "P-Habilitado":
              habilitado+=dat["datos3"][i].costo_por_par;
              break;
              case "P-Aparado":
              aparado+=dat["datos3"][i].costo_por_par;
              break;
              case "P-Alistado":
              alistado+=dat["datos3"][i].costo_por_par;
              break;
              case "P-Montaje":
              montaje+=dat["datos3"][i].costo_por_par;
              break;
              case "P-Acabado":
              acabado+=dat["datos3"][i].costo_por_par;
              break;
            }
          }
          $("#o_corte").text(corte)
          $("#o_habilitado").text(habilitado)
          $("#o_aparado").text(aparado)
          $("#o_alistado").text(alistado)
          $("#o_montaje").text(montaje)
          $("#o_acabado").text(acabado)
          cotizacion_mano(corte,habilitado,aparado,alistado,montaje,acabado);
          grafico()
        }
      });
    });
    $("#cantidad").change(function(){
      var corte=$("#m_corte").text()
      var habilitado=$("#m_habilitado").text()
      var aparado=$("#m_aparado").text()
      var alistado=$("#m_alistado").text()
      var montaje=$("#m_montaje").text()
      var acabado=$("#m_acabado").text()
      cotizacion_material(corte,habilitado,aparado,alistado,montaje,acabado);
      corte=$("#o_corte").text()
      habilitado=$("#o_habilitado").text()
      aparado=$("#o_aparado").text()
      alistado=$("#o_alistado").text()
      montaje=$("#o_montaje").text()
      acabado=$("#o_acabado").text()
      cotizacion_mano(corte,habilitado,aparado,alistado,montaje,acabado);
      cotizacion_produccion();
      cotizacion_operativo();
      $("#valor_venta").change();
      $("#valor_venta_cotizacion").change();

    })
    $("#valor_venta").change(function(){
      let valor_venta=$("#valor_venta").val();
      let cantidad=$("#cantidad").val();
      let costo=$("td[class~='otros']");
      let costo_par=$("td[class~='g_otros']");
      let cotizacion=$("td[class~='c_otros']");
      let porcentaje=$("td[class~='p_otros']");
      let total_cotizacion=total=0;
      for(let i=0;i<costo.length;i++)
      {
        let par=valor_venta*(parseFloat(costo[i].textContent)/100);
        costo_par[i].textContent=par.toFixed(6)
        cotizacion[i].textContent=(par*cantidad).toFixed(6);
        total=total+par;
        total_cotizacion=total_cotizacion+(par*cantidad)
      }
      $("#t_otros").text(total.toFixed(6));
      $("#t_c_otros").text(total_cotizacion.toFixed(6));
      for(let i=0;i<costo.length;i++)
      {
        let porcentaje_otor=(parseFloat(cotizacion[i].textContent)/total_cotizacion)*100;
        porcentaje[i].textContent=porcentaje_otor.toFixed(2);
      }
      inversion();
      resultados();
    });
    $("#valor_venta_cotizacion").change(function(){
    resultado_cotizacion();
    })
    function resultado_cotizacion(){
      let resultado=$("td[class~='resultado_cotizacion']");
      resultado[0].textContent=parseFloat($("#inversion_cotizacion").text()).toFixed(2);
      let precio_venta=parseFloat($("#valor_venta_cotizacion").val());
      let inversion_par=parseFloat(resultado[0].textContent);
      let utilidad_bruta=precio_venta-inversion_par;
      resultado[1].textContent=utilidad_bruta.toFixed(2);
      let reparticion_utilidad=utilidad_bruta*(parseFloat($("#utilidades").text())/100)
      resultado[2].textContent=reparticion_utilidad.toFixed(2)
      let utilidad_antes_impuesto=utilidad_bruta-reparticion_utilidad;
      resultado[3].textContent=utilidad_antes_impuesto.toFixed(2)
      let cod_impuesto="{{$empresa[0]->cod_regimen_renta}}"
      let impuesto_renta=$("#renta").text();
      let impuesto_calculado=0;
      switch (cod_impuesto) {
        case "1":
          impuesto_calculado=precio_venta*(impuesto_renta/100)
        break;
        case "2":
          impuesto_calculado=utilidad_antes_impuesto*(impuesto_renta/100)
        break;
        case "3":
          impuesto_calculado=utilidad_antes_impuesto*(impuesto_renta/100)
        break;
        default:
      }
      resultado[4].textContent=impuesto_calculado.toFixed(2)
      let utilidad_neta=utilidad_antes_impuesto-impuesto_calculado;
      resultado[5].textContent=utilidad_neta.toFixed(2);
      let precio=precio_venta*1.18;
      resultado[6].textContent=precio.toFixed(2)
      grafico_pie_cotizacion();
    }
    function inversion(){
      let material=parseFloat($("#t_material").text())
      let obra=parseFloat($("#t_mano").text())
      let produccion=parseFloat($("#t_produccion").text())
      let operativo=parseFloat($("#t_operativo").text())
      let otro=parseFloat($("#t_otros").text())
      let total=material+obra+produccion+operativo+otro;
      $("#inversion_par").text(total.toFixed(6))
      $("#inversion_par_input").val(total.toFixed(6))
      material=parseFloat($("#m_total").text())
      obra=parseFloat($("#o_total").text())
      produccion=parseFloat($("#t_c_produccion").text())
      operativo=parseFloat($("#t_c_operativo").text())
      otro=parseFloat($("#t_c_otros").text())
      total=material+obra+produccion+operativo+otro;
      $("#inversion_cotizacion").text(total.toFixed(6))
    }
    function resultados(){
      let resultado=$("td[class~='resultado']");
      resultado[0].textContent=parseFloat($("#inversion_par").text()).toFixed(2);
      let precio_venta=parseFloat($("#valor_venta").val());
      let inversion_par=parseFloat(resultado[0].textContent);
      let utilidad_bruta=precio_venta-inversion_par;
      resultado[1].textContent=utilidad_bruta.toFixed(2);
      let reparticion_utilidad=utilidad_bruta*(parseFloat($("#utilidades").text())/100)
      resultado[2].textContent=reparticion_utilidad.toFixed(2)
      let utilidad_antes_impuesto=utilidad_bruta-reparticion_utilidad;
      resultado[3].textContent=utilidad_antes_impuesto.toFixed(2)
      let cod_impuesto="{{$empresa[0]->cod_regimen_renta}}"
      let impuesto_renta=$("#renta").text();
      let impuesto_calculado=0;
      switch (cod_impuesto) {
        case "1":
          impuesto_calculado=precio_venta*(impuesto_renta/100)
        break;
        case "2":
          impuesto_calculado=utilidad_antes_impuesto*(impuesto_renta/100)
        break;
        case "3":
          impuesto_calculado=utilidad_antes_impuesto*(impuesto_renta/100)
        break;
        default:
      }
      resultado[4].textContent=impuesto_calculado.toFixed(2)
      let utilidad_neta=utilidad_antes_impuesto-impuesto_calculado;
      resultado[5].textContent=utilidad_neta.toFixed(2);
      $("#resultado_input").val(utilidad_neta.toFixed(2))
      let precio=precio_venta*1.18;
      resultado[6].textContent=precio.toFixed(2)
      $("#precio_f").val(precio.toFixed(2))
      grafico_pie();
    }
    function cotizacion_material(corte,habilitado,aparado,alistado,montaje,acabado)
    {
      var total=parseFloat(corte)+parseFloat(habilitado)+parseFloat(aparado)+parseFloat(alistado)+parseFloat(montaje)+parseFloat(acabado);
      $("#t_material").text(total.toFixed(6))
      var cantidad=$("#cantidad").val();
      if(cantidad<1)
      {
        cantidad=2;
        $("#cantidad").val("2");
      }
      $("#i_m_corte").text((corte*cantidad).toFixed(6))
      $("#p_m_corte").text((((corte*cantidad)/(parseFloat(total)*cantidad))*100).toFixed(2))
      $("#i_m_habilitado").text((habilitado*cantidad).toFixed(6))
      $("#p_m_habilitado").text((((habilitado*cantidad)/(parseFloat(total)*cantidad))*100).toFixed(2))
      $("#i_m_aparado").text((aparado*cantidad).toFixed(6))
      $("#p_m_aparado").text((((aparado*cantidad)/(parseFloat(total)*cantidad))*100).toFixed(2))
      $("#i_m_alistado").text((alistado*cantidad).toFixed(6))
      $("#p_m_alistado").text((((alistado*cantidad)/(parseFloat(total)*cantidad))*100).toFixed(2))
      $("#i_m_montaje").text((montaje*cantidad).toFixed(6))
      $("#p_m_montaje").text((((montaje*cantidad)/(parseFloat(total)*cantidad))*100).toFixed(2))
      $("#i_m_acabado").text((acabado*cantidad).toFixed(6))
      $("#p_m_acabado").text((((acabado*cantidad)/(parseFloat(total)*cantidad))*100).toFixed(2))
      $("#m_total").text((parseFloat(total)*cantidad).toFixed(6))

    }
    function cotizacion_mano(corte,habilitado,aparado,alistado,montaje,acabado)
    {
      var total=parseFloat(corte)+parseFloat(habilitado)+parseFloat(aparado)+parseFloat(alistado)+parseFloat(montaje)+parseFloat(acabado);
      $("#t_mano").text(total.toFixed(6))
      var cantidad=$("#cantidad").val();
      if(cantidad<1)
      {
        cantidad=2;
        $("#cantidad").val("2");
      }
      $("#i_o_corte").text((corte*cantidad).toFixed(6))
      $("#p_o_corte").text((((corte*cantidad)/(parseFloat(total)*cantidad))*100).toFixed(2))
      $("#i_o_habilitado").text((habilitado*cantidad).toFixed(6))
      $("#p_o_habilitado").text((((habilitado*cantidad)/(parseFloat(total)*cantidad))*100).toFixed(2))
      $("#i_o_aparado").text((aparado*cantidad).toFixed(6))
      $("#p_o_aparado").text((((aparado*cantidad)/(parseFloat(total)*cantidad))*100).toFixed(2))
      $("#i_o_alistado").text((alistado*cantidad).toFixed(6))
      $("#p_o_alistado").text((((alistado*cantidad)/(parseFloat(total)*cantidad))*100).toFixed(2))
      $("#i_o_montaje").text((montaje*cantidad).toFixed(6))
      $("#p_o_montaje").text((((montaje*cantidad)/(parseFloat(total)*cantidad))*100).toFixed(2))
      $("#i_o_acabado").text((acabado*cantidad).toFixed(6))
      $("#p_o_acabado").text((((acabado*cantidad)/(parseFloat(total)*cantidad))*100).toFixed(2))
      $("#o_total").text((parseFloat(total)*cantidad).toFixed(6))
    }
    function cotizacion_produccion()
    {
      let produccion=$("td[class~='g_produccion']");
      let cotizacion=$("td[class~='c_produccion']");
      let porcentaje=$("td[class~='p_produccion']");
      let cantidad=$("#cantidad").val();
      let total=total_cotizacion=0;
      for(let i=0;i<produccion.length;i++)
      {
        total=total+parseFloat(produccion[i].textContent);
        total_cotizacion=total_cotizacion+(parseFloat(produccion[i].textContent)*parseFloat(cantidad))
        cotizacion[i].textContent=(parseFloat(produccion[i].textContent)*parseFloat(cantidad)).toFixed(6);
      }
      for(let i=0;i<produccion.length;i++)
      {
        let porcentaje_calculo=((parseFloat(produccion[i].textContent)/total)*100).toFixed(2);
        porcentaje[i].textContent=porcentaje_calculo+"%";
      }
      $("#t_c_produccion").text(total_cotizacion.toFixed(6))
      $("#t_produccion").text(total.toFixed(6))
    }
    function cotizacion_operativo()
    {
      let produccion=$("td[class~='g_operativo']");
      let cotizacion=$("td[class~='c_operativo']");
      let porcentaje=$("td[class~='p_operativo']");
      let cantidad=$("#cantidad").val();
      let total=total_cotizacion=0;
      for(let i=0;i<produccion.length;i++)
      {
        total=total+parseFloat(produccion[i].textContent);
        total_cotizacion=total_cotizacion+(parseFloat(produccion[i].textContent)*parseFloat(cantidad))
        cotizacion[i].textContent=(parseFloat(produccion[i].textContent)*parseFloat(cantidad)).toFixed(6);
      }
      for(let i=0;i<produccion.length;i++)
      {
        let porcentaje_calculo=((parseFloat(produccion[i].textContent)/total)*100).toFixed(2);
        porcentaje[i].textContent=porcentaje_calculo+"%";
      }
      $("#t_c_operativo").text(total_cotizacion.toFixed(6))
      $("#t_operativo").text(total.toFixed(6))
    }
    function limpiar()
    {
      $("#linea").val("")
      $("#descripcion").val("")
      $("#serie").val("")
      $("#talla").val("")
      $("#talla_base").val("")
      $("#capellada").val("")
      $("#forro").val("")
      $("#piso").val("")
      $("#plantilla").val("")
    }
    function grafico(){
      let p_m_corte=$("#p_m_corte").text()
      let p_m_habilitado=$("#p_m_habilitado").text()
      let p_m_aparado=$("#p_m_aparado").text()
      let p_m_alistado=$("#p_m_alistado").text()
      let p_m_montaje=$("#p_m_montaje").text()
      let p_m_acabado=$("#p_m_acabado").text()

      let p_o_corte=$("#p_o_corte").text()
      let p_o_habilitado=$("#p_o_habilitado").text()
      let p_o_aparado=$("#p_o_aparado").text()
      let p_o_alistado=$("#p_o_alistado").text()
      let p_o_montaje=$("#p_o_montaje").text()
      let p_o_acabado=$("#p_o_acabado").text()
      var valor = {corte:p_m_corte,habilitado:p_m_habilitado,aparado:p_m_aparado,alistado:p_m_alistado
        ,montaje:p_m_montaje,acabado:p_m_acabado};
      let html="<canvas id='myChart' ></canvas>";
      $("#id_mats").empty();
      $("#id_mats").append(html);
      var ctx = document.getElementById('myChart');
      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: ['Corte', 'Habilitado', 'Aparado', 'Alistado', 'Montaje', 'Acabado'],
              datasets: [{
                  label: 'Costos Directos Materiales',
                  data: [p_m_corte, p_m_habilitado, p_m_aparado, p_m_alistado, p_m_montaje, p_m_acabado],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(255, 99, 132, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(255, 99, 132, 1)',
                      'rgba(255, 99, 132, 1)',
                      'rgba(255, 99, 132, 1)',
                      'rgba(255, 99, 132, 1)',
                      'rgba(255, 99, 132, 1)'
                  ],
                  borderWidth: 1
              },
              {
                  label: 'Costos Indirectos Materiales',
                  data: [p_o_corte, p_o_habilitado, p_o_aparado, p_o_alistado, p_o_montaje, p_o_acabado],
                  backgroundColor: [
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(54, 162, 235, 0.2)'
                  ],
                  borderColor: [
                      'rgba(54, 162, 235, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(54, 162, 235, 1)'
                  ],
                  borderWidth: 1
              }
            ]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    }
    function grafico_pie(){
      let resultado=$("td[class~='resultado']");
      let inversion=resultado[0].textContent
      let reparticion=resultado[2].textContent
      let impuesto=resultado[4].textContent
      let utilidad=resultado[5].textContent
      let html="<canvas id='Pastel_Par' ></canvas>"
      $("#id_grafico").empty()
      $("#id_grafico").append(html)
      let ctx = document.getElementById('Pastel_Par');
      let myPieChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
              labels: ['Inversion Por Par', 'Reparticion de Utilidades', 'Impuesto a la Renta','Utilidad Neta'],
              datasets: [{

                  data: [inversion,reparticion ,impuesto ,utilidad],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(123, 96, 255, 0.2)',
                      'rgba(194, 96, 255, 0.2)',
                      'rgba(210, 255, 96, 0.2)'
                  ],
                  borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(123, 96, 255, 1)',
                    'rgba(194, 96, 255, 1)',
                    'rgba(210, 255, 96, 1)'
                  ],
                  borderWidth: 1
              }
            ]
          },
          options: {

          }
      });
    }
    function grafico_pie_cotizacion(){
      let resultado=$("td[class~='resultado_cotizacion']");
      let inversion=resultado[0].textContent
      let reparticion=resultado[2].textContent
      let impuesto=resultado[4].textContent
      let utilidad=resultado[5].textContent
      let html="<canvas id='Pastel_Cotizacion' ></canvas>"
      $("#id_grafico_cotizacion").empty()
      $("#id_grafico_cotizacion").append(html)
      let ctx = document.getElementById('Pastel_Cotizacion');
      let myPieChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
              labels: ['Inversion Por Par', 'Reparticion de Utilidades', 'Impuesto a la Renta','Utilidad Neta'],
              datasets: [{

                  data: [inversion,reparticion ,impuesto ,utilidad],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(123, 96, 255, 0.2)',
                      'rgba(194, 96, 255, 0.2)',
                      'rgba(210, 255, 96, 0.2)'
                  ],
                  borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(123, 96, 255, 1)',
                    'rgba(194, 96, 255, 1)',
                    'rgba(210, 255, 96, 1)'
                  ],
                  borderWidth: 1
              }
            ]
          },
          options: {

          }
      });
    }
  });
</script>
@endsection
