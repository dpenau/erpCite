@extends ('layouts.admin')
@section ('contenido')
<div class="preloader">

</div>
<div>
<h3 class="font-weight-bold">Listado de modelos Base
<button id="act_costo" class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Actualizar costos Directos</button>
<button id="act" class="bttn-unite bttn-md bttn-success float-right mr-sm-5">Actualizar costos Indirectos</button>
</h3>
<span class="badge badge-warning">Consumo Desactualizado</span>
<span class="badge badge-info">Precio Desactualizado</span>
</div>
<div id="buscando" style="display:none; margin:20px;">
  <div class="alert alert-success" role="alert">
    Actualizando Constos...
  </div>
</div>
<div class="x_content table-responsive">
  <table id="example" class="display">
    <thead>
      <tr>
        <th></th>
        <th>Imagen</th>
        <th>Codigo del Modelo</th>
        <th>Serie</th>
        <th>Nombre del Modelo</th>
        <th>Costo de Material <br> Directos</th>
        <th>Costo de Mano de <br> Obra</th>
        <th>Total Costo Directo <br> de Fabricacion</th>
        <th>Cabecera</th>
        <th>Materiales Directos</th>
        <th>Mano de Obra Directo</th>
        <th>Articulos</th>
      </tr>
    </thead>
    <tbody>
      @foreach($modelos as $mat)
      <script type="text/javascript">
        console.log("{{$empresa}}")
      </script>
      <tr>
        <td>
          <div class="row">
            @if($mat->estado==9)
              <div class="col-md-6 bg-warning" style="height:20px; width:10px">
              </div>
            @endif
            @if($mat->estado==8)
              <div class="col-md-6 bg-warning" style="height:20px; width:10px">
              </div>
              <div class="col-md-6">
                <br>
              </div>
              <div class="col-md-6 bg-info"  style="height:20px; width:10px">
              </div>
            @endif
            @if($mat->estado==7)
              <div class="col-md-6 bg-info"  style="height:20px; width:10px">
              </div>
            @endif
          </div>
        </td>
        <td>  <a href="" data-target="#modal-imagen-{{$mat->cod_modelo}}" data-toggle="modal">
           <button class="bttn-unite bttn-md bttn-primary">{{Html::image('photo/modelos/'.$empresa.'/'.$mat->imagen,$mat->nombre,array( 'width' => 70, 'height' => 70 ))}}</button></a></td>
        <td>{{$mat->cod_modelo}}</td>
        <td>{{$mat->nombre_serie}}</td>
        <td>{{$mat->nombre}}</td>
        <td>{{number_format($mat->total_materiales,6)}}</td>
        <td>{{number_format($mat->total_obra,6)}}</td>
        <td>{{number_format($mat->total_materiales+$mat->total_obra,6)}}</td>
        <td>
          <a href="{!! route('norma_tecnica',['var'=>$mat->cod_modelo.'+'.$mat->codigo_serie.'+'.$mat->codigo]) !!}" >
            <button class="bttn-unite bttn-md bttn-primary "><i class="fas fa-newspaper"></i></button>
            </a>
        </td>
        <td>
          <a href="{!! route('costos_directos',['var'=>$mat->cod_modelo.'+'.$mat->codigo]) !!}" >
            <button class="bttn-unite bttn-md bttn-primary "><i class="fas fa-shopping-basket"></i></button></a>
        </td>
        <td>
          <a href="{!! route('mano_directo',['var'=>$mat->cod_modelo.'+'.$mat->codigo]) !!}" >
            <button class="bttn-unite bttn-md bttn-primary "><i class="fas fa-user-alt"></i></button></a>
        </td>
        <td>
          <a href="{!! route('modelo_articulo',['var'=>$mat->cod_modelo.'+'.$mat->codigo_serie.'+'.$mat->codigo]) !!}" >
            <button class="bttn-unite bttn-md bttn-warning "><i class="fas fa-th"></i></button></a>
        </td>
      </tr>
      @include('costos.modal_imagen')
      @endforeach
    </tbody>
  </table>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $("#act_costo").on("click",function(){
    $("#buscando").toggle("slow");
    $.ajax({
      url: "act_costo/",
      success: function(html){
        $("#buscando").toggle("slow");
          location.reload();
      }
    });
  })
  $("#act").on("click",function(){
    $("#buscando").toggle("slow");
    $.ajax({
      url: "/costo/actualizar/mat_indirecto",
      success: function(html){
        $("#buscando").toggle("slow");
          location.reload();
      }
    });
  })
});
</script>
@endsection
