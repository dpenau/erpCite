@extends ('layouts.admin')
@section ('contenido')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@if(count($codigo_costo)>0)
  @if(count($detalle)>0)
      {!!Form::open(array('url'=>'costo/directo/mano/update','method'=>'PUT','autocomplete'=>'off'))!!}
      {{Form::token()}}
        <script type="text/javascript">
          $(document).ready(function(){
            @foreach($detalle as $mat)
              var tipo=""
              var t=""
              var unidad="";
              @foreach($tipo_trabajador as $tipo)
                @if($mat->cod_tipo_trabajador==$tipo->cod_tipo_trabajador)
                  tipo+="<option value='{{$tipo->cod_tipo_trabajador}}' selected>{{$tipo->descrip_tipo_trabajador}}</option>"
                  t="{{$tipo->cod_tipo_trabajador}}";
                @else
                  tipo+="<option value='{{$tipo->cod_tipo_trabajador}}'>{{$tipo->descrip_tipo_trabajador}}</option>"
                @endif
              @endforeach
              switch (t) {
                case "1": unidad="Mes";
                  break;
                case "2":unidad="Semana";
                break;
                case "3":unidad="Docena";
                break;
                default:
              }
              var area="{{$mat->descrip_area}}";
              switch (area)
              {
                case "P-Acabado":

                  $('#P-Acabado> tbody:last-child').append("<tr id='f{{$mat->cod_detalle_costo_mano}}'>"
                  +"<td style='display:none'><input class='form-control' type='text' name='estado[]' value='{{$mat->cod_detalle_costo_mano}}'></td>"
                  +"<td><textarea class='form-control' rows='3' type='text' name='operacion[]'>{{$mat->operacion}}</textarea></td>"
                  +"<td> <select class='form-control' id='{{$mat->cod_detalle_costo_mano}}' name=tipo[]><option disabled selected>Tipo de Trabajador</option>"+tipo+"</select></td>"
                  +"<td><input class='form-control' id='u{{$mat->cod_detalle_costo_mano}}' disabled type='text' value='"+unidad+"' disabled></td>"
                  +"<td><input class='form-control' type='text' id='c{{$mat->cod_detalle_costo_mano}}' value='{{$mat->costo}}' name='costo[]'></td>"
                  +"<td><input class='form-control  block' type='text' name='beneficio[]' value='{{$mat->beneficio}}' id='b{{$mat->cod_detalle_costo_mano}}' disabled ></td>"
                  +"<td><input class='form-control' type='text' id='o{{$mat->cod_detalle_costo_mano}}' value='{{$mat->otros_costos}}' name='otros[]'></td>"
                  +"<td><input class='form-control block acabado' disabled type='text'  value='{{$mat->costo_por_par}}' id='p{{$mat->cod_detalle_costo_mano}}' name='costo_par[]' ></td>"
                  +"<td><input class='form-control' disabled type='text'  name='porcentaje[]' ></td>"
                  +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Acabado'></td>"
                  +"<td><a id='d{{$mat->cod_detalle_costo_mano}}' class='btn btn-danger'>-</a></td>"
                  +"</tr>");
                  $("#o{{$mat->cod_detalle_costo_mano}}").off("focusout");
                  $("#o{{$mat->cod_detalle_costo_mano}}").on("focusout",function(){
                    var otro=$(this).val();
                    if (otro!="") {
                      var id=$(this).attr('id');
                      id=id.substring(1);
                      var costo=$("#c"+id).val()
                      var valor_trabajo=$("#"+id).val();
                      if (valor_trabajo=="") {
                        alert("Ingrese tipo de trabajador para calcular el costo por par");
                        $(this).val("")
                        $("#"+id).addClass("bg-warning");
                        setTimeout(function(){
                          $("#"+id).removeClass("bg-warning");
                        },1500)
                      }
                      else {
                        var produccion_promedio={{$produccion_promedio}}
                        if(valor_trabajo==1)
                        {

                          var beneficio={{$beneficios}}
                          var beneficios=parseFloat(costo)*(parseFloat(beneficio)/100);
                          beneficios=beneficios.toFixed(6)
                          $("#b"+id).val(beneficios)
                          var costo_par=(parseFloat(beneficios)+parseFloat(costo)+parseFloat(otro))/parseFloat(produccion_promedio);
                          costo_par=costo_par.toFixed(6);
                          $("#p"+id).val(costo_par)
                        }
                        else {
                          if (valor_trabajo==3) {
                            $("#b"+id).val(0);
                            var costo_par=(parseFloat(costo)+parseFloat(otro))/12;
                            costo_par=costo_par.toFixed(6);
                            $("#p"+id).val(costo_par)
                          }
                          else {
                            if (valor_trabajo==2) {
                              $("#b"+id).val(0);
                              var costo_par=(parseFloat(costo)+parseFloat(otro))/(parseFloat(produccion_promedio)/4);
                              costo_par=costo_par.toFixed(6);
                              $("#p"+id).val(costo_par)
                            }
                          }
                        }
                      }
                    }
                  });
                  $("#c{{$mat->cod_detalle_costo_mano}}").off("focusout");
                  $("#c{{$mat->cod_detalle_costo_mano}}").on("focusout",function(){
                    var costo=$(this).val();
                    if (costo!="") {
                      var id=$(this).attr('id');
                      id=id.substring(1);
                      var valor_trabajo=$("#"+id).val();
                      ($("#"+id))
                      if (valor_trabajo=="") {
                        alert("Ingrese tipo de trabajodor para calcular el costo por par");
                        $(this).val("")
                        $("#"+id).addClass("bg-warning");
                        setTimeout(function(){
                          $("#"+id).removeClass("bg-warning");
                        },1500)
                      }
                    }
                  });
                  $("#{{$mat->cod_detalle_costo_mano}}").off("change");
                  $("#{{$mat->cod_detalle_costo_mano}}").on("change",function(){
                    var val=$(this).val();
                    var id=$(this).attr('id');
                    switch (val) {
                      case "1": $("#u"+id).val("Mes");
                        break;
                      case "2":$("#u"+id).val("Semana")
                      break;
                      case "3":$("#u"+id).val("Docena")
                      break;
                      default:
                    }
                    $("#c"+id).val("");
                  });
                  $("#d{{$mat->cod_detalle_costo_mano}}").off("click");
                  $("#d{{$mat->cod_detalle_costo_mano}}").on("click",function(){
                    var id=$(this).attr('id');
                    id=id.substring(1)
                    $("#f"+id).remove();
                  });
                break;
                case "P-Aparado":
                $('#P-Aparado> tbody:last-child').append("<tr id='f{{$mat->cod_detalle_costo_mano}}'>"
                +"<td style='display:none'><input class='form-control' type='text' name='estado[]' value='{{$mat->cod_detalle_costo_mano}}'></td>"
                +"<td><textarea class='form-control' rows='3' type='text' name='operacion[]'>{{$mat->operacion}}</textarea></td>"
                +"<td> <select class='form-control' id={{$mat->cod_detalle_costo_mano}} name=tipo[]><option disabled selected>Tipo de Trabajador</option>"+tipo+"</select></td>"
                +"<td><input class='form-control' id='u{{$mat->cod_detalle_costo_mano}}' disabled type='text' value='"+unidad+"' disabled></td>"
                +"<td><input class='form-control' type='text' id='c{{$mat->cod_detalle_costo_mano}}' value='{{$mat->costo}}' name='costo[]'></td>"
                +"<td><input class='form-control block' type='text' name='beneficio[]' value='{{$mat->beneficio}}' id='b{{$mat->cod_detalle_costo_mano}}' disabled ></td>"
                +"<td><input class='form-control' type='text' id='o{{$mat->cod_detalle_costo_mano}}' value='{{$mat->otros_costos}}' name='otros[]'></td>"
                +"<td><input class='form-control block aparado' disabled type='text' value='{{$mat->costo_por_par}}' id='p{{$mat->cod_detalle_costo_mano}}' name='costo_par[]' ></td>"
                +"<td><input class='form-control' disabled type='text'  name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Aparado'></td>"
                +"<td><a id='d{{$mat->cod_detalle_costo_mano}}' class='btn btn-danger' >-</a></td>"
                +"</tr>");
                $("#o{{$mat->cod_detalle_costo_mano}}").off("focusout");
                $("#o{{$mat->cod_detalle_costo_mano}}").on("focusout",function(){
                  var otro=$(this).val();
                  if (otro!="") {
                    var id=$(this).attr('id');
                    id=id.substring(1);
                    var costo=$("#c"+id).val()
                    var valor_trabajo=$("#"+id).val();
                    if (valor_trabajo=="") {
                      alert("Ingrese tipo de trabajador para calcular el costo por par");
                      $(this).val("")
                      $("#"+id).addClass("bg-warning");
                      setTimeout(function(){
                        $("#"+id).removeClass("bg-warning");
                      },1500)
                    }
                    else {
                      var produccion_promedio={{$produccion_promedio}}
                      if(valor_trabajo==1)
                      {

                        var beneficio={{$beneficios}}
                        var beneficios=parseFloat(costo)*(parseFloat(beneficio)/100);
                        beneficios=beneficios.toFixed(6)
                        $("#b"+id).val(beneficios)
                        var costo_par=(parseFloat(beneficios)+parseFloat(costo))/parseFloat(produccion_promedio);
                        costo_par=costo_par.toFixed(6);
                        $("#p"+id).val(costo_par)
                      }
                      else {
                        if (valor_trabajo==3) {
                          $("#b"+id).val(0);
                          var costo_par=parseFloat(costo)/12;
                          costo_par=costo_par.toFixed(6);
                          $("#p"+id).val(costo_par)
                        }
                        else {
                          if (valor_trabajo==2) {
                            $("#b"+id).val(0);
                            var costo_par=parseFloat(costo)/(parseFloat(produccion_promedio)/4);
                            costo_par=costo_par.toFixed(6);
                            $("#p"+id).val(costo_par)
                          }
                        }
                      }
                    }
                  }
                });
                $("#c{{$mat->cod_detalle_costo_mano}}").off("focusout");
                $("#c{{$mat->cod_detalle_costo_mano}}").on("focusout",function(){
                  var costo=$(this).val();
                  if (costo!="") {
                    var id=$(this).attr('id');
                    id=id.substring(1);
                    var valor_trabajo=$("#"+id).val();
                    if (valor_trabajo==null) {
                      alert("Ingrese tipo de trabajodor para calcular el costo por par");
                      $(this).val("")
                      $("#"+id).addClass("bg-warning");
                      setTimeout(function(){
                        $("#"+id).removeClass("bg-warning");
                      },1500)
                    }

                  }
                })
                $("#{{$mat->cod_detalle_costo_mano}}").off("change");
                $("#{{$mat->cod_detalle_costo_mano}}").on("change",function(){
                  var val=$(this).val();
                  var id=$(this).attr('id');
                  switch (val) {
                    case "1": $("#u"+id).val("Mes")
                      break;
                    case "2":$("#u"+id).val("Semana")
                    break;
                    case "3":$("#u"+id).val("Docena")
                    break;
                    default:
                  }
                  $("#c"+id).val("");
                });
                $("#d{{$mat->cod_detalle_costo_mano}}").off("click");
                $("#d{{$mat->cod_detalle_costo_mano}}").on("click",function(){
                  var id=$(this).attr('id');
                  id=id.substring(1);
                  $("#f"+id).remove();
                });
                break;
                case "P-Corte":
                $('#P-Corte> tbody:last-child').append("<tr id='f{{$mat->cod_detalle_costo_mano}}'>"
                +"<td style='display:none'><input class='form-control' type='text' name='estado[]' value='{{$mat->cod_detalle_costo_mano}}'></td>"
                +"<td><textarea class='form-control' rows='3' type='text' name='operacion[]'>{{$mat->operacion}}</textarea></td>"
                +"<td> <select class='form-control' id={{$mat->cod_detalle_costo_mano}} name=tipo[]><option disabled selected>Tipo de Trabajador</option>"+tipo+"</select></td>"
                +"<td><input class='form-control' id='u{{$mat->cod_detalle_costo_mano}}' disabled type='text' value='"+unidad+"' disabled></td>"
                +"<td><input class='form-control' type='text' id='c{{$mat->cod_detalle_costo_mano}}' value='{{$mat->costo}}' name='costo[]'></td>"
                +"<td><input class='form-control block' type='text' name='beneficio[]' value='{{$mat->beneficio}}' id='b{{$mat->cod_detalle_costo_mano}}' disabled ></td>"
                +"<td><input class='form-control' type='text' id='o{{$mat->cod_detalle_costo_mano}}' value='{{$mat->otros_costos}}' name='otros[]'></td>"
                +"<td><input class='form-control block corte' disabled type='text' value='{{$mat->costo_por_par}}' id='p{{$mat->cod_detalle_costo_mano}}' name='costo_par[]' ></td>"
                +"<td><input class='form-control' disabled type='text'  name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Corte'></td>"
                +"<td><a id='d{{$mat->cod_detalle_costo_mano}}' class='btn btn-danger' >-</a></td>"
                +"</tr>");
                $("#o{{$mat->cod_detalle_costo_mano}}").off("focusout");
                $("#o{{$mat->cod_detalle_costo_mano}}").on("focusout",function(){
                  var otro=$(this).val();
                  if (otro!="") {
                    var id=$(this).attr('id');
                    id=id.substring(1);
                    var costo=$("#c"+id).val()
                    var valor_trabajo=$("#"+id).val();
                    if (valor_trabajo=="") {
                      alert("Ingrese tipo de trabajador para calcular el costo por par");
                      $(this).val("")
                      $("#"+id).addClass("bg-warning");
                      setTimeout(function(){
                        $("#"+id).removeClass("bg-warning");
                      },1500)
                    }
                    else {
                      var produccion_promedio={{$produccion_promedio}}
                      if(valor_trabajo==1)
                      {

                        var beneficio={{$beneficios}}
                        var beneficios=parseFloat(costo)*(parseFloat(beneficio)/100);
                        beneficios=beneficios.toFixed(6)
                        $("#b"+id).val(beneficios)
                        var costo_par=(parseFloat(beneficios)+parseFloat(costo)+parseFloat(otro))/parseFloat(produccion_promedio);
                        costo_par=costo_par.toFixed(6);
                        $("#p"+id).val(costo_par)
                      }
                      else {
                        if (valor_trabajo==3) {
                          $("#b"+id).val(0);
                          var costo_par=(parseFloat(costo)+parseFloat(otro))/12;
                          costo_par=costo_par.toFixed(6);
                          $("#p"+id).val(costo_par)
                        }
                        else {
                          if (valor_trabajo==2) {
                            $("#b"+id).val(0);
                            var costo_par=(parseFloat(costo)+parseFloat(otro))/(parseFloat(produccion_promedio)/4);
                            costo_par=costo_par.toFixed(6);
                            $("#p"+id).val(costo_par)
                          }
                        }
                      }
                    }
                  }
                });
                $("#c{{$mat->cod_detalle_costo_mano}}").off("focusout");
                $("#c{{$mat->cod_detalle_costo_mano}}").on("focusout",function(){
                  var costo=$(this).val();
                  if (costo!="") {
                    var id=$(this).attr('id');
                    id=id.substring(1);
                    var valor_trabajo=$("#"+id).val();
                    if (valor_trabajo==null) {
                      alert("Ingrese tipo de trabajodor para calcular el costo por par");
                      $(this).val("")
                      $("#"+id).addClass("bg-warning");
                      setTimeout(function(){
                        $("#"+id).removeClass("bg-warning");
                      },1500)
                    }
                  }
                })
                $("#{{$mat->cod_detalle_costo_mano}}").off("change");
                $("#{{$mat->cod_detalle_costo_mano}}").on("change",function(){
                  var val=$(this).val();
                  var id=$(this).attr('id');
                  switch (val) {
                    case "1": $("#u"+id).val("Mes")
                      break;
                    case "2":$("#u"+id).val("Semana")
                    break;
                    case "3":$("#u"+id).val("Docena")
                    break;
                    default:
                  }
                  $("#c"+id).val("");
                });
                $("#d{{$mat->cod_detalle_costo_mano}}").off("click");
                $("#d{{$mat->cod_detalle_costo_mano}}").on("click",function(){
                  var id=$(this).attr('id');
                  id=id.substring(1);
                  $("#f"+id).remove();
                });

                break;
                case "P-Habilitado":
                $('#P-Habilitado> tbody:last-child').append("<tr id='f{{$mat->cod_detalle_costo_mano}}'>"
                +"<td style='display:none'><input class='form-control' type='text' name='estado[]' value='{{$mat->cod_detalle_costo_mano}}'></td>"
                +"<td><textarea class='form-control' rows='3' type='text' name='operacion[]'>{{$mat->operacion}}</textarea></td>"
                +"<td> <select class='form-control' id={{$mat->cod_detalle_costo_mano}} name=tipo[]><option disabled selected>Tipo de Trabajador</option>"+tipo+"</select></td>"
                +"<td><input class='form-control' id='u{{$mat->cod_detalle_costo_mano}}' disabled type='text' value='"+unidad+"' disabled></td>"
                +"<td><input class='form-control' type='text' id='c{{$mat->cod_detalle_costo_mano}}' value='{{$mat->costo}}' name='costo[]'></td>"
                +"<td><input class='form-control block' type='text' name='beneficio[]' value='{{$mat->beneficio}}' id='b{{$mat->cod_detalle_costo_mano}}' disabled ></td>"
                +"<td><input class='form-control' type='text' id='o{{$mat->cod_detalle_costo_mano}}' value='{{$mat->otros_costos}}' name='otros[]'></td>"
                +"<td><input class='form-control block habilitado' disabled type='text' value='{{$mat->costo_por_par}}' id='p{{$mat->cod_detalle_costo_mano}}' name='costo_par[]' ></td>"
                +"<td><input class='form-control' disabled type='text'  name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Habilitado'></td>"
                +"<td><a id='d{{$mat->cod_detalle_costo_mano}}' class='btn btn-danger' >-</a></td>"
                +"</tr>");
                $("#o{{$mat->cod_detalle_costo_mano}}").off("focusout");
                $("#o{{$mat->cod_detalle_costo_mano}}").on("focusout",function(){
                  var otro=$(this).val();
                  if (otro!="") {
                    var id=$(this).attr('id');
                    id=id.substring(1);
                    var costo=$("#c"+id).val()
                    var valor_trabajo=$("#"+id).val();
                    if (valor_trabajo=="") {
                      alert("Ingrese tipo de trabajador para calcular el costo por par");
                      $(this).val("")
                      $("#"+id).addClass("bg-warning");
                      setTimeout(function(){
                        $("#"+id).removeClass("bg-warning");
                      },1500)
                    }
                    else {
                      var produccion_promedio={{$produccion_promedio}}
                      if(valor_trabajo==1)
                      {

                        var beneficio={{$beneficios}}
                        var beneficios=parseFloat(costo)*(parseFloat(beneficio)/100);
                        beneficios=beneficios.toFixed(6)
                        $("#b"+id).val(beneficios)
                        var costo_par=(parseFloat(beneficios)+parseFloat(costo)+parseFloat(otro))/parseFloat(produccion_promedio);
                        costo_par=costo_par.toFixed(6);
                        $("#p"+id).val(costo_par)
                      }
                      else {
                        if (valor_trabajo==3) {
                          $("#b"+id).val(0);
                          var costo_par=(parseFloat(costo)+parseFloat(otro))/12;
                          costo_par=costo_par.toFixed(6);
                          $("#p"+id).val(costo_par)
                        }
                        else {
                          if (valor_trabajo==2) {
                            $("#b"+id).val(0);
                            var costo_par=(parseFloat(costo)+parseFloat(otro))/(parseFloat(produccion_promedio)/4);
                            costo_par=costo_par.toFixed(6);
                            $("#p"+id).val(costo_par)
                          }
                        }
                      }
                    }
                  }
                });
                $("#c{{$mat->cod_detalle_costo_mano}}").off("focusout");
                $("#c{{$mat->cod_detalle_costo_mano}}").on("focusout",function(){
                  var costo=$(this).val();
                  if (costo!="") {
                    var id=$(this).attr('id');
                    id=id.substring(1);
                    var valor_trabajo=$("#"+id).val();
                    if (valor_trabajo==null) {
                      alert("Ingrese tipo de trabajodor para calcular el costo por par");
                      $(this).val("")
                      $("#"+id).addClass("bg-warning");
                      setTimeout(function(){
                        $("#"+id).removeClass("bg-warning");
                      },1500)
                    }
                  }
                })
                $("#{{$mat->cod_detalle_costo_mano}}").off("change");
                $("#{{$mat->cod_detalle_costo_mano}}").on("change",function(){
                  var val=$(this).val();
                  var id=$(this).attr('id');
                  switch (val) {
                    case "1": $("#u"+id).val("Mes")
                      break;
                    case "2":$("#u"+id).val("Semana")
                    break;
                    case "3":$("#u"+id).val("Docena")
                    break;
                    default:
                  }
                  $("#c"+id).val("");
                });
                $("#d{{$mat->cod_detalle_costo_mano}}").off("click");
                $("#d{{$mat->cod_detalle_costo_mano}}").on("click",function(){
                  var id=$(this).attr('id');
                  id=id.substring(1);
                  $("#f"+id).remove();
                });

                break;
                case "P-Alistado":
                $('#P-Alistado> tbody:last-child').append("<tr id='f{{$mat->cod_detalle_costo_mano}}'>"
                +"<td style='display:none'><input class='form-control' type='text' name='estado[]' value='{{$mat->cod_detalle_costo_mano}}'></td>"
                +"<td><textarea class='form-control' rows='3' type='text' name='operacion[]'>{{$mat->operacion}}</textarea></td>"
                +"<td> <select class='form-control' id={{$mat->cod_detalle_costo_mano}} name=tipo[]><option disabled selected>Tipo de Trabajador</option>"+tipo+"</select></td>"
                +"<td><input class='form-control' id='u{{$mat->cod_detalle_costo_mano}}' disabled type='text' value='"+unidad+"' disabled></td>"
                +"<td><input class='form-control' type='text' id='c{{$mat->cod_detalle_costo_mano}}' value='{{$mat->costo}}' name='costo[]'></td>"
                +"<td><input class='form-control block' type='text' name='beneficio[]' value='{{$mat->beneficio}}' id='b{{$mat->cod_detalle_costo_mano}}' disabled ></td>"
                +"<td><input class='form-control' type='text' id='o{{$mat->cod_detalle_costo_mano}}' value='{{$mat->otros_costos}}' name='otros[]'></td>"
                +"<td><input class='form-control block alistado' disabled type='text' value='{{$mat->costo_por_par}}' id='p{{$mat->cod_detalle_costo_mano}}' name='costo_par[]' ></td>"
                +"<td><input class='form-control' disabled type='text'  name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Alistado'></td>"
                +"<td><a id='d{{$mat->cod_detalle_costo_mano}}' class='btn btn-danger' >-</a></td>"
                +"</tr>");
                $("#o{{$mat->cod_detalle_costo_mano}}").off("focusout");
                $("#o{{$mat->cod_detalle_costo_mano}}").on("focusout",function(){
                  var otro=$(this).val();
                  if (otro!="") {
                    var id=$(this).attr('id');
                    id=id.substring(1);
                    var costo=$("#c"+id).val()
                    var valor_trabajo=$("#"+id).val();
                    if (valor_trabajo=="") {
                      alert("Ingrese tipo de trabajador para calcular el costo por par");
                      $(this).val("")
                      $("#"+id).addClass("bg-warning");
                      setTimeout(function(){
                        $("#"+id).removeClass("bg-warning");
                      },1500)
                    }
                    else {

                      var produccion_promedio={{$produccion_promedio}}
                      if(valor_trabajo==1)
                      {

                        var beneficio={{$beneficios}}
                        var beneficios=parseFloat(costo)*(parseFloat(beneficio)/100);
                        beneficios=beneficios.toFixed(6)
                        $("#b"+id).val(beneficios)
                        var costo_par=(parseFloat(beneficios)+parseFloat(costo)+parseFloat(otro))/parseFloat(produccion_promedio);
                        costo_par=costo_par.toFixed(6);

                        $("#p"+id).val(costo_par)
                      }
                      else {
                        if (valor_trabajo==3) {
                          $("#b"+id).val(0);
                          var costo_par=(parseFloat(costo)+parseFloat(otro))/12;
                          costo_par=costo_par.toFixed(6);
                          $("#p"+id).val(costo_par)
                        }
                        else {
                          if (valor_trabajo==2) {
                            $("#b"+id).val(0);
                            var costo_par=(parseFloat(costo)+parseFloat(otro))/(parseFloat(produccion_promedio)/4);
                            costo_par=costo_par.toFixed(6);
                            $("#p"+id).val(costo_par)
                          }
                        }
                      }
                    }
                  }
                });
                $("#c{{$mat->cod_detalle_costo_mano}}").off("focusout");
                $("#c{{$mat->cod_detalle_costo_mano}}").on("focusout",function(){
                  var costo=$(this).val();
                  if (costo!="") {
                    var id=$(this).attr('id');
                    id=id.substring(1);
                    var valor_trabajo=$("#"+id).val();
                    if (valor_trabajo==null) {
                      alert("Ingrese tipo de trabajodor para calcular el costo por par");
                      $(this).val("")
                      $("#"+id).addClass("bg-warning");
                      setTimeout(function(){
                        $("#"+id).removeClass("bg-warning");
                      },1500)
                    }
                  }
                })
                $("#{{$mat->cod_detalle_costo_mano}}").off("change");
                $("#{{$mat->cod_detalle_costo_mano}}").on("change",function(){
                  var val=$(this).val();
                  var id=$(this).attr('id');
                  switch (val) {
                    case "1": $("#u"+id).val("Mes")
                      break;
                    case "2":$("#u"+id).val("Semana")
                    break;
                    case "3":$("#u"+id).val("Docena")
                    break;
                    default:
                  }
                  $("#c"+id).val("");
                });
                $("#d{{$mat->cod_detalle_costo_mano}}").off("click");
                $("#d{{$mat->cod_detalle_costo_mano}}").on("click",function(){
                  var id=$(this).attr('id');
                  id=id.substring(1);
                  $("#f"+id).remove();
                });

                break;
                case "P-Montaje":
                $('#P-Montaje> tbody:last-child').append("<tr id='f{{$mat->cod_detalle_costo_mano}}'>"
                +"<td style='display:none'><input class='form-control' type='text' name='estado[]' value='{{$mat->cod_detalle_costo_mano}}'></td>"
                +"<td><textarea class='form-control' rows='3' type='text' name='operacion[]'>{{$mat->operacion}}</textarea></td>"
                +"<td> <select class='form-control' id={{$mat->cod_detalle_costo_mano}} name=tipo[]><option disabled selected>Tipo de Trabajador</option>"+tipo+"</select></td>"
                +"<td><input class='form-control' id='u{{$mat->cod_detalle_costo_mano}}' disabled type='text' value='"+unidad+"' disabled></td>"
                +"<td><input class='form-control' type='text' id='c{{$mat->cod_detalle_costo_mano}}' value='{{$mat->costo}}' name='costo[]'></td>"
                +"<td><input class='form-control block' type='text' name='beneficio[]' value='{{$mat->beneficio}}' id='b{{$mat->cod_detalle_costo_mano}}' disabled ></td>"
                +"<td><input class='form-control' type='text' id='o{{$mat->cod_detalle_costo_mano}}' value='{{$mat->otros_costos}}' name='otros[]'></td>"
                +"<td><input class='form-control block montaje' disabled type='text' value='{{$mat->costo_por_par}}' id='p{{$mat->cod_detalle_costo_mano}}' name='costo_par[]' ></td>"
                +"<td><input class='form-control' disabled type='text'  name='porcentaje[]' ></td>"
                +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Montaje'></td>"
                +"<td><a id='d{{$mat->cod_detalle_costo_mano}}' class='btn btn-danger' >-</a></td>"
                +"</tr>");
                $("#o{{$mat->cod_detalle_costo_mano}}").off("focusout");
                $("#o{{$mat->cod_detalle_costo_mano}}").on("focusout",function(){
                  var otro=$(this).val();
                  if (otro!="") {
                    var id=$(this).attr('id');
                    id=id.substring(1);
                    var costo=$("#c"+id).val()
                    var valor_trabajo=$("#"+id).val();
                    if (valor_trabajo=="") {
                      alert("Ingrese tipo de trabajador para calcular el costo por par");
                      $(this).val("")
                      $("#"+id).addClass("bg-warning");
                      setTimeout(function(){
                        $("#"+id).removeClass("bg-warning");
                      },1500)
                    }
                    else {
                      var produccion_promedio={{$produccion_promedio}}
                      if(valor_trabajo==1)
                      {

                        var beneficio={{$beneficios}}
                        var beneficios=parseFloat(costo)*(parseFloat(beneficio)/100);
                        beneficios=beneficios.toFixed(6)
                        $("#b"+id).val(beneficios)
                        var costo_par=(parseFloat(beneficios)+parseFloat(costo)+parseFloat(otro))/parseFloat(produccion_promedio);
                        costo_par=costo_par.toFixed(6);
                        $("#p"+id).val(costo_par)
                      }
                      else {
                        if (valor_trabajo==3) {
                          $("#b"+id).val(0);
                          var costo_par=(parseFloat(costo)+parseFloat(otro))/12;
                          costo_par=costo_par.toFixed(6);
                          $("#p"+id).val(costo_par)
                        }
                        else {
                          if (valor_trabajo==2) {
                            $("#b"+id).val(0);
                            var costo_par=(parseFloat(costo)+parseFloat(otro))/(parseFloat(produccion_promedio)/4);
                            costo_par=costo_par.toFixed(6);
                            $("#p"+id).val(costo_par)
                          }
                        }
                      }
                    }
                  }
                });
                $("#c{{$mat->cod_detalle_costo_mano}}").off("focusout");
                $("#c{{$mat->cod_detalle_costo_mano}}").on("focusout",function(){
                  var costo=$(this).val();
                  if (costo!="") {
                    var id=$(this).attr('id');
                    id=id.substring(1);
                    var valor_trabajo=$("#"+id).val();
                    if (valor_trabajo==null) {
                      alert("Ingrese tipo de trabajodor para calcular el costo por par");
                      $(this).val("")
                      $("#"+id).addClass("bg-warning");
                      setTimeout(function(){
                        $("#"+id).removeClass("bg-warning");
                      },1500)
                    }
                  }
                })
                $("#{{$mat->cod_detalle_costo_mano}}").off("change");
                $("#{{$mat->cod_detalle_costo_mano}}").on("change",function(){
                  var val=$(this).val();
                  var id=$(this).attr('id');
                  switch (val) {
                    case "1": $("#u"+id).val("Mes")
                      break;
                    case "2":$("#u"+id).val("Semana")
                    break;
                    case "3":$("#u"+id).val("Docena")
                    break;
                    default:
                  }
                  $("#c"+id).val("");
                });
                $("#d{{$mat->cod_detalle_costo_mano}}").off("click");
                $("#d{{$mat->cod_detalle_costo_mano}}").on("click",function(){
                  var id=$(this).attr('id');
                  id=id.substring(1);
                  $("#f"+id).remove();
                });
                break;
                }
                @if($mat->estado_trabajador==3 || $mat->estado_trabajador==2)

                $("#f{{$mat->cod_detalle_costo_mano}}").addClass("bg-info")
                @endif
            @endforeach
          });
        </script>

  @else
      {!!Form::open(array('url'=>'costo/directo/mano/create','method'=>'POST','autocomplete'=>'off'))!!}
      {{Form::token()}}
  @endif
  @foreach($codigo_costo as $cod)
  <input type="text" name="costo_codigo" style="display:none" value="{{$cod->cod_costo_modelo}}">
  @endforeach
  <div class="right_col" role="main">
    <div class="border-bottom">
      <div class="row">
        <div class="col-md-6">
          <h1>Codigo de Modelo:{{$var}}</h1>
        </div>
        <div class="col-md-6">
          <h3 id="material_total">Costo Total de materiales: S/.</h3>
          <input style="display:none" id="mt"  name="total_materiales" class="form-control" value="">
        </div>
      </div>
      <span class="badge badge-info">Costo desactualizado</span>

    </div>
    <div class="">
      <div class="row">
        <div class="col-md-3">
          <h2>Area: Corte</h2>
        </div>
        <div class="col-md-2">
          <a class="boton_agregar" id="c">
            <button type="button" class="bttn-unite bttn-md bttn-primary "><i class="fa fa-plus" ></i></button></a>
        </div>
      </div>
      <div class="x_content table-responsive">
        <table id="P-Corte" class="table stacktable">
          <thead>
            <tr>
              <th>Operacion</th>
              <th>Tipo de trabajador</th>
              <th>Unidad de Medida</th>
              <th>Costo</th>
              <th>Beneficio</th>
              <th>Otros costos mensuales</th>
              <th>Costo por Par</th>
              <th>%</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="corte_txt">Total:</h5>
    </div>
    <div class="">
      <div class="row">
        <div class="col-md-3">
          <h2>Area: Habilitado</h2>
        </div>
        <div class="col-md-2">
          <a class="boton_agregar" id="h">
            <button type="button" class="bttn-unite bttn-md bttn-primary "><i class="fa fa-plus" ></i></button></a>
        </div>
      </div>
      <div class="x_content table-responsive">
        <table id="P-Habilitado" class="table stacktable">
          <thead>
            <tr>
              <th>Operacion</th>
              <th>Tipo de trabajador</th>
              <th>Unidad de Medida</th>
              <th>Costo</th>
              <th>Beneficio</th>
              <th>Otros costos mensuales</th>
              <th>Costo por Par</th>
              <th>%</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="habilitado_txt">Total:</h5>
    </div>
    <div class="">
      <div class="row">
        <div class="col-md-3">
          <h2>Area: Aparado</h2>
        </div>
        <div class="col-md-2">
          <a class="boton_agregar" id="a">
            <button type="button" class="bttn-unite bttn-md bttn-primary "><i class="fa fa-plus" ></i></button></a>
        </div>
      </div>

      <div class="x_content table-responsive">
        <table id="P-Aparado" class="table stacktable">
          <thead>
            <tr>
              <th>Operacion</th>
              <th>Tipo de trabajador</th>
              <th>Unidad de Medida</th>
              <th>Costo</th>
              <th>Beneficio</th>
              <th>Otros costos mensuales</th>
              <th>Costo por Par</th>
              <th>%</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="aparado_txt">Total:</h5>
    </div>
    <div class="">
      <div class="row">
        <div class="col-md-3">
          <h2>Area: Alistado</h2>
        </div>
        <div class="col-md-2">
          <a class="boton_agregar" id="al">
            <button type="button" class="bttn-unite bttn-md bttn-primary "><i class="fa fa-plus" ></i></button></a>
        </div>
      </div>

      <div class="x_content table-responsive">
        <table id="P-Alistado" class="table stacktable">
          <thead>
            <tr>
              <th>Operacion</th>
              <th>Tipo de trabajador</th>
              <th>Unidad de Medida</th>
              <th>Costo</th>
              <th>Beneficio</th>
              <th>Otros costos mensuales</th>
              <th>Costo por Par</th>
              <th>%</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="alistado_txt">Total:</h5>
    </div>
    <div class="">
      <div class="row">
        <div class="col-md-3">
          <h2>Area: Montaje</h2>
        </div>
        <div class="col-md-2">
          <a class="boton_agregar" id="m">
            <button type="button" class="bttn-unite bttn-md bttn-primary "><i class="fa fa-plus" ></i></button></a>
        </div>
      </div>

      <div class="x_content table-responsive">
        <table id="P-Montaje" class="table stacktable">
          <thead>
            <tr>
              <th>Operacion</th>
              <th>Tipo de trabajador</th>
              <th>Unidad de Medida</th>
              <th>Costo</th>
              <th>Beneficio</th>
              <th>Otros costos mensuales</th>
              <th>Costo por Par</th>
              <th>%</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="montaje_txt">Total:</h5>
    </div>
    <div class="">
      <div class="row">
        <div class="col-md-3">
          <h2>Area: Acabado</h2>
        </div>
        <div class="col-md-2">
          <a class="boton_agregar" id="ac">
            <button type="button" class="bttn-unite bttn-md bttn-primary "><i class="fa fa-plus" ></i></button></a>
        </div>
      </div>

      <div class="x_content table-responsive">
        <table id="P-Acabado" class="table stacktable">
          <thead>
            <tr>
              <th>Operacion</th>
              <th>Tipo de trabajador</th>
              <th>Unidad de Medida</th>
              <th>Costo</th>
              <th>Beneficio</th>
              <th>Otros costos mensuales</th>
              <th>Costo por Par</th>
              <th>%</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      <h5 id="acabado_txt">Total:</h5>
    </div>
    <p>
      <a  id="btn_total"  ><button type="button" class="bttn-unite bttn-md bttn-primary">Calcular Total</button></a></p>
    <button type="submit" class="bttn-unite bttn-md bttn-primary" target="_blank" id="sub">Guardar</button>

      <a  href="{{ url('costo/vermodelos') }}" ><button type="button" class="bttn-unite bttn-md bttn-danger">Cancelar</button></a>
      {!!Form::close()!!}
  </div>
@else
  <div class="alert alert-danger">
  <a href="{!! route('norma_tecnica',['var'=>$var]) !!}" class="alert-link">Cree Primero una ficha tecnica</a>
  </div>
    <a type="button" class="bttn-unite bttn-md bttn-danger " href="{{ url('costo/vermodelos') }}" >Atras</a>
@endif


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  var c=1;
  var tipo=""
  @foreach($tipo_trabajador as $tipo)
   tipo+="<option value='{{$tipo->cod_tipo_trabajador}}'>{{$tipo->descrip_tipo_trabajador}}</option>"
  @endforeach

  $(".boton_agregar").click(function()
  {
    var id=$(this).attr('id');
    agregar(id)
  });
  function agregar(posicion){
    var area=posicion;
    if(area!=null)
    {

      switch (area)
      {
          case "ac":
          $('#P-Acabado> tbody:last-child').append("<tr id='f"+c+"'>"
          +"<td style='display:none'><input class='form-control' type='text' name='estado[]' value='nuevo'></td>"
          +"<td><textarea class='form-control' rows='3' type='text' name='operacion[]'></textarea></td>"
          +"<td> <select class='form-control' id='"+c+"' name=tipo[]><option disabled selected>Tipo de Trabajador</option>"+tipo+"</select></td>"
          +"<td><input class='form-control' id='u"+c+"' disabled type='text' disabled></td>"
          +"<td><input class='form-control' type='text' id='c"+c+"' name='costo[]'></td>"
          +"<td><input class='form-control block' type='text' name='beneficio[]' id='b"+c+"' disabled ></td>"
          +"<td><input class='form-control'  type='text' id='o"+c+"' name='otros[]'></td>"
          +"<td><input class='form-control block acabado' disabled type='text' id='p"+c+"' name='costo_par[]' ></td>"
          +"<td><input class='form-control' disabled type='text'  name='porcentaje[]' ></td>"
          +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Acabado'></td>"
          +"<td><a id='d"+c+"' class='btn btn-danger'>-</a></td>"
          +"</tr>");
          $("#o"+c).off("focusout");
          $("#o"+c).on("focusout",function(){
            var otro=$(this).val();
            if (otro!="") {
              var id=$(this).attr('id');
              id=id.substring(1);
              var costo=$("#c"+id).val();
              var valor_trabajo=$("#"+id).val();
              if (valor_trabajo==null) {
                alert("Ingrese tipo de trabajodor para calcular el costo por par");
                $(this).val("")
                $("#"+id).addClass("bg-warning");
                setTimeout(function(){
                  $("#"+id).removeClass("bg-warning");
                },1500)
              }
              else {
                var produccion_promedio={{$produccion_promedio}}
                if(valor_trabajo==1)
                {

                  var beneficio={{$beneficios}}
                  var beneficios=parseFloat(costo)*(parseFloat(beneficio)/100);
                  beneficios=beneficios.toFixed(6)
                  $("#b"+id).val(beneficios)
                  var costo_par=(parseFloat(beneficios)+parseFloat(costo)+parseFloat(otro))/parseFloat(produccion_promedio);
                  costo_par=costo_par.toFixed(6);
                  $("#p"+id).val(costo_par)
                }
                else {
                  if (valor_trabajo==3) {
                    $("#b"+id).val(0);
                    var costo_par=(parseFloat(costo)+parseFloat(otro))/12;
                    costo_par=costo_par.toFixed(6);
                    $("#p"+id).val(costo_par)
                  }
                  else {
                    if (valor_trabajo==2) {
                      $("#b"+id).val(0);
                      var costo_par=(parseFloat(costo)+parseFloat(otro))/(parseFloat(produccion_promedio)/4);
                      costo_par=costo_par.toFixed(6);
                      $("#p"+id).val(costo_par)
                    }
                  }
                }
              }
            }
          });
          $("#c"+c).off("focusout");
          $("#c"+c).on("focusout",function(){
            var costo=$(this).val();
            if (costo!="") {
              var id=$(this).attr('id');
              id=id.substring(1);
              var valor_trabajo=$("#"+id).val();
              if (valor_trabajo=="") {
                alert("Ingrese tipo de trabajodor para calcular el costo por par");
                $(this).val("")
                $("#"+id).addClass("bg-warning");
                setTimeout(function(){
                  $("#"+id).removeClass("bg-warning");
                },1500)
              }
            }
          });
          $("#"+c).off("change");
          $("#"+c).on("change",function(){
            var val=$(this).val();
            var id=$(this).attr('id');
            switch (val) {
              case "1": $("#u"+id).val("Mes");
                break;
              case "2":$("#u"+id).val("Semana");
              break;
              case "3":$("#u"+id).val("Docena");
              break;
              default:
            }
            $("#c"+id).val("");
          });
          $("#d"+c).off("click");
          $("#d"+c).on("click",function(){
            var id=$(this).attr('id');
            id=id.substring(1)
            $("#f"+id).remove();
          });
          break;
          case "a":
          $('#P-Aparado> tbody:last-child').append("<tr id='f"+c+"'>"
          +"<td style='display:none'><input class='form-control' type='text' name='estado[]' value='nuevo'></td>"
          +"<td><textarea class='form-control' rows='3' type='text' name='operacion[]'></textarea></td>"
          +"<td> <select class='form-control' id="+c+" name=tipo[]><option disabled selected>Tipo de Trabajador</option>"+tipo+"</select></td>"
          +"<td><input class='form-control' id='u"+c+"' disabled type='text' disabled></td>"
          +"<td><input class='form-control' type='text' id='c"+c+"' name='costo[]'></td>"
          +"<td><input class='form-control block' type='text' name='beneficio[]' id='b"+c+"' disabled ></td>"
          +"<td><input class='form-control'  type='text' id='o"+c+"' name='otros[]'></td>"
          +"<td><input class='form-control block aparado' disabled type='text' id='p"+c+"' name='costo_par[]' ></td>"
          +"<td><input class='form-control' disabled type='text'  name='porcentaje[]' ></td>"
          +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Aparado'></td>"
          +"<td><a id='d"+c+"' class='btn btn-danger' >-</a></td>"
          +"</tr>");
          $("#o"+c).off("focusout");
          $("#o"+c).on("focusout",function(){
            var otro=$(this).val();
            if (otro!="") {
              var id=$(this).attr('id');
              id=id.substring(1);
              var costo=$("#c"+id).val();
              var valor_trabajo=$("#"+id).val();
              if (valor_trabajo==null) {
                alert("Ingrese tipo de trabajodor para calcular el costo por par");
                $(this).val("")
                $("#"+id).addClass("bg-warning");
                setTimeout(function(){
                  $("#"+id).removeClass("bg-warning");
                },1500)
              }
              else {
                var produccion_promedio={{$produccion_promedio}}
                if(valor_trabajo==1)
                {

                  var beneficio={{$beneficios}}
                  var beneficios=parseFloat(costo)*(parseFloat(beneficio)/100);
                  beneficios=beneficios.toFixed(6)
                  $("#b"+id).val(beneficios)
                  var costo_par=(parseFloat(beneficios)+parseFloat(costo)+parseFloat(otro))/parseFloat(produccion_promedio);
                  costo_par=costo_par.toFixed(6);
                  $("#p"+id).val(costo_par)
                }
                else {
                  if (valor_trabajo==3) {
                    $("#b"+id).val(0);
                    var costo_par=(parseFloat(costo)+parseFloat(otro))/12;
                    costo_par=costo_par.toFixed(6);
                    $("#p"+id).val(costo_par)
                  }
                  else {
                    if (valor_trabajo==2) {
                      $("#b"+id).val(0);
                      var costo_par=(parseFloat(costo)+parseFloat(otro))/(parseFloat(produccion_promedio)/4);
                      costo_par=costo_par.toFixed(6);
                      $("#p"+id).val(costo_par)
                    }
                  }
                }
              }
            }
          });
          $("#c"+c).off("focusout");
          $("#c"+c).on("focusout",function(){
            var costo=$(this).val();
            if (costo!="") {
              var id=$(this).attr('id');
              id=id.substring(1);
              var valor_trabajo=$("#"+id).val();
              if (valor_trabajo==null) {
                alert("Ingrese tipo de trabajodor para calcular el costo por par");
                $(this).val("")
                $("#"+id).addClass("bg-warning");
                setTimeout(function(){
                  $("#"+id).removeClass("bg-warning");
                },1500)
              }
            }
          })
          $("#"+c).off("change");
          $("#"+c).on("change",function(){
            var val=$(this).val();
            var id=$(this).attr('id');
            switch (val) {
              case "1": $("#u"+id).val("Mes");
                break;
              case "2":$("#u"+id).val("Semana");
              break;
              case "3":$("#u"+id).val("Docena");
              break;
              default:
            }
            $("#c"+id).val("");
          });
          $("#d"+c).off("click");
          $("#d"+c).on("click",function(){
            var id=$(this).attr('id');
            id=id.substring(1);
            $("#f"+id).remove();
          });
          break;
          case "c":
          $('#P-Corte> tbody:last-child').append("<tr id='f"+c+"'>"
          +"<td style='display:none'><input class='form-control' type='text' name='estado[]' value='nuevo'></td>"
          +"<td><textarea class='form-control' rows='3' type='text' name='operacion[]'></textarea></td>"
          +"<td> <select class='form-control' id="+c+" name=tipo[]><option disabled selected>Tipo de Trabajador</option>"+tipo+"</select></td>"
          +"<td><input class='form-control' id='u"+c+"' disabled type='text' disabled></td>"
          +"<td><input class='form-control' type='text' id='c"+c+"' name='costo[]'></td>"
          +"<td><input class='form-control block' type='text' name='beneficio[]' id='b"+c+"' disabled ></td>"
          +"<td><input class='form-control'  type='text' id='o"+c+"' name='otros[]'></td>"
          +"<td><input class='form-control block corte' disabled type='text' id='p"+c+"' name='costo_par[]' ></td>"
          +"<td><input class='form-control' disabled type='text'  name='porcentaje[]' ></td>"
          +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Corte'></td>"
          +"<td><a id='d"+c+"' class='btn btn-danger' >-</a></td>"
          +"</tr>");
          $("#o"+c).off("focusout");
          $("#o"+c).on("focusout",function(){
            var otro=$(this).val();
            if (otro!="") {
              var id=$(this).attr('id');
              id=id.substring(1);
              var costo=$("#c"+id).val();
              var valor_trabajo=$("#"+id).val();
              if (valor_trabajo==null) {
                alert("Ingrese tipo de trabajodor para calcular el costo por par");
                $(this).val("")
                $("#"+id).addClass("bg-warning");
                setTimeout(function(){
                  $("#"+id).removeClass("bg-warning");
                },1500)
              }
              else {
                var produccion_promedio={{$produccion_promedio}}
                if(valor_trabajo==1)
                {

                  var beneficio={{$beneficios}}
                  var beneficios=parseFloat(costo)*(parseFloat(beneficio)/100);
                  beneficios=beneficios.toFixed(6)
                  $("#b"+id).val(beneficios)
                  var costo_par=(parseFloat(beneficios)+parseFloat(costo)+parseFloat(otro))/parseFloat(produccion_promedio);
                  costo_par=costo_par.toFixed(6);
                  $("#p"+id).val(costo_par)
                }
                else {
                  if (valor_trabajo==3) {
                    $("#b"+id).val(0);
                    var costo_par=(parseFloat(costo)+parseFloat(otro))/12;
                    costo_par=costo_par.toFixed(6);
                    $("#p"+id).val(costo_par)
                  }
                  else {
                    if (valor_trabajo==2) {
                      $("#b"+id).val(0);
                      var costo_par=(parseFloat(costo)+parseFloat(otro))/(parseFloat(produccion_promedio)/4);
                      costo_par=costo_par.toFixed(6);
                      $("#p"+id).val(costo_par)
                    }
                  }
                }
              }
            }
          });
          $("#c"+c).off("focusout");
          $("#c"+c).on("focusout",function(){
            var costo=$(this).val();
            if (costo!="") {
              var id=$(this).attr('id');
              id=id.substring(1);
              var valor_trabajo=$("#"+id).val();
              if (valor_trabajo==null) {
                alert("Ingrese tipo de trabajodor para calcular el costo por par");
                $(this).val("")
                $("#"+id).addClass("bg-warning");
                setTimeout(function(){
                  $("#"+id).removeClass("bg-warning");
                },1500)
              }
            }
          })
          $("#"+c).off("change");
          $("#"+c).on("change",function(){
            var val=$(this).val();
            var id=$(this).attr('id');
            switch (val) {
              case "1": $("#u"+id).val("Mes");
                break;
              case "2":$("#u"+id).val("Semana");
              break;
              case "3":$("#u"+id).val("Docena");
              break;
              default:
            }
            $("#c"+id).val("");
          });
          $("#d"+c).off("click");
          $("#d"+c).on("click",function(){
            var id=$(this).attr('id');
            id=id.substring(1);
            $("#f"+id).remove();
          });
          break;
          case "h":
          $('#P-Habilitado> tbody:last-child').append("<tr id='f"+c+"'>"
          +"<td style='display:none'><input class='form-control' type='text' name='estado[]' value='nuevo'></td>"
          +"<td><textarea class='form-control' rows='3' type='text' name='operacion[]'></textarea></td>"
          +"<td> <select class='form-control' id="+c+" name=tipo[]><option disabled selected>Tipo de Trabajador</option>"+tipo+"</select></td>"
          +"<td><input class='form-control' id='u"+c+"' disabled type='text' disabled></td>"
          +"<td><input class='form-control' type='text' id='c"+c+"' name='costo[]'></td>"
          +"<td><input class='form-control block' type='text' name='beneficio[]' id='b"+c+"' disabled ></td>"
          +"<td><input class='form-control'  type='text' id='o"+c+"' name='otros[]'></td>"
          +"<td><input class='form-control block habilitado' disabled type='text' id='p"+c+"' name='costo_par[]' ></td>"
          +"<td><input class='form-control' disabled type='text'  name='porcentaje[]' ></td>"
          +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Habilitado'></td>"
          +"<td><a id='d"+c+"' class='btn btn-danger' >-</a></td>"
          +"</tr>");
          $("#o"+c).off("focusout");
          $("#o"+c).on("focusout",function(){
            var otro=$(this).val();
            if (otro!="") {
              var id=$(this).attr('id');
              id=id.substring(1);
              var costo=$("#c"+id).val();
              var valor_trabajo=$("#"+id).val();
              if (valor_trabajo==null) {
                alert("Ingrese tipo de trabajodor para calcular el costo por par");
                $(this).val("")
                $("#"+id).addClass("bg-warning");
                setTimeout(function(){
                  $("#"+id).removeClass("bg-warning");
                },1500)
              }
              else {
                var produccion_promedio={{$produccion_promedio}}
                if(valor_trabajo==1)
                {

                  var beneficio={{$beneficios}}
                  var beneficios=parseFloat(costo)*(parseFloat(beneficio)/100);
                  beneficios=beneficios.toFixed(6)
                  $("#b"+id).val(beneficios)
                  var costo_par=(parseFloat(beneficios)+parseFloat(costo)+parseFloat(otro))/parseFloat(produccion_promedio);
                  costo_par=costo_par.toFixed(6);
                  $("#p"+id).val(costo_par)
                }
                else {
                  if (valor_trabajo==3) {
                    $("#b"+id).val(0);
                    var costo_par=(parseFloat(costo)+parseFloat(otro))/12;
                    costo_par=costo_par.toFixed(6);
                    $("#p"+id).val(costo_par)
                  }
                  else {
                    if (valor_trabajo==2) {
                      $("#b"+id).val(0);
                      var costo_par=(parseFloat(costo)+parseFloat(otro))/(parseFloat(produccion_promedio)/4);
                      costo_par=costo_par.toFixed(6);
                      $("#p"+id).val(costo_par)
                    }
                  }
                }
              }
            }
          });
          $("#c"+c).off("focusout");
          $("#c"+c).on("focusout",function(){
            var costo=$(this).val();
            if (costo!="") {
              var id=$(this).attr('id');
              id=id.substring(1);
              var valor_trabajo=$("#"+id).val();
              if (valor_trabajo==null) {
                alert("Ingrese tipo de trabajodor para calcular el costo por par");
                $(this).val("")
                $("#"+id).addClass("bg-warning");
                setTimeout(function(){
                  $("#"+id).removeClass("bg-warning");
                },1500)
              }
            }
          })
          $("#"+c).off("change");
          $("#"+c).on("change",function(){
            var val=$(this).val();
            var id=$(this).attr('id');
            switch (val) {
              case "1": $("#u"+id).val("Mes");
                break;
              case "2":$("#u"+id).val("Semana");
              break;
              case "3":$("#u"+id).val("Docena");
              break;
              default:
            }
            $("#c"+id).val("");
          });
          $("#d"+c).off("click");
          $("#d"+c).on("click",function(){
            var id=$(this).attr('id');
            id=id.substring(1);
            $("#f"+id).remove();
          });
          break;
          case "al":
          $('#P-Alistado> tbody:last-child').append("<tr id='f"+c+"'>"
          +"<td style='display:none'><input class='form-control' type='text' name='estado[]' value='nuevo'></td>"
          +"<td><textarea class='form-control' rows='3' type='text' name='operacion[]'></textarea></td>"
          +"<td> <select class='form-control' id="+c+" name=tipo[]><option disabled selected>Tipo de Trabajador</option>"+tipo+"</select></td>"
          +"<td><input class='form-control' id='u"+c+"' disabled type='text' disabled></td>"
          +"<td><input class='form-control' type='text' id='c"+c+"' name='costo[]'></td>"
          +"<td><input class='form-control block' type='text' name='beneficio[]' id='b"+c+"' disabled ></td>"
          +"<td><input class='form-control'  type='text' id='o"+c+"' name='otros[]'></td>"
          +"<td><input class='form-control block alistado' disabled type='text' id='p"+c+"' name='costo_par[]' ></td>"
          +"<td><input class='form-control' disabled type='text'  name='porcentaje[]' ></td>"
          +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Alistado'></td>"
          +"<td><a id='d"+c+"' class='btn btn-danger' >-</a></td>"
          +"</tr>");
          $("#o"+c).off("focusout");
          $("#o"+c).on("focusout",function(){
            var otro=$(this).val();
            if (otro!="") {
              var id=$(this).attr('id');
              id=id.substring(1);
              var costo=$("#c"+id).val();
              var valor_trabajo=$("#"+id).val();
              if (valor_trabajo==null) {
                alert("Ingrese tipo de trabajodor para calcular el costo por par");
                $(this).val("")
                $("#"+id).addClass("bg-warning");
                setTimeout(function(){
                  $("#"+id).removeClass("bg-warning");
                },1500)
              }
              else {
                var produccion_promedio={{$produccion_promedio}}
                if(valor_trabajo==1)
                {

                  var beneficio={{$beneficios}}
                  var beneficios=parseFloat(costo)*(parseFloat(beneficio)/100);
                  beneficios=beneficios.toFixed(6)
                  $("#b"+id).val(beneficios)
                  var costo_par=(parseFloat(beneficios)+parseFloat(costo)+parseFloat(otro))/parseFloat(produccion_promedio);
                  costo_par=costo_par.toFixed(6);
                  $("#p"+id).val(costo_par)
                }
                else {
                  if (valor_trabajo==3) {
                    $("#b"+id).val(0);
                    var costo_par=(parseFloat(costo)+parseFloat(otro))/12;
                    costo_par=costo_par.toFixed(6);
                    $("#p"+id).val(costo_par)
                  }
                  else {
                    if (valor_trabajo==2) {
                      $("#b"+id).val(0);
                      var costo_par=(parseFloat(costo)+parseFloat(otro))/(parseFloat(produccion_promedio)/4);
                      costo_par=costo_par.toFixed(6);
                      $("#p"+id).val(costo_par)
                    }
                  }
                }
              }
            }
          });
          $("#c"+c).off("focusout");
          $("#c"+c).on("focusout",function(){
            var costo=$(this).val();
            if (costo!="") {
              var id=$(this).attr('id');
              id=id.substring(1);
              var valor_trabajo=$("#"+id).val();
              if (valor_trabajo==null) {
                alert("Ingrese tipo de trabajodor para calcular el costo por par");
                $(this).val("")
                $("#"+id).addClass("bg-warning");
                setTimeout(function(){
                  $("#"+id).removeClass("bg-warning");
                },1500)
              }
            }
          })
          $("#"+c).off("change");
          $("#"+c).on("change",function(){
            var val=$(this).val();
            var id=$(this).attr('id');
            switch (val) {
              case "1": $("#u"+id).val("Mes");
                break;
              case "2":$("#u"+id).val("Semana");
              break;
              case "3":$("#u"+id).val("Docena");
              break;
              default:
            }
            $("#c"+id).val("");
          });
          $("#d"+c).off("click");
          $("#d"+c).on("click",function(){
            var id=$(this).attr('id');
            id=id.substring(1);
            $("#f"+id).remove();
          });
          break;
          case "m":
          $('#P-Montaje> tbody:last-child').append("<tr id='f"+c+"'>"
          +"<td style='display:none'><input class='form-control' type='text' name='estado[]' value='nuevo'></td>"
          +"<td><textarea class='form-control' rows='3' type='text' name='operacion[]'></textarea></td>"
          +"<td> <select class='form-control' id="+c+" name=tipo[]><option disabled selected>Tipo de Trabajador</option>"+tipo+"</select></td>"
          +"<td><input class='form-control' id='u"+c+"' disabled type='text' disabled></td>"
          +"<td><input class='form-control' type='text' id='c"+c+"' name='costo[]'></td>"
          +"<td><input class='form-control block' type='text' name='beneficio[]' id='b"+c+"' disabled ></td>"
          +"<td><input class='form-control'  type='text' id='o"+c+"' name='otros[]'></td>"
          +"<td><input class='form-control block montaje' disabled type='text' id='p"+c+"' name='costo_par[]' ></td>"
          +"<td><input class='form-control' disabled type='text'  name='porcentaje[]' ></td>"
          +"<td style='display:none'><input class='form-control' type='text' name='area[]' value='P-Montaje'></td>"
          +"<td><a id='d"+c+"' class='btn btn-danger' >-</a></td>"
          +"</tr>");
          $("#o"+c).off("focusout");
          $("#o"+c).on("focusout",function(){
            var otro=$(this).val();
            if (otro!="") {
              var id=$(this).attr('id');
              id=id.substring(1);
              var costo=$("#c"+id).val();
              var valor_trabajo=$("#"+id).val();
              if (valor_trabajo==null) {
                alert("Ingrese tipo de trabajodor para calcular el costo por par");
                $(this).val("")
                $("#"+id).addClass("bg-warning");
                setTimeout(function(){
                  $("#"+id).removeClass("bg-warning");
                },1500)
              }
              else {
                var produccion_promedio={{$produccion_promedio}}
                if(valor_trabajo==1)
                {

                  var beneficio={{$beneficios}}
                  var beneficios=parseFloat(costo)*(parseFloat(beneficio)/100);
                  beneficios=beneficios.toFixed(6)
                  $("#b"+id).val(beneficios)
                  var costo_par=(parseFloat(beneficios)+parseFloat(costo)+parseFloat(otro))/parseFloat(produccion_promedio);
                  costo_par=costo_par.toFixed(6);
                  $("#p"+id).val(costo_par)
                }
                else {
                  if (valor_trabajo==3) {
                    $("#b"+id).val(0);
                    var costo_par=(parseFloat(costo)+parseFloat(otro))/12;
                    costo_par=costo_par.toFixed(6);
                    $("#p"+id).val(costo_par)
                  }
                  else {
                    if (valor_trabajo==2) {
                      $("#b"+id).val(0);
                      var costo_par=(parseFloat(costo)+parseFloat(otro))/(parseFloat(produccion_promedio)/4);
                      costo_par=costo_par.toFixed(6);
                      $("#p"+id).val(costo_par)
                    }
                  }
                }
              }
            }
          });
          $("#c"+c).off("focusout");
          $("#c"+c).on("focusout",function(){
            var costo=$(this).val();
            if (costo!="") {
              var id=$(this).attr('id');
              id=id.substring(1);
              var valor_trabajo=$("#"+id).val();
              if (valor_trabajo==null) {
                alert("Ingrese tipo de trabajodor para calcular el costo por par");
                $(this).val("")
                $("#"+id).addClass("bg-warning");
                setTimeout(function(){
                  $("#"+id).removeClass("bg-warning");
                },1500)
              }
            }
          })
          $("#"+c).off("change");
          $("#"+c).on("change",function(){
            var val=$(this).val();
            var id=$(this).attr('id');
            switch (val) {
              case "1": $("#u"+id).val("Mes");
                break;
              case "2":$("#u"+id).val("Semana");
              break;
              case "3":$("#u"+id).val("Docena");
              break;
              default:
            }
            $("#c"+id).val("");
          });
          $("#d"+c).off("click");
          $("#d"+c).on("click",function(){
            var id=$(this).attr('id');
            id=id.substring(1);
            $("#f"+id).remove();
          });
        }
        c++;
    }
}
  $("#sub").click(function(event){
    $("#btn_total").click();
    $(".block").prop('disabled',false);
  });
  $("#btn_total").click(function(){
      var totales=$("input[class~='acabado']");
      var cortes=$("input[class~='corte']");
      var habilitado=$("input[class~='habilitado']");
      var aparado=$("input[class~='aparado']");
      var alistado=$("input[class~='alistado']");
      var montaje=$("input[class~='montaje']");
      var total=0.00,totalcorte=0.00,totalhabilitado=0.00,totalaparado=0.00,totalalistado=0.00,totalmontaje=0.00;
      for (var i = 0; i < cortes.length; i++) {
        var valor=cortes[i].value;
        totalcorte=parseFloat(valor)+parseFloat(totalcorte);
      }
      for (var i = 0; i < habilitado.length; i++) {
        var valor=habilitado[i].value;
        totalhabilitado=parseFloat(valor)+parseFloat(totalhabilitado);
      }
      for (var i = 0; i < aparado.length; i++) {
        var valor=aparado[i].value;
        totalaparado=parseFloat(valor)+parseFloat(totalaparado);
      }
      for (var i = 0; i < alistado.length; i++) {
        var valor=alistado[i].value;
        totalalistado=parseFloat(valor)+parseFloat(totalalistado);
      }
      for (var i = 0; i < montaje.length; i++) {
        var valor=montaje[i].value;
        totalmontaje=parseFloat(valor)+parseFloat(totalmontaje);
      }
      for(var i=0;i<totales.length;i++)
       {
         var valor=totales[i].value;
         total=parseFloat(valor)+parseFloat(total);
      }

      $("#corte_txt").text("Total: S/."+totalcorte.toFixed(6))
      $("#habilitado_txt").text("Total: S/."+totalhabilitado.toFixed(6))
      $("#aparado_txt").text("Total: S/."+totalaparado.toFixed(6))
      $("#alistado_txt").text("Total: S/."+totalalistado.toFixed(6))
      $("#montaje_txt").text("Total: S/."+totalmontaje.toFixed(6))
      $("#acabado_txt").text("Total: S/."+total.toFixed(6))
      var t=totalcorte+totalhabilitado+totalaparado+totalalistado+totalmontaje+total;
      $("#material_total").text("Costo Total de Materiales S/. "+t.toFixed(6))
      $("#mt").val(t)
  });
  @if(count($detalle)>0)
    $("#btn_total").click();
  @endif
});
</script>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>
@endpush
@endsection
