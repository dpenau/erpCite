@extends ('layouts.admin')
@section ('contenido')
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2 class="font-weight-bold">Nuevo Articulo de Calzado</h2>
                      <div class="clearfix"></div>
                    </div>
          @if (count($errors)>0)
          <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
            </ul>
          </div>
          @endif

          {!!Form::open(array('url'=>'costo/directo/reg_articulo/create','files'=>true,'method'=>'POST','autocomplete'=>'off'))!!}
                {{Form::token()}}
                @foreach($modelo as $model)
                 <input type="text" style="display:none" value="{{$var}}" name="ur" required="required" >
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script >
                  $(document).ready(function(){
                  var a="{{$model->nombre_serie}}"
                  var modelo="{{$model->codigo_modelo}}"
                  var cantidad="{{$cantidad+1}}"
                  a=a.split(' ');
                  iniciales="";
                  for (var i = 0; i < a.length; i++) {
                    iniciales+=a[i][0].toUpperCase();
                    iniciales+=a[i][1];
                  }
                  $("#c_modelo").val(modelo+"-"+iniciales+"-"+cantidad);
                  $("#codigo_modelo").val(modelo+"-"+iniciales+"-"+cantidad);
                  });
                </script>
                <div class="row">
                  <div class="form-group  col-md-4  col-xs-12">
                    <div class="">
                      <label>Coleccion:</label>
                       <select name="coleccion" required='required' class="custom-select">
                        <option value="" disabled>--- Seleccionar Coleccion ---</option>
                         <option value="{{$model->cod_coleccion}}" selected>{{$model->nombre_coleccion}}</option>
                         </select>
                      </div>
                      <div class="">
                        <label>Coleccion:</label>
                        <select name="coleccion" required='required' class="custom-select">
                          <option value="" disabled>--- Seleccionar Coleccion ---</option>
                           <option value="{{$model->cod_coleccion}}" selected>{{$model->nombre_coleccion}}</option>
                           </select>
                      </div>
                      <div class="">
                        <label>Linea:</label>
                         <select name="linea" required='required' class="custom-select">
                          <option value="" disabled>--- Seleccionar Linea ---</option>
                           <option value="{{$model->cod_linea}}" selected>{{$model->nombre_linea}}</option>
                           </select>
                        </div>
                        <div class="">
                            <label for="codigo_calzado">Codigo de Modelo</label>
                             <input type="text" id="c_modelo" value=""  class="form-control" disabled>
                             <input type="text" style="display:none" id="codigo_modelo" name="codigo_modelo" required="required"  class="form-control ">
                        </div>
                      <div class="">
                            <label for="nombre_modelo">Nombre de Modelo</label>
                             <input type="text" value="{{$model->nombre}}" disabled required="required" maxlength="50" class="form-control ">
                             <input type="text" style="display:none" id="nombre_modelo" name="nombre_modelo" value="{{$model->nombre}}" required="required" maxlength="50" class="form-control ">
                        </div>
                      <div class="">
                            <label for="descripcion_modelo">Descripcion de Modelo</label>
                             <textarea type="text" id="descripcion_modelo" name="descripcion_modelo" required="required" maxlength="200" class="form-control "></textarea>
                        </div>
                        <div class="">
                            <label>Serie:</label>
                              <input type="text"  class="form-control" disabled value="{{$model->nombre_serie}}">
                              <input type="text" style="display:none" class="form-control" name="serie" value="{{$model->cod_serie}}">
                        </div>
                  </div>
                  <div class="form-group  col-md-4 offset-md-2 col-xs-12">
                    <div class="">
                      <label for="">Horma:</label>
                      <select  class="custom-select">
                       <option value="" disabled>--- Seleccionar Horma ---</option>
                        <option value="{{$model->cod_horma}}" selected disabled>{{substr($model->descrip_material,0,strpos($model->descrip_material,'-'))}}</option>
                        </select>
                        <input type="text" style="display:none"  name="horma" value="{{$model->cod_horma}}">
                      </div>
                      <div class="">
                        <label for="">Capellada:</label>
                         <select name="capellada" required='required' class="custom-select">
                          <option value="" selected disabled>--- Seleccionar Capellada ---</option>
                          @foreach ($capellada as $linea)
                           <option value="{{$linea->cod_capellada}}" >{{$linea->descrip_capellada}}</option>
                           @endforeach
                           </select>
                        </div>
                        <div class="">
                          <label for="">Forro:</label>
                           <select name="forro" required='required' class="custom-select">
                            <option value="" selected disabled>--- Seleccionar Forro ---</option>
                            @foreach ($capellada as $linea)
                             <option value="{{$linea->cod_capellada}}" >{{$linea->descrip_capellada}}</option>
                             @endforeach
                             </select>
                          </div>
                          <div class="">
                            <label for="">Plantilla:</label>
                             <select name="plantilla" required='required' class="custom-select">
                              <option value="" selected disabled>--- Seleccionar Plantilla ---</option>
                              @foreach ($capellada as $linea)
                               <option value="{{$linea->cod_capellada}}" >{{$linea->descrip_capellada}}</option>
                               @endforeach
                               </select>
                            </div>
                            <div class="">
                              <label for="">Piso:</label>
                               <select name="piso" required='required' class="custom-select">
                                <option value="" selected disabled>--- Seleccionar Piso ---</option>
                                @foreach ($capellada as $linea)
                                 <option value="{{$linea->cod_capellada}}" >{{$linea->descrip_capellada}}</option>
                                 @endforeach
                                 </select>
                            </div>
                            <div class="">
                              <label for="exampleFormControlFile1">Imagen de modelo:</label>
                              <input type="file" class="form-control-file"  name="photo" required>
                            </div>
                  </div>
                </div>
                          <div class="ln_solid"></div>
                      <div class="form-group">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                              <button type="submit" class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5">Guardar</button>
                            </div>
                          </div>
              @endforeach
          {!!Form::close()!!}
        </div>
      </div>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>
@endpush
@endsection
