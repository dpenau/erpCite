
<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-edit-{{$op->cod_operacion_d}}">
    {{Form::Open(array('action'=>array('OperacionDirectaController@update',$op->cod_operacion_d),'files'=>true,'method'=>'patch'))}}
    <div class="modal-dialog"  style="max-width: 50%;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titulo_mod">Modificar Operacion Directa: </h4>
                <input type="hidden" id="{{$op->cod_operacion_d}}" value="{{$op->cod_operacion_d}}" name="codigo_operacion">
            </div>
            <div class="modal-body">
                <div class="x_content">

                    <div class="row">
                        <div class="form-group  col-md-6"  style="text-align: left ; margin:2% 0 2% 0;">
                            <label for="">Proceso:</label>
                            <select id="proceso" required="required" name="proceso" class="custom-select">
                                <option value="" selected disabled>Seleccionar Proceso</option>
                                @foreach ($proceso as $proc)

                                    @if($proc->cod_proceso==$op->cod_proceso)
                                    <option value="{{$proc->cod_proceso}}" selected>
                                        {{ $proc->codigo }} {{$proc->nombre}}
                                    </option>
                                    @else
                                    <option value="{{$proc->cod_proceso}}">
                                        {{ $proc->codigo }} {{$proc->nombre}}
                                    </option>
                                    @endif
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group  col-md-6" style="text-align: left ; margin:2% 0 2% 0;">
                            <label for="total_orden_compra">Operación:</label>
                            <input type="text" id="operacion" value="{{$op->operacion_nombre}}" name="operacion" required="required" maxlength="100"
                                class="form-control ">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group  col-md-6" style="text-align: left; margin:2% 0 2% 0;">
                            <label for="">Tipo de Operación:</label>
                            <select id="tipo_operacio{{$op->cod_operacion_d}}" name="tipo_operacion" class="custom-select">
                                <option value="" selected disabled>Seleccionar Tipo Operacion</option>
                                @foreach ($tipo as $tip)

                                    @if($tip->cod_pago==$op->cod_tipo_pago)
                                    <option value="{{$tip->cod_pago}}" selected>
                                       {{$tip->nombre_pago}}
                                    </option>
                                    @else
                                    <option value="{{$tip->cod_pago}}">
                                       {{$tip->nombre_pago}}
                                    </option>
                                    @endif
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group  col-md-6" style="text-align: left ; margin:2% 0 2% 0;">
                            <label for="total_orden_compra">Unidad de Medida:</label>
                            <input type="text" id="unidad_medida" value="{{ $op->unidad}}" name="unidad_medida" maxlength="100" class="form-control "
                                readonly>
                        </div>
                    </div>
                    <div class="row" style="text-align: left; margin:2% 0 2% 0;">
                        <div class="form-group  col-md-4">
                            <label for="total_orden_compra">Costo:</label>
                            S/.<input type="number" value="{{$op->costo}}" step="0.0001" id="costos{{$op->cod_operacion_d}}" name="costo" required="required" maxlength="100"
                                class="form-control ">
                        </div>
                        <div class="form-group  col-md-4">
                            <label for="total_orden_compra">Beneficio:</label>
                            S/.<input type="text"  value="{{$op->beneficio}}" step="0.0001" id="beneficio{{$op->cod_operacion_d}}" name="beneficio" maxlength="100"
                                class="form-control " readonly>
                        </div>
                        <div class="form-group  col-md-4">
                            <label for="total_orden_compra">Otros Costos Mensuales:</label>
                            S/.<input type="number" value="{{$op->otros_costos}}" step="0.0001" id="otros_costos{{$op->cod_operacion_d}}" name="otros_costos" required="required"
                                maxlength="100" class="form-control ">
                        </div>
                    </div>
                    <div class="row" style="text-align: left; margin:2% 0 2% 0;">
                        <div class="form-group  col-md-8">
                            <label for="total_orden_compra">Costo por par:</label>
                            S/. <input type="text"  value="{{$op->costo_par}}" step="0.0001" id="costo_par{{$op->cod_operacion_d}}" name="costo_par" class="form-control "
                                readonly>
                        </div>

                    </div>

                <div class="modal-footer">
                    <button type="submit" class="bttn-unite bttn-md bttn-primary">Confirmar</button>
                    <button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    {{Form::Close()}}

</div>

