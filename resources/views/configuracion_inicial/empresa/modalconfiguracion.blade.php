@extends ('layouts.admin')
@section('contenido')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="font-weight-bold">Datos de la Empresa</h2>
                    <div class="clearfix"></div>
                </div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{ url('configuracion/empresa/update') }}" enctype="multipart/form-data">
                    <!-- CSRF Token -->
                    @csrf
                    <div class="x_content">

                        <div class="row ">
                            <div class="col-md-6">
                                <input type="text" id="ruc" required="required" name="nombre_empresa_mod"
                                    class="form-control col-md-12" placeholder="Nombre de Empresa*"
                                    value="{{ $empresa->nom_empresa }}">
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> RUC: </p>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <input readonly="readonly" type="text" maxlength="11" id="ruc" name="ruc_empresa_mod"
                                    required="required" class="form-control" placeholder="RUC*"
                                    value="{{ $empresa->RUC_empresa }}">

                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Razón Social: </p>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" id="razon_social" name="razon_social_empresa_mod" required
                                    class="form-control" placeholder="Razon Social *"
                                    value="{{ $empresa->razon_social }}">
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Tipo de Contribuyente: </p>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <select name="tipo_contribuyente" class="custom-select">
                                    <option value="{{ $empresa->cod_tipo_contribuyente }}" selected>
                                        {{ $empresa->descrip_tipo_contribuyente }}</option>
                                    @foreach ($tipo_contribuyente as $tipo)
                                        @if ($tipo->cod_tipo_contribuyente != $empresa->cod_tipo_contribuyente)
                                            <option value="{{ $tipo->cod_tipo_contribuyente }}">
                                                {{ $tipo->descrip_tipo_contribuyente }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Nombre Comercial: </p>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" id="nombre_comercial" name="nombre_comercial_mod" required="required"
                                    class="form-control" placeholder="Nombre Comercial*"
                                    value="{{ $empresa->nombre_comercial }}">
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Domicilio: </p>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" id="domicilio" name="domicilio_mod" required="required"
                                    class="form-control " placeholder="Domicilio*" value="{{ $empresa->domicilio }}">
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Correo: </p>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <input type="email" id="correo" name="correo_mod" class="form-control"
                                    placeholder="Correo" value="{{ $empresa->correo }}">
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Celular: </p>
                                </label>
                            </div>
                            <div class=" input-group col-md-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">+51</span>
                                </div>
                                <input type="text" maxlength="9" onkeypress="return isNumberKey(event)" id="telefono"
                                    name="telefono_mod" class="form-control" placeholder="Telefono"
                                    value="{{ $empresa->telefono }}">
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Pagina Web: </p>
                                </label>
                            </div>

                            <div class="col-md-4">
                                <input type="text" id="pagina_web" name="pagina_web_mod" class="form-control"
                                    placeholder="Pagina Web" value="{{ $empresa->pagina_web }}">
                            </div>
                        </div>

                        <div class=" row mt-2">
                            <div class="col-md-3">
                                <label for="exampleFormControlFile1">Logo de la empresa:</label>
                                <input type="File" class="form-control-file" id="foto" name="foto" onchange="vista_preliminar(event)">

                            </div>
                            <div class="col-md-3">
                                <img src="" alt="" id="img-foto" width="110" height="110" style="margin-left:6%;" />

                            </div>
                        </div>


                        <hr class="my-3">

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Tipo de Renta: </p>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <select id="regimen_renta_mod" name="regimen_renta" class="custom-select" onchange="setRegimenRenta(event)">
                                    <option value="" disabled>Regimen de Renta</option>
                                    <option value="{{ $empresa->cod_regimen_renta }}" selected>
                                        {{ $empresa->descrip_regimen_renta }}</option>
                                    @foreach ($regimen_renta as $regi)
                                        @if ($regi->cod_regimen_renta != $empresa->cod_regimen_renta)
                                            <option value="{{ $regi->cod_regimen_renta }}">
                                                {{ $regi->descrip_regimen_renta }}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-1">
                                <label>
                                    <p> % de Imp. a la Renta </p>
                                </label>
                            </div>
                            <div class="col-md-2">
                                <input type="test" id="porcentaje_renta" name="porcentaje_renta_mod" class="form-control"
                                    placeholder="Porcentaje Impuesto a la Renta" value="" disabled>
                            </div>
                            <div class="col-md-1">
                                <label>
                                    <p> Afecto a: </p>
                                </label>
                            </div>
                            <div class="col-md-2">
                                <input type="test" id="utilidades" name="utilidades_mod" class="form-control"
                                    placeholder="Afecta a" value="" disabled>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Reparticion Utilidades: </p>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <select name="repaticion_utilidades" class="custom-select">
                                    <option value="" disabled selected>Opciones</option>
                                    <option value="1" {{$empresa->reparticion_utilidades == 1 ? "selected" : ""}}>Si</option>
                                    <option value="0" {{$empresa->reparticion_utilidades == 0 ? "selected" : ""}}>No</option>
                                </select>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Regimen Laboral: </p>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <select id="regimen_laboral_mod" name="regimen_laboral" class="custom-select" onchange="setRegimenLaboral(event)">
                                    <option value="" disabled>Regimen Laboral</option>
                                    <option value="{{ $empresa->cod_regimen_laboral }}" selected>
                                        {{ $empresa->descrip_regimen_laboral }}</option>
                                    @foreach ($regimen_laboral as $reg)
                                        @if ($reg->cod_regimen_laboral != $empresa->cod_regimen_laboral)
                                            <option value="{{ $reg->cod_regimen_laboral }}">
                                                {{ $reg->descrip_regimen_laboral }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> % Beneficio: </p>
                                </label>
                            </div>
                            <div class="col-md-2">
                                <input type="text" id="porcentaje_beneficio" name="porcentaje_beneficio_mod"
                                    class="form-control" placeholder="Porcentaje Beneficio" value="" disabled>
                            </div>
                        </div>

                        <hr class="my-3">

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Prod. Mensual Prom: </p>
                                </label>
                            </div>
                            <div class="input-group col-md-4">
                                <input type="text" id="produccion_promedio" name="produccion_promedio"
                                    class="form-control" placeholder="Porcentaje Beneficio" value="{{isset($politica_desarrollo->produccion_promedio) ? $politica_desarrollo->produccion_promedio : ''}}" >
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">pares</span>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Pol. Des. de Producto: </p>
                                </label>
                            </div>
                            <div class="input-group col-md-4">
                                <input type="text" id="producto" name="producto"
                                    class="form-control" placeholder="Porcentaje Beneficio" value="{{isset($politica_desarrollo->producto) ? $politica_desarrollo->producto : ''}}" >
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">pares</span>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Pol. Des. de Hormas: </p>
                                </label>
                            </div>
                            <div class="input-group col-md-4">
                                <input type="text" id="hormas" name="hormas"
                                    class="form-control" placeholder="Porcentaje Beneficio" value="{{isset($politica_desarrollo->hormas) ? $politica_desarrollo->hormas : ''}}" >
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">pares</span>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2">
                                <label>
                                    <p> Pol. Des. de Troqueles: </p>
                                </label>
                            </div>
                            <div class="input-group col-md-4">
                                <input type="text" id="troqueles" name="troqueles"
                                    class="form-control" placeholder="Porcentaje Beneficio" value="{{isset($politica_desarrollo->troqueles) ? $politica_desarrollo->troqueles : ''}}" >
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">pares</span>
                                </div>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <hr class="my-3">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-6 col-xs-12">
                                <input type="submit" name="btn" value="Guardar" id="submitBtn" data-toggle="modal"
                                    data-target="#confirm-submits"
                                    class="bttn-unite bttn-md bttn-success col-md-2 col-md-offset-5" />
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog"
                        aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    Confirmar Actualizacion
                                </div>
                                <div class="modal-body">
                                    ¿Desea guardar los cambios realizados?

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="bttn-unite bttn-md bttn-danger"
                                        data-dismiss="modal">Cancelar</button>
                                    <a href="#" id="submit" class="bttn-unite bttn-md bttn-primary">Aceptar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script type="text/javascript">
            var valor_renta = $('#regimen_renta_mod').val();
            var valor_laboral = $('#regimen_laboral_mod').val();
            const empresa = @json($empresa);
            console.log(empresa);
            const tipo_contribuyente = @json($tipo_contribuyente);
            const tipo_renta = @json($regimen_renta);
            const regimen_laboral = @json($regimen_laboral);
            const asd = "asdf";
            $(document).ready(function() {
                tipo_renta.map((renta) => {
                    if(valor_renta == renta.cod_regimen_renta){
                        $("#porcentaje_renta").val(renta.porcentaje);
                        $("#utilidades").val(renta.afecta_a)
                    }
                })
                regimen_laboral.map((laboral) => {
                    if(valor_laboral == laboral.cod_regimen_laboral){
                        $("#porcentaje_beneficio").val(laboral.porcentaje_beneficio)
                    }
                })
            })
            let vista_preliminar = (event) => {
                let leer_img = new FileReader();
                let id_img = document.getElementById('img-foto');

                leer_img.onload = () => {
                    if (leer_img.readyState == 2) {
                        id_img.src = leer_img.result
                    }
                }
                leer_img.readAsDataURL(event.target.files[0])
            }

            let setRegimenRenta = (event) => {
                tipo_renta.map((renta) => {
                    if(event.target.value == renta.cod_regimen_renta){
                        $("#porcentaje_renta").val(renta.porcentaje);
                        $("#utilidades").val(renta.afecta_a)
                    }
                })
            }
            let setRegimenLaboral = (event) => {
                regimen_laboral.map((laboral) => {
                    if(event.target.value == laboral.cod_regimen_laboral){
                        $("#porcentaje_beneficio").val(laboral.porcentaje_beneficio)
                    }
                })
            }
        </script>
    </div>
@endsection
