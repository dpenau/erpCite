@extends('layouts.app')
@section ('content')
<div class="container">


    <div>
    <h3 class="font-weight-bold">Listado de Contribuyentes <a href="Contribuyente/create">
      <button class="bttn-unite bttn-md bttn-success ">Nuevo Contribuyente</button></a></h3>
	</div>
              <div class="x_content table-responsive">
                <table id="example" class="display">
                  <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Editar</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                  	@foreach($contribuyentes as $contr)
                    @if($contr->estado_tipo_contribuyente==0)
                    <tr class="bg-danger">
                    @endif
                    	<td>{{$contr->descrip_tipo_contribuyente}}</td>
                      <td>
                         <a href="" data-target="#modal-edit-{{$contr->cod_tipo_contribuyente}}" data-toggle="modal">
                          <button class="bttn-unite bttn-md bttn-warning"><i class="far fa-edit"></i></button></a>
                      </td>
                      <td>
                        @if($contr->estado_tipo_contribuyente==1)
                         <a href="" data-target="#modal-delete-{{$contr->cod_tipo_contribuyente}}" data-toggle="modal">
                          <button class="bttn-unite bttn-md bttn-danger"><i class="far fa-trash-alt"></i></button></a>
                        @else
                        <a href="" data-target="#modal-active-{{$contr->cod_tipo_contribuyente}}" data-toggle="modal">
                         <button class="bttn-unite bttn-md bttn-success"><i class="far fa-check-circle"></i></button></a>
                        @endif
                      </td>
                   	</tr>
                    @include('Mantenimiento.Contribuyente.modalactivar')
                    @include('Mantenimiento.Contribuyente.modaleditar')
                    @include('Mantenimiento.Contribuyente.modaleliminar')
                    @endforeach
                  </tbody>
                </table>
              </div>
              </div>
@endsection
