@extends ('layouts.app')
@section ('content')
<div class="preloader">

</div>
<div class="container" align="center">
  <div>
    <h3 class="font-weight-bold">Indicador de operaciones en el año</h3>
  </div>
  <br>
  <select id="empresa">
    <option value="" disabled selected>Seleccione una empresa</option>
    <option value="t">Totas las empresas</option>
    @foreach($empresa as $emp)
    <option value="{{$emp->RUC_empresa}}">{{$emp->razon_social}}</option>
    @endforeach
  </select>
    <div>
        <h4>Recursos Humanos</h4>
        <div id="recursos_caja" style="width:50%">
            <canvas id="recursos_grafico"></canvas>
        </div>
    </div>
    <div>
        <h4>Logistica</h4>
        <div id="logistica_caja" style="width:50%">
            <canvas id="logistica_grafico"></canvas>
        </div>
    </div>
    <div>
        <h4>Desarrollo de Producto</h4>
        <div id="desarrollo_caja" style="width:50%">
            <canvas id="desarrollo_grafico"></canvas>
        </div>
    </div>
    <div>
        <h4>Estructura de costos</h4>
        <h5>SIN IMPLEMENTARSE</h5>
    </div>
    <div>
        <h4>Produccion</h4>
        <h5>SIN IMPLEMENTARSE</h5>
    </div>
  <div class="x_content table-responsive">
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>

<script type="text/javascript">
$(document).ready( function () {
    $("#empresa").change(function (){
        let ruc=$("#empresa").val();
        obtener_datos(ruc)
    })
    function obtener_datos(ruc)
    {
        $.ajax({
            url:"/Admin/indicador/obt_data/"+ruc,
            success:(data)=>{
                generar_grafico_logistica(data)
                generar_grafico_desarrollo(data)
                generar_grafico_recursos(data)
            }

        })
    }
    function random_rgba_1() {
        var o = Math.round, r = Math.random, s = 255;
        return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',';
    }
    function generar_grafico_logistica(data)
    {  
        let totales=[0,0,0,0,0,0,0,0,0,0,0,0]
        let totales_orden
        let totales_salida
        let totales_ingreso
        let colores_borde=[];
        let colores_fondo=[];
        for(let i=0;i<3;i++)
        {
            color=random_rgba_1()
            colores_borde[i]=color+"0.2)";
            colores_fondo[i]=color+"1)";
        }
        $.each(data[0],(i,val)=>{
            let fecha_orden=new Date(val.fecha_orden_compra);
            let fecha_orden_string=fecha_orden.getMonth();
            switch(fecha_orden_string)
            {
                case 0:totales[0]=totales[0]+1
                break;
                case 1:totales[1]=totales[1]+1
                break;
                case 2:totales[2]=totales[2]+1
                break;
                case 3:totales[3]=totales[3]+1
                break;
                case 4:totales[4]=totales[4]+1
                break;
                case 5:totales[5]=totales[5]+1
                break;
                case 6:totales[6]=totales[6]+1
                break;
                case 7:totales[7]=totales[7]+1
                break;
                case 8:totales[8]=totales[8]+1
                break;
                case 9:totales[9]=totales[9]+1
                break;
                case 10:totales[10]=totales[10]+1
                break;
                case 11:totales[11]=totales[11]+1
                break;
            }
        })
        totales_orden=totales;
        totales=[0,0,0,0,0,0,0,0,0,0,0,0]
        $.each(data[2],(i,val)=>{
            let fecha_orden=new Date(val.fecha_ingreso);
            let fecha_orden_string=fecha_orden.getMonth();
            switch(fecha_orden_string)
            {
                case 0:totales[0]=totales[0]+1
                break;
                case 1:totales[1]=totales[1]+1
                break;
                case 2:totales[2]=totales[2]+1
                break;
                case 3:totales[3]=totales[3]+1
                break;
                case 4:totales[4]=totales[4]+1
                break;
                case 5:totales[5]=totales[5]+1
                break;
                case 6:totales[6]=totales[6]+1
                break;
                case 7:totales[7]=totales[7]+1
                break;
                case 8:totales[8]=totales[8]+1
                break;
                case 9:totales[9]=totales[9]+1
                break;
                case 10:totales[10]=totales[10]+1
                break;
                case 11:totales[11]=totales[11]+1
                break;
            }
        })
        totales_ingreso=totales;
        totales=[0,0,0,0,0,0,0,0,0,0,0,0]
        $.each(data[1],(i,val)=>{
            let fecha_orden=new Date(val.fecha_salida);
            let fecha_orden_string=fecha_orden.getMonth();
            switch(fecha_orden_string)
            {
                case 0:totales[0]=totales[0]+1
                break;
                case 1:totales[1]=totales[1]+1
                break;
                case 2:totales[2]=totales[2]+1
                break;
                case 3:totales[3]=totales[3]+1
                break;
                case 4:totales[4]=totales[4]+1
                break;
                case 5:totales[5]=totales[5]+1
                break;
                case 6:totales[6]=totales[6]+1
                break;
                case 7:totales[7]=totales[7]+1
                break;
                case 8:totales[8]=totales[8]+1
                break;
                case 9:totales[9]=totales[9]+1
                break;
                case 10:totales[10]=totales[10]+1
                break;
                case 11:totales[11]=totales[11]+1
                break;
            }
        })
        totales_salida=totales;

      let html="<canvas id='logistica_grafico'></canvas>";
      $("#logistica_caja").empty();
      $("#logistica_caja").append(html);
      var ctx = document.getElementById('logistica_grafico');
      var myChart = new Chart(ctx, {
          type: 'line',
          data: {
              labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
              datasets: [
                {
                  label: 'Ordenes de Compra',
                  data: totales_orden,
                  backgroundColor: [
                        colores_borde[0]
                  ],
                  borderColor: [
                        colores_fondo[0]
                  ],
                  borderWidth: 1
                },
                {
                    label: 'Ingresos',
                  data: totales_ingreso,
                  backgroundColor: [
                        colores_borde[1]
                  ],
                  borderColor: [
                        colores_fondo[1]
                  ],
                  borderWidth: 1
                },
                {
                    label: 'Salidas',
                  data: totales_salida,
                  backgroundColor: [
                        colores_borde[2]
                  ],
                  borderColor: [
                        colores_fondo[2]
                  ],
                  borderWidth: 1
                }
            ]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    }
    function generar_grafico_desarrollo(data)
    {
        let totales=[0,0,0,0,0,0,0,0,0,0,0,0]
        let totales_modelos_enespera
        let totales_modelosbases_aprobados
        let totales_articulos
        let colores_borde=[];
        let colores_fondo=[];
        for(let i=0;i<3;i++)
        {
            color=random_rgba_1()
            colores_borde[i]=color+"0.2)";
            colores_fondo[i]=color+"1)";
        }
        $.each(data[3],(i,val)=>{
            let fecha_orden=new Date(val.creado);
            let fecha_orden_string=fecha_orden.getMonth();
            switch(fecha_orden_string)
            {
                case 0:totales[0]=totales[0]+1
                break;
                case 1:totales[1]=totales[1]+1
                break;
                case 2:totales[2]=totales[2]+1
                break;
                case 3:totales[3]=totales[3]+1
                break;
                case 4:totales[4]=totales[4]+1
                break;
                case 5:totales[5]=totales[5]+1
                break;
                case 6:totales[6]=totales[6]+1
                break;
                case 7:totales[7]=totales[7]+1
                break;
                case 8:totales[8]=totales[8]+1
                break;
                case 9:totales[9]=totales[9]+1
                break;
                case 10:totales[10]=totales[10]+1
                break;
                case 11:totales[11]=totales[11]+1
                break;
            }
        })
        totales_modelos_enespera=totales;
        totales=[0,0,0,0,0,0,0,0,0,0,0,0]
        $.each(data[4],(i,val)=>{
            let fecha_orden=new Date(val.creado);
            let fecha_orden_string=fecha_orden.getMonth();
            switch(fecha_orden_string)
            {
                case 0:totales[0]=totales[0]+1
                break;
                case 1:totales[1]=totales[1]+1
                break;
                case 2:totales[2]=totales[2]+1
                break;
                case 3:totales[3]=totales[3]+1
                break;
                case 4:totales[4]=totales[4]+1
                break;
                case 5:totales[5]=totales[5]+1
                break;
                case 6:totales[6]=totales[6]+1
                break;
                case 7:totales[7]=totales[7]+1
                break;
                case 8:totales[8]=totales[8]+1
                break;
                case 9:totales[9]=totales[9]+1
                break;
                case 10:totales[10]=totales[10]+1
                break;
                case 11:totales[11]=totales[11]+1
                break;
            }
        })
        totales_modelosbases_aprobados=totales;
        totales=[0,0,0,0,0,0,0,0,0,0,0,0]
        $.each(data[5],(i,val)=>{
            let fecha_orden=new Date(val.creado);
            let fecha_orden_string=fecha_orden.getMonth();
            switch(fecha_orden_string)
            {
                case 0:totales[0]=totales[0]+1
                break;
                case 1:totales[1]=totales[1]+1
                break;
                case 2:totales[2]=totales[2]+1
                break;
                case 3:totales[3]=totales[3]+1
                break;
                case 4:totales[4]=totales[4]+1
                break;
                case 5:totales[5]=totales[5]+1
                break;
                case 6:totales[6]=totales[6]+1
                break;
                case 7:totales[7]=totales[7]+1
                break;
                case 8:totales[8]=totales[8]+1
                break;
                case 9:totales[9]=totales[9]+1
                break;
                case 10:totales[10]=totales[10]+1
                break;
                case 11:totales[11]=totales[11]+1
                break;
            }
        })
        totales_articulos=totales;
        totales=[0,0,0,0,0,0,0,0,0,0,0,0]
        let html="<canvas id='desarrollo_grafico'></canvas>";
      $("#desarrollo_caja").empty();
      $("#desarrollo_caja").append(html);
      var ctx = document.getElementById('desarrollo_grafico');
      var myChart = new Chart(ctx, {
          type: 'line',
          data: {
              labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
              datasets: [
                {
                  label: 'Modelos aun sin aprobar',
                  data: totales_modelos_enespera,
                  backgroundColor: [
                        colores_borde[0]
                  ],
                  borderColor: [
                        colores_fondo[0]
                  ],
                  borderWidth: 1
                },
                {
                    label: 'Modelos Aprobados',
                  data: totales_modelosbases_aprobados,
                  backgroundColor: [
                        colores_borde[1]
                  ],
                  borderColor: [
                        colores_fondo[1]
                  ],
                  borderWidth: 1
                },
                {
                    label: 'Articulos Aprobados',
                  data: totales_articulos,
                  backgroundColor: [
                        colores_borde[2]
                  ],
                  borderColor: [
                        colores_fondo[2]
                  ],
                  borderWidth: 1
                }
            ]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
    }
    function generar_grafico_recursos(data)
    {
        let totales_trabajador=[0,0,0]
        let colores_borde=[];
        let colores_fondo=[];
        for(let i=0;i<3;i++)
        {
            color=random_rgba_1()
            colores_borde[i]=color+"0.2)";
            colores_fondo[i]=color+"1)";
        }
        $.each(data[6],(i,val)=>{
            let cod=val.cod_tipo_trabajador
            console.log(cod)
            switch(cod)
            {
                case 1:totales_trabajador[0]=totales_trabajador[0]+1
                break;
                case 2:totales_trabajador[1]=totales_trabajador[1]+1
                break;
                case 3:totales_trabajador[2]=totales_trabajador[2]+1
                break;
            }
        })
        console.log(totales_trabajador)
        let html="<canvas id='recursos_grafico'></canvas>";
      $("#recursos_caja").empty();
      $("#recursos_caja").append(html);
      var ctx = document.getElementById('recursos_grafico');
      var myChart = new Chart(ctx, {
          type: 'pie',
          data: {
              labels: ['Planilla', 'Jornal', 'Destajo'],
              datasets: [
                {
                  label: 'Trabajadores',
                  data: totales_trabajador,
                  backgroundColor: [
                        colores_borde[0],
                        colores_borde[1],
                        colores_borde[2]
                  ],
                  borderColor: [
                        colores_fondo[0],
                        colores_fondo[1],
                        colores_fondo[2]
                  ],
                  borderWidth: 1
                }
            ]
          }
      });
    }
});
</script>
@endsection
