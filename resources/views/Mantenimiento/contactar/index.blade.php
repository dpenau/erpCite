@extends('layouts.app')
@section ('content')
<div class="container">


    <div>
    <h3 class="font-weight-bold">Contactos desde la pagina </h3>
	</div>
              <div class="x_content table-responsive">
                <table id="example" class="display">
                  <thead>
                    <tr>
                      <th>Empresa</th>
                      <th>Nombre</th>
                      <th>Telefono</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($contacto as $cont)
                    <tr>
                        <td>{{$cont->empresa_contacto}}</td>
                        <td>{{$cont->nombre_contacto}}</td>
                        <td>{{$cont->telefono_contacto}}</td>
                   	</tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              </div>
@endsection
