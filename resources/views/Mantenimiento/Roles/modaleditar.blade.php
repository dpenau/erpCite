<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-edit-{{$role->id}}">
	{{Form::Open(array('action'=>array('RolesController@update',$role->id
  ),'method'=>'put'))}}
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modificar Rol</h4>
			</div>
			<div class="modal-body">
        <input type="text" style="display:none" name="cod_rol_editar" value="{{$role->id}}">
				<div class="form-group  ">
          <label for="total_orden_compra">Codigo del rol:</label>
           <input type="text" class="form-control" disabled value="{{$role->id}}">
        </div>
        <div class="form-group  ">
          <label for="total_orden_compra">Nombre del rol:</label>
           <input type="text" class="form-control" name="nombre" value="{{$role->name}}">
        </div>
        <div class="form-group  ">
          <label for="total_orden_compra">Descripcion del rol:</label>
           <input type="text" class="form-control" name="descripcion" value="{{$role->description}}">
        </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="bttn-unite bttn-md bttn-primary ">Confirmar</button>
				<button type="button" class="bttn-unite bttn-md bttn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
	{{Form::Close()}}
</div>
