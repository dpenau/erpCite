<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class GrupoDeTrabajo extends Model
{
    protected $table = 'grupo_trabajo';

    protected $primaryKey = "codigo_grupo_trabajo";

    protected $keyType = "string";

    public $timestamps = false;

    protected $fillable = ['codigo_orden_pedido_produccion', 'especialidad', 'tiempo', 'proceso', 'fecha_creacion'];

    protected $guarded = [];
}
