<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class RegimenLaboralModel extends Model
{
    protected $table='regimen_laboral';

    protected $primaryKey="cod_regimen_laboral";

    public $timestamps=false;


    protected $fillable=['descrip_regimen_laboral', 'estado_regimen_laboral', 'porcentaje_beneficio'];

    protected $guarded=[];
}
