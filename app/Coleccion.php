<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Coleccion extends Model
{
  protected $table='coleccion';

  protected $primaryKey="codigo_coleccion";

  protected $keyType="string";

  public $timestamps=false;

  protected $fillable=['nombre_coleccion','RUC_empresa', 'estado_coleccion'];

  protected $guarded=[];
}
