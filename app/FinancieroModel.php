<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class FinancieroModel extends Model
{
  protected $table='gasto_financiero';

  protected $primaryKey="cod_gasto";
  public $timestamps=false;

  protected $fillable=['intereses','RUC_empresa','fecha_prestamo','meses_pago','estado','descripcion_financiera'];

  protected $guarded=[];
}
