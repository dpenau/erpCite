<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class EscalaSalarialModel extends Model
{
  protected $table='escala_salarial';

  protected $primaryKey="cod_trabajador";

  public $timestamps=false;

  protected $fillable=['tasa_beneficio','cod_regimen_laboral', 'RUC_empresa', 'sueldo_mensual', 'tipo_seguro'];

  protected $guarded=[];
}
