<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Merma extends Model
{
    protected $table='merma';

    protected $primaryKey="id";

    public $timestamps=false;

    protected $keyType="int";
    protected $fillable=['tipo_merma','subcategoria_id','porcentaje_merma','estado_registro','empresa_id'];

    protected $guarded=[];

    public function subcategoria()
    {
        return $this->belongsTo(SubcategoriaModel::class,'subcategoria_id','cod_subcategoria');
    }
}
