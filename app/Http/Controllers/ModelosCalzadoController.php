<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use erpCite\Modelo;
use erpCite\ModeloCombinacion;
use erpCite\CostoModelo;
use erpCite\DetalleCostoModeloMaterial;
use erpCite\DetalleCostoModeloMano;
use erpCite\SerieModeloModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ModelosCalzadoController extends Controller
{
    public function __construct()
    {
        $this->middleware('desarrollo');
    }
    public function index(Request $request)
    {
        if ($request) {
            $LineasModelos = DB::table('modelo')
                ->where('modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
                ->join(
                    'coleccion',
                    'modelo.cod_coleccion',
                    '=',
                    'coleccion.codigo_coleccion'
                )
                ->join('serie', 'modelo.cod_serie', '=', 'serie.cod_serie')
                ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
                //->join('material','modelo.cod_horma','=','material.cod_material')
                //->where('modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
                //->where(function($query){
                //  $query->orwhere('modelo.estado_modelo','=','0')
                //  ->orWhere('modelo.estado_modelo','=','1');
                //})
                ->get();
            $serie = DB::table('serie')
                ->where('RUC_empresa', Auth::user()->RUC_empresa)
                ->where('estado_serie', '=', 1)
                ->get();
            $seriemodelo = DB::Table('serie_modelo')
                ->join(
                    'serie',
                    'serie_modelo.codigo_serie',
                    '=',
                    'serie.cod_serie'
                )
                ->where(
                    'serie_modelo.RUC_empresa',
                    '=',
                    Auth::user()->RUC_empresa
                )
                ->where('serie_modelo.estado', '=', '1')
                ->get();
            $todaserie = DB::table('serie')
                ->where('RUC_empresa', Auth::user()->RUC_empresa)
                ->get();
            $capellada = DB::table('capellada')->get();
            $Lineas = DB::table('linea')
                ->where('RUC_empresa', Auth::user()->RUC_empresa)
                ->where('estado_linea', '=', 1)
                ->get();
            $coleccion = DB::Table('coleccion')
                ->where('RUC_empresa', Auth::user()->RUC_empresa)
                ->where('estado_coleccion', '=', 1)
                ->get();
            $horma = DB::table('material')
                ->where('RUC_empresa', Auth::user()->RUC_empresa)
                ->where('cod_subcategoria', '=', '969')
                ->where('t_compra', '=', '1')
                ->orderBy('descrip_material', 'asc')
                ->get();
            return view('Produccion.modelos_calzado.index', [
                'horma' => $horma,
                'coleccion' => $coleccion,
                'Lineas' => $Lineas,
                'LineasModelos' => $LineasModelos,
                'seriemodelo' => $seriemodelo,
                'todaserie' => $todaserie,
                'serie'=>$serie,
                'capellada' => $capellada,
            ]);
        }
        //return view('Produccion.modelo.index');
    }
    public function create(Request $request)
    {
        $coleccion = DB::Table('coleccion')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_coleccion', '=', 1)
            ->get();
        $Lineas = DB::table('linea')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_linea', '=', 1)
            ->get();
        $serie = DB::table('serie')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_serie', '=', 1)
            ->get();
        $capellada = DB::table('capellada')->get();
        $plantilla = DB::table('plantilla')->get();
        $forroF = DB::table('forro')->get();
        $piso = DB::table('piso')->get();
        $horma = DB::table('material')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('cod_subcategoria', '=', '969')
            ->where('t_compra', '=', '1')
            ->orderBy('descrip_material', 'asc')
            ->get();
        return view('Produccion.modelos_calzado.create', [
            'piso' => $piso,
            'forroF' => $forroF,
            'horma' => $horma,
            'coleccion' => $coleccion,
            'Lineas' => $Lineas,
            'serie' => $serie,
            'capellada' => $capellada,
            'plantilla' => $plantilla,
        ]);
    }
    public function store(Request $data)
    {
        $sigla = DB::table('empresa')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->get();
        $var = Input::get('codigo_modelo');
        $siglax = $sigla[0]->siglas;
        $res = $siglax .'-'. $var;

        $ModeloCalzado = new Modelo();
        $empresa = Auth::user()->RUC_empresa;
        if (!file_exists('photo/modelos/' . $empresa)) {
            mkdir('photo/modelos/' . $empresa);
        }
        $photo = '';
        $destination = 'photo/modelos/' . $empresa;
        $file = $data->photo;
        $extension = $file->getClientOriginalExtension();
        $filename = $res . Input::get('photo') . '.' . $extension;
        $file->move($destination, $filename);
        $photo = $filename;

        $ModeloCalzado->codigo = $var;
        $ModeloCalzado->cod_linea = Input::get('linea');
        $ModeloCalzado->cod_serie = Input::get('serie');
        $ModeloCalzado->cod_capellada = Input::get('capellada');
        $ModeloCalzado->cod_forro = Input::get('forro');
        $ModeloCalzado->num_combinacion= 0;
        $ModeloCalzado->cod_plantilla = Input::get('plantilla');
        $ModeloCalzado->cod_piso = Input::get('piso');
        $ModeloCalzado->nombre = Input::get('descripcion_modelo');
        $ModeloCalzado->RUC_empresa = $empresa;
        $ModeloCalzado->descripcion = Input::get('descripcion_modelo');
        $ModeloCalzado->cod_horma = 'NO';
        $ModeloCalzado->estado_modelo = 1;
        $ModeloCalzado->imagen = $photo;
        $ModeloCalzado->cod_coleccion = Input::get('coleccion');
        $ModeloCalzado->save();


        //CRECION PRIMER MODELO BASE:
        $ModeloCombinacionCalzado=new ModeloCombinacion;
        $ModeloCombinacionCalzado->codigo_comb=$var;
        $ModeloCombinacionCalzado->descripcion=Input::get('descripcion_modelo');
        $ModeloCombinacionCalzado->image=$photo;
        $ModeloCombinacionCalzado->cod_modelo=$ModeloCalzado->cod_modelo;
        $ModeloCombinacionCalzado->estado_modelo=1;
        $ModeloCombinacionCalzado->aprobado=0;
        $ModeloCombinacionCalzado->modelo_base=1;
        $ModeloCombinacionCalzado->costo_mano_directa=0;
        $ModeloCombinacionCalzado->costo_material_directo=0;
        $ModeloCombinacionCalzado->RUC_empresa=$empresa;
        $ModeloCombinacionCalzado->save();

        $modelo_actualizado=Modelo::where('cod_modelo',$ModeloCalzado->cod_modelo)->first();
        $modelo_actualizado=Modelo::where('cod_modelo',$ModeloCalzado->cod_modelo)
        ->update(['num_combinacion'=>($modelo_actualizado->num_combinacion)+1,
            'comb_proceso'=>($modelo_actualizado->comb_proceso)+1,
          ]);
        /*
    $seriestotales=Input::get('serie');
    //dd($seriestotales);
    for($i=0;$i<count($seriestotales);$i++)
    {
      $serieModelo=new SerieModeloModel;
      $serieModelo->codigo=$siglax.rand(1,99999);
      $serieModelo->codigo_modelo=$res;
      $serieModelo->codigo_serie=$seriestotales[$i];
      $serieModelo->estado=1;
      $serieModelo->RUC_empresa=$empresa;
      $serieModelo->save();
    }
*/
        session()->flash('success', 'Modelo de calzado creado');
        return Redirect::to('Produccion/modelos_calzado');
    }
    public function show()
    {
        /* return view('logistica.clasificacion.index',["clasificacion"=>$clasi]);*/
    }
    public function edit($id)
    {
        /* return Redirect::to('logistica/clasificacion');*/
    }
    public function update(Request $data)
    {

        $bandera= Input::get('bandera');


        $sigla = DB::table('empresa')
        ->where('RUC_empresa', Auth::user()->RUC_empresa)
        ->get();
        $var = Input::get('codigo_modelo');
        $siglax = $sigla[0]->siglas;
        $res = $siglax .'-'. $var;
        $codigo= Input::get('codigo');
        $descripcion=Input::get('descripcion_modelo');
        $codigo_modelo=Input::get('codigo_modelo');
        $linea=Input::get('linea');
        $serie=Input::get('serie');
        $coleccion=Input::get('coleccion');

        $empresa = Auth::user()->RUC_empresa;
        if (!file_exists('photo/modelos/' . $empresa)) {
            mkdir('photo/modelos/' . $empresa);
        }

        $photo = '';
        $destination = 'photo/modelos/' . $empresa;
        $file = $data->photo;
        if($file!=""){
            $extension = $file->getClientOriginalExtension();
            $filename = $res . Input::get('photo') . '.' . $extension;
            $file->move($destination, $filename);
            $photo = $filename;

            $modelo_actualizado=Modelo::where('cod_modelo',$codigo)
            ->update(["descripcion"=>$descripcion,
              'codigo'=>$codigo_modelo,
              'cod_linea'=>$linea,
              'cod_serie'=>$serie,
              'cod_coleccion'=>$coleccion,
              'imagen'=> $photo]);
              if($bandera==="1"){

                $combinacionModel=ModeloCombinacion::where('RUC_empresa',Auth::user()->RUC_empresa)
                ->where('modelo_base', 1)
                ->where('cod_modelo',  Input::get('codigo'))
                ->update(["descripcion"=>$descripcion,
                  'codigo_comb'=>$codigo_modelo,
                  'image'=> $photo]);
            }
        }else{

            $modelo_actualizado=Modelo::where('cod_modelo',$codigo)
            ->update(['descripcion'=>$descripcion,
              'codigo'=>$codigo_modelo,
              'cod_linea'=>$linea,
              'cod_serie'=>$serie,
              'cod_coleccion'=>$coleccion,
              ]);
              if($bandera==="1"){

                $combinacionModel=ModeloCombinacion::where('RUC_empresa',Auth::user()->RUC_empresa)
                ->where('modelo_base', 1)
                ->where('cod_modelo',  Input::get('codigo'))
                ->update(["descripcion"=>$descripcion,
                  'codigo_comb'=>$codigo_modelo,
                ]);
            }

        }


        $urlDireccion = Input::get('urlCod');
        if( $urlDireccion=="combinacion"){
        session()->flash('success', 'Modelo Actualizado');
        return Redirect::to('/Produccion/combinacion_calzado/listado/'.$codigo);
        }else{
            session()->flash('success', 'Modelo Actualizado');
            return Redirect::to('Produccion/modelos_calzado');
        }
    }

    public function destroy()
    {
        $email = Input::get('email');
        $estado = Input::get('estado');
        if ($estado == 0) {
            $mensaje = 'Desactivado';
        } else {
            $mensaje = 'Activado';
        }
        $act = Modelo::where('cod_modelo', $email)->update([
            'estado_modelo' => $estado,
        ]);
        session()->flash('success', 'Codigo de Modelo ' . $mensaje);
        return Redirect::to('Produccion/modelos_calzado');
    }
    public function ficha_producto()
    {
        $LineasModelos = DB::table('modelo')
            ->where('modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->join(
                'coleccion',
                'modelo.cod_coleccion',
                '=',
                'coleccion.codigo_coleccion'
            )
            ->join('serie', 'modelo.cod_serie', '=', 'serie.cod_serie')
            ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
            //->join('material','modelo.cod_horma','=','material.cod_material')
            //->where('modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
            //->where(function($query){
            //  $query->orwhere('modelo.estado_modelo','=','0')
            //  ->orWhere('modelo.estado_modelo','=','1');
            //})
            ->get();
        $seriemodelo = DB::Table('serie_modelo')
            ->join('serie', 'serie_modelo.codigo_serie', '=', 'serie.cod_serie')
            ->where('serie_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('serie_modelo.estado', '=', '1')
            ->get();
        $todaserie = DB::table('serie')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->get();
        $capellada = DB::table('capellada')->get();
        $Lineas = DB::table('linea')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_linea', '=', 1)
            ->get();
        $coleccion = DB::Table('coleccion')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_coleccion', '=', 1)
            ->get();
        $horma = DB::table('material')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('cod_subcategoria', '=', '969')
            ->where('t_compra', '=', '1')
            ->orderBy('descrip_material', 'asc')
            ->get();
        return view('Produccion.modelos_calzado.ficha', [
            'horma' => $horma,
            'coleccion' => $coleccion,
            'Lineas' => $Lineas,
            'LineasModelos' => $LineasModelos,
            'seriemodelo' => $seriemodelo,
            'todaserie' => $todaserie,
            'capellada' => $capellada,
        ]);
    }

    public function crear_ficha()
    {
        $LineasModelos = DB::table('modelo')
            ->where('modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->join(
                'coleccion',
                'modelo.cod_coleccion',
                '=',
                'coleccion.codigo_coleccion'
            )
            ->join('serie', 'modelo.cod_serie', '=', 'serie.cod_serie')
            ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
            //->join('material','modelo.cod_horma','=','material.cod_material')
            //->where('modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
            //->where(function($query){
            //  $query->orwhere('modelo.estado_modelo','=','0')
            //  ->orWhere('modelo.estado_modelo','=','1');
            //})
            ->get();
        $serie = DB::table('serie')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_serie', '=', 1)
            ->get();
        $seriemodelo = DB::Table('serie_modelo')
            ->join('serie', 'serie_modelo.codigo_serie', '=', 'serie.cod_serie')
            ->where('serie_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('serie_modelo.estado', '=', '1')
            ->get();
        $todaserie = DB::table('serie')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->get();
        $capellada = DB::table('capellada')->get();
        $Lineas = DB::table('linea')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_linea', '=', 1)
            ->get();
        $coleccion = DB::Table('coleccion')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_coleccion', '=', 1)
            ->get();
        $horma = DB::table('material')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('cod_subcategoria', '=', '969')
            ->where('t_compra', '=', '1')
            ->orderBy('descrip_material', 'asc')
            ->get();
        return view('Produccion.modelos_calzado.create_ficha', [
            'horma' => $horma,
            'serie' => $serie,
            'coleccion' => $coleccion,
            'Lineas' => $Lineas,
            'LineasModelos' => $LineasModelos,
            'seriemodelo' => $seriemodelo,
            'todaserie' => $todaserie,
            'capellada' => $capellada,
        ]);
    }

    //PDF
    public function descarga($var)
    {
        $query = trim($var);
        $pdf = \App::make('dompdf.wrapper');
        $pdf->setPaper('a4', 'landscape');
        $pdf->loadHTML($this->convert_data($query));
        return $pdf->stream();
    }
    //OBTENER DATA
    function get_data_cabecera($query)
    {
        $orden_data = CostoModelo::where('modelo_serie', $query)
            ->join(
                'serie_modelo',
                'costo_modelo.modelo_serie',
                '=',
                'serie_modelo.codigo'
            )
            ->join(
                'modelo',
                'serie_modelo.codigo_modelo',
                '=',
                'modelo.cod_modelo'
            )
            ->join('serie', 'serie_modelo.codigo_serie', '=', 'serie.cod_serie')
            ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
            ->join(
                'coleccion',
                'modelo.cod_coleccion',
                '=',
                'coleccion.codigo_coleccion'
            )
            ->join('material', 'modelo.cod_horma', '=', 'material.cod_material')
            ->select(
                'serie_modelo.codigo',
                'costo_modelo.estado',
                'costo_modelo.cod_costo_modelo',
                'modelo.imagen',
                'serie_modelo.codigo_modelo',
                'serie.nombre_serie',
                'linea.nombre_linea',
                'coleccion.nombre_coleccion',
                'material.descrip_material',
                'serie_modelo.RUC_empresa',
                'serie.tallaInicial',
                'serie.tallaFinal'
            )
            ->get();
        return $orden_data;
    }
    function get_data_materiales($query)
    {
        $orden_data = DetalleCostoModeloMaterial::where(
            'cod_costo_modelo',
            $query
        )
            ->join(
                'material',
                'detalle_costo_modelo_materiales.cod_material',
                '=',
                'material.cod_material'
            )
            ->join(
                'unidad_compra',
                'material.unidad_compra',
                '=',
                'unidad_compra.cod_unidad_medida'
            )
            ->join(
                'unidad_medida',
                'material.unidad_medida',
                '=',
                'unidad_medida.cod_unidad_medida'
            )
            ->join(
                'area',
                'detalle_costo_modelo_materiales.cod_area',
                '=',
                'area.cod_area'
            )
            ->select(
                'detalle_costo_modelo_materiales.cod_area',
                'detalle_costo_modelo_materiales.cod_material',
                'material.descrip_material',
                'material.t_moneda',
                'unidad_compra.descrip_unidad_compra',
                'detalle_costo_modelo_materiales.consumo_por_par',
                'area.descrip_area',
                'unidad_medida.descrip_unidad_medida',
                'detalle_costo_modelo_materiales.total',
                'material.factor_equivalencia',
                'material.costo_sin_igv_material'
            )
            ->orderBy('descrip_area', 'asc')
            ->get();
        return $orden_data;
    }
    function get_data_mano($query)
    {
        $orden_data = DetalleCostoModeloMano::where('cod_costo_modelo', $query)
            ->join(
                'area',
                'detalle_costo_modelo_manoobra.cod_area',
                '=',
                'area.cod_area'
            )
            ->select(
                'area.descrip_area',
                'detalle_costo_modelo_manoobra.operacion',
                'detalle_costo_modelo_manoobra.cod_detalle_costo_mano'
            )
            ->get();
        return $orden_data;
    }
    function get_imagen()
    {
        $idempresa = Auth::user()->RUC_empresa;
        $imagen = DB::table('empresa')
            ->where('RUC_empresa', '=', $idempresa)
            ->limit(1)
            ->get();
        return $imagen;
    }
    //DISEÑAR PDF
    function convert_data($query)
    {
        $cabecera = $this->get_data_cabecera($query);
        if (count($cabecera) > 0) {
            $detalle = $this->get_data_materiales(
                $cabecera[0]->cod_costo_modelo
            );
            $detalle2 = $this->get_data_mano($cabecera[0]->cod_costo_modelo);
            $img = $this->get_imagen();
            $photo = '';
            foreach ($img as $i) {
                if ($i->imagen != '') {
                    $photo = $i->imagen;
                }
            }
            $output = '<html><head><style>
        @page {
              margin: 0cm 0cm;
        }
        body {
              margin-top: 4cm;
              margin-left: 2cm;
              margin-right: 2cm;
              margin-bottom: 2cm;
        }
        header {

              position: fixed;
              top: 0.5cm;
              left: 0.5cm;
              right: 0cm;
              height: 3cm;
        }
        footer {
              margin-right: 0cm;
              position: fixed;
              bottom: 0cm;
              left: 0cm;
              right: 0cm;
              height: 2cm;
        }
        </style></head><body>';
            $idempresa = Auth::user()->RUC_empresa;
            $cantidad_mat = count($detalle) + 2;
            $cantidad_mano = count($detalle2) + 1;
            $rows = $cantidad_mat + $cantidad_mano;
            $tallabase =
                (floatval($cabecera[0]->tallaFinal) +
                    floatval($cabecera[0]->tallaInicial)) /
                2;
            $horma = explode('-', $cabecera[0]->descrip_material);
            if ($photo != '') {
                $output .=
                    '
          <header>
          <table align="center" border="1" style="width:90%">
            <tr align="center">
              <th  rowspan="4"><img  src="photo/' .
                    $photo .
                    '" alt="" style="width:120px;" class="img-rounded center-block"></th>
              <th  colspan="6"><div align="center">Ficha Tecnica de Prototipado</div></th>
            </tr>
            <tr align="center">
              <th><strong>Modelo:</strong></th>
              <td>' .
                    $cabecera[0]->codigo_modelo .
                    '</td>
              <th><strong>Linea:</strong></th>
              <td>' .
                    $cabecera[0]->nombre_linea .
                    '</td>
              <th><strong>Coleccion:</strong></th>
              <td>' .
                    $cabecera[0]->nombre_coleccion .
                    '</td>
            </tr>
            <tr align="center">
              <th><strong>Horma:</strong></th>
              <td>' .
                    $horma[0] .
                    '</td>
              <th><strong>Version:</strong></th>
              <td>00</td>
              <th><strong>Talla base:</strong></th>
              <td>' .
                    $tallabase .
                    '</td>
            </tr>
            <tr align="center">
              <th><strong>Elaborado por:</strong></th>
              <td></td>
              <th><strong>Aprobado por:</strong></th>
              <td></td>
              <th><strong>Firma Aprobacion:</strong></th>
              <td></td>
            </tr>
          </table>
          </header>
          <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
          ';
            } else {
                $output .=
                    '
          <header>
          <table align="center" border="1" style="width:90%">
            <tr align="center">
              <th  rowspan="3">Sin Logo</th>
              <th  colspan="6"><div align="center">Ficha Tecnica de Prototipado</div></th>
            </tr>
            <tr align="center">
              <th><strong>Modelo:</strong></th>
              <td>' .
                    $cabecera[0]->codigo_modelo .
                    '</td>
              <th><strong>Linea:</strong></th>
              <td>' .
                    $cabecera[0]->nombre_linea .
                    '</td>
              <th><strong>Coleccion:</strong></th>
              <td>' .
                    $cabecera[0]->nombre_coleccion .
                    '</td>
            </tr>
            <tr align="center">
              <th><strong>Horma:</strong></th>
              <td>' .
                    $horma[0] .
                    '</td>
              <th><strong>Version:</strong></th>
              <td>00</td>
              <th><strong>Talla base:</strong></th>
              <td>' .
                    $tallabase .
                    '</td>
            </tr>
          </table>
          </header>
          <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
          ';
            }
            $output .= '
        <br>
        <table align="center" border="1" style="width:100%">';
            $output .=
                '<tr align="center">
            <th rowspan="' .
                $rows .
                '"><img  src="photo/modelos/' .
                $idempresa .
                '/' .
                $cabecera[0]->imagen .
                '" alt="" style="width:125px" class="img-rounded center-block"></th>
            <th>Area</th>
            <th>Material</th>
            <th>Precio S/.</th>
            <th>Consumo</th>
            <th>Medida</th>
            <th>Costo_Par S/.</th>
          </tr>';
            //echo $detalle;
            $total_directo = 0;
            $lista_area = [
                'P-Corte',
                'P-Habilitado',
                'P-Aparado',
                'P-Alistado',
                'P-Montaje',
                'P-Acabado',
            ];
            for ($i = 0; $i < count($lista_area); $i++) {
                foreach ($detalle as $det) {
                    if ($lista_area[$i] == $det->descrip_area) {
                        $precio = $det->costo_sin_igv_material;
                        if ($det->t_moneda == 1) {
                            $precio = $precio * 3.33;
                        }
                        $output .=
                            '
              <tr>
              <td>' .
                            $det->descrip_area .
                            '</td>
              <td>' .
                            $det->descrip_material .
                            '</td>
              <td>' .
                            $precio .
                            '</td>
              <td>' .
                            $det->consumo_por_par .
                            '</td>
              <td>' .
                            $det->descrip_unidad_medida .
                            '</td>
              <td>' .
                            number_format($det->total, 5, '.', '') .
                            '</td>
              </tr>';
                        $total_directo = $total_directo + $det->total;
                    }
                }
            }
            $output .=
                '<tr><td colspan="5">Total</td><td style="background-color:yellow">' .
                $total_directo .
                '</td></tr>
        <tr>
        <th>Area</th>
        <th colspan="5">Operacion</th>
        </tr>';
            for ($i = 0; $i < count($lista_area); $i++) {
                foreach ($detalle2 as $det) {
                    if ($lista_area[$i] == $det->descrip_area) {
                        $precio = $det->costo_sin_igv_material;
                        if ($det->t_moneda == 1) {
                            $precio = $precio * 3.33;
                        }
                        $output .=
                            '
              <tr>
              <td>' .
                            $det->descrip_area .
                            '</td>
              <td colspan="5">' .
                            $det->operacion .
                            '</td>
              </tr>';
                    }
                }
            }
            $output .= '</table>
        </body></html>
        ';
            return $output;
        } else {
            return 'El modelo no posee ningun material ni operacion registrada';
        }
    }
    function aprobar_modelo(Request $request)
    {
        try {
            $codigo = $request['codigo'];
            $aprobacion = CostoModelo::where('modelo_serie', $codigo)->update([
                'estado' => '1',
            ]);
            session()->flash('success', 'Modelo Aprobado con exito');
        } catch (\Exception $e) {
            session()->flash('error', 'Error al aprobar el modelo');
        }
    }
}
