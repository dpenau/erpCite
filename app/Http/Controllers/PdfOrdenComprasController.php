<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use PDF;
use \Milon\Barcode\DNS1D;

class PdfOrdenComprasController extends Controller
{
    public function index($var)
    {
        if ($var != "") {
            $query = $var;
            $pdf = \App::make('dompdf.wrapper');
            $pdf->setPaper('a4', 'landscape');
            $pdf->loadHTML($this->convert_data($query));
            return $pdf->stream();
        }
    }

    function get_data($query)
    {
        $orden_data = DB::table('orden_compras')
            ->join('detalle_orden_compra_temp', 'orden_compras.id', '=', 'detalle_orden_compra_temp.cod_cab_ordencompra')
            ->join('material', 'detalle_orden_compra_temp.cod_material', '=', 'material.cod_material')
            ->join('unidad_compra', 'material.unidad_compra', '=', 'unidad_compra.cod_unidad_medida')
            ->where('orden_compras.id', '=', $query)
            ->get();
        return $orden_data;
    }

    function get_data_cab($query)
    {
        $orden_data_cab = DB::table('orden_compras')
            ->where('orden_compras.id', '=', $query)
            ->get();
        return $orden_data_cab;
    }

    function get_imagen()
    {
        $idempresa = Auth::user()->RUC_empresa;
        $imagen = DB::table('empresa')->where('RUC_empresa', '=', $idempresa)->limit(1)->get();
        return $imagen;
    }

    function convert_data($query)
    {
        $detalle = $this->get_data($query);
        $cab = $this->get_data_cab($query);
        $img = $this->get_imagen();
        $photo = "";

        foreach ($img as $i) {
            if ($i->imagen != "") {
                $photo = $i->imagen;
            }
        }

        $output = '<html><head><style>
    @page {
          margin: 0cm 0cm;
    }
    body {
          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {
          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    .column {
      float: left;
      width: 50%;
      padding: 5px;
    }

    .row::after {
      content: "";
      clear: both;
      display: table;
    }
    </style></head><body>';
        if ($photo != "") {
            $output .= '
              <header>
                  <div class="row">
                    <div class="column">
                      <img src="photo/' . $photo . '" alt="" style="width:120px;" class="img-rounded">
                    </div>
                    <div class="column" style="margin-top:3%;">
                      <img src="photo/pie3.png" width="100%"/>
                    </div>
                  </div>
              </header>';
        }
        foreach ($cab as $cabF) {
            $output .= '<h1>Orden de Compra Normal</h1>

              <table style="width:100%;" CELLPADDING="10"
              ALIGN="center">
                            <tr>
                                <td>Orden de compra: ' . $cabF->codigo_orden . '</td>
                                <td></td>
                                <td style="text-align:right;">Fecha de emisión: ' . $cabF->fecha . '</td>
                            </tr>
              </table>
                        <br>
              <table style="width:100%;border-collapse: collapse; border: 1px solid black;">
                <tr>
                  <th style="border-collapse: collapse; border: 1px solid black;">Codigo Material</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">Descripción Material</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">Costo sin igv</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">Cantidad Pedida</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">Unidad Compra</th>
                </tr>';
        }
        foreach ($detalle as $dat) {
            $output .= '
              <tr>
                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->codigo_orden . '</td>
                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->descrip_material . '</td>

                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">S/.' . $dat->costo_sin_igv_material . '</td>
                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->cantidad . '</td>
                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->descrip_unidad_compra . '</td>
              </tr>   ';
        }

        foreach ($cab as $cabFin) {
            $output .= '</table>
              <br><br>
              <table  CELLPADDING="3" ALIGN="right">
                <tr>
                  <td>Cantidad Total: </td>
                  <td>' . $cabFin->cantidad_total . '</td>
                </tr>
                <tr>
                    <td>SubTotal (Sin igv): </td>
                    <td>S/.' . $cabFin->subtotal . '</td>
                </tr>
                <tr>
                      <td>Total (Con igv): </td>
                      <td>S/.' . $cabFin->total . '</td>
                </tr>
              </table>';
        }
        $output .= '</body></html>';
        return $output;
    }
}

