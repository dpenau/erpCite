<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use erpCite\Modelo;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;

class ReporteGruposController extends Controller
{
    //

	public function __construct()
  {
    $this->middleware('jefe');
  }
	public function index()
  {
   $Grupo_trabajo = DB::table('grupo_trabajo','grupo_trabajador','trabajador')
     ->join('grupo_trabajador','grupo_trabajo.codigo_grupo_trabajo','=','grupo_trabajador.codigo_grupo_trabajo')
     ->join('trabajador','grupo_trabajador.codigo_grupo_trabajador','=','trabajador.DNI_trabajador')
     ->select('grupo_trabajador.codigo_grupo_trabajo','grupo_trabajador.codigo_grupo_trabajador','trabajador.apellido_paterno','trabajador.apellido_materno','trabajador.nombres','trabajador.estado_trabajador')
     ->orderBy('grupo_trabajador.codigo_grupo_trabajo')
     ->get();
      
	return view('Produccion.Reportes.ReporteGrupos.index',['Grupo_trabajo'=>$Grupo_trabajo]);
	}

	public function create()
	{

	}	
public function store(Request $data)
	{

	}
public function show()
	{
 /* return view('logistica.clasificacion.index',["clasificacion"=>$clasi]);*/
	}
public function edit()
	{
 /* return Redirect::to('logistica/clasificacion');*/
	}
public function update()
  	{
  	}
public function destroy()
	{

	}
}
