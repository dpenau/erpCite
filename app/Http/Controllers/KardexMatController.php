<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\Http\Requests\MaterialRequest;
use DB;
use Carbon\Carbon;
use erpCite\kardex;
use erpCite\kardex_talla;
use erpCite\kardex_talla_tallas;
use erpCite\kardex_cab;
use erpCite\kardex_cab_talla;
use erpCite\kardex_cab_talla_tallas;
use erpCite\kardexTienda;
use erpCite\DetalleKardexTienda;
use erpCite\KardexDetalle;
use Maatwebsite\Excel\Facades\Excel;

class KardexMatController extends Controller
{
    public function __construct()
    {
        $this->middleware('logistica');
    }

    public function index(Request $request)
    {
        if ($request) {
            $empresa = Auth::user()->RUC_empresa;
            $kardex = DB::table('kardex_material')
                ->join('material', 'kardex_material.cod_material', '=', 'material.cod_material')
                ->join('unidad_medida', 'material.unidad_compra', '=', 'unidad_medida.cod_unidad_medida')
                ->join('subcategoria', 'material.cod_subcategoria', '=', 'subcategoria.cod_subcategoria')
                ->join('almacen', 'kardex_material.cod_almacen', '=', 'almacen.cod_almacen')
                ->where('kardex_material.RUC_empresa', '=', $empresa)
                ->orderBy('material.descrip_material', 'asc')
                ->get();
            $materiales = DB::Table('kardex_material')
                ->join('material', 'kardex_material.cod_material', '=', 'material.cod_material')
                ->join('unidad_medida', 'material.unidad_compra', '=', 'unidad_medida.cod_unidad_medida')
                ->join('subcategoria', 'material.cod_subcategoria', '=', 'subcategoria.cod_subcategoria')
                ->where('kardex_material.RUC_empresa', '=', $empresa)
                ->orderBy('material.descrip_material', 'asc')
                ->select('material.cod_material', 'material.descrip_material', 'kardex_material.cod_kardex_material')
                ->get();
            $minimo = $maximo = 0;
            $mat_minimo = $mat_maximo = [];
            for ($i = 0; $i < count($kardex); $i++) {
                if ($kardex[$i]->stock_total <= $kardex[$i]->stock_minimo) {
                    $minimo++;
                    $mat_minimo[$i] = $kardex[$i]->descrip_material;
                } else {
                    if ($kardex[$i]->stock_total >= $kardex[$i]->stock_maximo) {
                        $maximo++;
                        $mat_maximo[$i] = $kardex[$i]->descrip_material;
                    }
                }
            }
            $subcategoria = DB::Table('subcategoria')
                ->where('estado_subcategoria', '=', '1')
                ->orderBy('nom_subcategoria', 'asc')
                ->get();
            $area = DB::Table('area')
                ->where('estado_area', '=', '1')
                ->orderBy('descrip_area', 'asc')
                ->get();
            $almacenes = DB::Table('almacen')
                ->where('RUC_empresa', '=', $empresa)
                ->where('estado_almacen', '=', 1)
                ->where('tipo_almacen', '=', '1')
                ->orderBy('nom_almacen', 'asc')
                ->get();
            $almacen_normales = DB::table('almacen')
                ->where('tipo_almacen', '=', 0)
                ->where('estado_almacen', '=', 1)
                ->where('RUC_empresa', '=', $empresa)
                ->get();
            $valores = DB::table('configuracion_valores')->get();
            return view('logistica.kardex.index', ["mat_maximo" => $mat_maximo, "mat_minimo" => $mat_minimo, "minimo" => $minimo, "maximo" => $maximo, "almacen_normal" => $almacen_normales, "almacenes" => $almacenes, "kardex" => $kardex, 'area' => $area, "materiales" => $materiales, "subcategoria" => $subcategoria, "valores" => $valores]);
        }
    }

    public function create(Request $request)
    {
        if ($request) {
            return view("logistica.kardex.create");
        }

    }

    public function store(Request $request)
    {
        //DEVOLUCION
        $tipo_accion = Input::get('tipoAccion');
        //Accion 1 = normal
        //Accion 2 = talla
        if ($tipo_accion == 1) {
            $empresa = Auth::user()->RUC_empresa;
            $fecha = date("Y-m-d H:i:s");
            $cod_material = Input::get('codmatbuscar');
            $cantidadt = Input::get('cantidad_devuelta');
            $costo_sin_igv = Input::get('costo_sin_igv2');
            $costo_con_igv = Input::get('costo_con_igv2');

            $kardexs = DB::table('kardex')
                ->where('kardex.cod_material', '=', $cod_material)
                ->get();

            //KARDEX CAB
            //      //0-DEVOLUCION   1-INGRESO   2-SALIDA
            $karde_cab = new kardex_cab;
            $karde_cab->movimiento = 0;
            $karde_cab->cod_material = $cod_material;
            $karde_cab->cantidad = $cantidadt;
            $karde_cab->costo_total_sin_igv = $cantidadt * $costo_sin_igv;
            $karde_cab->costo_total_con_igv = $cantidadt * $costo_con_igv;
            $karde_cab->$fecha;
            $karde_cab->igv = ($cantidadt * $costo_con_igv) - ($cantidadt * $costo_sin_igv);
            $karde_cab->RUC_empresa = $empresa;
            $karde_cab->save();

            $kardex = kardex::where('cod_material', $cod_material)
                ->update([
                    'stock_actual' => $kardexs[0]->stock_actual + $cantidadt,
                    'costo_total_sin_igv' => $kardexs[0]->costo_total_sin_igv + ($cantidadt * $costo_sin_igv),
                    'costo_total_con_igv' => $kardexs[0]->costo_total_con_igv + ($cantidadt * $costo_con_igv),
                    'igv' => $kardexs[0]->igv + (($cantidadt * $costo_con_igv) - ($cantidadt * $costo_sin_igv))
                ]);

            session()->flash('success', 'Salida Registrada');
            return Redirect::to('logistica/kardex');
        } else {
            $now = Carbon::now();
            $empresa = Auth::user()->RUC_empresa;
            $fecha = date("Y-m-d H:i:s");
            $cod_material = Input::get('devolucion_codmat');
            $salida_tallas = Input::get('devolucion_tallas');
            $salida_talla = Input::get('devolucion_talla');
            $costo_sin_igv = Input::get('devolucion_costo_sin_igv');
            $costo_con_igv = Input::get('devolucion_costo_con_igv');
            $salida_total = Input::get('devolucion_total');
            $cod_kardex_now = $cod_material . "-" . $now;

            $karde_cab = new kardex_cab_talla;
            $karde_cab->cod_kardex = $cod_kardex_now;
            $karde_cab->movimiento = 0;
            $karde_cab->cod_material = $cod_material;
            $karde_cab->cantidad = $salida_total;
            $karde_cab->costo_total_sin_igv = $salida_total * $costo_sin_igv;
            $karde_cab->costo_total_con_igv = $salida_total * $costo_con_igv;
            $karde_cab->fecha = $fecha;
            $karde_cab->igv = ($salida_total * $costo_con_igv) - ($salida_total * $costo_sin_igv);
            $karde_cab->RUC_empresa = $empresa;
            $karde_cab->save();

            $kardexs = DB::table('kardex_talla')
                ->where('kardex_talla.cod_material', '=', $cod_material)
                ->get();

            $kardex_talla = kardex_talla::where('cod_material', $cod_material)
                ->update([
                    'stock_actual' => $kardexs[0]->stock_actual + $salida_total,
                    'costo_total_sin_igv' => $kardexs[0]->costo_total_sin_igv + ($salida_total * $costo_sin_igv),
                    'costo_total_con_igv' => $kardexs[0]->costo_total_con_igv + ($salida_total * $costo_con_igv),
                    'igv' => $kardexs[0]->igv + ($salida_total * $costo_con_igv) - ($salida_total * $costo_sin_igv)
                ]);

            $contador = 0;
            foreach ($salida_tallas as $talla) {
                $talla_tallas = new kardex_cab_talla_tallas;
                $talla_tallas->talla = $salida_talla[$contador];
                $talla_tallas->cantidad = $talla;
                $talla_tallas->cod_kardex = $cod_kardex_now;
                $talla_tallas->save();

                $kardexs_tallas = DB::table('kardex_talla_tallas')
                    ->join('kardex_talla', 'kardex_talla_tallas.cod_kardex', '=', 'kardex_talla.id_kardex')
                    ->where('cod_material', '=', $cod_material)
                    ->where('talla', '=', $salida_talla[$contador])
                    ->get();

                $kardex_talla_tallas = kardex_talla_tallas::where('cod_kardex', $kardexs_tallas[0]->cod_kardex)
                    ->where('talla', '=', $salida_talla[$contador])
                    ->update([
                        'cantidad' => $kardexs_tallas[0]->cantidad + $talla
                    ]);
                $contador++;
            }
            session()->flash('success', 'Salida Registrada');
            return Redirect::to('logistica/kardex/talla');
        }
    }

    public function show()
    {
        return view('logistica.ingreso_salida.index');
    }

    public function edit($id)
    {
        return Redirect::to('logistica/ingreso_salida.update');
    }

    public function update(Request $request)
    {
        //SALIDA
        $tipo_accion = Input::get('tipoAccion');
        //Accion 1 = normal
        //Accion 2 = talla
        if ($tipo_accion == 1) {
            $empresa = Auth::user()->RUC_empresa;
            $fecha = date("Y-m-d H:i:s");
            $cod_material = Input::get('cod_mat_buscar');
            $cantidadt = Input::get('cantidad_entregar');
            $costo_sin_igv = Input::get('costo_sin_igv');
            $costo_con_igv = Input::get('costo_con_igv');

            $kardexs = DB::table('kardex')
                ->where('kardex.cod_material', '=', $cod_material)
                ->get();

            //KARDEX CAB

            $karde_cab = new kardex_cab;
            $karde_cab->movimiento = 2;
            $karde_cab->cod_material = $cod_material;
            $karde_cab->cantidad = $cantidadt;
            $karde_cab->costo_total_sin_igv = $cantidadt * $costo_sin_igv;
            $karde_cab->costo_total_con_igv = $cantidadt * $costo_con_igv;
            $karde_cab->fecha = $fecha;
            $karde_cab->igv = ($cantidadt * $costo_con_igv) - ($cantidadt * $costo_sin_igv);
            $karde_cab->RUC_empresa = $empresa;
            $karde_cab->save();

            $kardex = kardex::where('cod_material', $cod_material)
                ->update([
                    'stock_actual' => $kardexs[0]->stock_actual - $cantidadt,
                    'costo_total_sin_igv' => $kardexs[0]->costo_total_sin_igv - ($cantidadt * $costo_sin_igv),
                    'costo_total_con_igv' => $kardexs[0]->costo_total_con_igv - ($cantidadt * $costo_con_igv),
                    'igv' => $kardexs[0]->igv - (($cantidadt * $costo_con_igv) - ($cantidadt * $costo_sin_igv))
                ]);

            session()->flash('success', 'Salida Registrada');
            return Redirect::to('logistica/kardex');
        } else {
            $now = Carbon::now();
            $empresa = Auth::user()->RUC_empresa;
            $fecha = date("Y-m-d H:i:s");
            $cod_material = Input::get('cod_mat_buscar');
            $salida_tallas = Input::get('salida_tallas');
            $salida_talla = Input::get('salida_talla');
            $costo_sin_igv = Input::get('costo_sin_igv');
            $costo_con_igv = Input::get('costo_con_igv');
            $salida_total = Input::get('salida_total');
            $cod_kardex_now = $cod_material . "-" . $now;

            $karde_cab = new kardex_cab_talla;
            $karde_cab->cod_kardex = $cod_kardex_now;
            $karde_cab->movimiento = 2;
            $karde_cab->cod_material = $cod_material;
            $karde_cab->cantidad = $salida_total;
            $karde_cab->costo_total_sin_igv = $salida_total * $costo_sin_igv;
            $karde_cab->costo_total_con_igv = $salida_total * $costo_con_igv;
            $karde_cab->fecha = $fecha;
            $karde_cab->igv = ($salida_total * $costo_con_igv) - ($salida_total * $costo_sin_igv);
            $karde_cab->RUC_empresa = $empresa;
            $karde_cab->save();

            $kardexs = DB::table('kardex_talla')
                ->where('kardex_talla.cod_material', '=', $cod_material)
                ->get();

            $kardex_talla = kardex_talla::where('cod_material', $cod_material)
                ->update([
                    'stock_actual' => $kardexs[0]->stock_actual - $salida_total,
                    'costo_total_sin_igv' => $kardexs[0]->costo_total_sin_igv - ($salida_total * $costo_sin_igv),
                    'costo_total_con_igv' => $kardexs[0]->costo_total_con_igv - ($salida_total * $costo_con_igv),
                    'igv' => $kardexs[0]->igv - ($salida_total * $costo_con_igv) - ($salida_total * $costo_sin_igv)
                ]);

            $contador = 0;
            foreach ($salida_tallas as $talla) {

                $talla_tallas = new kardex_cab_talla_tallas;
                $talla_tallas->talla = $salida_talla[$contador];
                $talla_tallas->cantidad = $talla;
                $talla_tallas->cod_kardex = $cod_kardex_now;
                $talla_tallas->save();

                $kardexs_tallas = DB::table('kardex_talla_tallas')
                    ->join('kardex_talla', 'kardex_talla_tallas.cod_kardex', '=', 'kardex_talla.id_kardex')
                    ->where('cod_material', '=', $cod_material)
                    ->where('talla', '=', $salida_talla[$contador])
                    ->get();

                $kardex_talla_tallas = kardex_talla_tallas::where('cod_kardex', $kardexs_tallas[0]->cod_kardex)
                    ->where('talla', '=', $salida_talla[$contador])
                    ->update([
                        'cantidad' => $kardexs_tallas[0]->cantidad - $talla
                    ]);

                $contador++;
            }
            session()->flash('success', 'Salida Registrada');
            return Redirect::to('logistica/kardex/talla');
        }
    }

    public function destroy($id)
    {
        return Redirect::to('logistica/ingreso_salida');
    }

    public function excel()
    {
        return view('logistica.kardex.excel');
    }

    public function subirexcel(Request $data)
    {
        $file = $data->photo;
        $material_errores = "";
        Excel::load($file, function ($reader) use ($material_errores) {
            $mat = "";
            foreach ($reader->get() as $book) {
                $persona = $book->trasladador_material;
                $descri = $book->codigo;
                $empresa = Auth::user()->RUC_empresa;
                $mytime = date('Y-m-d G:i:s');
                for ($i = 30; $i < 47; $i++) {
                    if ($book->$i > 0) {
                        $aleatorio = rand(10000, 99999);
                        $material = DB::table('material')
                            ->where('RUC_empresa', '=', $empresa)
                            ->where('cod_material', '=', $descri)
                            ->get();
                        $nombre = explode("-", $material[0]->descrip_material);
                        $cod = DB::table('material')
                            ->where('RUC_empresa', '=', $empresa)
                            ->where('descrip_material', '=', $nombre[0] . "-" . $i)
                            ->get();
                        if (count($cod) > 0) {
                            $esta_kardex = DB::table('kardex_material')
                                ->where('cod_material', '=', $cod[0]->cod_material)
                                ->get();
                            if (count($esta_kardex) > 0 && $book->$i <= $esta_kardex[0]->stock_total) {
                                $salidas = $book->$i;
                                $stock_tot = $esta_kardex[0]->stock_total;
                                $materiales_espera = DB::table('detalle_kardex_material')
                                    ->where('cod_kardex_material', '=', $cod[0]->cod_material)
                                    ->where('estado_detalle_kardex', '=', '1')
                                    ->get();
                                for ($j = 0; $j < count($materiales_espera); $j++) {
                                    if ($salidas > 0) {
                                        $entregado = 0;
                                        if ($salidas <= $materiales_espera[$j]->restante_detalle) {
                                            $restante_nuevo = $materiales_espera[$j]->restante_detalle;
                                            $restante_nuevo = $restante_nuevo - $salidas;
                                            $estado = 1;
                                            if ($restante_nuevo == 0) {
                                                $estado = 0;
                                            }
                                            $act = KardexDetalle::where('id_detalle', $materiales_espera[$j]->id_detalle)
                                                ->update(["estado_detalle_kardex" => $estado, "restante_detalle" => $restante_nuevo]);
                                            $entregado = $salidas;
                                            $salidas = 0;
                                        } else {
                                            $act = KardexDetalle::where('id_detalle', $materiales_espera[$j]->id_detalle)
                                                ->update(["estado_detalle_kardex" => "0", "restante_detalle" => "0"]);
                                            $salidas = $salidas - $materiales_espera[$j]->restante_detalle;
                                            $entregado = $materiales_espera[$j]->restante_detalle;
                                        }
                                        $stock_tot = $stock_tot - $entregado;
                                        $detalle = new KardexDetalle;
                                        $detalle->dni_usuario = Auth::user()->id;
                                        $detalle->cod_area = 83205;
                                        $detalle->cod_kardex_material = $esta_kardex[0]->cod_material;
                                        $detalle->costo_material = $materiales_espera[$j]->costo_material;
                                        $detalle->fecha_salida = $mytime;
                                        $detalle->cantidad_salida = $entregado;
                                        $detalle->trasladador_material = $persona;
                                        $detalle->stock = $stock_tot;
                                        $detalle->comentario_devolucion = $aleatorio;
                                        $detalle->estado_detalle_kardex = 0;
                                        $detalle->restante_detalle = 0;
                                        $detalle->save();
                                        $stockact = 0;
                                        $stockact = $stock_tot;
                                        $act = Kardex::where('cod_material', $esta_kardex[0]->cod_material)
                                            ->update(['stock_total' => $stockact]);
                                    } else {
                                        break;
                                    }
                                }
                            } else {
                                session()->flash('success', 'Salida con Excel realizado excepto los materiales ' . $nombre[0]);
                            }
                        }
                    }
                }
            }
        });
        session()->flash('success', 'Salida con Excel realizado');
        return Redirect::to('logistica/kardex');
    }

    public function salida_tienda()
    {
        if (Input::get('stock_mat') < Input::get('cantidad_entregar')) {
            session()->flash('error', 'Cantidad por entregar no puede superar al stock');
        } else {
            $mytime = date("Y-m-d H:i:s");
            $almacen = Input::get('cod_almacen_busc');
            $aleatorio = rand(10000, 99999);
            $cod_material = Input::get('cod_mat_buscar');
            $cantidadsalida = Input::get('cantidad_entregar');
            $area = Input::get('area');
            $stocktotal = 0;
            $salidas = $cantidadsalida;
            $codigo_mat_tienda = $almacen . $cod_material;
            $materiales_espera = DB::table('detalle_kardex_tienda')
                ->where('cod_kardex_tienda', '=', $codigo_mat_tienda)
                ->where('estado_detalle_kardex', '=', '1')
                ->get();
            for ($i = 0; $i < count($materiales_espera); $i++) {
                if ($salidas > 0) {
                    $entregado = 0;
                    if ($salidas <= $materiales_espera[$i]->restante_detalle) {
                        $restante_nuevo = $materiales_espera[$i]->restante_detalle;
                        $restante_nuevo = $restante_nuevo - $salidas;
                        $estado = 1;
                        if ($restante_nuevo == 0) {
                            $estado = 0;
                        }
                        $act = DetalleKardexTienda::where('cod_detalle_tienda', $materiales_espera[$i]->cod_detalle_tienda)
                            ->update(["estado_detalle_kardex" => $estado, "restante_detalle" => $restante_nuevo]);
                        $entregado = $salidas;
                        $salidas = 0;
                    } else {
                        $act = DetalleKardexTienda::where('cod_detalle_tienda', $materiales_espera[$i]->cod_detalle_tienda)
                            ->update(["estado_detalle_kardex" => "0", "restante_detalle" => "0"]);
                        $salidas = $salidas - $materiales_espera[$i]->restante_detalle;
                        $entregado = $materiales_espera[$i]->restante_detalle;
                    }
                    $detalle = new DetalleKardexTienda;
                    $persona = Input::get('trasladador_material');
                    $detalle->dni_usuario = Auth::user()->id;
                    $detalle->cod_area = $area;
                    $detalle->cod_kardex_tienda = $codigo_mat_tienda;
                    $detalle->fecha_salida = $mytime;
                    $detalle->cantidad_salida = $entregado;
                    $detalle->trasladador_material = $persona;
                    $detalle->comentario_devolucion = $aleatorio;
                    $detalle->costo_material = $materiales_espera[$i]->costo_material;
                    $record = DB::table('detalle_kardex_tienda')->where('cod_kardex_tienda', '=', $codigo_mat_tienda)->get();
                    if (count($record)) {
                        $cantidad = count($record) - 1;
                        $stock_tot = $record[$cantidad]->stock - $entregado;
                        $detalle->stock = $stock_tot;
                    } else {
                        $stock_tot = $record[0]->stock - $entregado;
                        $detalle->stock = $stock_tot;
                    }
                    $detalle->save();
                    $act = KardexTienda::where('cod_kardex_tienda', $codigo_mat_tienda)
                        ->update(['stock_total_tienda' => $stock_tot]);

                    if ($area == "47812") {
                        $material_kardex = Kardex::where('cod_kardex_material', $cod_material)->select('stock_total')->get();
                        $nuevo_stock = $material_kardex[0]->stock_total + $cantidadsalida;
                        $actualizar = Kardex::where('cod_kardex_material', $cod_material)->update(['stock_total' => $nuevo_stock]);
                        $mat = DB::table('material')->where('cod_material', $cod_material)->select('costo_sin_igv_material')->get();
                        $mytime = date("Y-m-d H:i:s");
                        $nuevo_detalle = new KardexDetalle;
                        $nuevo_detalle->dni_usuario = Auth::user()->id;
                        $nuevo_detalle->cod_area = 47812;
                        $nuevo_detalle->cod_kardex_material = $cod_material;
                        $nuevo_detalle->costo_material = $mat[0]->costo_sin_igv_material;
                        $nuevo_detalle->fecha_ingreso = $mytime;
                        $nuevo_detalle->cantidad_ingresada = $cantidadsalida;
                        $nuevo_detalle->trasladador_material = $persona;
                        $nuevo_detalle->stock = $nuevo_stock;
                        $nuevo_detalle->estado_detalle_kardex = 1;
                        $nuevo_detalle->restante_detalle = $cantidadsalida;
                        $nuevo_detalle->save();
                    }
                    session()->flash('success', 'Salida Registrada');
                }
            }
        }
        return Redirect::to('logistica/kardex');
    }

    public function tienda()
    {
        $empresa = Auth::user()->RUC_empresa;
        $kardex = DB::table('kardex_tienda')
            ->join('material', 'kardex_tienda.codigo_material', '=', 'material.cod_material')
            ->join('unidad_medida', 'material.unidad_compra', '=', 'unidad_medida.cod_unidad_medida')
            ->join('subcategoria', 'material.cod_subcategoria', '=', 'subcategoria.cod_subcategoria')
            ->join('almacen', 'kardex_tienda.codigo_almacen', '=', 'almacen.cod_almacen')
            ->where('kardex_tienda.RUC_empresa', '=', $empresa)
            ->orderBy('material.descrip_material', 'asc')
            ->select()
            ->get();
        $materiales = DB::Table('kardex_tienda')
            ->join('material', 'kardex_tienda.codigo_material', '=', 'material.cod_material')
            ->join('unidad_medida', 'material.unidad_compra', '=', 'unidad_medida.cod_unidad_medida')
            ->join('subcategoria', 'material.cod_subcategoria', '=', 'subcategoria.cod_subcategoria')
            ->join('almacen', 'kardex_tienda.codigo_almacen', '=', 'almacen.cod_almacen')
            ->where('kardex_tienda.RUC_empresa', '=', $empresa)
            ->orderBy('material.descrip_material', 'asc')
            ->select('material.cod_material', 'material.descrip_material', 'kardex_tienda.cod_kardex_tienda', 'almacen.nom_almacen')
            ->get();
        $minimo = $maximo = 0;
        $mat_minimo = $mat_maximo = [];
        for ($i = 0; $i < count($kardex); $i++) {
            if ($kardex[$i]->stock_total_tienda <= $kardex[$i]->stock_minimo) {
                $minimo++;
                $mat_minimo[$i] = $kardex[$i]->descrip_material . " // " . $kardex[$i]->nom_almacen;
            } else {
                if ($kardex[$i]->stock_total_tienda >= $kardex[$i]->stock_maximo) {
                    $maximo++;
                    $mat_maximo[$i] = $kardex[$i]->descrip_material . " // " . $kardex[$i]->nom_almacen;
                }
            }
        }
        $subcategoria = DB::Table('subcategoria')
            ->where('estado_subcategoria', '=', '1')
            ->orderBy('nom_subcategoria', 'asc')
            ->get();
        $area = DB::Table('area')
            ->where('estado_area', '=', '1')
            ->orderBy('descrip_area', 'asc')
            ->get();
        $almacenes = DB::Table('almacen')
            ->where('RUC_empresa', '=', $empresa)
            ->where('estado_almacen', '=', 1)
            ->where('tipo_almacen', '=', 0)
            ->orderBy('nom_almacen', 'asc')
            ->get();
        $almacenes_tienda = DB::Table('almacen')
            ->where('RUC_empresa', '=', $empresa)
            ->where('estado_almacen', '=', 1)
            ->where('tipo_almacen', '=', 1)
            ->orderBy('nom_almacen', 'asc')
            ->get();
        $valores = DB::table('configuracion_valores')->get();
        return view('logistica.tienda.index', ["mat_maximo" => $mat_maximo, "mat_minimo" => $mat_minimo, "minimo" => $minimo, "maximo" => $maximo, "materiales" => $materiales, "almacen_tienda" => $almacenes_tienda, "almacenes" => $almacenes, "kardex" => $kardex, 'area' => $area, "subcategoria" => $subcategoria, "valores" => $valores]);
    }

}
