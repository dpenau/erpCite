<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\UnidadMedidaModel;
use erpCite\UnidadCompraModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class UnidadMedidaController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index(Request $request)
    {
        if($request)
        {
            $medida=DB::table('unidad_medida')->get();
            return view('Mantenimiento.Unidad_Medida.index',["medida"=>$medida]);
        }
    }
    public function create(Request $request)
    {
        if($request)
        {
            return view("Mantenimiento.Unidad_Medida.create");
        }
    }
    public function store()
    {
        $identificador=rand(10000,99999);
        $medida=new UnidadCompraModel;
        $medidas=new UnidadMedidaModel;
        $medidas->cod_unidad_medida=$identificador;
        $medida->cod_unidad_medida=$identificador;
        $medidas->unidad=Input::get('nombre');
        $medida->unidad=Input::get('nombre');
        $medidas->descrip_unidad_medida=Input::get('descripcion');
        $medida->descrip_unidad_compra=Input::get('descripcion');
        $medidas->magnitud_unidad_medida=Input::get('magnitud');
        $medida->magnitud_unidad_medida=Input::get('magnitud');
        $medidas->estado_unidad_medida=1;
        $medida->estado_unidad_medida=1;
        $medidas->save();
        $medida->save();
        session()->flash('success','Unidad de Medida Registrado');
        return Redirect::to('Mantenimiento/Unidad_Medida');
    }
    public function show()
    {
        return view('Mantenimiento.Unidad_Medida.index');
    }
    public function edit($id)
    {
        return Redirect::to('Mantenimiento/Unidad_Medida');
    }
    public function update()
    {
      $id=Input::get("cod");
      $nombre=Input::get("nombre");
      $descrip=Input::get("descrip");
      $magnitud=Input::get("magni");
      $act=UnidadMedidaModel::where('cod_unidad_medida',$id)
      ->update(['unidad'=>$nombre,'descrip_unidad_medida'=>$descrip,"magnitud_unidad_medida"=>$magnitud]);
      session()->flash('success','Unidad de medida Actualizada');
        return Redirect::to('Mantenimiento/Unidad_Medida');
    }
    public function destroy($id)
    {
        $id=Input::get("unidad");
        $accion=Input::get("accion");

        if($accion==0){$mensaje="Desactivado";}
        else{$mensaje="Activado" ;}
        $act=UnidadMedidaModel::where('cod_unidad_medida',$id)
        ->update(['estado_unidad_medida'=>$accion]);
        session()->flash('success','Unidad de medida '.$mensaje);
        return Redirect::to('Mantenimiento/Unidad_Medida');
    }
}
