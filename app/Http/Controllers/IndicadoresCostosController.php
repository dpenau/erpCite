<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
class IndicadoresCostosController extends Controller
{
    public function index()
    {
        $empresa=Auth::user()->RUC_empresa;
        $year=date('Y');
        $areas=DB::table('area')->where('estado_area','=',1)->select('descrip_area')->orderby('descrip_area','desc')->get();
        //GRAFICO DE BARRAS POR AREAS
        $materiales_indirectos=DB::table('gasto_suministro')
        ->join('area','gasto_suministro.cod_area','=','area.cod_area')
        ->where('gasto_suministro.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->select('area.descrip_area',DB::raw('SUM(gasto_suministro.gasto_mensual_suministro) as costo'))
        ->groupBy('area.descrip_area')
        ->orderby('area.descrip_area','desc')
        ->get();
        $depreciacion=DB::Table('gasto_depreciacion_mantenimiento')
        ->join('area','gasto_depreciacion_mantenimiento.cod_area','=','area.cod_area')
        ->join('tipo_activo','gasto_depreciacion_mantenimiento.cod_tipo_activo','=','tipo_activo.cod_tipo_activo')
        ->where('gasto_depreciacion_mantenimiento.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->select('area.descrip_area','gasto_depreciacion_mantenimiento.depreciacion_mensual',
        'gasto_depreciacion_mantenimiento.gasto_mensual_depreciacion','gasto_depreciacion_mantenimiento.anio_compra',
        'gasto_depreciacion_mantenimiento.anio_uso','tipo_activo.anios_vida_util')
        ->get();
        $lista_depreciacion = new Collection();
        foreach($depreciacion as $dep)
        {
            $anio=date('Y')-($dep->anio_compra-$dep->anio_uso);
            if($anio<=$dep->anios_vida_util)
            {
                $lista_depreciacion->push(['area'=>$dep->descrip_area,'depreciacion'=>$dep->depreciacion_mensual,'mantenimiento'=>$dep->gasto_mensual_depreciacion]);
            }
        }
        //GRAFICO DE TIEMPO
        $mano_obra_indirecta=DB::table('gasto_sueldos')
        ->join('area','gasto_sueldos.cod_area','=','area.cod_area')
        ->where('gasto_sueldos.es_externo','=',0)
        ->where('gasto_sueldos.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->whereYear('gasto_sueldos.fecha_creacion', '=', $year)
        ->select('gasto_sueldos.fecha_creacion','gasto_sueldos.gasto_mensual')
        ->get();
        $servicio=DB::table('gasto_servicios_basicos')
        ->where('gasto_servicios_basicos.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->whereYear('gasto_servicios_basicos.fecha_creacion', '=', $year)
        ->select('gasto_servicios_basicos.fecha_creacion','gasto_servicios_basicos.gasto_mensual')
        ->get();
        $prototipo=DB::table('gasto_desarrollo_producto')
        ->where('gasto_desarrollo_producto.RUC_empresa',Auth::user()->RUC_empresa)
        ->whereYear('fecha_compra','=',$year)
        ->select('fecha_compra','importe')
        ->get();
        $representacion=DB::Table('gasto_representacion')
        ->where('gasto_representacion.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->whereYear('gasto_representacion.fecha_creacion', '=', $year)
        ->select('fecha_creacion','gasto')
        ->get();
        $distribucion=DB::table('gasto_distribucion')
        ->where('gasto_distribucion.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->whereYear('gasto_distribucion.fecha_creacion', '=', $year)
        ->select('fecha_creacion','gasto_mensual')
        ->get();
        $otrosmarketing=DB::table('gasto_otros')
        ->where('gasto_otros.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->whereYear('gasto_otros.fecha_creacion', '=', $year)
        ->select('fecha_creacion','gasto')
        ->get();
        $externos=DB::table('gasto_sueldos')
        ->join('area','gasto_sueldos.cod_area','=','area.cod_area')
        ->where('gasto_sueldos.es_externo','=',1)
        ->where('gasto_sueldos.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->whereYear('gasto_sueldos.fecha_creacion', '=', $year)
        ->select('gasto_sueldos.fecha_creacion','gasto_sueldos.gasto_mensual')
        ->get();
        return view('costos.indicador.index',['areas'=>$areas,'materiales_indirectos'=>$materiales_indirectos
        ,'depreciacion'=>$lista_depreciacion,'mano_obra_indirecta'=>$mano_obra_indirecta,
        'servicio'=>$servicio,'prototipo'=>$prototipo,'representacion'=>$representacion,'distribucion'=>$distribucion,
        'otrosmarketing'=>$otrosmarketing,'externos'=>$externos]);
    }
}
