<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use erpCite\DetalleOrdenPedidoModel;
use erpCite\Empresa;
use Illuminate\Support\Facades\Input;
use erpCite\OrdenProduccion;
use erpCite\ClienteModel;
use erpCite\kardex;
use Illuminate\Support\Facades\Redirect;

use DB;

use Illuminate\Http\Request;

class OrdenProduccionController extends Controller
{

  public function __construct()
  {
    $this->middleware('jefe');
  }

  public function index($id)
  {
    $id_pedido = $id;

    $ordenProducciones = DB::table('orden_pedido_produccion')->where('codigo_orden_pedido', '=', $id)->get(); //where('RUC_empresa','=',Auth::user()->RUC_empresa)->get();
    $codigo_cliente = DB::table('orden_pedido')->select('codigo_cliente')->distinct()->where('codigo_pedido', '=', $id)->get();
    $codigoCliente = $codigo_cliente[0]->codigo_cliente;
    $Cliente_nombre = DB::table('cliente')->select('nombre')->where('codigo', '=', $codigoCliente)->get();
    $Cliente = explode('"', $Cliente_nombre);
    $Cliente = $Cliente[3];
    return view('Produccion.orden_produccion.index', compact('ordenProducciones', 'id_pedido', 'Cliente'));
  }

  public function act_lista($var)
  {
    $ordenesProd = DB::table('detalle_orden_pedido', 'serie_modelo', 'serie_modelo', 'serie')
      ->select('serie_modelo.codigo','material.descrip_material','serie_modelo.codigo_modelo', 'serie_modelo.codigo_serie','serie.nombre_serie','detalle_orden_pedido.cantidades_ord_prod', 'detalle_orden_pedido.cantidades', 'serie_modelo.estado', 'detalle_orden_pedido.cantidades', 'serie.tallaFinal', 'serie.tallaInicial', 'detalle_orden_pedido.tipo_urgencia')
      ->join('serie_modelo', 'serie_modelo.codigo', '=', 'detalle_orden_pedido.codigo_serie_articulo')
      ->join('modelo','serie_modelo.codigo_modelo','=','modelo.cod_modelo')
      ->join('serie', 'serie.cod_serie', '=', 'serie_modelo.codigo_serie')
      ->join('material','modelo.cod_horma','=','material.cod_material')
      ->where('detalle_orden_pedido.codigo_orden_pedido', $var )
      ->get();
      return $ordenesProd;
  }
  public function create($cod_pedido)
  {
    $cliente = DB::table('orden_pedido', 'detalle_orden_pedido')
      ->select('orden_pedido.codigo_cliente', 'orden_pedido.estado_orden_pedido')
      ->where('orden_pedido.RUC_empresa', Auth::user()->RUC_empresa)
      ->where('orden_pedido.codigo_pedido', $cod_pedido)
      ->get();

    $infoEmpresa = Empresa::select('siglas')->where('empresa.RUC_empresa', Auth::user()->RUC_empresa)->get();

    $ordenesProd = DB::table('detalle_orden_pedido', 'serie_modelo', 'serie_modelo', 'serie')
      ->select('serie_modelo.codigo','material.descrip_material','serie_modelo.codigo_modelo', 'serie_modelo.codigo_serie','serie.nombre_serie','detalle_orden_pedido.cantidades_ord_prod', 'detalle_orden_pedido.cantidades', 'serie_modelo.estado', 'detalle_orden_pedido.cantidades', 'serie.tallaFinal', 'serie.tallaInicial', 'detalle_orden_pedido.tipo_urgencia')
      ->join('serie_modelo', 'serie_modelo.codigo', '=', 'detalle_orden_pedido.codigo_serie_articulo')
      ->join('modelo','serie_modelo.codigo_modelo','=','modelo.cod_modelo')
      ->join('serie', 'serie.cod_serie', '=', 'serie_modelo.codigo_serie')
      ->join('material','modelo.cod_horma','=','material.cod_material')
      ->where('detalle_orden_pedido.codigo_orden_pedido', $cod_pedido )
      ->get();

    $cantidadesGrupo = DB::table('detalle_orden_pedido', 'serie_modelo')
      ->select('detalle_orden_pedido.cantidades','detalle_orden_pedido.cantidades_ord_prod')
      ->join('serie_modelo', 'serie_modelo.codigo', '=', 'detalle_orden_pedido.codigo_serie_articulo')
      ->where('detalle_orden_pedido.codigo_orden_pedido', $cod_pedido)
      ->get();
    $cantidad_modelos=count($cantidadesGrupo);
    $cantidadesGrupo= json_decode($cantidadesGrupo);
    $tallas_pedidas;
    $suma_tallas;
    $cantidad_tallas;
    
    for ($i=0; $i < $cantidad_modelos; $i++) {
      $suma=0;
      $subCantidad[$i] =$cantidadesGrupo[$i]->cantidades;
      if ($cantidadesGrupo[$i]->cantidades_ord_prod==null) {
        $cantidad_op=[];
      }
      else {
        $cantidad_op=explode(",",$cantidadesGrupo[$i]->cantidades_ord_prod);
      }
      $cantidades=explode(",",$cantidadesGrupo[$i]->cantidades);
      $cantidades_tallas[$i]=count($cantidades);
      if (count($cantidad_op)==0) {
        for ($j=0; $j < count($cantidades); $j++) {
          $suma=$suma+(int)$cantidades[$j];
          $tallas_pedidas[$i][$j]=$cantidades[$j];
        }
      }
      else {
        for ($j=0; $j < count($cantidades); $j++) {
          $suma=$suma+((int)$cantidades[$j]-(int)$cantidad_op[$j]);
          $tallas_pedidas[$i][$j]=(int)$cantidades[$j]-(int)$cantidad_op[$j];
        }
      }
      $suma_tallas[$i]=$suma;
    }

    return view(
      'Produccion.orden_produccion.create',
      ["ordenesProd" => $ordenesProd, "codigoOrdenPed" => $cod_pedido, "cliente" => $cliente, "Cant" => $tallas_pedidas, "sumaSubTall" => $suma_tallas, "tamCant2" => $cantidad_modelos,"cantidad_tallas"=>$cantidades_tallas]
    );
  }
  public function store()
  {
    $codigo_serie_articulo = Input::get('codigo_serie');
    $cantidadesDetalleProduccion = Input::get('cantidadesDetProduccion');
    $cantidadesPedido=Input::get('cantidadesPedido');
    $urgencia=Input::get('tipo_urgencia');
    $orden_pedido=Input::get('orden_pedido');
    $fecha=Input::get('fecha');
    $hormas=Input::get('hormas_totales');
    $cants=explode(",",$cantidadesDetalleProduccion);
    $tmp_cantpedido=explode(",",$cantidadesPedido);
    $nueva_cantidad=[];
    $sumas=0;
    if ($cantidadesPedido==null) {
      $nueva_cantidad=$cants;
    }
    else {
      for ($i=0; $i < count($cants); $i++) {
        $sumas=$sumas+$cants[$i];
        $nueva_cantidad[$i]=(int)$tmp_cantpedido[$i]+(int)$cants[$i];
      }
    }
    if ($sumas<=intval($hormas)) {
      $infoEmpresa = Empresa::select('siglas')->where('empresa.RUC_empresa', Auth::user()->RUC_empresa)->get();
      $ord_prod=OrdenProduccion::where('RUC_empresa',Auth::user()->RUC_empresa)
      ->select('codigo_orden_pedido_produccion')
      ->orderby('fecha_creacion','desc')
      ->get();
      $codigo="";
      if (count($ord_prod)==0) {
        $codigo=$infoEmpresa[0]->siglas."-".date('Y')."-0001";
      }
      else {
        $codigo=$infoEmpresa[0]->siglas."-".date('Y');
        $tmp=explode("-",$ord_prod[0]);
        $num=(int)$tmp[2];
        $num++;
        $num=strval($num);
        switch (strlen($num)) {
          case 1:
            $codigo=$codigo."-000".$num;
          break;
          case 2:
            $codigo=$codigo."-00".$num;
          break;
          case 3:
            $codigo=$codigo."-0".$num;
          break;
          case 4:
            $codigo=$codigo."-".$num;
          break;
          default:
            // code...
            break;
        }
      }
      $nueva_orden_prod = new OrdenProduccion;
        $nueva_orden_prod->codigo_orden_pedido_produccion=$codigo;
        $nueva_orden_prod->codigo_orden_pedido=$orden_pedido;
        $nueva_orden_prod->RUC_empresa=Auth::user()->RUC_empresa;
        $nueva_orden_prod->codigo_serie_articulo=$codigo_serie_articulo;
        $nueva_orden_prod->cantidades=$cantidadesDetalleProduccion;
        $nueva_orden_prod->estado_orden_pedido_produccion=$urgencia;
        $nueva_orden_prod->fecha_aprox_entrega=$fecha;
      try {
        $nueva_orden_prod->save();
        $nueva_cantidad=implode(',',$nueva_cantidad);
        DetalleOrdenPedidoModel::where([
          ['codigo_orden_pedido', '=', $orden_pedido],
          ['codigo_serie_articulo', '=',$codigo_serie_articulo]])
          ->update(['cantidades_ord_prod' => $nueva_cantidad ]);
        echo $codigo_serie_articulo;
      } catch (\Exception $e) {
        echo "Error";
      }
    }
    else {
      echo "Error";
    }
  }

  public function show($id)
  {
    $ordenProducciones = OrdenProduccion::find($id);

    $datos = DB::Table('orden_pedido_produccion')
      ->join('serie_modelo', 'orden_pedido_produccion.codigo_serie_articulo', '=', 'serie_modelo.codigo')
      ->join('modelo', 'serie_modelo.codigo_modelo', '=', 'modelo.cod_modelo')
      ->join('material','modelo.cod_horma','=','material.cod_material')
      ->join('coleccion', 'modelo.cod_coleccion', '=', 'coleccion.codigo_coleccion')
      ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
      ->join('serie', 'serie_modelo.codigo_serie', '=', 'serie.cod_serie')
      ->where('serie_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
      ->where('orden_pedido_produccion.codigo_orden_pedido_produccion', '=', $id)
      //->select('orden_pedido_produccion.*','modelo.cod_modelo','coleccion.nombre_coleccion','linea.nombre_linea','serie.nombre_serie')
      ->select('serie_modelo.codigo','serie.tallaInicial','serie.tallaFinal','orden_pedido_produccion.estado_orden_pedido_produccion','orden_pedido_produccion.cantidades','material.descrip_material','orden_pedido_produccion.codigo_orden_pedido_produccion', 'serie_modelo.codigo_modelo', 'coleccion.nombre_coleccion', 'linea.nombre_linea', 'serie.nombre_serie', 'orden_pedido_produccion.fecha_aprox_entrega', 'orden_pedido_produccion.estado_orden_pedido_produccion')
      ->get();
      $componenetes=DB::table('costo_modelo')
      ->join('detalle_costo_modelo_materiales','costo_modelo.cod_costo_modelo','=','detalle_costo_modelo_materiales.cod_costo_modelo')
      ->join('area','detalle_costo_modelo_materiales.cod_area','=','area.cod_area')
      ->join('material','detalle_costo_modelo_materiales.cod_material','=','material.cod_material')
      ->where('modelo_serie','=',$datos[0]->codigo)
      ->select('material.descrip_material','area.descrip_area')
      ->get();
      $operaciones=DB::table('costo_modelo')
      ->join('detalle_costo_modelo_manoobra','costo_modelo.cod_costo_modelo','=','detalle_costo_modelo_manoobra.cod_costo_modelo')
      ->join('area','detalle_costo_modelo_manoobra.cod_area','=','area.cod_area')
      ->where('modelo_serie','=',$datos[0]->codigo)
      ->select('detalle_costo_modelo_manoobra.operacion','area.descrip_area')
      ->get();
    return view('Produccion.orden_produccion.verFichaProduccion', ["datos" => $datos,"componenetes"=>$componenetes,"operaciones"=>$operaciones], compact('ordenProducciones'));
  }
  public function edit($id)
  {
    /* return Redirect::to('logistica/clasificacion');*/ }
  public function update()
  {

    return Redirect::to('logistica/articulo');
  }
  public function destroy($id)
  {
    $data = OrdenProduccion::where('codigo_orden_pedido_produccion', '=', $id)->First();
    $data->update(['estado_orden_pedido_produccion' => '0']);
    return redirect()->back();
  }

  public function ajax()
  {
    $identificador = rand(10000, 99999);
    $serie = DB::table('serie')->where('estado_serie', '=', '1')->where('RUC_empresa', '=', Auth::user()->RUC_empresa)->get();

    $empresa = DB::table('empresa')->where('RUC_empresa', '=', Auth::user()->RUC_empresa)->get();

    $states = DB::table('linea')->where('estado_linea', '=', '1')->where('RUC_empresa', Auth::user()->RUC_empresa)->paginate(10);

    return view('Produccion.orden_produccion.create', ["states" => $states, "empresa" => $empresa, "identificador" => $identificador, "serie" => $serie]);
  }

  public function ajaxAct($id, $id2)
  {
    if ($id2 == 1) {
      $cities = DB::table('linea')

        ->join('linea_modelo', 'linea.cod_linea', '=', 'linea_modelo.cod_linea')
        ->join('modelo', 'linea_modelo.cod_modelo', '=', 'modelo.cod_modelo')
        ->where("estado_modelo", "=", "1")
        ->where("linea.cod_linea", $id)
        ->pluck("modelo.cod_modelo", "modelo.cod_modelo");

      return json_encode($cities);
    }
    if ($id2 == 2) {
      $cities = DB::table('modelo')


        ->where("modelo.cod_modelo", $id)

        ->pluck("nombre", "descripcion");

      return json_encode($cities);
    }
    if ($id2 == 3) {
      $cities = DB::table('serie')


        ->where("serie.cod_serie", $id)

        ->pluck("tallaInicial", "tallaFinal", "nombre_serie");

      return json_encode($cities);
    }
  }

  public function pdf667(request $request)
  {
    $email = Input::get('email');
    if ($email) {
      return redirect()->action(
        'PdfOrdenPController@create',
        ['codigo' => $email]
      );
    }
  }
  public function obt_hormas($var)
  {
    $horma=explode("-",$var);
    $hormas=DB::table('material')
    ->join('kardex_material','material.cod_material','=','kardex_material.cod_material')
    ->where('material.descrip_material','LIKE',$horma[0].'%')
    ->sum('kardex_material.stock_total');
    return $hormas;
  }
  public function pdf666(request $request)
  {

    $checkboxvar = Input::get('valor');
    if ($checkboxvar == null) {
      session()->flash('warning', 'Seleccione una o mas ordenes de produccion');

      return Redirect::to('Produccion/orden_produccion');
    }


    $str_arr = explode(",", $checkboxvar);

    //dd($request->all());
    //dd($str_arr);

    return redirect()->action(
      'PdfOrdenPController@create',
      ['checkboxvar' => $str_arr]
    );
  }
  public function pdf_orden($var)
  {
    echo $var;
  }
}
