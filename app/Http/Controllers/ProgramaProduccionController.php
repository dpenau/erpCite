<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use erpCite\Modelo;
use OrdenProduccion;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;

class ProgramaProduccionController extends Controller
{

	public function __construct()
	{
		$this->middleware('jefe');
	}
	public function index()
	{

		$orden_data = DB::table('grupo_trabajo','serie_modelo','orden_pedido_produccion')
		->join('orden_pedido_produccion','grupo_trabajo.codigo_orden_pedido_produccion','=','orden_pedido_produccion.codigo_orden_pedido_produccion')
		->join('serie_modelo','orden_pedido_produccion.codigo_serie_articulo','=','serie_modelo.codigo')
		->select('grupo_trabajo.codigo_grupo_trabajo','grupo_trabajo.especialidad','grupo_trabajo.tiempo','grupo_trabajo.proceso','serie_modelo.codigo_modelo')
		->get();

		$modelos = Modelo::select('cod_modelo','cod_coleccion','coleccion.nombre_coleccion')
		->join('coleccion','modelo.cod_coleccion','=','coleccion.codigo_coleccion')
		->where('modelo.RUC_empresa', Auth::user()->RUC_empresa )
		->get();


		$grupos = DB::table('grupo_trabajo')->select('codigo_grupo_trabajo')->get();

		$procesos = DB::table('grupo_trabajo')->distinct()->select('proceso')->get();

		$produccion = DB::table('orden_pedido_produccion')
		->distinct()->select('codigo_orden_pedido_produccion','RUC_empresa','codigo_serie_articulo','cantidades','estado_orden_pedido_produccion')->get();

		return view('Produccion.Programa_produccion.index', ['orden_data'=>$orden_data, 'modelos'=>$modelos, 'grupos'=>$grupos, 'procesos'=>$procesos, 'produccion'=>$produccion]);
	}

	public function create()
	{

	}
	public function store(Request $data)
	{

	}
	public function show()
	{
		/* return view('logistica.clasificacion.index',["clasificacion"=>$clasi]);*/
	}
	public function edit()
	{
		/* return Redirect::to('logistica/clasificacion');*/
	}
	public function update()
	{
	}
	public function destroy()
	{

	}


}
