<?php

namespace erpCite\Http\Controllers;

use erpCite\CostoModelo;
use erpCite\FichaMateriales;
use erpCite\Modelo;
use erpCite\ModeloCombinacion;
use erpCite\OperacionManoObra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class CombinacionFichaCalzadoController extends Controller
{
    var $id_ficha_producto_nueva = 0;
    public function __construct()
    {
        $this->middleware('desarrollo');
    }
    public function index($id)
    {

        $LineasModelos = DB::table('modelo')
            ->where('modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('modelo.cod_modelo', '=', $id)
            ->join('coleccion', 'modelo.cod_coleccion', '=', 'coleccion.codigo_coleccion')
            ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
            ->join('serie', 'modelo.cod_serie', '=', 'serie.cod_serie')
            //->join('material','modelo.cod_horma','=','material.cod_material')
            //->where('modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
            //->where(function($query){
            //  $query->orwhere('modelo.estado_modelo','=','0')
            //  ->orWhere('modelo.estado_modelo','=','1');
            //})
            ->get();
        $listaCombinacion = DB::table('combinacion_modelo')
            ->where('combinacion_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('combinacion_modelo.cod_modelo', '=', $id)
            ->where('estado_modelo', '=', 1)
            ->get();
        $seriemodelo = DB::Table('serie_modelo')
            ->join('serie', 'serie_modelo.codigo_serie', '=', 'serie.cod_serie')
            ->where('serie_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('serie_modelo.codigo_modelo', '=', $id)
            ->where('serie_modelo.estado', '=', '1')
            ->get();
        $todaserie = DB::table('serie')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->get();
        $capellada = DB::table('capellada')
            ->get();
        $Lineas = DB::table('linea')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_linea', '=', 1)
            ->get();
        $coleccion = DB::Table('coleccion')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_coleccion', '=', 1)
            ->get();
        $horma = DB::table('material')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('cod_subcategoria', '=', '969')
            ->where('t_compra', '=', '1')
            ->orderBy('descrip_material', 'asc')
            ->get();
        return view('Produccion.combinacion_ficha_calzado.index', compact('horma', 'listaCombinacion', 'todaserie', 'seriemodelo', 'capellada', 'Lineas', 'coleccion', 'LineasModelos'));
        //return view('Produccion.modelo.index');
    }
    public function ficha($id)
    {
        $id_ficha_producto_nueva = $id;
        $LineasModelos = DB::table('modelo')
            ->where('modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('modelo.cod_modelo', '=', 21)
            ->join('coleccion', 'modelo.cod_coleccion', '=', 'coleccion.codigo_coleccion')
            ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
            ->join('serie', 'modelo.cod_serie', '=', 'serie.cod_serie')
            //->join('material','modelo.cod_horma','=','material.cod_material')
            //->where('modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
            //->where(function($query){
            //  $query->orwhere('modelo.estado_modelo','=','0')
            //  ->orWhere('modelo.estado_modelo','=','1');
            //})
            ->get();
        $listaComb = DB::table('combinacion_modelo')
            ->where('combinacion_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('combinacion_modelo.cod_combinacion', '=', $id)
            ->join('modelo', 'combinacion_modelo.cod_modelo', '=', 'modelo.cod_modelo')
            ->join('coleccion', 'modelo.cod_coleccion', '=', 'coleccion.codigo_coleccion')
            ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
            ->join('serie', 'modelo.cod_serie', '=', 'serie.cod_serie')

            ->get();

        $seriemodelo = DB::Table('serie_modelo')
            ->join('serie', 'serie_modelo.codigo_serie', '=', 'serie.cod_serie')
            ->where('serie_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('serie_modelo.codigo_modelo', '=', $id)
            ->where('serie_modelo.estado', '=', '1')
            ->get();
        $todaserie = DB::table('serie')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->get();
        $capellada = DB::table('capellada')
            ->get();
        $Lineas = DB::table('linea')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_linea', '=', 1)
            ->get();
        $coleccion = DB::Table('coleccion')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_coleccion', '=', 1)
            ->get();
        $materialDirecto = DB::table('material')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('cod_subcategoria', '=', '969')
            ->where('t_compra', '=', '1')
            ->orderBy('descrip_material', 'asc')
            ->get();
        $empresa = Auth::user()->RUC_empresa;
        $fichaMaterial = DB::table('ficha_materiales')
            ->where('ficha_materiales.RUC_empresa', '=', $empresa)
            ->where('ficha_materiales.cod_modelo_comb', '=', $id)
            ->join('proceso', 'ficha_materiales.cod_proceso', '=', 'proceso.cod_proceso')
            ->join('material', 'ficha_materiales.cod_material', '=', 'material.cod_material')
            ->join('unidad_compra', 'material.unidad_medida', '=', 'unidad_compra.cod_unidad_medida')
            ->get();
        $fichaMaterialTodos = DB::table('ficha_materiales')
            ->where('ficha_materiales.RUC_empresa', '=', $empresa)
            ->join('proceso', 'ficha_materiales.cod_proceso', '=', 'proceso.cod_proceso')
            ->join('material', 'ficha_materiales.cod_material', '=', 'material.cod_material')
            ->join('combinacion_modelo', 'ficha_materiales.cod_modelo_comb', '=', 'combinacion_modelo.cod_combinacion')
            ->join('unidad_compra', 'material.unidad_medida', '=', 'unidad_compra.cod_unidad_medida')
            ->where('combinacion_modelo.aprobado', '=', 1)
            ->get();
        $number = 1;
        $id_comb = $id;
        $listaCombinacion = DB::table('combinacion_modelo')
            ->where('combinacion_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('estado_modelo', '=', 1)
            ->where('aprobado', '=', 1)
            ->get();
        return view('Produccion.combinacion_ficha_calzado.fichaindex', compact('id_comb', 'fichaMaterialTodos', 'listaCombinacion', 'fichaMaterial', 'number', 'materialDirecto', 'listaComb', 'todaserie', 'seriemodelo', 'capellada', 'Lineas', 'coleccion', 'LineasModelos'));
        //return view('Produccion.modelo.index');
    }

    public function getMaterialesDirectosFicha($id)
    {
        $empresa = Auth::user()->RUC_empresa;
        $fichaMaterial = DB::table('ficha_materiales')
            ->where('ficha_materiales.RUC_empresa', '=', $empresa)
            ->where('ficha_materiales.cod_modelo_comb', '=', $id)
            ->join('proceso', 'ficha_materiales.cod_proceso', '=', 'proceso.cod_proceso')
            ->join('material', 'ficha_materiales.cod_material', '=', 'material.cod_material')
            ->join('unidad_compra', 'material.unidad_medida', '=', 'unidad_compra.cod_unidad_medida')
            ->get();

        return response()->json(["ficha_materiales" => $fichaMaterial]);
    }

    public function getManoObraDirecta($id)
    {
        $operaciones = DB::table('mano_obra')
            ->where('mano_obra.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('mano_obra.cod_combinacion_modelo', '=', $id)
            ->where('operacion_directa.estado_registro','A')
            ->join('operacion_directa', 'mano_obra.cod_operacion_directa', '=', 'operacion_directa.cod_operacion_d')
            ->join('proceso', 'operacion_directa.cod_proceso', '=', 'proceso.cod_proceso')
            ->join('tipo_pago_nuevo', 'operacion_directa.cod_tipo_pago', '=', 'tipo_pago_nuevo.cod_pago')
            ->join('unidad_medida', 'tipo_pago_nuevo.cod_unidad_medida', '=', 'unidad_medida.cod_unidad_medida')
            ->get();

        return response()->json(["mano_obra_directas" => $operaciones]);
    }
    public function aprobar_m()
    {

        $codigos_operacion = Input::get('codigos_operacion');
        $codigos_operacion_d = Input::get('codigos_operacion_d');
        $codigo_combinacion = Input::get('codigo_combinacion');
        $cod_modelo_comb_e = Input::get('codigo_modelo');
        $cod_combinacion_m = Input::get('codigo_e');
        $cod_modelo_comb = Input::get('codigo');
        $idempresa = Auth::user()->RUC_empresa;
        $ficha_base = Input::get('ficha_base');
        $boton_aprobar = Input::get('boton_aprobar');
        $boton_guardar = Input::get('boton_guardar');

        if ($boton_guardar === "1") {
            if ($codigos_operacion == null) {
                $mano_obra_p = OperacionManoObra::where('cod_combinacion_modelo', $cod_modelo_comb)->delete();
                $modelo_combinacion_t = ModeloCombinacion::where('cod_combinacion', $cod_modelo_comb)
                    ->update(["costo_mano_directa" => 0 * 1]);
                return Redirect::to('/Produccion/combinacion_ficha/listado/' . $cod_modelo_comb_e);
            } else {
                $mano_obra_p = OperacionManoObra::where('cod_combinacion_modelo', $cod_modelo_comb)->delete();
                for ($i = 0; $i < count($codigos_operacion); $i++) {
                    $mano_obra_o = new OperacionManoObra();
                    $mano_obra_o->cod_combinacion_modelo = $cod_modelo_comb;
                    $mano_obra_o->cod_operacion_directa = $codigos_operacion[$i];
                    $mano_obra_o->RUC_empresa = $idempresa;
                    $mano_obra_o->save();
                }
                $total_mat = 0;
                $modelo_comb_o = DB::table('mano_obra')
                    ->where('mano_obra.cod_combinacion_modelo', '=', $cod_modelo_comb)
                    ->join('operacion_directa', 'mano_obra.cod_operacion_directa', '=', 'operacion_directa.cod_operacion_d')
                    ->get();

                foreach ($modelo_comb_o as $item) {
                    $total_mat = $total_mat + $item->costo_par;
                }
                $modelo_combinacion_t = ModeloCombinacion::where('cod_combinacion', $cod_modelo_comb)
                    ->update(["costo_mano_directa" => $total_mat * 1]);

                $modelo_comb = DB::table('combinacion_modelo')
                    ->where('combinacion_modelo.cod_combinacion', '=', $cod_modelo_comb)
                    ->get();
                foreach ($modelo_comb as $item) {
                    $aux = $item->cod_modelo;
                }

                return Redirect::to('/Produccion/combinacion_ficha/listado/' . $aux);
            }
        } elseif ($ficha_base === "2") {
            $mano_obra_po = OperacionManoObra::where('cod_combinacion_modelo', $codigo_combinacion)
                ->delete();

            for ($i = 0; $i < count($codigos_operacion_d); $i++) {
                $mano_obra_o = new OperacionManoObra();
                $mano_obra_o->cod_combinacion_modelo = $codigo_combinacion;
                $mano_obra_o->cod_operacion_directa = $codigos_operacion_d[$i];
                $mano_obra_o->RUC_empresa = $idempresa;
                $mano_obra_o->save();
            }
            $total_mat = 0;
            $modelo_comb_o = DB::table('mano_obra')
                ->where('mano_obra.cod_combinacion_modelo', '=', $codigo_combinacion)
                ->join('operacion_directa', 'mano_obra.cod_operacion_directa', '=', 'operacion_directa.cod_operacion_d')
                ->get();

            foreach ($modelo_comb_o as $item) {
                $total_mat = $total_mat + $item->costo_par;
            }
            $modelo_combinacion_t = ModeloCombinacion::where('cod_combinacion', $codigo_combinacion)
                ->update(["costo_mano_directa" => $total_mat * 1]);

            $modelo_comb = DB::table('combinacion_modelo')
                ->where('combinacion_modelo.cod_combinacion', '=', $codigo_combinacion)
                ->get();
            foreach ($modelo_comb as $item) {
                $aux = $item->cod_modelo;
            }

            return Redirect::to('/Produccion/combinacion_ficha/ficha_producto_m/' . $cod_combinacion_m);
        } elseif ($boton_aprobar === "0") {
            $modelo_combinacion = ModeloCombinacion::where('cod_combinacion', $cod_modelo_comb)
                ->update(["aprobado" => 1]);
            $modelo_comb = DB::table('combinacion_modelo')
                ->where('combinacion_modelo.cod_combinacion', '=', $cod_modelo_comb)
                ->get();

            $modelo_aprobado = Modelo::where('cod_modelo', $modelo_comb[0]->cod_modelo)
                ->get();

            $modelo_aprobado_s = Modelo::where('cod_modelo', $modelo_comb[0]->cod_modelo)
                ->update([
                    "comb_proceso" => ($modelo_aprobado[0]->comb_proceso) - 1,
                    "comb_aprobado" => ($modelo_aprobado[0]->comb_aprobado) + 1,
                ]);
            $modelo_comb = DB::table('combinacion_modelo')
                ->where('combinacion_modelo.cod_combinacion', '=', $cod_modelo_comb)
                ->get();
            foreach ($modelo_comb as $item) {
                $aux = $item->cod_modelo;
            }
            return Redirect::to('/Produccion/combinacion_ficha/listado/' . $aux);
        }
    }
    public function ficha_obra($id)
    {
        $operaciones = DB::table('mano_obra')
            ->where('mano_obra.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('mano_obra.cod_combinacion_modelo', '=', $id)
            ->join('operacion_directa', 'mano_obra.cod_operacion_directa', '=', 'operacion_directa.cod_operacion_d')
            ->join('proceso', 'operacion_directa.cod_proceso', '=', 'proceso.cod_proceso')
            ->join('tipo_pago_nuevo', 'operacion_directa.cod_tipo_pago', '=', 'tipo_pago_nuevo.cod_pago')
            ->join('unidad_medida', 'tipo_pago_nuevo.cod_unidad_medida', '=', 'unidad_medida.cod_unidad_medida')
            ->get();

        //ANTERIOR LO DE MATERIALES

        $LineasModelos = DB::table('modelo')
            ->where('modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('modelo.cod_modelo', '=', 21)
            ->join('coleccion', 'modelo.cod_coleccion', '=', 'coleccion.codigo_coleccion')
            ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
            ->join('serie', 'modelo.cod_serie', '=', 'serie.cod_serie')
            //->join('material','modelo.cod_horma','=','material.cod_material')
            //->where('modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
            //->where(function($query){
            //  $query->orwhere('modelo.estado_modelo','=','0')
            //  ->orWhere('modelo.estado_modelo','=','1');
            //})
            ->get();
        $listaComb = DB::table('combinacion_modelo')
            ->where('combinacion_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('combinacion_modelo.cod_combinacion', '=', $id)
            ->join('modelo', 'combinacion_modelo.cod_modelo', '=', 'modelo.cod_modelo')
            ->join('coleccion', 'modelo.cod_coleccion', '=', 'coleccion.codigo_coleccion')
            ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
            ->join('serie', 'modelo.cod_serie', '=', 'serie.cod_serie')

            ->get();

        $seriemodelo = DB::Table('serie_modelo')
            ->join('serie', 'serie_modelo.codigo_serie', '=', 'serie.cod_serie')
            ->where('serie_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('serie_modelo.codigo_modelo', '=', $id)
            ->where('serie_modelo.estado', '=', '1')
            ->get();
        $todaserie = DB::table('serie')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->get();
        $capellada = DB::table('capellada')
            ->get();
        $Lineas = DB::table('linea')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_linea', '=', 1)
            ->get();
        $coleccion = DB::Table('coleccion')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_coleccion', '=', 1)
            ->get();
        $materialDirecto = DB::table('material')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('cod_subcategoria', '=', '969')
            ->where('t_compra', '=', '1')
            ->orderBy('descrip_material', 'asc')
            ->get();
        $empresa = Auth::user()->RUC_empresa;
        $fichaMaterial = DB::table('ficha_materiales')
            ->where('ficha_materiales.RUC_empresa', '=', $empresa)
            ->where('ficha_materiales.cod_modelo_comb', '=', $id)
            ->join('proceso', 'ficha_materiales.cod_proceso', '=', 'proceso.cod_proceso')
            ->join('material', 'ficha_materiales.cod_material', '=', 'material.cod_material')
            ->join('unidad_compra', 'material.unidad_medida', '=', 'unidad_compra.cod_unidad_medida')
            ->get();
        $fichaMaterialTodos = DB::table('mano_obra')
            ->where('mano_obra.RUC_empresa', '=', $empresa)
            ->join('operacion_directa', 'mano_obra.cod_operacion_directa', '=', 'operacion_directa.cod_operacion_d')
            ->join('proceso', 'operacion_directa.cod_proceso', '=', 'proceso.cod_proceso')
            ->join('tipo_pago_nuevo', 'operacion_directa.cod_tipo_pago', '=', 'tipo_pago_nuevo.cod_pago')
            ->join('unidad_medida', 'tipo_pago_nuevo.cod_unidad_medida', '=', 'unidad_medida.cod_unidad_medida')
            ->join('combinacion_modelo', 'mano_obra.cod_combinacion_modelo', '=', 'combinacion_modelo.cod_combinacion')
            ->where('combinacion_modelo.aprobado', '=', 1)
            ->get();
        $number = 1;
        $id_comb = $id;
        $listaCombinacion = DB::table('combinacion_modelo')
            ->where('combinacion_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('estado_modelo', '=', 1)
            ->where('aprobado', '=', 1)
            ->get();
        return view('Produccion.combinacion_ficha_calzado.fichaindexm', compact('operaciones', 'id_comb', 'fichaMaterialTodos', 'listaCombinacion', 'fichaMaterial', 'number', 'materialDirecto', 'listaComb', 'todaserie', 'seriemodelo', 'capellada', 'Lineas', 'coleccion', 'LineasModelos'));
        //return view('Produccion.modelo.index');
    }
    public function material($id)
    {

        $id_combinacion = $id;
        $proceso = DB::Table('proceso')
            ->where('operacion', 1)
            ->where('estado_proceso', '=', 1)
            ->get();

        $categoria = DB::Table('categoria')
            ->where('estado_categoria', '=', 1)
            ->get();

        $subcategoria = DB::Table('subcategoria')
            ->where('estado_subcategoria', '=', 1)
            ->orderBy('nom_subcategoria', 'asc')
            ->get();

        $materiales = DB::Table('material')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('estado_material', '=', 1)
            ->join('unidad_compra', 'material.unidad_medida', '=', 'unidad_compra.cod_unidad_medida')
            ->orderBy('descrip_material', 'asc')
            ->get();

        return view('Produccion.combinacion_ficha_calzado.fichamaterial', compact('proceso', 'categoria', 'subcategoria', 'materiales', 'id_combinacion'));
        //return view('Produccion.modelo.index');
    }
    public function mano_obra($id)
    {

        $id_combinacion = $id;
        $proceso = DB::Table('proceso')
            ->where('operacion', '=', 1)
            ->where('estado_proceso', '=', 1)
            ->get();

        $operacion_directa = DB::Table('operacion_directa')
            ->where('operacion_directa.RUC_empresa', Auth::user()->RUC_empresa)
            ->where('operacion_directa.estado_operacion', '=', 1)
            ->where('operacion_directa.estado_registro', 'A')
            ->join('proceso', 'operacion_directa.cod_proceso', '=', 'proceso.cod_proceso')
            ->join('tipo_pago_nuevo', 'operacion_directa.cod_tipo_pago', '=', 'tipo_pago_nuevo.cod_pago')
            ->get();

        return view('Produccion.combinacion_ficha_calzado.fichamano', compact('proceso', 'operacion_directa', 'id_combinacion'));
        //return view('Produccion.modelo.index');
    }

    public function actualizar_materiales($id)
    {

        $id_combinacion = $id;
        $cantidadMat = Input::get('codigo');
        $cantidadTotal = Input::get('cantidad_total');
        if ($cantidadMat != null) {
            for ($i = 0; $i < count($cantidadMat); $i++) {
                if ($cantidadMat[$i] != null) {
                    $modelo_comb_actualizado = FichaMateriales::where('cod_combinacion', $id_combinacion)
                        ->update([
                            'consumo_real' => $cantidadMat[$i],
                            'costo_por_par' => $cantidadTotal[$i],
                        ]);
                }
            }
        }
        return Redirect::to('/Produccion/combinacion_ficha/ficha_producto/' . $id_combinacion);
    }

    public function create()
    {
        $total_mat = Input::get('total_mat');
        $idempresa = Auth::user()->RUC_empresa;
        $cod_modelo_comb = Input::get('codigo');
        $proceso = Input::get('process');
        $material = Input::get('material');
        $cantidadMat = Input::get('cantidad_material');
        $cantidadTotal = Input::get('cantidad_total');
        $id_combinacion_c = Input::get('id_combinacion_c');

        if ($proceso == null) {
            session()->flash('error', 'Debe seleccionar al menos un material');
            return Redirect::to('/Produccion/combinacion_ficha/ficha_material/' . $id_combinacion_c);
        }

        for ($i = 0; $i < count($proceso); $i++) {
            $search_f = DB::table('ficha_materiales')
                ->where('ficha_materiales.cod_modelo_comb', '=', $cod_modelo_comb)
                ->where('ficha_materiales.cod_proceso', '=', $proceso[$i])
                ->where('ficha_materiales.cod_material', '=', $material[$i])
                ->where('ficha_materiales.RUC_empresa', '=', $idempresa)
                ->get();

            if ($search_f->isEmpty()) {
                $ficha_materiales = new FichaMateriales();
                $ficha_materiales->cod_modelo_comb = $cod_modelo_comb;
                $ficha_materiales->cod_proceso = $proceso[$i];
                $ficha_materiales->cod_material = $material[$i];
                $ficha_materiales->consumo_real = $cantidadMat[$i];
                $ficha_materiales->costo_por_par = $cantidadTotal[$i];
                $ficha_materiales->RUC_empresa = $idempresa;
                $ficha_materiales->save();
            } else {
                continue;
            }
        }

        $modelo_comb = DB::table('combinacion_modelo')
            ->where('combinacion_modelo.cod_combinacion', '=', $cod_modelo_comb)
            ->get();

        $f_comb_actualizado = FichaMateriales::where('cod_modelo_comb', $cod_modelo_comb)
            ->get();
        $total_aux = 0;
        foreach ($f_comb_actualizado as $item) {
            $total_aux = $total_aux + $item->costo_por_par;
        }

        $modelo_combinacion_t = ModeloCombinacion::where('cod_combinacion', $cod_modelo_comb)
            ->update(["costo_material_directo" => $total_aux]);
        return Redirect::to('/Produccion/combinacion_ficha/ficha_producto/' . $cod_modelo_comb);
    }
    public function create_mano()
    {
        $operaciones = Input::get('operacion');
        $idempresa = Auth::user()->RUC_empresa;
        $cod_modelo_comb = Input::get('codigo');
        $proceso = Input::get('process');
        $total_mat = 0;
        $id_combinacion_c = Input::get('id_combinacion_c');

        if ($operaciones == null) {
            session()->flash('error', 'Debe seleccionar al menos una operación');
            return Redirect::to('/Produccion/combinacion_ficha/ficha_obra/' . $id_combinacion_c);
        }

        for ($i = 0; $i < count($operaciones); $i++) {
            $search_f = DB::table('mano_obra')
                ->where('mano_obra.cod_combinacion_modelo', '=', $cod_modelo_comb)
                ->where('mano_obra.cod_operacion_directa', '=', $operaciones[$i])
                ->where('mano_obra.RUC_empresa', '=', $idempresa)
                ->get();
            if ($search_f->isEmpty()) {

                $combinacion_operacion_d = DB::table('operacion_directa')
                    ->where('operacion_directa.cod_operacion_d', '=', $operaciones[$i])
                    ->get();
                $total_mat = $total_mat + ($combinacion_operacion_d[0]->costo_par) * 1;
                $mano_obra_o = new OperacionManoObra();
                $mano_obra_o->cod_combinacion_modelo = $cod_modelo_comb;
                $mano_obra_o->cod_operacion_directa = $operaciones[$i];
                $mano_obra_o->RUC_empresa = $idempresa;
                $mano_obra_o->save();
            } else {
                continue;
            }
        }

        $modelo_comb = DB::table('combinacion_modelo')
            ->where('combinacion_modelo.cod_combinacion', '=', $cod_modelo_comb)
            ->get();
        $modelo_combinacion_t = ModeloCombinacion::where('cod_combinacion', $cod_modelo_comb)
            ->update(["costo_mano_directa" => ($modelo_comb[0]->costo_mano_directa) + $total_mat * 1]);

        return Redirect::to('/Produccion/combinacion_ficha/ficha_producto_m/' . $cod_modelo_comb);
    }
    public function aprobar()
    {
        $total_materiales = Input::get('total_mat');
        $modificar = Input::get('guardar_materiales');
        $ficha_base = Input::get('ficha_base');
        $idempresa = Auth::user()->RUC_empresa;
        $cod_modelo_comb = Input::get('codigo');
        $cod_modelo_comb_e = Input::get('codigo_e');
        $codigo_modelo_c = Input::get('codigo_modelo');
        $boton_aprobar = Input::get('boton_aprobar');
        $cod_modelo_comb_ficha = Input::get('codigo_ficha');
        $cantidadMat = Input::get('cantidad_material');
        $cantidadTotal = Input::get('cantidad_total');
        // SI ESQUE SE PRECIONA EL BOTON EDITAR SE ACTIVAN IMPUTS AQUI SE ACTUALIZAN DE ESTE MISMO MODELO

        $material_F = Input::get('material_ficha');
        $email = Input::get('email');
        $estado = Input::get('estado');

        /*  if($modificar==="55"){

            $cod_modelo = Input::get('codigo_material');
            dd($cod_modelo);
            $cod_combina = Input::get('cod_combina');
            $act = FichaMateriales::where('cod_ficha_m', $cod_modelo)->delete();
            session()->flash('success', 'Material en Ficha de Producto eliminado satisfactoriamente' );
            return Redirect::to('Produccion/combinacion_ficha/ficha_producto/'.$cod_combina);
        }
        else*/
        if ($modificar === "1" && $cantidadMat != null) {
            for ($i = 0; $i < count($cantidadMat); $i++) {
                if ($cantidadMat[$i] != null) {
                    $modelo_comb_actualizado = FichaMateriales::where('cod_ficha_m', $cod_modelo_comb_ficha[$i])
                        ->update([
                            'consumo_real' => $cantidadMat[$i],
                            'costo_por_par' => $cantidadTotal[$i],
                        ]);
                }
            }
            $modelo_comb = DB::table('combinacion_modelo')
                ->where('combinacion_modelo.RUC_empresa', '=', $idempresa)
                ->where('combinacion_modelo.cod_combinacion', '=', $cod_modelo_comb)
                ->get();

            $f_comb_actualizado = FichaMateriales::where('cod_modelo_comb', $cod_modelo_comb)
                ->get();
            $total_aux = 0;
            foreach ($f_comb_actualizado as $item) {
                $total_aux = $total_aux + $item->costo_por_par;
            }

            $modelo_combinacion_t = ModeloCombinacion::where('cod_combinacion', $cod_modelo_comb)
                ->update(["costo_material_directo" => $total_aux]);
        }
        // CUANDO SE PRESIONA LA CARGA DE FICHA-MATERIAL DE OTRA COMBINACION DEL MISMO MODELO U OTRO
        // BORRANDO LO ANTERIOR Y ACTUALIZANDO CON LO NUEVO
        elseif ($ficha_base === "2") {
            $materiales_cant = 0;

            $act = FichaMateriales::where('cod_modelo_comb', $cod_modelo_comb_e)->delete();

            for ($i = 0; $i < count($cod_modelo_comb_ficha); $i++) {

                $auxiliar_m = FichaMateriales::where('cod_ficha_m', (int)$cod_modelo_comb_ficha[$i])->get();

                $ficha_materiales = new FichaMateriales();
                $ficha_materiales->cod_modelo_comb = $cod_modelo_comb_e;
                $ficha_materiales->cod_proceso = $auxiliar_m[0]->cod_proceso;
                $ficha_materiales->cod_material = $auxiliar_m[0]->cod_material;
                $ficha_materiales->consumo_real = $auxiliar_m[0]->consumo_real;
                $ficha_materiales->costo_por_par = $auxiliar_m[0]->costo_por_par;
                $ficha_materiales->RUC_empresa = $auxiliar_m[0]->RUC_empresa;
                $ficha_materiales->save();
                $materiales_cant = $materiales_cant + $auxiliar_m[0]->costo_por_par;
            }

            $modelo_combinacion = ModeloCombinacion::where('cod_combinacion', $cod_modelo_comb_e)
                ->update([
                    "costo_material_directo" => $materiales_cant,

                ]);

            return Redirect::to('/Produccion/combinacion_ficha/ficha_producto/' . $cod_modelo_comb_e);
        } elseif ($modificar === "1") {
            $materiales_cant = 0;
            $arreglo[] = 0;

            if ($cod_modelo_comb_ficha == null) {
                dd("entro if");
                //$act = FichaMateriales::where('cod_modelo_comb', $cod_modelo_comb_e)->delete();
                $modelo_combinacion = ModeloCombinacion::where('cod_combinacion', $cod_modelo_comb_e)
                    ->update([
                        "costo_material_directo" => $materiales_cant,

                    ]);
                return Redirect::to('/Produccion/combinacion_ficha/listado/' . $codigo_modelo_c);
            } else {
                for ($i = 0; $i < count($cod_modelo_comb_ficha); $i++) {

                    $auxiliar_m = FichaMateriales::where('cod_ficha_m', (int)$cod_modelo_comb_ficha[$i])->get();

                    $ficha_materiales = new FichaMateriales();
                    $ficha_materiales->cod_modelo_comb = $cod_modelo_comb_e;
                    $ficha_materiales->cod_proceso = $auxiliar_m[0]->cod_proceso;
                    $ficha_materiales->cod_material = $auxiliar_m[0]->cod_material;
                    $ficha_materiales->consumo_real = $auxiliar_m[0]->consumo_real;
                    $ficha_materiales->costo_por_par = $auxiliar_m[0]->costo_por_par;
                    $ficha_materiales->RUC_empresa = $auxiliar_m[0]->RUC_empresa;
                    $arreglo[$i] = $ficha_materiales;
                    //$ficha_materiales->save();

                }
                $act = FichaMateriales::where('cod_modelo_comb', $cod_modelo_comb_e)->delete();

                for ($i = 0; $i < count($arreglo); $i++) {

                    $ficha_materiales = new FichaMateriales();
                    $ficha_materiales->cod_modelo_comb = $cod_modelo_comb_e;
                    $ficha_materiales->cod_proceso = $arreglo[$i]->cod_proceso;
                    $ficha_materiales->cod_material = $arreglo[$i]->cod_material;
                    $ficha_materiales->consumo_real = $arreglo[$i]->consumo_real;
                    $ficha_materiales->costo_por_par = $arreglo[$i]->costo_por_par;
                    $ficha_materiales->RUC_empresa = $arreglo[$i]->RUC_empresa;
                    $ficha_materiales->save();
                    $materiales_cant = $materiales_cant + $arreglo[$i]->costo_por_par;
                }

                $modelo_combinacion = ModeloCombinacion::where('cod_combinacion', $cod_modelo_comb_e)
                    ->update([
                        "costo_material_directo" => $materiales_cant,

                    ]);

                return Redirect::to('/Produccion/combinacion_ficha/listado/' . $codigo_modelo_c);
            }
        }
        // CUANDO SE PASA DE MODELO EN PROCESO A APROBADO
        elseif ($boton_aprobar === "0") {

            $modelo_combinacion = ModeloCombinacion::where('cod_combinacion', $cod_modelo_comb)
                ->update(["aprobado" => 1]);
            $modelo_comb = DB::table('combinacion_modelo')
                ->where('combinacion_modelo.cod_combinacion', '=', $cod_modelo_comb)
                ->get();

            $modelo_aprobado = Modelo::where('cod_modelo', $modelo_comb[0]->cod_modelo)
                ->get();

            $modelo_aprobado_s = Modelo::where('cod_modelo', $modelo_comb[0]->cod_modelo)
                ->update([
                    "comb_proceso" => ($modelo_aprobado[0]->comb_proceso) - 1,
                    "comb_aprobado" => ($modelo_aprobado[0]->comb_aprobado) + 1,
                ]);
        }

        $modelo_comb = DB::table('combinacion_modelo')
            ->where('combinacion_modelo.cod_combinacion', '=', $cod_modelo_comb)
            ->get();
        foreach ($modelo_comb as $item) {
            $aux = $item->cod_modelo;
        }

        return Redirect::to('/Produccion/combinacion_ficha/listado/' . $aux);
    }
    public function proceso()
    {
        $idempresa = Auth::user()->RUC_empresa;
        $cod_modelo_comb = Input::get('codigo');
        $codigo_modelo = Input::get('codigo_modelo');

        $modelo_combinacion = ModeloCombinacion::where('cod_combinacion', $cod_modelo_comb)
            ->update(["aprobado" => 0]);

        $modelo_comb = DB::table('combinacion_modelo')
            ->where('combinacion_modelo.cod_combinacion', '=', $cod_modelo_comb)
            ->get();

        $modelo_aprobado = Modelo::where('cod_modelo', $modelo_comb[0]->cod_modelo)
            ->get();

        $modelo_aprobado_s = Modelo::where('cod_modelo', $modelo_comb[0]->cod_modelo)
            ->update([
                "comb_proceso" => ($modelo_aprobado[0]->comb_proceso) + 1,
                "comb_aprobado" => ($modelo_aprobado[0]->comb_aprobado) - 1
            ]);

        foreach ($modelo_comb as $item) {
            $aux2 = $item->cod_modelo;
        }

        return Redirect::to('/Produccion/combinacion_ficha/listado/' . $aux2);
    }
    public function store(Request $data)
    {

        return Redirect::to('Produccion/modelos_calzado');
    }
    public function show($id)
    {

        $empresa = Auth::user()->RUC_empresa;
        $cod_modelo_comb = Input::get('codCom');
        $descripcion = Input::get('descripCom');
        $cod_modelo = Input::get('codigo');
        $ModeloCombinacionCalzado = new ModeloCombinacion;
        if (!file_exists('photo/modelos/' . $empresa)) {
            mkdir('photo/modelos/' . $empresa);
        }
        $sigla = DB::table('empresa')->where('RUC_empresa', Auth::user()->RUC_empresa)->get();
        $var = Input::get('codigo');
        $siglax = $sigla[0]->siglas;
        $res = $siglax . '-' . $var . '-' . $cod_modelo_comb . $empresa;
        $photo = "";
        $destination = 'photo/modelos/' . $empresa;
        $file = Input::file('photo');
        $extension = $file->getClientOriginalExtension();
        $filename = $res . Input::get('photo') . "." . $extension;
        $file->move($destination, $filename);
        $photo = $filename;
        $ModeloCombinacionCalzado->codigo_comb = $cod_modelo_comb;
        $ModeloCombinacionCalzado->descripcion = $descripcion;
        $ModeloCombinacionCalzado->imagen = $photo;
        $ModeloCombinacionCalzado->cod_modelo = $cod_modelo;
        $ModeloCombinacionCalzado->estado_modelo = 1;
        $ModeloCombinacionCalzado->RUC_empresa = $empresa;
        $ModeloCombinacionCalzado->save();

        $modelo_actualizado = Modelo::where('cod_modelo', $cod_modelo)->first();
        $modelo_actualizado = Modelo::where('cod_modelo', $cod_modelo)
            ->update([
                'num_combinacion' => ($modelo_actualizado->num_combinacion) + 1,
            ]);
        return Redirect::to('/Produccion/combinacion_calzado/listado/' . $cod_modelo);
    }
    public function edit($id)
    {
        /* return Redirect::to('logistica/clasificacion');*/
    }
    public function update(Request $data)
    {
        //Codigo de modelo PK
        $cod_modelo = Input::get('codigo');
        //codigo de combinacion a editar PK
        $cod_modelo_combinacion = Input::get('codigo_combinacion');
        //Codigo ingresado por teclado por el usuario
        $codigo_combinacion = Input::get('codCom');
        //Descripcion digitada por el cliente
        $descripcion_combinacion = Input::get('descripCom');

        $sigla = DB::table('empresa')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->get();
        $var = Input::get('codigo_modelo');
        $siglax = $sigla[0]->siglas;
        $empresa = Auth::user()->RUC_empresa;
        $res = $siglax . '-' . $var . '-' . $cod_modelo_combinacion . $empresa;

        if (!file_exists('photo/modelos/' . $empresa)) {
            mkdir('photo/modelos/' . $empresa);
        }
        $photo = '';
        $destination = 'photo/modelos/' . $empresa;
        $file = $data->photo;
        if ($file != "") {
            $extension = $file->getClientOriginalExtension();
            $filename = $res . Input::get('photo') . '.' . $extension;
            $file->move($destination, $filename);
            $photo = $filename;

            $modelo_comb_actualizado = ModeloCombinacion::where('cod_combinacion', $cod_modelo_combinacion)
                ->update([
                    "codigo_comb" => $codigo_combinacion,
                    'descripcion' => $descripcion_combinacion,
                    'imagen' => $photo
                ]);
        } else {

            $modelo_comb_actualizado = ModeloCombinacion::where('cod_combinacion', $cod_modelo_combinacion)
                ->update([
                    'codigo_comb' => $codigo_combinacion,
                    'descripcion' => $descripcion_combinacion,
                ]);
        }

        session()->flash('success', 'Combinación de Modelo Actualizado');
        return Redirect::to('/Produccion/combinacion_calzado/listado/' . $cod_modelo);
    }
    public function destroy()
    {
        $material_F = Input::get('material_ficha');
        $email = Input::get('email');
        $estado = Input::get('estado');
        $cod_modelo = Input::get('codigo_material');
        if ($material_F == 10) {
            $cod_combina = Input::get('cod_combina');
            // $act = FichaMateriales::where('cod_ficha_m', $cod_modelo)->delete();
            session()->flash('success', 'Material en Ficha de Producto eliminado satisfactoriamente');
            return Redirect::to('Produccion/combinacion_ficha/ficha_producto/' . $cod_combina);
        } else {
            if ($estado == 0) {
                $mensaje = "Desactivado";
            } else {
                $mensaje = "Activado";
            }
            $act = ModeloCombinacion::where('cod_combinacion', $email)
                ->update(['estado_modelo' => $estado]);
            session()->flash('success', 'Codigo de Combinación de Modelo ' . $mensaje);
            return Redirect::to('/Produccion/combinacion_calzado/listado/' . $cod_modelo);
        }
    }
    public function ficha_producto()
    {
        $cambio = DB::table("configuracion_valores")
            ->where('codigo_valor', '=', 1)
            ->get();
        $modelos = DB::table('serie_modelo')
            ->join("modelo", 'serie_modelo.codigo_modelo', '=', 'modelo.cod_modelo')
            ->join('material', 'modelo.cod_horma', '=', 'material.cod_material')
            ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
            ->join('coleccion', 'modelo.cod_coleccion', '=', 'codigo_coleccion')
            ->join("serie", 'serie_modelo.codigo_serie', '=', 'serie.cod_serie')
            ->leftJoin('costo_modelo', 'serie_modelo.codigo', '=', 'costo_modelo.modelo_serie')
            ->where('serie_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where(function ($query) {
                $query->orWhere('serie_modelo.estado', '=', '1');
            })
            ->whereNull('costo_modelo.estado')
            ->select('serie_modelo.codigo', 'costo_modelo.estado', 'costo_modelo.cod_costo_modelo', 'modelo.imagen', 'serie_modelo.codigo_modelo', 'serie.nombre_serie', 'linea.nombre_linea', 'coleccion.nombre_coleccion', 'material.descrip_material', 'serie_modelo.RUC_empresa', 'serie.tallaInicial', 'serie.tallaFinal')
            ->get();
        $categorias = DB::table('categoria')
            ->where('estado_categoria', '=', '1')
            ->select('nom_categoria', 'cod_categoria')
            ->orderBy('nom_categoria', 'asc')
            ->get();
        $subcategoria = DB::table('subcategoria')
            ->where('estado_subcategoria', '=', '1')
            ->select('nom_subcategoria', 'cod_subcategoria', 'cod_categoria')
            ->orderBy('nom_subcategoria', 'asc')
            ->get();
        $costeo = DB::table('costo_modelo')
            ->join('serie_modelo', 'costo_modelo.modelo_serie', '=', 'serie_modelo.codigo')
            ->join('serie', 'serie_modelo.codigo_serie', '=', 'serie.cod_serie')
            ->where('costo_modelo.estado', '=', '1')
            ->where('serie_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->select('serie_modelo.codigo', 'serie_modelo.codigo_modelo', 'serie.nombre_serie')
            ->get();
        return view("Produccion.modelos_calzado.costeo", ["cambio" => $cambio, "categoria" => $categorias, "subcategoria" => $subcategoria, "costeo" => $costeo, "modelos" => $modelos]);
    }

    //PDF
    public function descarga($var)
    {
        $query = trim($var);
        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('a4', 'landscape');
        $pdf->loadHTML($this->convert_data($query));
        return $pdf->stream();
    }
    //OBTENER DATA
    public function get_data_cabecera($query)
    {
        $orden_data = CostoModelo::where('modelo_serie', $query)
            ->join('serie_modelo', 'costo_modelo.modelo_serie', '=', 'serie_modelo.codigo')
            ->join('modelo', 'serie_modelo.codigo_modelo', '=', 'modelo.cod_modelo')
            ->join('serie', 'serie_modelo.codigo_serie', '=', 'serie.cod_serie')
            ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
            ->join('coleccion', 'modelo.cod_coleccion', '=', 'coleccion.codigo_coleccion')
            ->join('material', 'modelo.cod_horma', '=', 'material.cod_material')
            ->select('serie_modelo.codigo', 'costo_modelo.estado', 'costo_modelo.cod_costo_modelo', 'modelo.imagen', 'serie_modelo.codigo_modelo', 'serie.nombre_serie', 'linea.nombre_linea', 'coleccion.nombre_coleccion', 'material.descrip_material', 'serie_modelo.RUC_empresa', 'serie.tallaInicial', 'serie.tallaFinal')
            ->get();
        return $orden_data;
    }

    public function get_imagen()
    {
        $idempresa = Auth::user()->RUC_empresa;
        $imagen = DB::table('empresa')->where('RUC_empresa', '=', $idempresa)->limit(1)->get();
        return $imagen;
    }
    //DISEÑAR PDF
    public function convert_data($query)
    {
        $cabecera = $this->get_data_cabecera($query);
        if (count($cabecera) > 0) {
            $detalle = $this->get_data_materiales($cabecera[0]->cod_costo_modelo);
            $detalle2 = $this->get_data_mano($cabecera[0]->cod_costo_modelo);
            $img = $this->get_imagen();
            $photo = "";
            foreach ($img as $i) {
                if ($i->imagen != "") {
                    $photo = $i->imagen;
                }
            }
            $output = '<html><head><style>
            @page {
                  margin: 0cm 0cm;
            }
            body {
                  margin-top: 4cm;
                  margin-left: 2cm;
                  margin-right: 2cm;
                  margin-bottom: 2cm;
            }
            header {

                  position: fixed;
                  top: 0.5cm;
                  left: 0.5cm;
                  right: 0cm;
                  height: 3cm;
            }
            footer {
                  margin-right: 0cm;
                  position: fixed;
                  bottom: 0cm;
                  left: 0cm;
                  right: 0cm;
                  height: 2cm;
            }
            </style></head><body>';
            $idempresa = Auth::user()->RUC_empresa;
            $cantidad_mat = count($detalle) + 2;
            $cantidad_mano = count($detalle2) + 1;
            $rows = $cantidad_mat + $cantidad_mano;
            $tallabase = (floatval($cabecera[0]->tallaFinal) + floatval($cabecera[0]->tallaInicial)) / 2;
            $horma = explode("-", $cabecera[0]->descrip_material);
            if ($photo != "") {
                $output .= '
              <header>
              <table align="center" border="1" style="width:90%">
                <tr align="center">
                  <th  rowspan="4"><img  src="photo/' . $photo . '" alt="" style="width:120px;" class="img-rounded center-block"></th>
                  <th  colspan="6"><div align="center">Ficha Tecnica de Prototipado</div></th>
                </tr>
                <tr align="center">
                  <th><strong>Modelo:</strong></th>
                  <td>' . $cabecera[0]->codigo_modelo . '</td>
                  <th><strong>Linea:</strong></th>
                  <td>' . $cabecera[0]->nombre_linea . '</td>
                  <th><strong>Coleccion:</strong></th>
                  <td>' . $cabecera[0]->nombre_coleccion . '</td>
                </tr>
                <tr align="center">
                  <th><strong>Horma:</strong></th>
                  <td>' . $horma[0] . '</td>
                  <th><strong>Version:</strong></th>
                  <td>00</td>
                  <th><strong>Talla base:</strong></th>
                  <td>' . $tallabase . '</td>
                </tr>
                <tr align="center">
                  <th><strong>Elaborado por:</strong></th>
                  <td></td>
                  <th><strong>Aprobado por:</strong></th>
                  <td></td>
                  <th><strong>Firma Aprobacion:</strong></th>
                  <td></td>
                </tr>
              </table>
              </header>
              <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
              ';
            } else {
                $output .= '
              <header>
              <table align="center" border="1" style="width:90%">
                <tr align="center">
                  <th  rowspan="3">Sin Logo</th>
                  <th  colspan="6"><div align="center">Ficha Tecnica de Prototipado</div></th>
                </tr>
                <tr align="center">
                  <th><strong>Modelo:</strong></th>
                  <td>' . $cabecera[0]->codigo_modelo . '</td>
                  <th><strong>Linea:</strong></th>
                  <td>' . $cabecera[0]->nombre_linea . '</td>
                  <th><strong>Coleccion:</strong></th>
                  <td>' . $cabecera[0]->nombre_coleccion . '</td>
                </tr>
                <tr align="center">
                  <th><strong>Horma:</strong></th>
                  <td>' . $horma[0] . '</td>
                  <th><strong>Version:</strong></th>
                  <td>00</td>
                  <th><strong>Talla base:</strong></th>
                  <td>' . $tallabase . '</td>
                </tr>
              </table>
              </header>
              <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
              ';
            }
            $output .= '
            <br>
            <table align="center" border="1" style="width:100%">';
            $output .= '<tr align="center">
                <th rowspan="' . $rows . '"><img  src="photo/modelos/' . $idempresa . '/' . $cabecera[0]->imagen . '" alt="" style="width:125px" class="img-rounded center-block"></th>
                <th>Area</th>
                <th>Material</th>
                <th>Precio S/.</th>
                <th>Consumo</th>
                <th>Medida</th>
                <th>Costo_Par S/.</th>
              </tr>';
            //echo $detalle;
            $total_directo = 0;
            $lista_area = ["P-Corte", "P-Habilitado", "P-Aparado", "P-Alistado", "P-Montaje", "P-Acabado"];
            for ($i = 0; $i < count($lista_area); $i++) {
                foreach ($detalle as $det) {
                    if ($lista_area[$i] == $det->descrip_area) {
                        $precio = $det->costo_sin_igv_material;
                        if ($det->t_moneda == 1) {
                            $precio = $precio * 3.33;
                        }
                        $output .= '
                  <tr>
                  <td>' . $det->descrip_area . '</td>
                  <td>' . $det->descrip_material . '</td>
                  <td>' . $precio . '</td>
                  <td>' . $det->consumo_por_par . '</td>
                  <td>' . $det->descrip_unidad_medida . '</td>
                  <td>' . number_format($det->total, 5, '.', '') . '</td>
                  </tr>';
                        $total_directo = $total_directo + $det->total;
                    }
                }
            }
            $output .= '<tr><td colspan="5">Total</td><td style="background-color:yellow">' . $total_directo . '</td></tr>
            <tr>
            <th>Area</th>
            <th colspan="5">Operacion</th>
            </tr>';
            for ($i = 0; $i < count($lista_area); $i++) {
                foreach ($detalle2 as $det) {
                    if ($lista_area[$i] == $det->descrip_area) {
                        $precio = $det->costo_sin_igv_material;
                        if ($det->t_moneda == 1) {
                            $precio = $precio * 3.33;
                        }
                        $output .= '
                  <tr>
                  <td>' . $det->descrip_area . '</td>
                  <td colspan="5">' . $det->operacion . '</td>
                  </tr>';
                    }
                }
            }
            $output .= '</table>
            </body></html>
            ';
            return $output;
        } else {
            return "El modelo no posee ningun material ni operacion registrada";
        }
    }
    public function aprobar_modelo(Request $request)
    {
        try {
            $codigo = $request["codigo"];
            $aprobacion = CostoModelo::where('modelo_serie', $codigo)
                ->update(["estado" => "1"]);
            session()->flash('success', 'Modelo Aprobado con exito');
        } catch (\Exception $e) {
            session()->flash('error', 'Error al aprobar el modelo');
        }
    }
}
