<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use erpCite\EscalaSalarialModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\DetalleCostoModeloMano;
use erpCite\CostoModelo;
use erpCite\Sueldos;
use DB;
class EscalaSalarialController extends Controller
{
  public function __construct()
  {
    $this->middleware('rhumanos');
  }
  public function index(Request $request)
  {
    if ($request) {
      $empresa=Auth::user()->RUC_empresa;
      $escala=DB::table('escala_salarial')
      ->join('trabajador','escala_salarial.cod_trabajador','=','trabajador.DNI_trabajador')
      ->join('tipo_seguro','escala_salarial.tipo_seguro','=','tipo_seguro.cod')
      ->join('regimen_laboral','escala_salarial.cod_regimen_laboral','=','regimen_laboral.cod_regimen_laboral')
      ->where('trabajador.estado_trabajador','=','1')
      ->where('escala_salarial.RUC_empresa','=',$empresa)
      ->orderBy('trabajador.apellido_paterno','asc')
      ->get();
      $hora=DB::table('horas_trabajo')
      ->where('RUC_empresa','=',$empresa)
      ->get();
      $minutes=0;
      for ($i=0; $i < count($hora) ; $i++) {
        list($hour, $minute) = explode(':', $hora[$i]->hora);
         $minutes += $hour * 60;
         $minutes += $minute;
      }
      $hours = floor($minutes / 60);
      $minutes -= $hours * 60;
      $horas_totales=$hours.".".$minutes;
      $seguro=DB::table('tipo_seguro')
      ->get();
      $regimen_renta=DB::table('regimen_laboral')
      ->where('estado_regimen_laboral','=',1)
      ->get();
      return view('recursos_humanos.escala_salarial.index',["escala"=>$escala,"horas_totales"=>$horas_totales,"hora"=>$hora,"seguro"=>$seguro,"regimen_renta"=>$regimen_renta]);
    }
  }
  public function create(Request $request)
  {
    if($request)
    {
      $empresa=Auth::user()->RUC_empresa;
      $trabajadores=DB::table('trabajador')
      ->where('estado_trabajador','=','1')
      ->where('cod_tipo_trabajador','=','1')
      ->where('RUC_empresa','=',$empresa)
      ->orderBy('apellido_paterno','asc')
      ->get();

      $seguro=DB::table('tipo_seguro')
      ->get();
      $regimen_renta=DB::table('regimen_laboral')
      ->where('estado_regimen_laboral','=',1)
      ->get();
      return view('recursos_humanos.escala_salarial.create',["seguro"=>$seguro,"regimen_renta"=>$regimen_renta,"trabajador"=>$trabajadores]);
    }

  }
  public function store()
  {

    $sueldo=Input::get('sueldo');
    if($sueldo>=400)
    {
      $empresa=Auth::user()->RUC_empresa;
      $regimen_empresa=db::table('empresa')->where('RUC_empresa','=',$empresa)->select('cod_regimen_laboral')->get();
      $escala=new EscalaSalarialModel;
      $escala->cod_regimen_laboral=Input::Get('regimen');
      $escala->RUC_empresa=$empresa;
      $escala->cod_trabajador=Input::get('trabajador');
      $escala->sueldo_mensual=Input::get('sueldo');
      if (Input::Get('regimen')==2 || Input::Get('regimen')==3) {
        $escala->tipo_seguro=2;
      }
      else {
        $escala->tipo_seguro=Input::get('seguro');
      }
      $beneficio=$this->beneficio(Input::get('sueldo'),Input::Get('regimen'),Input::get('seguro'));
      $escala->tasa_beneficio=$beneficio;
      try {
        $escala->save();
        session()->flash('success','Trabajador registrado en Escala Salarial ');
        //aqui iria el codigo para que costos cambie
        if ($regimen_empresa[0]->cod_regimen_laboral==Input::Get('regimen')) {
          $act=DetalleCostoModeloMano::where('cod_tipo_trabajador','1')
          ->update(['estado_trabajador'=>2]);
          $actualizar=DetalleCostoModeloMano::where('cod_tipo_trabajador','1')->select('cod_costo_modelo')->get();
          $anterior="";
          for($i=0;$i<count($actualizar);$i++)
          {
            if($anterior!=$actualizar[$i]->cod_costo_modelo)
            {
              $anterior=$actualizar[$i]->cod_costo_modelo;
              $modelo=DB::table('costo_modelo')
              ->where('cod_costo_modelo','=',$anterior)
              ->select('estado')
              ->get();
              if($modelo[0]->estado==9)
              {
                $act=CostoModelo::where('cod_costo_modelo',$anterior)
                ->update(['estado'=>"8"]);
              }
              else {
                if ($modelo[0]->estado==1 || $modelo[0]->estado==3) {
                  $act=CostoModelo::where('cod_costo_modelo',$anterior)
                  ->update(['estado'=>"7"]);
                }
              }
            }
          }
          session()->flash('warning','Precio de costos mano de obra desactualizado');
        }
        //
      } catch (\Exception $e) {
        session()->flash('error','No se puedo ingresar trabajador duplicado');
      }
    }
    else {
      session()->flash('error','Ingresar un sueldo mayor a S/.400');
    }
    return Redirect::to('recursos_humanos/escala_salarial');
  }
  private function beneficio($sueldo,$regimen,$seguro)
  {
    $sueldito=floatval($sueldo);
    if ($regimen==3) {
      $sueldito=$sueldo+93.00;
    }
    $sueldo_laboral=number_format(($sueldito*296)/30,2,'.',"");
    $sueldo_dominical=number_format(($sueldito*52)/30,2,'.',"");
    $sueldo_feriado=number_format(($sueldito*12)/30,2,'.',"");
    $sueldo_vacaciones=number_format((floatval($sueldo_laboral)+floatval($sueldo_dominical)+floatval($sueldo_feriado))/(296+52+12)*15,2,'.',"");
    $sueldo_bruto=number_format(floatval($sueldo_laboral)+floatval($sueldo_dominical)+floatval($sueldo_feriado),2,'.',"");

    $sueldo_men=number_format($sueldo_bruto/12,2,'.',"");
    if($regimen==3 || $regimen==2)
    {
      $sctr=number_format($sueldo_men*0.0123*12,2,'.',"");
      $essalud=number_format($sueldo_men*0.09*12,2,'.',"");

      if($regimen==2)
      {
        $gratificacion_jul=number_format($sueldo_men*0.5*(1+0.09),2,'.',"");
        $gratificacion_dic=number_format($sueldo_men*0.5*(1+0.09),2,'.',"");
        $cts=number_format($sueldo_men*0.5,2,'.',"");
        $beneficos=number_format(($gratificacion_jul+$gratificacion_dic+$sctr+$essalud+$cts+$sueldo_vacaciones),2,'.',"");
      }
      else {
        $sueldo_vacaciones=number_format((floatval($sueldo_laboral)+floatval($sueldo_dominical)+floatval($sueldo_feriado))/(296+52+12)*30,2,'.',"");
        $gratificacion_jul=number_format($sueldo_men*1*(1+0.09),2,'.',"");
        $gratificacion_dic=number_format($sueldo_men*1*(1+0.09),2,'.',"");
        $cts=number_format($sueldo_men*1,2,'.',"");
        $beneficos=number_format(($gratificacion_jul+$gratificacion_dic+$sctr+$essalud+$cts+$sueldo_vacaciones)+(93*12)  ,2,'.',"");
      }
    }
    else {
      if($seguro==1)
      {
        $sis=number_format(180,2,'.',"");
        $beneficos=number_format($sis+$sueldo_vacaciones,2,'.',"");
      }
      else
      {
        $essalud=number_format($sueldo_men*0.09*12,2,'.',"");
        $beneficos=number_format($essalud+$sueldo_vacaciones,2,'.',"");
      }
    }

    $tasa=number_format((($beneficos/$sueldo)/12)*100,2,'.',"");
    return $tasa;
  }
  public function show()
  {
  }
  public function edit($id)
  {

  }
  public function update()
  {
    $cod=Input::Get('cod_editar');
    $regimen=Input::Get('regimen');
    $sueldo=Input::get('sueldo');
    if ($regimen==2 || $regimen==3) {
      $tipo=2;
    }
    else {
      $tipo=Input::get('seguro');
    }
    $beneficio=$this->beneficio(Input::get('sueldo'),Input::Get('regimen'),Input::get('seguro'));
    $anterior=EscalaSalarialModel::where('cod_trabajador',$cod)->select('cod_regimen_laboral')->get();
    $act=EscalaSalarialModel::where('cod_trabajador',$cod)
    ->update(["cod_regimen_laboral"=>$regimen,
  "sueldo_mensual"=>$sueldo,
"tipo_seguro"=>$tipo,
"tasa_beneficio"=>$beneficio]);
// aqui iria el codigo para que costo cambie
  $empresa=Auth::user()->RUC_empresa;
  $regimen_empresa=db::table('empresa')->where('RUC_empresa','=',$empresa)->select('cod_regimen_laboral')->get();
  if((($anterior[0]->cod_regimen_laboral!=$regimen) || ($anterior[0]->cod_regimen_laboral==$regimen)) &&
  (($anterior[0]->cod_regimen_laboral==$regimen_empresa[0]->cod_regimen_laboral)||($regimen==$regimen_empresa[0]->cod_regimen_laboral)) )
  {
    $act=Sueldos::where('RUC_empresa',$empresa)
    ->update(['estado'=>2]);
    $act=DetalleCostoModeloMano::where('cod_tipo_trabajador','1')
    ->update(['estado_trabajador'=>2]);
    $actualizar=DetalleCostoModeloMano::where('cod_tipo_trabajador','1')->select('cod_costo_modelo')->get();
    $anterior="";
    for($i=0;$i<count($actualizar);$i++)
    {
      if($anterior!=$actualizar[$i]->cod_costo_modelo)
      {
        $anterior=$actualizar[$i]->cod_costo_modelo;
        $modelo=DB::table('costo_modelo')
        ->where('cod_costo_modelo','=',$anterior)
        ->select('estado')
        ->get();
        if($modelo[0]->estado==9)
        {
          $act=CostoModelo::where('cod_costo_modelo',$anterior)
          ->update(['estado'=>"8"]);
        }
        else {
          if ($modelo[0]->estado==1 || $modelo[0]->estado==3) {
            $act=CostoModelo::where('cod_costo_modelo',$anterior)
            ->update(['estado'=>"7"]);
          }
        }
      }
    }
    session()->flash('warning','Precio de costos mano de obra desactualizado');
  }
      session()->flash('success','Trabajador actualizado en Escala Salarial');
      return Redirect::to('recursos_humanos/escala_salarial');
  }
  public function destroy($id)
  {

  }
}
