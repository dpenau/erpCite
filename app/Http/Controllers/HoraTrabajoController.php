<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use erpCite\HoraTrabajoModel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class HoraTrabajoController extends Controller
{
  public function __construct()
  {
    $this->middleware('rhumanos');
  }
  public function index(Request $request)
  {
    if ($request) {
      $empresa=Auth::user()->RUC_empresa;
      $horario=DB::table('horas_trabajo')
      ->where('RUC_empresa','=',$empresa)
      ->get();
      return view('recursos_humanos.configuracion.index',["horario"=>$horario]);
    }
  }
  public function create(Request $request)
  {


  }
  public function store()
  {
    $empresa=Auth::user()->RUC_empresa;
    $lunes=Input::Get('lunes');
    $martes=Input::Get('martes');
    $miercoles=Input::Get('miercoles');
    $jueves=Input::Get('jueves');
    $viernes=Input::Get('viernes');
    $sabado=Input::Get('sabado');
    $array=array($lunes,$martes,$miercoles,$jueves,$viernes,$sabado);
    $dias=array("Lu","Ma","Mi","Ju","Vi","Sa");
    for ($i=0; $i <6 ; $i++) {
      $act=new HoraTrabajoModel;
      $act->cod=rand(1000,99999);
      $act->RUC_empresa=$empresa;
      $act->hora=$array[$i];
      $act->dia=$dias[$i];
      $act->save();
    }
    session()->flash('success','Horas Creadas Satisfactoriamente');
    return Redirect::to('recursos_humanos/configuracion');
  }
  public function show()
  {
  }
  public function edit()
  {

  }
  public function update()
  {
    $empresa=Auth::user()->RUC_empresa;

    $dias=DB::Table('horas_trabajo')
    ->where('RUC_empresa','=',$empresa)
    ->get();

    for ($i=0; $i < count($dias) ; $i++) {
      $horas="";
      switch ($dias[$i]->dia) {
        case "Lu":
          $horas=Input::get("lunes");
        break;
        case "Ma":
          $horas=Input::get("martes");
        break;
        case "Mi":
          $horas=Input::get("miercoles");
        break;
        case "Ju":
          $horas=Input::get("jueves");
        break;
        case "Vi":
          $horas=Input::get("viernes");
        break;
        case "Sa":
          $horas=Input::get("sabado");
        break;
      }
      $act=HoraTrabajoModel::where('cod',$dias[$i]->cod)
      ->update(["hora"=>$horas]);
    }

    session()->flash('success','Horas Actualizadas');
    return Redirect::to('recursos_humanos/configuracion');
  }
  public function destroy($id)
  {

  }
}
