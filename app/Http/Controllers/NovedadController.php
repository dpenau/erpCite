<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class NovedadController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function index()
  {
    return view('layouts.novedad');
  }
  public function obtener_noticias()
  {
    $noticias=DB::table('noticias')
    ->join('categoria_noticias','noticias.categoria_difusion','=','categoria_noticias.cod_categoria')
    ->select('noticias.cod','noticias.url_imagen','noticias.nombre_evento','noticias.descripcion','categoria_noticias.nombre_categoria')
    ->get();
    return $noticias;
  }
}
