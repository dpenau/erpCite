<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use PDF;
use \Milon\Barcode\DNS1D;

class PdfFichaProductoController extends Controller
{
    public function index($var)
    {
        if ($var != "") {
            $query = $var;
            $pdf = App::make('dompdf.wrapper');
            //$pdf->setPaper('a4', 'portrait');
            $pdf->setPaper('a4', 'landscape');
            $pdf->loadHTML($this->convert_data($query));
            return $pdf->stream();
        }
    }

    function get_data($query)
    {
        $empresa = Auth::user()->RUC_empresa;
        $fichaMaterial = DB::table('ficha_materiales')
            ->where('ficha_materiales.RUC_empresa', '=', $empresa)
            ->where('ficha_materiales.cod_modelo_comb', '=', $query)
            ->join('proceso', 'ficha_materiales.cod_proceso', '=', 'proceso.cod_proceso')
            ->join('material', 'ficha_materiales.cod_material', '=', 'material.cod_material')
            ->join('unidad_compra', 'material.unidad_medida', '=', 'unidad_compra.cod_unidad_medida')
            ->get();
        return $fichaMaterial;
    }

    function get_data_operacion($query)
    {
        $empresa = Auth::user()->RUC_empresa;
        $operaciones = DB::table('mano_obra')
            ->where('mano_obra.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('mano_obra.cod_combinacion_modelo', '=', $query)
            ->where('operacion_directa.estado_registro','A')
            ->join('operacion_directa', 'mano_obra.cod_operacion_directa', '=', 'operacion_directa.cod_operacion_d')
            ->join('proceso', 'operacion_directa.cod_proceso', '=', 'proceso.cod_proceso')
            ->join('tipo_pago_nuevo', 'operacion_directa.cod_tipo_pago', '=', 'tipo_pago_nuevo.cod_pago')
            ->join('unidad_medida', 'tipo_pago_nuevo.cod_unidad_medida', '=', 'unidad_medida.cod_unidad_medida')
            ->get();
        return $operaciones;
    }

    function get_data_cab($query)
    {
        $listaComb = DB::table('combinacion_modelo')
            ->where('combinacion_modelo.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->where('combinacion_modelo.cod_combinacion', '=', $query)
            ->join('modelo', 'combinacion_modelo.cod_modelo', '=', 'modelo.cod_modelo')
            ->join('coleccion', 'modelo.cod_coleccion', '=', 'coleccion.codigo_coleccion')
            ->join('linea', 'modelo.cod_linea', '=', 'linea.cod_linea')
            ->join('serie', 'modelo.cod_serie', '=', 'serie.cod_serie')

            ->get();

        return $listaComb;
    }

    function get_imagen()
    {
        $idempresa = Auth::user()->RUC_empresa;
        $imagen = DB::table('empresa')->where('RUC_empresa', '=', $idempresa)->limit(1)->get();
        return $imagen;
    }

    function get_imagen_modelo($query)
    {
        $idempresa = Auth::user()->RUC_empresa;
        $imagen = DB::table('combinacion_modelo')->where('cod_combinacion', '=', $query)->limit(1)->get();

        return $imagen[0]->image;
    }

    function convert_data($query)
    {
        $detalle = $this->get_data($query);
        $detalle_operacion = $this->get_data_operacion($query);
        $cab = $this->get_data_cab($query);
        $img = $this->get_imagen();
        $img_modelo = $this->get_imagen_modelo($query);
        $photo = "";
        $photo_modelo = $idempresa = Auth::user()->RUC_empresa . "/";
        $suma = 0;

        $photo_modelo = $photo_modelo . $img_modelo;

        foreach ($img as $i) {
            if ($i->imagen != "") {
                $photo = $i->imagen;
            }
        }



        $output = '<html><head><style>
    @page {
          margin: 0cm 0cm;
    }
    body {
          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {
          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    .column {
      float: left;
      width: 50%;
      padding: 5px;
    }

    .row::after {
      content: "";
      clear: both;
      display: table;
    }
    </style></head><body>';
        if ($photo != "") {
            $output .= '
              <header>
                  <div class="row">
                    <div class="column">
                      <img src="photo/' . $photo . '" alt="" style="width:120px;" class="img-rounded">
                    </div>
                    <div class="column" style="margin-top:3%;">
                      <img src="photo/pie3.png" width="100%"/>
                    </div>
                  </div>
              </header>';
        }
        foreach ($cab as $cabF) {
            $output .= '<h1><center>FICHA DE PRODUCTO</center></h1>

              <table style="width:100%;border-collapse: collapse; border: 1px solid black; text-align:center; ">
                            <tr>
                                <td style="border-collapse: collapse; border: 1px solid black; background-color:#F0B27A;">CODIGO: </td>
                                <td style="border-collapse: collapse; border: 1px solid black;">' . $cabF->codigo_comb . '</td>
                                <td style="border-collapse: collapse; border: 1px solid black; background-color:#F0B27A;">LÍNEA: </td>
                                <td style="border-collapse: collapse; border: 1px solid black;">' . $cabF->nombre_linea . '</td>
                                <td style="border-collapse: collapse; border: 1px solid black;  background-color:#F0B27A;">COLECCIÓN: </td>
                                <td style="border-collapse: collapse; border: 1px solid black;">' . $cabF->nombre_coleccion . '</td>
                            </tr>
                            <tr>
                                <td style="border-collapse: collapse; border: 1px solid black;  background-color:#F0B27A;">DESCRIPCION: </td>
                                <td style="border-collapse: collapse; border: 1px solid black;">' . $cabF->descripcion . '</td>
                                <td style="border-collapse: collapse; border: 1px solid black;  background-color:#F0B27A;">SERIE: </td>
                                <td style="border-collapse: collapse; border: 1px solid black;">' . $cabF->nombre_serie . '</td>
                                <td colspan="2" > <img src="photo/modelos/' . $photo_modelo . '" alt="" style="width:120px;" class="img-rounded"></td>

                            </tr>
              </table>
                        <br><br>

                <h3> Materiales Directos </h3>
              <table style="width:100%;border-collapse: collapse; border: 1px solid black;">
                <tr style="background-color:#F0B27A;">
                  <th style="border-collapse: collapse; border: 1px solid black;">Proceso</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">Material</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">Consumo Par</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">Unidad Compra</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">Costo por Unidad de Medida</th>
                  <th style="border-collapse: collapse; border: 1px solid black;">Costo por Par</th>
                </tr>';
        }
        $costo_material_directo = 0;
        foreach ($detalle as $dat) {
            $output .= '
              <tr>
                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->nombre . '</td>
                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->descrip_material . '</td>

                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->consumo_real . '</td>
                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->descrip_unidad_compra . '</td>
                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">S/.' . $dat->costo_con_igv_material . '</td>
                <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">S/.' . ($dat->consumo_real * $dat->costo_con_igv_material ) . '</td>
              </tr>   ';
            $costo_material_directo+=($dat->consumo_real * $dat->costo_con_igv_material );
        }
        $output .= '</table>';

        foreach ($cab as $cabFin) {
            $output .= '

              <table  CELLPADDING="3" ALIGN="right">
                <tr>
                  <td style="border-collapse: collapse; border: 1px solid black;  background-color:#F0B27A;">SubTotal-MP: </td>
                  <td style="border-collapse: collapse; border: 1px solid black; margin: 0 5% 0 5%; ">S/.' . $costo_material_directo . '</td>
                </tr>

              </table>';
        }

        $output .= '<br><br>
        <h3> Mano de Obra Directa </h3>
        <table style="width:100%;border-collapse: collapse; border: 1px solid black;">
        <tr style="background-color:#F0B27A;">
          <th style="border-collapse: collapse; border: 1px solid black;">Área</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Operación</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Tipo de Trabajador</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Costo por Par</th>
         </tr>';
        $costo_mano_directa = 0;
        foreach ($detalle_operacion as $op) {
            $output .= '
            <tr>
              <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $op->nombre . '</td>
              <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $op->operacion_nombre . '</td>

              <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $op->nombre_pago . '</td>

              <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">S/.' . $op->costo_par . '</td>
            </tr>   ';
            $costo_mano_directa += $op->costo_par;
        }
        $output .= '</table>';
        foreach ($cab as $cabFin) {
            $output .= '
          <table  CELLPADDING="3" ALIGN="right">
            <tr>
              <td style="border-collapse: collapse; border: 1px solid black;  background-color:#F0B27A;">SubTotal-MOD: </td>
              <td style="border-collapse: collapse; border: 1px solid black;">S/.' . $costo_mano_directa . '</td>
            </tr>

          </table>';

        }
        $suma = $costo_material_directo + $costo_mano_directa;
        $output .= '<br><br>
          <table  CELLPADDING="3" ALIGN="right">
            <tr>
              <td style="border-collapse: collapse; border: 1px solid black;  background-color:#F0B27A;">COSTO TOTAL:</td>
              <td style="border-collapse: collapse; border: 1px solid black;">S/.' . $suma . '</td>
            </tr>

          </table>';
        $output .= '



        </body></html>';
        return $output;
    }
}
