<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\Http\Requests\MaterialRequest;
use DB;
use erpCite\kardex;
use erpCite\KardexDetalle;
use erpCite\KardexTienda;
use erpCite\DetalleKardexTienda;
use erpCite\OrdenSalidaModel;
use erpCite\OrdenSalidaTiendaModel;
class KardexSalidaController extends Controller
{
  public function __construct()
  {

    $this->middleware('logistica');
  }
  public function index(Request $request)
  {
    if ($request) {
      $empresa=Auth::user()->RUC_empresa;
      $categorias=DB::table('categoria')->get();
      $subcategorias=DB::table('subcategoria')->where('estado_subcategoria','=',1)->orderBy('nom_subcategoria')->get();
      $tiendas=DB::table('almacen')
      ->where('tipo_almacen','=',1)
      ->where('estado_almacen','=',1)
      ->where('RUC_empresa','=',$empresa)
      ->orderBy('nom_almacen','asc')
      ->get();
      $area=DB::table('area')
      ->where('estado_area','=','1')
      ->orderBy('descrip_area','asc')
      ->get();
      return view('logistica.salida.index',["tiendas"=>$tiendas,"categorias"=>$categorias,"subcategorias"=>$subcategorias,'area'=>$area]);
    }
  }
  public function create(Request $request)
  {
    if($request)
    {
      return view("logistica.salida.create");
    }

  }
  public function store(Request $request)
  {
    $mytime = date('Y-m-d G:i:s');
    $empresa=Auth::user()->RUC_empresa;
    $usuarios=Auth::user()->id;
    $cod_area=Input::get('area');
    $stocktotal=Input::get('stock');
    $cod_material=Input::get('codigo');
    $cod_tallas=Input::get('idtalla');
    $cantidadsalida=Input::get('salida');
    $persona=Input::get('persona');
    $lista_detalle=[];
    $material="";
    $codigos_os="";
    if(is_array($cod_material) || is_array($cod_tallas))
    {
      $orden_salida=new OrdenSalidaModel;
      $ultimo=Db::table('orden_salida')
      ->where('RUC_empresa','=',$empresa)
      ->orderBy('fecha_creacion','desc')
      ->get();
      $año_actual=date("Y");
      $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
      $siglax = $sigla[0]->siglas;
      $codigos_os=$siglax.$año_actual.'-';
      if(count($ultimo)>0)
      {
        $partes=explode("-",$ultimo[0]->cod_orden_salida);
        if(count($partes)==2)
        {
          $año_ordenes=substr($partes[0],3);
          if($año_ordenes==$año_actual)
          {
            $posible_fecha_ultima=$ultimo[0]->fecha_creacion;
            $cc=0;
            for($i=0;$i<count($ultimo);$i++)
            {
              if($posible_fecha_ultima==$ultimo[$i]->fecha_creacion)
              {
                $cc=$i;
              }
              else{
              break;
              }
            }
            $partes=null;
            $partes=explode('-',$ultimo[$cc]->cod_orden_salida);
            $continuacion=$partes[1]+1;
            $codigos_os=$codigos_os.$continuacion;
          }
          else {
            $codigos_os=$codigos_os.'0';
          }
        }
        else {
          $codigos_os=$codigos_os.'0';
        }
      }
      else {
        $codigos_os=$codigos_os.'0';
      }
      $orden_salida->cod_orden_salida=$codigos_os;
      $orden_salida->area_dirigida=$cod_area;
      if($cod_area=="47812")
      {
        $cod_almacen=Input::get('almacen_sal');
        $nombre_almacen_tienda=DB::table('almacen')->where('cod_almacen',$cod_almacen)->get();
        $orden_salida->observacion="***TRASLADO INTERNO A TIENDA (".$nombre_almacen_tienda[0]->nom_almacen.")***";
      }
      else
      {
        $orden_salida->observacion="";
      }
      $orden_salida->RUC_empresa=$empresa;
      $orden_salida->usuario_salida=$usuarios;
      $orden_salida->save();
    }
    if(is_array($cod_material))
    {
      for( $i=0;$i<count($cod_material);$i++)
      {
        $aleatorio=rand(10000,99999);
        if ($cantidadsalida[$i]!="" && $cantidadsalida[$i]<=$stocktotal[$i]) {
          $salidas=$cantidadsalida[$i];
          $stock_tot=$stocktotal[$i];
          $materiales_espera=DB::table('detalle_kardex_material')
          ->where('cod_kardex_material','=',$cod_material[$i])
          ->where('estado_detalle_kardex','=','1')
          ->get();
          for($j=0;$j<count($materiales_espera);$j++)
          {
            if($salidas>0)
            {
              $entregado=0;
              if($salidas<=$materiales_espera[$j]->restante_detalle)
              {

                $restante_nuevo=$materiales_espera[$j]->restante_detalle;
                $restante_nuevo=$restante_nuevo-$salidas;
                $estado=1;
                if($restante_nuevo==0)
                {
                  $estado=0;
                }
                $act=KardexDetalle::where('id_detalle',$materiales_espera[$j]->id_detalle)
                ->update(["estado_detalle_kardex"=>$estado,"restante_detalle"=>$restante_nuevo]);
                $entregado=$salidas;
                $salidas=0;
              }
              else {
                $act=KardexDetalle::where('id_detalle',$materiales_espera[$j]->id_detalle)
                ->update(["estado_detalle_kardex"=>"0","restante_detalle"=>"0"]);
                $salidas=$salidas-$materiales_espera[$j]->restante_detalle;
                $entregado=$materiales_espera[$j]->restante_detalle;
              }
              $stock_tot=$stock_tot-$entregado;
              $detalle=new KardexDetalle;
              $detalle->dni_usuario=Auth::user()->id;
              $detalle->cod_area=$cod_area;
              $detalle->cod_kardex_material=$cod_material[$i];
              $detalle->costo_material=$materiales_espera[$j]->costo_material;
              $detalle->fecha_salida=$mytime;
              $detalle->cantidad_salida=$entregado;
              $detalle->trasladador_material=$persona[$i];
              $detalle->stock=$stock_tot;
              $detalle->comentario_devolucion=$aleatorio;
              $detalle->estado_detalle_kardex=0;
              $detalle->restante_detalle=0;
              $detalle->codigo_salida=$codigos_os;
              $detalle->save();
            }
          }
          $stockact=0;
              $stockact=$stock_tot;
              $act=Kardex::where('cod_material',$cod_material[$i])
              ->update(['stock_total'=>$stockact]);

              if($cod_area=="47812")
              {
                $cod_almacen=Input::get('almacen_sal');
                $mat_tienda=KardexTienda::where('codigo_material',$cod_material[$i])
                ->where('codigo_almacen',$cod_almacen)->get();
                if(count($mat_tienda)>0)
                {
                  $codigo_tienda=$mat_tienda[0]->cod_kardex_tienda;
                  $stock_tienda=$mat_tienda[0]->stock_total_tienda;
                  $nuevo_stock_tienda=$stock_tienda+$cantidadsalida[$i];
                  $actualizar=KardexTienda::where('cod_kardex_tienda',$codigo_tienda)
                  ->update(["stock_total_tienda"=>$nuevo_stock_tienda]);
                  $mat=DB::table('material')->where('cod_material',$cod_material[$i])->select('costo_sin_igv_material')->get();
                  $mytime = date("Y-m-d H:i:s") ;
                  $nuevo_detalle_tienda=new DetalleKardexTienda;
                  $nuevo_detalle_tienda->fecha_ingreso=$mytime;
                  $nuevo_detalle_tienda->stock=$nuevo_stock_tienda;
                  $nuevo_detalle_tienda->cantidad_ingresada=$cantidadsalida[$i];
                  $nuevo_detalle_tienda->trasladador_material=$persona[$i];
                  $nuevo_detalle_tienda->cod_kardex_tienda=$codigo_tienda;
                  $nuevo_detalle_tienda->cod_area=47812;
                  $nuevo_detalle_tienda->estado_detalle_kardex=1;
                  $nuevo_detalle_tienda->restante_detalle=$cantidadsalida[$i];
                  $nuevo_detalle_tienda->costo_material=$mat[0]->costo_sin_igv_material;
                  $nuevo_detalle_tienda->comentario_devolucion="Almacen principal a tienda";
                  $nuevo_detalle_tienda->dni_usuario=Auth::user()->id;
                  $nuevo_detalle_tienda->save();
                }
                else
                {
                  $nuevo_material_tienda=new KardexTienda;
                  $nuevo_material_tienda->cod_kardex_tienda=$cod_almacen.$cod_material[$i];
                  $nuevo_material_tienda->codigo_material=$cod_material[$i];
                  $nuevo_material_tienda->codigo_almacen=$cod_almacen;
                  $nuevo_material_tienda->lugar_tienda="S/P";
                  $nuevo_material_tienda->stock_total_tienda=$cantidadsalida[$i];
                  $nuevo_material_tienda->RUC_empresa=Auth::user()->RUC_empresa;
                  $nuevo_material_tienda->save();
                  $mat=DB::table('material')->where('cod_material',$cod_material[$i])->select('costo_sin_igv_material')->get();
                  $mytime = date("Y-m-d H:i:s") ;
                  $nuevo_detalle_tienda=new DetalleKardexTienda;
                  $nuevo_detalle_tienda->fecha_ingreso=$mytime;
                  $nuevo_detalle_tienda->stock=$cantidadsalida[$i];
                  $nuevo_detalle_tienda->cantidad_ingresada=$cantidadsalida[$i];
                  $nuevo_detalle_tienda->trasladador_material=$persona[$i];
                  $nuevo_detalle_tienda->cod_kardex_tienda=$cod_almacen.$cod_material[$i];
                  $nuevo_detalle_tienda->cod_area=47812;
                  $nuevo_detalle_tienda->estado_detalle_kardex=1;
                  $nuevo_detalle_tienda->restante_detalle=$cantidadsalida[$i];
                  $nuevo_detalle_tienda->costo_material=$mat[0]->costo_sin_igv_material;
                  $nuevo_detalle_tienda->comentario_devolucion="Almacen principal a tienda";
                  $nuevo_detalle_tienda->dni_usuario=Auth::user()->id;
                  $nuevo_detalle_tienda->save();

                }
              }
        }
        else {
          $material.="$cod_material[$i] ";
        }
      }

    }
    if(is_array($cod_tallas))
    {
      for($i=0;$i<count($cod_tallas);$i++)
      {
          $codigos="";
          $persona="";
          $cantidadsalida="";$stocks="";
          $persona=Input::get($cod_tallas[$i].'persona');
          $cantidad_salida=Input::get($cod_tallas[$i].'ca');
          $stocks=Input::get($cod_tallas[$i].'stocks');
          $codigos=Input::get($cod_tallas[$i].'cods');
          $aleatorio=rand(10000,99999);
          for($j=0;$j<count($codigos);$j++)
          {
            if($cantidad_salida[$j]!="" && $cantidad_salida[$j]<=$stocks[$j])
            {
              $salidas=$cantidad_salida[$j];
              $stock_tot=$stocks[$j];
              $materiales_espera=DB::table('detalle_kardex_material')
              ->where('cod_kardex_material','=',$codigos[$j])
              ->where('estado_detalle_kardex','=','1')
              ->get();
              for ($k=0; $k < count($materiales_espera); $k++) {
                if ($salidas>0)
                {
                  $entregado=0;
                  if($salidas<=$materiales_espera[$k]->restante_detalle)
                  {
                    $restante_nuevo=$materiales_espera[$k]->restante_detalle;
                    $restante_nuevo=$restante_nuevo-$salidas;
                    $estado=1;
                    if($restante_nuevo==0)
                    {
                      $estado=0;
                    }
                    $act=KardexDetalle::where('id_detalle',$materiales_espera[$k]->id_detalle)
                    ->update(["estado_detalle_kardex"=>$estado,"restante_detalle"=>$restante_nuevo]);
                    $entregado=$salidas;
                    $salidas=0;
                  }
                  else {
                    $act=KardexDetalle::where('id_detalle',$materiales_espera[$k]->id_detalle)
                    ->update(["estado_detalle_kardex"=>"0","restante_detalle"=>"0"]);
                    $salidas=$salidas-$materiales_espera[$k]->restante_detalle;
                    $entregado=$materiales_espera[$k]->restante_detalle;
                  }
                  $stock_tot=$stock_tot-$entregado;
                  $detalle=new KardexDetalle;
                  $detalle->dni_usuario=Auth::user()->id;
                  $detalle->cod_area=$cod_area;
                  $detalle->cod_kardex_material=$codigos[$j];
                  $detalle->costo_material=$materiales_espera[$k]->costo_material;
                  $detalle->fecha_salida=$mytime;
                  $detalle->cantidad_salida=$entregado;
                  $detalle->trasladador_material=$persona[0];
                  $detalle->stock=$stock_tot;
                  $detalle->comentario_devolucion=$aleatorio;
                  $detalle->estado_detalle_kardex=0;
                  $detalle->restante_detalle=0;
                  $detalle->codigo_salida=$codigos_os;
                  $detalle->save();
                }
              }
              $act=Kardex::where('cod_material',$codigos[$j])
                  ->update(['stock_total'=>$stock_tot]);

                  if($cod_area=="47812")
                  {
                    $cod_almacen=Input::get('almacen_sal');
                    $mat_tienda=KardexTienda::where('codigo_material',$codigos[$j])
                    ->where('codigo_almacen',$cod_almacen)->get();
                    if(count($mat_tienda)>0)
                    {
                      $codigo_tienda=$mat_tienda[0]->cod_kardex_tienda;
                      $stock_tienda=$mat_tienda[0]->stock_total_tienda;
                      $nuevo_stock_tienda=$stock_tienda+$cantidad_salida[$j];
                      $actualizar=KardexTienda::where('cod_kardex_tienda',$codigo_tienda)
                      ->update(["stock_total_tienda"=>$nuevo_stock_tienda]);
                      $mat=DB::table('material')->where('cod_material',$codigos[$j])->select('costo_sin_igv_material')->get();
                      $mytime = date("Y-m-d H:i:s") ;
                      $nuevo_detalle_tienda=new DetalleKardexTienda;
                      $nuevo_detalle_tienda->fecha_ingreso=$mytime;
                      $nuevo_detalle_tienda->stock=$nuevo_stock_tienda;
                      $nuevo_detalle_tienda->cantidad_ingresada=$cantidad_salida[$j];
                      $nuevo_detalle_tienda->trasladador_material=$persona[0];
                      $nuevo_detalle_tienda->cod_kardex_tienda=$codigo_tienda;
                      $nuevo_detalle_tienda->cod_area=47812;
                      $nuevo_detalle_tienda->estado_detalle_kardex=1;
                      $nuevo_detalle_tienda->restante_detalle=$cantidad_salida[$j];
                      $nuevo_detalle_tienda->costo_material=$mat[0]->costo_sin_igv_material;
                      $nuevo_detalle_tienda->comentario_devolucion="Almacen principal a tienda";
                      $nuevo_detalle_tienda->dni_usuario=Auth::user()->id;
                      $nuevo_detalle_tienda->save();
                    }
                    else
                    {
                      $nuevo_material_tienda=new KardexTienda;
                      $nuevo_material_tienda->cod_kardex_tienda=$cod_almacen.$codigos[$j];
                      $nuevo_material_tienda->codigo_material=$codigos[$j];
                      $nuevo_material_tienda->codigo_almacen=$cod_almacen;
                      $nuevo_material_tienda->lugar_tienda="S/P";
                      $nuevo_material_tienda->stock_total_tienda=$cantidad_salida[$j];
                      $nuevo_material_tienda->RUC_empresa=Auth::user()->RUC_empresa;
                      $nuevo_material_tienda->save();
                      $mat=DB::table('material')->where('cod_material',$codigos[$j])->select('costo_sin_igv_material')->get();
                      $mytime = date("Y-m-d H:i:s") ;
                      $nuevo_detalle_tienda=new DetalleKardexTienda;
                      $nuevo_detalle_tienda->fecha_ingreso=$mytime;
                      $nuevo_detalle_tienda->stock=$cantidad_salida[$j];
                      $nuevo_detalle_tienda->cantidad_ingresada=$cantidad_salida[$j];
                      $nuevo_detalle_tienda->trasladador_material=$persona[0];
                      $nuevo_detalle_tienda->cod_kardex_tienda=$cod_almacen.$codigos[$j];
                      $nuevo_detalle_tienda->cod_area=47812;
                      $nuevo_detalle_tienda->estado_detalle_kardex=1;
                      $nuevo_detalle_tienda->restante_detalle=$cantidad_salida[$j];
                      $nuevo_detalle_tienda->costo_material=$mat[0]->costo_sin_igv_material;
                      $nuevo_detalle_tienda->comentario_devolucion="Almacen principal a tienda";
                      $nuevo_detalle_tienda->dni_usuario=Auth::user()->id;
                      $nuevo_detalle_tienda->save();

                    }
                  }
            }
            else {
              $material.="$codigos[$j] ";
            }
          }
      }
    }
    if($codigos_os!="")
    {
      if($material=="")
      {
        session()->flash('success','Salida de materiales registrada satisfactoriamente');
      }
      else {
        session()->flash('warning','Salida de materiales satisfactoriamente, excepto los siguientes materiales: '.$material);
      }
      session()->flash('info',array('Descargar Orden de Salida','Aqui',$codigos_os));
    }
    else
    {
      session()->flash('warning','Ingrese algun material');
    }
    
    return Redirect::to('logistica/kardex');
  }

  public function salida_material()
  {
    $empresa=Auth::user()->RUC_empresa;
    $categorias=DB::table('categoria')->get();
    $subcategorias=DB::table('subcategoria')->where('estado_subcategoria','=',1)->orderBy('nom_subcategoria','asc')->get();
    $tiendas=DB::table('almacen')
    ->where('tipo_almacen','=',0)
    ->where('estado_almacen','=',1)
    ->where('RUC_empresa','=',$empresa)
    ->get();
    $almacen_tienda=DB::table('almacen')
    ->where('tipo_almacen','=',1)
    ->where('estado_almacen','=',1)
    ->where('RUC_empresa','=',$empresa)
    ->get();
    $area=DB::table('area')
    ->where('estado_area','=','1')
    ->orderBy('descrip_area','asc')
    ->get();
    return view('logistica.salida.tienda',["almacen_tienda"=>$almacen_tienda,"tiendas"=>$tiendas,"categorias"=>$categorias,"subcategorias"=>$subcategorias,'area'=>$area]);

  }
  public function obtmateriales_tienda($var)
  {
    $datos=explode("+",$var);
    $empresa=Auth::user()->RUC_empresa;
    if($datos[2]=="p")
    {
      $materiales=DB::table('kardex_tienda')
      ->join('almacen','kardex_tienda.codigo_almacen','=','almacen.cod_almacen')
      ->join('material','kardex_tienda.codigo_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('kardex_tienda.RUC_empresa','=',$empresa)
      ->where('kardex_tienda.stock_total_tienda','>','0')
      ->where('material.t_compra','=',$datos[1])
      ->where('material.cod_subcategoria','=',$datos[0])
      ->select(DB::raw("SUM(kardex_tienda.stock_total_tienda) as stock_total_tienda"),'material.descrip_material','kardex_tienda.codigo_material','kardex_tienda.cod_kardex_tienda','subcategoria.nom_subcategoria','unidad_medida.descrip_unidad_medida','subcategoria.cod_categoria','material.cod_material')
      ->groupBy('kardex_tienda.codigo_material')
      ->orderBy('material.descrip_material',"asc")
      ->get();
      return $materiales;
    }
    else
    {
      $materiales=DB::table('kardex_tienda')
      ->join('almacen','kardex_tienda.codigo_almacen','=','almacen.cod_almacen')
      ->join('material','kardex_tienda.codigo_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('kardex_tienda.RUC_empresa','=',$empresa)
      ->where('kardex_tienda.stock_total_tienda','>','0')
      ->where('material.t_compra','=',$datos[1])
      ->where('material.cod_subcategoria','=',$datos[0])
      ->where('almacen.cod_almacen','=',$datos[2])
      ->orderBy('material.descrip_material',"asc")
      ->select("stock_total_tienda",'material.descrip_material','kardex_tienda.codigo_material','kardex_tienda.cod_kardex_tienda','subcategoria.nom_subcategoria','unidad_medida.descrip_unidad_medida','subcategoria.cod_categoria','material.cod_material')
      ->get();
      return $materiales;
    }
  }
  public function guardar_salida()
  {
    $mytime = date('Y-m-d G:i:s');
    $empresa=Auth::user()->RUC_empresa;
    $usuarios=Auth::user()->id;
    $almacen=Input::get('almacen_retirar');
    $cod_area=Input::get('area');
    $stocktotal=Input::get('stock');
    $cod_material=Input::get('codigo');
    $cod_tallas=Input::get('idtalla');
    $cantidadsalida=Input::get('salida');
    $persona=Input::get('persona');
    $material="";
    $codigos_os="";
    if(is_array($cod_material) || is_array($cod_tallas))
    {
      $orden_salida=new OrdenSalidaTiendaModel;
      $ultimo=Db::table('orden_salida_tienda')
      ->where('RUC_empresa','=',$empresa)
      ->orderBy('fecha_creacion','desc')
      ->get();
      $año_actual=date("Y");
      $sigla=DB::table('empresa')->where('RUC_empresa',Auth::user()->RUC_empresa)->get();
      $siglax = $sigla[0]->siglas;
      $codigos_os=$siglax.$año_actual.'-';
      if(count($ultimo)>0)
      {
        $partes=explode("-",$ultimo[0]->cod_orden_salida);
        if(count($partes)==2)
        {
          $año_ordenes=substr($partes[0],3);
          if($año_ordenes==$año_actual)
          {
            $posible_fecha_ultima=$ultimo[0]->fecha_creacion;
            $cc=0;
            for($i=0;$i<count($ultimo);$i++)
            {
              if($posible_fecha_ultima==$ultimo[$i]->fecha_creacion)
              {
                $cc=$i;
              }
              else{
              break;
              }
            }
            $partes=null;
            $partes=explode('-',$ultimo[$cc]->cod_orden_salida);
            $continuacion=$partes[1]+1;
            $codigos_os=$codigos_os.$continuacion;
          }
          else {
            $codigos_os=$codigos_os.'0';
          }
        }
        else {
          $codigos_os=$codigos_os.'0';
        }
      }
      else {
        $codigos_os=$codigos_os.'0';
      }
      $orden_salida->cod_orden_salida=$codigos_os;
      $orden_salida->area_dirigida=$cod_area;
      if($cod_area=="47812")
      {
        $cod_almacen=Input::get('almacen_sal');
        if($cod_almacen=="planta")
        {
          $orden_salida->observacion="***TRASLADO INTERNO A AlMACEN DE PLANTA***";
        }
        else
        {
          $nombre_almacen_tienda=DB::table('almacen')->where('cod_almacen',$cod_almacen)->get();
          $orden_salida->observacion="***TRASLADO INTERNO A AlMACEN DE TIENDA (".$nombre_almacen_tienda[0]->nom_almacen.")***";  
        }
      }
      else
      {
        $orden_salida->observacion="";
      }
      $orden_salida->RUC_empresa=$empresa;
      $orden_salida->usuario_salida=$usuarios;
      $orden_salida->save();
    }
    if($almacen!="p")
    {
      if(is_array($cod_material))
      {
        for( $i=0;$i<count($cod_material);$i++)
        {
          $aleatorio=rand(10000,99999);
          if ($cantidadsalida[$i]!="" && $cantidadsalida[$i]<=$stocktotal[$i]) {
            $salidas=$cantidadsalida[$i];
            $stock_tot=$stocktotal[$i];
            $materiales_espera=DB::table('detalle_kardex_tienda')
            ->where('cod_kardex_tienda','=',$almacen.$cod_material[$i])
            ->where('estado_detalle_kardex','=','1')
            ->get();
            for($j=0;$j<count($materiales_espera);$j++)
            {
              if($salidas>0)
              {
                $entregado=0;
                if($salidas<=$materiales_espera[$j]->restante_detalle)
                {

                  $restante_nuevo=$materiales_espera[$j]->restante_detalle;
                  $restante_nuevo=$restante_nuevo-$salidas;
                  $estado=1;
                  if($restante_nuevo==0)
                  {
                    $estado=0;
                  }
                  $act=DetalleKardexTienda::where('cod_detalle_tienda',$materiales_espera[$j]->cod_detalle_tienda)
                  ->update(["estado_detalle_kardex"=>$estado,"restante_detalle"=>$restante_nuevo]);
                  $entregado=$salidas;
                  $salidas=0;
                }
                else {
                  $act=DetalleKardexTienda::where('cod_detalle_tienda',$materiales_espera[$j]->cod_detalle_tienda)
                  ->update(["estado_detalle_kardex"=>"0","restante_detalle"=>"0"]);
                  $salidas=$salidas-$materiales_espera[$j]->restante_detalle;
                  $entregado=$materiales_espera[$j]->restante_detalle;
                }
                $stock_tot=$stock_tot-$entregado;
                $detalle=new DetalleKardexTienda;
                $detalle->dni_usuario=Auth::user()->id;
                $detalle->cod_area=$cod_area;
                $detalle->cod_kardex_tienda=$almacen.$cod_material[$i];
                $detalle->costo_material=$materiales_espera[$j]->costo_material;
                $detalle->fecha_salida=$mytime;
                $detalle->cantidad_salida=$entregado;
                $detalle->trasladador_material=$persona[$i];
                $detalle->stock=$stock_tot;
                $detalle->comentario_devolucion=$aleatorio;
                $detalle->estado_detalle_kardex=0;
                $detalle->restante_detalle=0;
                $detalle->codigo_salida=$codigos_os;
                $detalle->save();
              }
            }
            $stockact=0;
                $stockact=$stock_tot;
                $act=KardexTienda::where('cod_kardex_tienda',$almacen.$cod_material[$i])
                ->update(['stock_total_tienda'=>$stockact]);

                if($cod_area=="47812")
                {
                  $cod_almacen=Input::get('almacen_sal');
                  $mat=DB::table('material')->where('cod_material',$cod_material[$i])->select('costo_sin_igv_material')->get();
                  if($cod_almacen=="planta")
                  {
                    $detalle_material=Kardex::where('cod_kardex_material','=',$cod_material[$i])->select('stock_total')->get();
                    $nuevo_stock=$detalle_material[0]->stock_total+$cantidadsalida[$i];
                    $detalle=new KardexDetalle;
                    $detalle->dni_usuario=Auth::user()->id;
                    $detalle->cod_area=$cod_area;
                    $detalle->cod_kardex_material=$cod_material[$i];
                    $detalle->costo_material=$mat[0]->costo_sin_igv_material;
                    $detalle->fecha_ingreso=$mytime;
                    $detalle->cantidad_ingresada=$cantidadsalida[$i];
                    $detalle->trasladador_material=$persona[0];
                    $detalle->stock=$nuevo_stock;
                    $detalle->comentario_devolucion="Traslado de tienda a almacen";
                    $detalle->estado_detalle_kardex=1;
                    $detalle->restante_detalle=$cantidadsalida[$i];
                    $detalle->save();
                    $detalle_material=Kardex::where('cod_kardex_material','=',$cod_material[$i])
                    ->update(['stock_total'=>$nuevo_stock]);;
                  }
                  else
                  {
                    $mat_tienda=KardexTienda::where('codigo_material',$cod_material[$i])
                    ->where('codigo_almacen',$cod_almacen)->get();
                    
                    if(count($mat_tienda)>0)
                    {
                      $codigo_tienda=$mat_tienda[0]->cod_kardex_tienda;
                      $stock_tienda=$mat_tienda[0]->stock_total_tienda;
                      $nuevo_stock_tienda=$stock_tienda+$cantidadsalida[$i];
                      $actualizar=KardexTienda::where('cod_kardex_tienda',$codigo_tienda)
                      ->update(["stock_total_tienda"=>$nuevo_stock_tienda]);
                      $mat=DB::table('material')->where('cod_material',$cod_material[$i])->select('costo_sin_igv_material')->get();
                      $mytime = date("Y-m-d H:i:s") ;
                      $nuevo_detalle_tienda=new DetalleKardexTienda;
                      $nuevo_detalle_tienda->fecha_ingreso=$mytime;
                      $nuevo_detalle_tienda->stock=$nuevo_stock_tienda;
                      $nuevo_detalle_tienda->cantidad_ingresada=$cantidadsalida[$i];
                      $nuevo_detalle_tienda->trasladador_material=$persona[$i];
                      $nuevo_detalle_tienda->cod_kardex_tienda=$codigo_tienda;
                      $nuevo_detalle_tienda->cod_area=47812;
                      $nuevo_detalle_tienda->estado_detalle_kardex=1;
                      $nuevo_detalle_tienda->restante_detalle=$cantidadsalida[$i];
                      $nuevo_detalle_tienda->costo_material=$mat[0]->costo_sin_igv_material;
                      $nuevo_detalle_tienda->comentario_devolucion="Traslado de tienda a tienda";
                      $nuevo_detalle_tienda->dni_usuario=Auth::user()->id;
                      $nuevo_detalle_tienda->codigo_salida=$codigos_os;
                      $nuevo_detalle_tienda->save();
                    }
                    else
                    {
                      $nuevo_material_tienda=new KardexTienda;
                      $nuevo_material_tienda->cod_kardex_tienda=$cod_almacen.$cod_material[$i];
                      $nuevo_material_tienda->codigo_material=$cod_material[$i];
                      $nuevo_material_tienda->codigo_almacen=$cod_almacen;
                      $nuevo_material_tienda->lugar_tienda="S/P";
                      $nuevo_material_tienda->stock_total_tienda=$cantidadsalida[$i];
                      $nuevo_material_tienda->RUC_empresa=Auth::user()->RUC_empresa;
                      $nuevo_material_tienda->save();
                      $mat=DB::table('material')->where('cod_material',$cod_material[$i])->select('costo_sin_igv_material')->get();
                      $mytime = date("Y-m-d H:i:s") ;
                      $nuevo_detalle_tienda=new DetalleKardexTienda;
                      $nuevo_detalle_tienda->fecha_ingreso=$mytime;
                      $nuevo_detalle_tienda->stock=$cantidadsalida[$i];
                      $nuevo_detalle_tienda->cantidad_ingresada=$cantidadsalida[$i];
                      $nuevo_detalle_tienda->trasladador_material=$persona[$i];
                      $nuevo_detalle_tienda->cod_kardex_tienda=$cod_almacen.$cod_material[$i];
                      $nuevo_detalle_tienda->cod_area=47812;
                      $nuevo_detalle_tienda->estado_detalle_kardex=1;
                      $nuevo_detalle_tienda->restante_detalle=$cantidadsalida[$i];
                      $nuevo_detalle_tienda->costo_material=$mat[0]->costo_sin_igv_material;
                      $nuevo_detalle_tienda->comentario_devolucion="Traslado de tienda a tienda";
                      $nuevo_detalle_tienda->dni_usuario=Auth::user()->id;
                      $nuevo_detalle_tienda->codigo_salida=$codigos_os;
                      $nuevo_detalle_tienda->save();

                    }
                  }
                  
                }
          }
          else {
            $material.="$cod_material[$i] ";
          }
        }

      }
      if(is_array($cod_tallas))
      {
        for($i=0;$i<count($cod_tallas);$i++)
        {
            $codigos="";
            $persona="";
            $cantidadsalida="";$stocks="";
            $persona=Input::get($cod_tallas[$i].'persona');
            $cantidad_salida=Input::get($cod_tallas[$i].'ca');
            $stocks=Input::get($cod_tallas[$i].'stocks');
            $codigos=Input::get($cod_tallas[$i].'cods');
            $aleatorio=rand(10000,99999);
            for($j=0;$j<count($codigos);$j++)
            {
              if($cantidad_salida[$j]!="" && $cantidad_salida[$j]<=$stocks[$j])
              {
                $salidas=$cantidad_salida[$j];
                $stock_tot=$stocks[$j];
                $materiales_espera=DB::table('detalle_kardex_tienda')
                ->where('cod_kardex_tienda','=',$almacen.$codigos[$j])
                ->where('estado_detalle_kardex','=','1')
                ->get();
                for ($k=0; $k < count($materiales_espera); $k++) {
                  if ($salidas>0)
                  {
                    $entregado=0;
                    if($salidas<=$materiales_espera[$k]->restante_detalle)
                    {
                      $restante_nuevo=$materiales_espera[$k]->restante_detalle;
                      $restante_nuevo=$restante_nuevo-$salidas;
                      $estado=1;
                      if($restante_nuevo==0)
                      {
                        $estado=0;
                      }
                      $act=DetalleKardexTienda::where('cod_detalle_tienda',$materiales_espera[$k]->cod_detalle_tienda)
                      ->update(["estado_detalle_kardex"=>$estado,"restante_detalle"=>$restante_nuevo]);
                      $entregado=$salidas;
                      $salidas=0;
                    }
                    else {
                      $act=DetalleKardexTienda::where('cod_detalle_tienda',$materiales_espera[$k]->cod_detalle_tienda)
                      ->update(["estado_detalle_kardex"=>"0","restante_detalle"=>"0"]);
                      $salidas=$salidas-$materiales_espera[$k]->restante_detalle;
                      $entregado=$materiales_espera[$k]->restante_detalle;
                    }
                    $stock_tot=$stock_tot-$entregado;
                    $detalle=new DetalleKardexTienda;
                    $detalle->dni_usuario=Auth::user()->id;
                    $detalle->cod_area=$cod_area;
                    $detalle->cod_kardex_tienda=$almacen.$codigos[$j];
                    $detalle->costo_material=$materiales_espera[$k]->costo_material;
                    $detalle->fecha_salida=$mytime;
                    $detalle->cantidad_salida=$entregado;
                    $detalle->trasladador_material=$persona[0];
                    $detalle->stock=$stock_tot;
                    $detalle->comentario_devolucion=$aleatorio;
                    $detalle->estado_detalle_kardex=0;
                    $detalle->restante_detalle=0;
                    $detalle->codigo_salida=$codigos_os;
                    $detalle->save();
                  }
                }
                $act=KardexTienda::where('cod_kardex_tienda',$almacen.$codigos[$j])
                    ->update(['stock_total_tienda'=>$stock_tot]);

                    if($cod_area=="47812")
                    {
                      $cod_almacen=Input::get('almacen_sal');
                      $mat=DB::table('material')->where('cod_material',$codigos[$j])->select('costo_sin_igv_material')->get();
                      if($cod_almacen=="planta")
                      {
                        $detalle_material=Kardex::where('cod_kardex_material','=',$codigos[$j])->select('stock_total')->get();
                        $nuevo_stock=$detalle_material[0]->stock_total+$cantidad_salida[$j];
                        $detalle=new KardexDetalle;
                        $detalle->dni_usuario=Auth::user()->id;
                        $detalle->cod_area=$cod_area;
                        $detalle->cod_kardex_material=$codigos[$j];
                        $detalle->costo_material=$mat[0]->costo_sin_igv_material;
                        $detalle->fecha_ingreso=$mytime;
                        $detalle->cantidad_ingresada=$cantidad_salida[$j];
                        $detalle->trasladador_material=$persona[0];
                        $detalle->stock=$nuevo_stock;
                        $detalle->comentario_devolucion="Traslado de almacen de tienda a planta";
                        $detalle->estado_detalle_kardex=1;
                        $detalle->restante_detalle=$cantidad_salida[$j];
                        $detalle->save();
                        $detalle_material=Kardex::where('cod_kardex_material','=',$codigos[$j])
                        ->update(['stock_total'=>$nuevo_stock]);
                      }
                      else
                      {
                        $cod_almacen=Input::get('almacen_sal');
                        $mat_tienda=KardexTienda::where('codigo_material',$codigos[$j])
                        ->where('codigo_almacen',$cod_almacen)->get();
                        if(count($mat_tienda)>0)
                        {
                          $codigo_tienda=$mat_tienda[0]->cod_kardex_tienda;
                          $stock_tienda=$mat_tienda[0]->stock_total_tienda;
                          $nuevo_stock_tienda=$stock_tienda+$cantidad_salida[$j];
                          $actualizar=KardexTienda::where('cod_kardex_tienda',$codigo_tienda)
                          ->update(["stock_total_tienda"=>$nuevo_stock_tienda]);
                          $mat=DB::table('material')->where('cod_material',$codigos[$j])->select('costo_sin_igv_material')->get();
                          $mytime = date("Y-m-d H:i:s") ;
                          $nuevo_detalle_tienda=new DetalleKardexTienda;
                          $nuevo_detalle_tienda->fecha_ingreso=$mytime;
                          $nuevo_detalle_tienda->stock=$nuevo_stock_tienda;
                          $nuevo_detalle_tienda->cantidad_ingresada=$cantidad_salida[$j];
                          $nuevo_detalle_tienda->trasladador_material=$persona[0];
                          $nuevo_detalle_tienda->cod_kardex_tienda=$codigo_tienda;
                          $nuevo_detalle_tienda->cod_area=47812;
                          $nuevo_detalle_tienda->estado_detalle_kardex=1;
                          $nuevo_detalle_tienda->restante_detalle=$cantidad_salida[$j];
                          $nuevo_detalle_tienda->costo_material=$mat[0]->costo_sin_igv_material;
                          $nuevo_detalle_tienda->dni_usuario=Auth::user()->id;
                          $nuevo_detalle_tienda->comentario_devolucion="Traslado de Tienda a Tienda";
                          $nuevo_detalle_tienda->codigo_salida=$codigos_os;
                          $nuevo_detalle_tienda->save();
                        }
                        else
                        {
                          $nuevo_material_tienda=new KardexTienda;
                          $nuevo_material_tienda->cod_kardex_tienda=$cod_almacen.$codigos[$j];
                          $nuevo_material_tienda->codigo_material=$codigos[$j];
                          $nuevo_material_tienda->codigo_almacen=$cod_almacen;
                          $nuevo_material_tienda->lugar_tienda="S/P";
                          $nuevo_material_tienda->stock_total_tienda=$cantidad_salida[$j];
                          $nuevo_material_tienda->RUC_empresa=Auth::user()->RUC_empresa;
                          $nuevo_material_tienda->save();
                          $mat=DB::table('material')->where('cod_material',$codigos[$j])->select('costo_sin_igv_material')->get();
                          $mytime = date("Y-m-d H:i:s") ;
                          $nuevo_detalle_tienda=new DetalleKardexTienda;
                          $nuevo_detalle_tienda->fecha_ingreso=$mytime;
                          $nuevo_detalle_tienda->stock=$cantidad_salida[$j];
                          $nuevo_detalle_tienda->cantidad_ingresada=$cantidad_salida[$j];
                          $nuevo_detalle_tienda->trasladador_material=$persona[0];
                          $nuevo_detalle_tienda->cod_kardex_tienda=$cod_almacen.$codigos[$j];
                          $nuevo_detalle_tienda->cod_area=47812;
                          $nuevo_detalle_tienda->estado_detalle_kardex=1;
                          $nuevo_detalle_tienda->restante_detalle=$cantidad_salida[$j];
                          $nuevo_detalle_tienda->costo_material=$mat[0]->costo_sin_igv_material;
                          $nuevo_detalle_tienda->comentario_devolucion="Traslado de tienda a tienda";
                          $nuevo_detalle_tienda->dni_usuario=Auth::user()->id;
                          $nuevo_detalle_tienda->codigo_salida=$codigos_os;
                          $nuevo_detalle_tienda->save();
                        }
                      }
                    }
              }
              else {
                $material.="$codigos[$j] ";
              }
            }
        }
      }
    }
    else
    {
      $almacen_prioridades=DB::Table('almacen')
      ->where('tipo_almacen','=',1)
      ->where('estado_almacen','=',1)
      ->OrderBy('prioridad','asc')
      ->select('cod_almacen','prioridad')
      ->get();
      if(is_array($cod_material))
      {
        for( $i=0;$i<count($cod_material);$i++)
        {
          $aleatorio=rand(10000,99999);
          if ($cantidadsalida[$i]!="" && $cantidadsalida[$i]<=$stocktotal[$i]) {
            $salidas=$cantidadsalida[$i];
            $stock_tot=$stocktotal[$i];
            for($j=0;$j<count($almacen_prioridades);$j++)
            {
              if($salidas>0)
              {
                $materiales_espera=DB::table('detalle_kardex_tienda')
                ->where('cod_kardex_tienda','like',$almacen_prioridades[$j]->cod_almacen.$cod_material[$i])
                ->where('estado_detalle_kardex','=','1')
                ->get();
                $codigo="";
                for($h=0;$h<count($materiales_espera);$h++)
                {
                  if($salidas>0)
                  {
                    $entregado=0;
                    if($salidas<=$materiales_espera[$h]->restante_detalle)
                    {
                      $restante_nuevo=$materiales_espera[$h]->restante_detalle;
                      $restante_nuevo=$restante_nuevo-$salidas;
                      $estado=1;
                      if($restante_nuevo==0)
                      {
                        $estado=0;
                      }
                      $act=DetalleKardexTienda::where('cod_detalle_tienda',$materiales_espera[$h]->cod_detalle_tienda)
                      ->update(["estado_detalle_kardex"=>$estado,"restante_detalle"=>$restante_nuevo]);
                      $entregado=$salidas;
                      $salidas=0;
                    }
                    else {
                      $act=DetalleKardexTienda::where('cod_detalle_tienda',$materiales_espera[$h]->cod_detalle_tienda)
                      ->update(["estado_detalle_kardex"=>"0","restante_detalle"=>"0"]);
                      $salidas=$salidas-$materiales_espera[$h]->restante_detalle;
                      $entregado=$materiales_espera[$h]->restante_detalle;
                    }
                    $stock_tot=$entregado;
                    $detalle=new DetalleKardexTienda;
                    $detalle->dni_usuario=Auth::user()->id;
                    $detalle->cod_area=$cod_area;
                    $detalle->cod_kardex_tienda=$materiales_espera[$h]->cod_kardex_tienda;
                    $detalle->costo_material=$materiales_espera[$h]->costo_material;
                    $detalle->fecha_salida=$mytime;
                    $detalle->cantidad_salida=$entregado;
                    $detalle->trasladador_material=$persona[$i];
                    $detalle->stock=$stock_tot;
                    $detalle->comentario_devolucion=$aleatorio;
                    $detalle->estado_detalle_kardex=0;
                    $detalle->restante_detalle=0;
                    $detalle->codigo_salida=$codigos_os;
                    $detalle->save();
                    $codigo=$materiales_espera[$h]->cod_kardex_tienda;
                  }
                }
                $stockact=0;
                $stockact=$stock_tot;
                $act=KardexTienda::where('cod_kardex_tienda',$almacen_prioridades[$j]->cod_almacen.$cod_material[$i])
                ->decrement('stock_total_tienda',$stockact);
              }
              else
              {
              break;
              }
            }
          }
          else {
            $material.="$cod_material[$i] ";
          }
        }

      }
      if(is_array($cod_tallas))
      {

        for($i=0;$i<count($cod_tallas);$i++)
        {
            $codigos="";
            $persona="";
            $cantidadsalida="";$stocks="";
            $persona=Input::get($cod_tallas[$i].'persona');
            $cantidad_salida=Input::get($cod_tallas[$i].'ca');
            $stocks=Input::get($cod_tallas[$i].'stocks');
            $codigos=Input::get($cod_tallas[$i].'cods');
            $aleatorio=rand(10000,99999);
            
            for($j=0;$j<count($codigos);$j++)
            {
              if($cantidad_salida[$j]!="" && $cantidad_salida[$j]<=$stocks[$j])
              {
                $salidas=$cantidad_salida[$j];
                $stock_tot=$stocks[$j];
                for($k=0;$k<count($almacen_prioridades);$k++)
                {
                  if($salidas>0)
                  {
                    $materiales_espera=DB::table('detalle_kardex_tienda')
                    ->where('cod_kardex_tienda','like',$almacen_prioridades[$k]->cod_almacen.$codigos[$j])
                    ->where('estado_detalle_kardex','=','1')
                    ->get();
                    $codigo="";
                    for($h=0;$h<count($materiales_espera);$h++)
                    {
                      if($salidas>0)
                      {
                        $entregado=0;
                        if($salidas<=$materiales_espera[$h]->restante_detalle)
                        {
                          $restante_nuevo=$materiales_espera[$h]->restante_detalle;
                          $restante_nuevo=$restante_nuevo-$salidas;
                          $estado=1;
                          if($restante_nuevo==0)
                          {
                            $estado=0;
                          }
                          $act=DetalleKardexTienda::where('cod_detalle_tienda',$materiales_espera[$h]->cod_detalle_tienda)
                          ->update(["estado_detalle_kardex"=>$estado,"restante_detalle"=>$restante_nuevo]);
                          $entregado=$salidas;
                          $salidas=0;
                        }
                        else {
                          $act=DetalleKardexTienda::where('cod_detalle_tienda',$materiales_espera[$h]->cod_detalle_tienda)
                          ->update(["estado_detalle_kardex"=>"0","restante_detalle"=>"0"]);
                          $salidas=$salidas-$materiales_espera[$h]->restante_detalle;
                          $entregado=$materiales_espera[$h]->restante_detalle;
                        }
                        $stock_tot=$entregado;
                        $detalle=new DetalleKardexTienda;
                        $detalle->dni_usuario=Auth::user()->id;
                        $detalle->cod_area=$cod_area;
                        $detalle->cod_kardex_tienda=$materiales_espera[$h]->cod_kardex_tienda;
                        $detalle->costo_material=$materiales_espera[$h]->costo_material;
                        $detalle->fecha_salida=$mytime;
                        $detalle->cantidad_salida=$entregado;
                        $detalle->trasladador_material=$persona[$i];
                        $detalle->stock=$stock_tot;
                        $detalle->comentario_devolucion=$aleatorio;
                        $detalle->estado_detalle_kardex=0;
                        $detalle->restante_detalle=0;
                        $detalle->codigo_salida=$codigos_os;
                        $detalle->save();
                        $codigo=$materiales_espera[$h]->cod_kardex_tienda;
                      }
                    }
                    $stockact=0;
                    $stockact=$stock_tot;
                    $act=KardexTienda::where('cod_kardex_tienda',$codigo)
                    ->decrement('stock_total_tienda',$stockact);
                  }
                  else
                  {
                  break;
                  }
                }
              }
              else {
                $material.="$codigos[$j] ";
              }
            }
        }
      }
    }
    if($codigos_os!="")
    {
      if($material=="")
      {
        session()->flash('success','Salida de materiales registrada satisfactoriamente');
      }
      else {
        session()->flash('warning','Salida de materiales satisfactoriamente, excepto los siguientes materiales: '.$material);
      }
      session()->flash('salida_tienda',array('Descargar Orden de Salida','Aqui',$codigos_os));
    }
    else
    {
      session()->flash('warning','Ingrese algun material');
    }
    return Redirect::to('logistica/tiendas');
  }

  public function devolucion_material()
  {
    $empresa=Auth::user()->RUC_empresa;
    $categorias=DB::table('categoria')->get();
    $subcategorias=DB::table('subcategoria')->where('estado_subcategoria','=',1)->orderBy('nom_subcategoria')->get();
    $almacen_tienda=DB::table('almacen')
    ->where('tipo_almacen','=',1)
    ->where('estado_almacen','=',1)
    ->where('RUC_empresa','=',$empresa)
    ->get();
    return view('logistica.salida.devolucion_tienda',["categorias"=>$categorias,"subcategorias"=>$subcategorias,'almacen_tienda'=>$almacen_tienda]);
  }
  public function obt_material($var)
  {
    $datos=explode("+",$var);
    $empresa=Auth::user()->RUC_empresa;
    $materiales=DB::table('kardex_tienda')
      ->join('almacen','kardex_tienda.codigo_almacen','=','almacen.cod_almacen')
      ->join('material','kardex_tienda.codigo_material','=','material.cod_material')
      ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->where('kardex_tienda.RUC_empresa','=',$empresa)
      ->where('kardex_tienda.stock_total_tienda','>','0')
      ->where('material.t_compra','=',$datos[1])
      ->where('material.cod_subcategoria','=',$datos[0])
      ->orderBy('material.descrip_material',"asc")
      ->select("stock_total_tienda",'material.descrip_material','kardex_tienda.codigo_material','kardex_tienda.cod_kardex_tienda','subcategoria.nom_subcategoria','unidad_medida.descrip_unidad_medida','subcategoria.cod_categoria','material.cod_material')
      ->groupBy('kardex_tienda.codigo_material')
      ->get();
      return $materiales;
  }
  public function guardar_devolucion(Request $request)
  {
    $mytime = date('Y-m-d G:i:s');
    $empresa=Auth::user()->RUC_empresa;
    $almacen=Input::get('almacen_reingresar');
    $cod_material=Input::get('codigo');
    $cod_tallas=Input::get('idtalla');
    $cantidadsalida=Input::get('salida');//ingreso de material
    $persona=Input::get('persona');
    $comentario=Input::get('comentarios');
    $material="";
    if($almacen=="p")
    {
      $almacen_prioridades=DB::Table('almacen')
      ->where('tipo_almacen','=',1)
      ->where('estado_almacen','=',1)
      ->OrderBy('prioridad','asc')
      ->select('cod_almacen','prioridad')
      ->get();
      if(is_array($cod_material))
      {
        for( $i=0;$i<count($cod_material);$i++)
        {
          $aleatorio=$comentario[$i];
          $ingreso=$cantidadsalida[$i];
          $flag=0;
          for($j=0;$j<count($almacen_prioridades);$j++)
          {
            if($flag==0)
            {
              $materiales_espera=DB::table('kardex_tienda')
              ->join('material','kardex_tienda.codigo_material','=','material.cod_material')
              ->where('cod_kardex_tienda','=',$almacen_prioridades[$j]->cod_almacen.$cod_material[$i])
              ->select('kardex_tienda.cod_kardex_tienda','material.costo_sin_igv_material','kardex_tienda.stock_total_tienda')
              ->get();
              $codigo="";
              if(count($materiales_espera)>0)
              {
                $flag=1;
                $nuevo_stock=$materiales_espera[0]->stock_total_tienda+$ingreso;
                $detalle=new DetalleKardexTienda;
                $detalle->dni_usuario=Auth::user()->id;
                $detalle->cod_area=47812;
                $detalle->cod_kardex_tienda=$materiales_espera[0]->cod_kardex_tienda;
                $detalle->costo_material=$materiales_espera[0]->costo_sin_igv_material;
                $detalle->fecha_devolucion=$mytime;
                $detalle->cantidad_ingresada=$ingreso;
                $detalle->trasladador_material=$persona[$i];
                $detalle->stock=$nuevo_stock;
                $detalle->comentario_devolucion=$comentario[$i];
                $detalle->estado_detalle_kardex=1;
                $detalle->restante_detalle=$ingreso;
                $detalle->save();
                
                KardexTienda::where('cod_kardex_tienda','=',$materiales_espera[0]->cod_kardex_tienda)
                ->update(['stock_total_tienda'=>$nuevo_stock]);
              }
            }
            else{
            break;
            }
          } 
        }
      }
      if(is_array($cod_tallas))
      {
        for($i=0;$i<count($cod_tallas);$i++)
        {
            $codigos="";
            $persona="";
            $cantidad_ingreso="";$comentario="";
            $persona=Input::get($cod_tallas[$i].'persona');
            $comentario=Input::get($cod_tallas[$i].'comentarios');
            $cantidad_ingreso=Input::get($cod_tallas[$i].'ca');
            $codigos=Input::get($cod_tallas[$i].'cods');
            $aleatorio=rand(10000,99999);
            for($j=0;$j<count($codigos);$j++)
            {
              $flag=0;
              $ingresos=$cantidad_ingreso[$j];
              for($k=0;$k<count($almacen_prioridades);$k++)
              {
                if($flag==0)
                {
                  $materiales_espera=DB::table('kardex_tienda')
                  ->join('material','kardex_tienda.codigo_material','=','material.cod_material')
                  ->where('cod_kardex_tienda','=',$almacen_prioridades[$k]->cod_almacen.$codigos[$j])
                  ->select('kardex_tienda.cod_kardex_tienda','material.costo_sin_igv_material','kardex_tienda.stock_total_tienda')
                  ->get();
                  if(count($materiales_espera)>0)
                  {
                    $stock_tot=$materiales_espera[0]->stock_total_tienda+$ingresos;
                    $detalle=new DetalleKardexTienda;
                    $detalle->dni_usuario=Auth::user()->id;
                    $detalle->cod_area=47812;
                    $detalle->cod_kardex_tienda=$materiales_espera[0]->cod_kardex_tienda;
                    $detalle->costo_material=$materiales_espera[0]->costo_sin_igv_material;
                    $detalle->fecha_devolucion=$mytime;
                    $detalle->cantidad_ingresada=$ingresos;
                    $detalle->trasladador_material=$persona[$i];
                    $detalle->stock=$stock_tot;
                    $detalle->comentario_devolucion=$comentario[$i];
                    $detalle->estado_detalle_kardex=1;
                    $detalle->restante_detalle=$ingresos;
                    $detalle->save();
                    $act=KardexTienda::where('cod_kardex_tienda','=',$materiales_espera[0]->cod_kardex_tienda)
                    ->update(['stock_total_tienda'=>$stock_tot]);   
                  }    
                }
                else
                {
                break;
                }   
              }         
            }
        }
      }
    }
    if($material=="")
    {
      session()->flash('success','Salida de materiales registrada satisfactoriamente');
    }
    else {
      session()->flash('warning','Salida de materiales satisfactoriamente, excepto los siguientes materiales: '.$material);
    }
    return Redirect::to('logistica/tiendas');
  }
  public function orden_salida(){

  }
}
