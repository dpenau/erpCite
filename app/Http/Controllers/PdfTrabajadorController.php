<?php

namespace erpCite\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use PDF;
use \Milon\Barcode\DNS1D;
class PdfTrabajadorController extends Controller
{
  public function index(Request $request){
    if ($request) {
      $query=trim($request->get('codigo'));
      $pdf=\App::make('dompdf.wrapper');
      $pdf->loadHTML($this->convert_data($query));
      return $pdf->stream();
    }
  }
  function get_data($query)
  {
    $trabajador=DB::table('trabajador')
    ->join('area','trabajador.cod_area','=','area.cod_area')
    ->join('tipo_trabajador','trabajador.cod_tipo_trabajador','=','tipo_trabajador.cod_tipo_trabajador')
    ->join('distrito','trabajador.cod_distrito','=','distrito.cod_distrito')
    ->join('ciudad','distrito.cod_ciudad','=','ciudad.cod_ciudad')
    ->where('trabajador.RUC_empresa',Auth::user()->RUC_empresa)
    ->orderBy('apellido_paterno','ASC')
    ->get();
    return $trabajador;
  }
  function get_imagen()
  {
    $idempresa=Auth::user()->RUC_empresa;
    $imagen=DB::table('empresa')->where('RUC_empresa','=',$idempresa)->limit(1)->get();
    return $imagen;
  }
  function convert_data($query)
  {
    $detalle=$this->get_data($query);
    $img=$this->get_imagen();
    $photo="";
    foreach ($img as $i) {
        if($i->imagen!="")
        {
          $photo=$i->imagen;
        }
    }
    $output='<html><head><style>
    @page {
          margin: 0cm 0cm;
    }
    body {
          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {

          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    </style></head><body>';
    if ($photo!="") {
      $output.='
      <header>
      <div class="row">
        <div class="col-md-12">
          <img src="photo/'.$photo.'" alt="" style="width:120px;" class="img-rounded center-block">
        </div>
      </div>
      </header>
      <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
      ';
    }
  	$output.='<h1>Lista de Trabajadores</h1>
    <table style="width:100%;border-collapse: collapse; border: 1px solid black;">
    <tr>
      <th style="border-collapse: collapse; border: 1px solid black;">Nombre del Trabajador</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Puesto</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Tipo</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Area</th>
    </tr>
  ';
    foreach ($detalle as $dat) {
      if($dat->estado_trabajador==0)
      {
        $output.='
        <tr>
          <td style="border-collapse: collapse; border: 1px solid black; color:red;">'.$dat->apellido_paterno." ".$dat->apellido_materno.",".$dat->nombres.'</td>
          <td style="border-collapse: collapse; border: 1px solid black; color:red;">'.$dat->puesto.'</td>
          <td style="border-collapse: collapse; border: 1px solid black; color:red;">'.$dat->descrip_tipo_trabajador.'</td>
          <td style="border-collapse: collapse; border: 1px solid black; color:red;">'.$dat->descrip_area.'</td>
        </tr>
        ';
      }
      else {
        $output.='
        <tr>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->apellido_paterno." ".$dat->apellido_materno.",".$dat->nombres.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->puesto.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_tipo_trabajador.'</td>
          <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_area.'</td>
        </tr>
        ';
      }

    }
    $output.='</table>
    </body></html>
    ';
      return $output;

  }


}
