<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use PDF;
use \Milon\Barcode\DNS1D;

class PdfMaterialController extends Controller
{
    public function index($var)
    {
        if ($var != "") {
            $query = explode('-', $var);
            $pdf = \App::make('dompdf.wrapper');
            $pdf->setPaper('a4', 'landscape');
            $pdf->loadHTML($this->convert_data($query[0]));
            return $pdf->stream();
        }
    }
    function get_data($query)
    {
        $orden_data = DB::table('material')
            ->join('unidad_medida', 'material.unidad_compra', '=', 'unidad_medida.cod_unidad_medida')
            ->join('subcategoria', 'material.cod_subcategoria', '=', 'subcategoria.cod_subcategoria')
            ->join('categoria', 'subcategoria.cod_categoria', '=', 'categoria.cod_categoria')
            ->where('estado_material', '=', '1')
            ->where('RUC_empresa', Auth::user()->RUC_empresa)
            ->where('material.cod_subcategoria', '=', $query)
            ->orderBy('categoria.nom_categoria', 'asc')
            ->orderBy('material.descrip_material', 'asc')
            ->get();
        return $orden_data;
    }
    function get_imagen()
    {
        $idempresa = Auth::user()->RUC_empresa;
        $imagen = DB::table('empresa')->where('RUC_empresa', '=', $idempresa)->limit(1)->get();
        return $imagen;
    }
    function convert_data($query)
    {
        $detalle = $this->get_data($query);
        $img = $this->get_imagen();
        $photo = "";
        foreach ($img as $i) {
            if ($i->imagen != "") {
                $photo = $i->imagen;
            }
        }
        $output = '<html><head><style>
    @page {
          margin: 0cm 0cm;
    }
    body {
          margin-top: 1cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 1cm;
    }
    .header {

          position: absolute;
          right: 20px;
          top: -10px;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    </style></head><body>';
        if ($photo != "") {
            $output .= '
      <div class=header >
          <img  src="photo/' . $photo . '" alt="" style="width:70px;" class="img-rounded center-block">
      </div>
      <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
      ';
        }
        $output .= '        <div class="col-md-6 pl-3">
        <h2 class="pl-5">Lista de Materiales</h2>
        </div>
    <table style="width:100%;border-collapse: collapse; border: 1px solid black;">
    <tr>
      <th style="border-collapse: collapse; border: 1px solid black;">Codigo</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Categoria</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Subcategoria</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Descripcion del Material</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Codigo de Barras</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Unidad de compra</th>
      <th style="border-collapse: collapse; border: 1px solid black;">Valor Unitario</th>
    </tr>
  ';
        foreach ($detalle as $dat) {
            $output .= '
      <tr>
        <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->cod_material . '</td>
        <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->nom_categoria . '</td>
        <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->nom_subcategoria . '</td>
        <td style="border-collapse: collapse; border: 1px solid black;">' . $dat->descrip_material . '</td>
        <td style="border-collapse: collapse; border: 1px solid black;padding-left:10%;padding-bottom:5%;">' . DNS1D::getBarcodeHTML($dat->cod_material, "c128") . '</td>
        <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->descrip_unidad_medida . '</td>
        <td style="border-collapse: collapse; border: 1px solid black;text-align: center;">' . $dat->costo_sin_igv_material . '</td>
      </tr>
      ';
        }
        $output .= '</table>
    </body></html>
    ';
        return $output;
    }
}
