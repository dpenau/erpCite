<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class PdfAlmacenController extends Controller
{
    public function index($var){
        if ($var) {
          
          $pdf=\App::make('dompdf.wrapper');
          $pdf->loadHTML($this->convert_data($var));
          return $pdf->stream();
        }
      }
      function get_data($var)
      {
        $materiales_almacen=DB::table('kardex_material')
        ->join('material','kardex_material.cod_material','=','material.cod_material')
        ->join('unidad_medida','material.unidad_medida','=','unidad_medida.cod_unidad_medida')
        ->where('kardex_material.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->where('kardex_material.cod_almacen','=',$var)
        ->select('material.descrip_material','unidad_medida.descrip_unidad_medida','kardex_material.lugar_almacenaje','kardex_material.stock_total')
        ->orderBy('material.descrip_material','ASC')
        ->get();
        return $materiales_almacen;
      }
      function get_cabecera()
        {
            $cabecera=DB::table('empresa')
            ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
            ->get();
            return $cabecera;
        }
      function convert_data($var)
      {
        $data=explode("-",$var);
        $img=$this->get_cabecera();
        $photo="";
        $detalle=$this->get_data($data[0]);
        
          $output='
        <html><head><style>
        @page {
              margin: 0cm 0cm;
        }
        body {
    
              margin-top: 4cm;
              margin-left: 2cm;
              margin-right: 2cm;
              margin-bottom: 2cm;
        }
        header {
              position: fixed;
              top: 0.5cm;
              left: 0.5cm;
              right: 0cm;
              height: 3cm;
        }
        footer {
              margin-right: 0cm;
              position: fixed;
              bottom: 0cm;
              left: 0cm;
              right: 0cm;
              height: 2cm;
        }
        </style></head><body>';
        foreach ($img as $i) {
            if($i->imagen!="")
            {
              $photo=$i->imagen;
            }
        }
        if ($photo=="") {
          $output.='
          <h3>Kardex Actual:</h3>
          ';
        }
        else {
          $output.='
          <header>
          <div class="row">
            <div class="col-md-12">
              <img src="photo/'.$photo.'" alt="" style="width:120px;" class="img-rounded center-block">
            </div>
          </div>
          </header>
          <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
          <h1>Materiales en el almacen:'.$data[1].'</h1>
          <h3>Kardex del '.date("d-m-Y").':</h3>
          ';
        }
        $output.='
        <table style="width:100%;border-collapse: collapse; border: 1px solid black;">
        <tr>
          <th style="border-collapse: collapse; border: 1px solid black;">Descripcion del material</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Stock Actual</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Unidad de medida</th>
          <th style="border-collapse: collapse; border: 1px solid black;">Lugar de almacenaje</th>
        </tr>
      ';
        foreach ($detalle as $dat) {
          $output.='
          <tr>
            <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_material.'</td>
            <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->stock_total.'</td>
            <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->descrip_unidad_medida.'</td>
            <td style="border-collapse: collapse; border: 1px solid black;">'.$dat->lugar_almacenaje.'</td>
          </tr>
          ';
        }
        $output.='</table>
    <br>
    <br>
    <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer></body></html>
    ';
          return $output;
      }    
}
