<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class PdfKardexActualController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function index($var){
      $var=explode("-",$var);
      $pdf=\App::make('dompdf.wrapper');
      //$pdf->setPaper('a4','landscape');
      $pdf->loadHTML($this->convert_data($var));
      return $pdf->stream();
  }
  public function tienda_actual($var)
  {
    $var=explode("-",$var);
    $pdf=\App::make('dompdf.wrapper');
    //$pdf->setPaper('a4','landscape');
    $pdf->loadHTML($this->convert_data_tienda($var));
    return $pdf->stream();
  }
  function get_cabecera()
  {
    $cabecera=DB::table('empresa')
    ->where('RUC_empresa','=',Auth::user()->RUC_empresa)
    ->get();
    return $cabecera;
  }
  function convert_data($var)
  {
    $img=$this->get_cabecera();
    $photo="";
    $output='<html><head><style>
    @page {
          margin: 0cm 0cm;
    }
    body {
          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {

          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    </style></head><body>';
    foreach ($img as $i) {
        if($i->imagen!="")
        {
          $photo=$i->imagen;
        }
    }
    if ($photo=="") {
      $output.='
      <h3>Kardex Actual:</h3>
      ';
    }
    else {
      $output.='
      <header>
      <div class="row">
        <div class="col-md-12">
          <img src="photo/'.$photo.'" alt="" style="width:120px;" class="img-rounded center-block">
        </div>
      </div>
      </header>
      <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
      <h3>Kardex del '.date("d-m-Y").':</h3>
      <h4><small style="background-color:#ffae42">Stock Bajo</small> <small style="background-color:#bb2124;color:white">Stock Alto</small></h4>
      ';
    }
    $output.='
      <table  style="border-collapse: collapse; border: 1px solid black; margin-left: auto;  margin-right: auto; font-size: 12px;">
    ';
          $detalle=$orden_data=DB::table('kardex_material')
          ->join('almacen','kardex_material.cod_almacen','=','almacen.cod_almacen')
          ->join('material','kardex_material.cod_kardex_material','=','material.cod_material')
          ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
          ->where('kardex_material.RUC_empresa','=',Auth::user()->RUC_empresa)
          ->where('material.cod_subcategoria','=',$var[0])
          ->select('material.descrip_material','kardex_material.lugar_almacenaje','kardex_material.stock_total','almacen.nom_almacen','unidad_medida.descrip_unidad_medida','material.stock_maximo','material.stock_minimo')
          ->orderBy('material.descrip_material','asc')
          ->get();
          $output.='
            <tr >
            <td colspan="4" style="border-collapse: collapse; border: 1px solid black;background-color:#E9C5C5">'.strtoupper($var[1]).'</td>
            </tr>
          ';
          if(count($detalle)>0)
          {
            $output.='
            <tr>
              <th style="border-collapse: collapse; border: 1px solid black;">Material</th>
              <th style="border-collapse: collapse; border: 1px solid black;">Stock Actual</th>
              <th style="border-collapse: collapse; border: 1px solid black;">Unidad de Compra</th>
              <th style="border-collapse: collapse; border: 1px solid black;">Almacen - Ubicacion</th>
            </tr>
            ';
          }
          else {
            $output.='
              <tr>
              <td colspan="4" style="border-collapse: collapse; border: 1px solid black">NO HAY MATERIALES</td>
              </tr>
            ';
          }
          foreach($detalle as $det)
          {
            $total="";
            if ($det->stock_total<=$det->stock_minimo) {
              //amarillo
              $total.='<td style="border-collapse: collapse; border: 1px solid black;background-color:#ffae42">'.$det->stock_total.'</td>';
            }
            else {
              if ($det->stock_total>=$det->stock_maximo) {
                // ROJO
                $total.='<td style="border-collapse: collapse; border: 1px solid black;background-color:#bb2124">'.$det->stock_total.'</td>';
              }
              else {
                // code...
                $total.='<td style="border-collapse: collapse; border: 1px solid black;">'.$det->stock_total.'</td>';
              }
            }
            $output.='
            <tr>
            <td style="border-collapse: collapse; border: 1px solid black;">'.strtoupper($det->descrip_material).'</td>
            '.$total.'
            <td style="border-collapse: collapse; border: 1px solid black;">'.strtoupper($det->descrip_unidad_medida).'</td>
            <td style="border-collapse: collapse; border: 1px solid black;">'.strtoupper($det->nom_almacen).' -<br>'.strtoupper($det->lugar_almacenaje).'</td>
            </tr>
            ';
          }

    $output.=' </body></html>
    ';
    return $output;
  }
  function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
  }
  function convert_data_tienda($var)
  {
    $img=$this->get_cabecera();
    $photo="";
    $output='<html><head><style>
    @page {
          margin: 0cm 0cm;
    }
    body {
          margin-top: 4cm;
          margin-left: 2cm;
          margin-right: 2cm;
          margin-bottom: 2cm;
    }
    header {

          position: fixed;
          top: 0.5cm;
          left: 0.5cm;
          right: 0cm;
          height: 3cm;
    }
    footer {
          margin-right: 0cm;
          position: fixed;
          bottom: 0cm;
          left: 0cm;
          right: 0cm;
          height: 2cm;
    }
    </style></head><body>';
    foreach ($img as $i) {
        if($i->imagen!="")
        {
          $photo=$i->imagen;
        }
    }
    if ($photo=="") {
      $output.='
      <h3>Kardex Actual:</h3>
      ';
    }
    else {
      $output.='
      <header>
      <div class="row">
        <div class="col-md-12">
          <img src="photo/'.$photo.'" alt="" style="width:120px;" class="img-rounded center-block">
        </div>
      </div>
      </header>
      <footer><img src="photo/pie2.png" width="100%" height="100%"/></footer>
      <h3>Kardex del '.date("d-m-Y").':</h3>
      <h4><small style="background-color:#ffae42">Stock Bajo</small> <small style="background-color:#bb2124;color:white">Stock Alto</small></h4>
      ';
    }
    $output.='
      <table  style="border-collapse: collapse; border: 1px solid black; margin-left: auto;  margin-right: auto; font-size: 12px;">
    ';
      $almacenes_tienda=DB::table('almacen')
      ->where('tipo_almacen','=',1)
      ->where('estado_almacen','=',1)
      ->select('cod_almacen','nom_almacen')
      ->get();    
      foreach($almacenes_tienda as $almacen)
      {
        $detalle=$orden_data=DB::table('kardex_tienda')
        ->join('almacen','kardex_tienda.codigo_almacen','=','almacen.cod_almacen')
        ->join('material','kardex_tienda.codigo_material','=','material.cod_material')
        ->join('unidad_medida','material.unidad_compra','=','unidad_medida.cod_unidad_medida')
        ->where('kardex_tienda.codigo_almacen','=',$almacen->cod_almacen)
        ->where('kardex_tienda.RUC_empresa','=',Auth::user()->RUC_empresa)
        ->where('material.cod_subcategoria','=',$var[0])
        ->select('material.descrip_material','kardex_tienda.lugar_tienda','kardex_tienda.stock_total_tienda','almacen.nom_almacen','unidad_medida.descrip_unidad_medida','material.stock_maximo','material.stock_minimo')
        ->orderBy('material.descrip_material','asc')
        ->get();
        $color_random=$this->random_color_part();
        $output.='
         <tr >
          <td colspan="4" style="border-collapse: collapse; border: 1px solid black;background-color:#80E650;">'.strtoupper($almacen->nom_almacen).'</td>
          </tr>
         ';
         if(count($detalle)>0)
         {
           $output.='
           <tr>
             <th style="border-collapse: collapse; border: 1px solid black;">Material</th>
             <th style="border-collapse: collapse; border: 1px solid black;">Stock Actual</th>
             <th style="border-collapse: collapse; border: 1px solid black;">Unidad de Compra</th>
             <th style="border-collapse: collapse; border: 1px solid black;">Almacen - Ubicacion</th>
           </tr>
           ';
            foreach($detalle as $det)
            {
              $total="";
              if ($det->stock_total_tienda<=$det->stock_minimo) {
                //amarillo
                $total.='<td style="border-collapse: collapse; border: 1px solid black;background-color:#ffae42">'.$det->stock_total_tienda.'</td>';
              }
              else {
                if ($det->stock_total_tienda>=$det->stock_maximo) {
                  // ROJO
                  $total.='<td style="border-collapse: collapse; border: 1px solid black;background-color:#bb2124">'.$det->stock_total_tienda.'</td>';
                }
                else {
                  // code...
                  $total.='<td style="border-collapse: collapse; border: 1px solid black;">'.$det->stock_total_tienda.'</td>';
                }
              }
              $output.='
              <tr>
              <td style="border-collapse: collapse; border: 1px solid black;">'.strtoupper($det->descrip_material).'</td>
              '.$total.'
              <td style="border-collapse: collapse; border: 1px solid black;">'.strtoupper($det->descrip_unidad_medida).'</td>
              <td style="border-collapse: collapse; border: 1px solid black;">'.strtoupper($det->nom_almacen).' -<br>'.strtoupper($det->lugar_tienda).'</td>
              </tr>
              ';
            }
         }
         else {
           $output.='
           <tr>
             <td colspan="4" style="border-collapse: collapse; border: 1px solid black">NO HAY MATERIALES</td>
           </tr>
           ';
         }
      }    
    $output.=' </body></html>
    ';
    return $output;
  }
}
