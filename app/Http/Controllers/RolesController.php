<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\RolesModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index(Request $request)
    {
        if($request)
        {
            $roles=DB::table('roles')->where('id','!=','1')->get();
            return view('Mantenimiento.Roles.index',["roles"=>$roles]);
        }


    }
    public function create(Request $request)
    {
        if($request)
        {
            return view("Mantenimiento.Roles.create");
        }
    }
    public function store()
    {
        //ini_set('date.timezone','America/Lima');
        //$tiempo= date("g:i a", strtotime(date('Y-m-d H:i',time())));
        $identificador=rand(10000,99999);
        $rol=new RolesModel;
        $rol->id=$identificador;
        $rol->name=Input::get('name');
        $rol->description=Input::get('description');
        $rol->estado_rol=1;
        $rol->save();
        session()->flash('success','Rol Registrado');
        return Redirect::to('Mantenimiento/Roles');
    }
    public function show()
    {
        return view('Mantenimiento.Roles.index');
    }
    public function edit($id)
    {
        return Redirect::to('Mantenimiento/Roles');
    }
    public function update()
    {
      $cod=Input::get('cod_rol_editar');
      $nombre=Input::get('nombre');
      $descripcion=Input::get('descripcion');
      $act=RolesModel::where('id',$cod)
      ->update(['name'=>$nombre,'description'=>$descripcion]);
      session()->flash('success','Rol Actualizado');
        return Redirect::to('Mantenimiento/Roles');
    }
    public function destroy($id)
    {
      $cod=Input::get('cod_rol_eliminar');
      $accion=Input::get('accion');
      if($accion==0)
      {
        $mensaje="Desactivado";
      }
      else {
        $mensaje="Activado";
      }
      $act=RolesModel::where('id',$cod)
      ->update(['estado_rol'=>$accion]);
      session()->flash('success','Rol '.$mensaje);
        return Redirect::to('Mantenimiento/Roles');
    }
}
