<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use erpCite\TipoContribuyenteModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
class TipoContribuyenteController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index(Request $request)
    {
        if($request)
        {
            $contribuyentes=DB::table('tipo_contribuyente')->get();
            return view('Mantenimiento.Contribuyente.index',["contribuyentes"=>$contribuyentes]);
        }


    }
    public function create(Request $request)
    {
        if($request)
        {
            return view("Mantenimiento.Contribuyente.create");
        }
    }
    public function store()
    {
        $identificador=rand(10000,99999);
        $contribuyente=new TipoContribuyenteModel;
        $contribuyente->cod_tipo_contribuyente=$identificador;
        $contribuyente->descrip_tipo_contribuyente=Input::get('descripcion');
        $contribuyente->estado_tipo_contribuyente=1;
        $contribuyente->save();
        session()->flash('success','Contribuyente Registrado');
        return Redirect::to('Mantenimiento/Contribuyente');
    }
    public function show()
    {
        return view('Mantenimiento.Contribuyente.index');
    }
    public function edit($id)
    {
        return Redirect::to('Mantenimiento/Contribuyente');
    }
    public function update()
    {
      $cod=Input::get('cod_contribuyente_editar');
      $descrip_nueva=Input::get('descripcion');
      $act=TipoContribuyenteModel::where('cod_tipo_contribuyente',$cod)
      ->update(['descrip_tipo_contribuyente'=>$descrip_nueva]);
      session()->flash('success','Contribuyente Actualizado');
        return Redirect::to('Mantenimiento/Contribuyente');
    }
    public function destroy($id)
    {
      $cod=Input::get('cod_contribuyente_eliminar');
      $accion=Input::get('accion');
      if($accion==0)
      {
        $mensaje="Desactivado";
      }
      else {
        $mensaje="Activado";
      }
      $act=TipoContribuyenteModel::where('cod_tipo_contribuyente',$cod)
      ->update(['estado_tipo_contribuyente'=>$accion]);
      session()->flash('success','Contribuyente '.$mensaje);
        return Redirect::to('Mantenimiento/Contribuyente');
    }
}
