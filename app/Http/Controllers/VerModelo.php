<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Charts;
use erpCite\CatalogoPreciosModel;
use erpCite\DetalleCostoModeloMaterial;
use erpCite\DetalleCostoModeloMano;
use erpCite\CostoModelo;
use Illuminate\Support\Facades\Auth;
class VerModelo extends Controller
{
  public function __construct()
  {
    $this->middleware('costo');
  }
  public function index(Request $request)
  {
    if ($request) {
      $empresa=Auth::user()->RUC_empresa;
      $modelos=DB::table('serie_modelo')
      ->join("modelo",'serie_modelo.codigo_modelo','=','modelo.cod_modelo')
      ->join("serie",'serie_modelo.codigo_serie','=','serie.cod_serie')
      ->leftJoin('costo_modelo','serie_modelo.codigo','=','costo_modelo.modelo_serie')
      ->where('serie_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
      ->where('costo_modelo.estado','!=','0')
      ->where(function($query){
        $query->orWhere('serie_modelo.estado','=','1');
      })
      ->get();

      return view('costos.index',['modelos'=>$modelos,"empresa"=>$empresa]);
    }
  }
  public function actualizar_costo()
  {
    $actualizar=DB::table('detalle_costo_modelo_materiales')
    ->join('material','detalle_costo_modelo_materiales.cod_material','=','material.cod_material')
    ->where('detalle_costo_modelo_materiales.estado_material_costos','=','3')
    ->select('detalle_costo_modelo_materiales.cod_detalle_costo_material','detalle_costo_modelo_materiales.consumo_por_par',
    'material.costo_sin_igv_material','detalle_costo_modelo_materiales.total',
    'material.factor_equivalencia','detalle_costo_modelo_materiales.cod_costo_modelo','detalle_costo_modelo_materiales.estado_material_costos')
    ->get();
    $obra=DB::table('detalle_costo_modelo_manoobra')
    ->where('detalle_costo_modelo_manoobra.estado_trabajador','=','2')
    ->select('detalle_costo_modelo_manoobra.cod_detalle_costo_mano','detalle_costo_modelo_manoobra.otros_costos',
    'detalle_costo_modelo_manoobra.costo','detalle_costo_modelo_manoobra.costo_por_par','detalle_costo_modelo_manoobra.cod_tipo_trabajador','detalle_costo_modelo_manoobra.cod_costo_modelo')
    ->get();
    $total_material=0;
    $anterior=0;
    if(count($actualizar)>=1)
    {
      $anterior=$actualizar[0]->cod_costo_modelo;
    }
    for($i=0;$i<=count($actualizar);$i++)
    {
      if ($i==count($actualizar)) {
        $act=CostoModelo::where('cod_costo_modelo',$anterior)->select('total_materiales')->get();
        if(count($act)==1)
        {
          $total_materiales=number_format($act[0]->total_materiales+$total_material, 6, '.', '');
          $modelo=DB::table('costo_modelo')
          ->join('serie_modelo','costo_modelo.modelo_serie','=','serie_modelo.codigo')
          ->where('costo_modelo.cod_costo_modelo','=',$anterior)
          ->select('serie_modelo.estado AS serie_estado','costo_modelo.estado AS modelo_estado')
          ->get();
          if($modelo[0]->modelo_estado==7)
          {
            $act_costo=CostoModelo::where('cod_costo_modelo',$anterior)
            ->update(['total_materiales'=>$total_materiales,'estado'=>$modelo[0]->serie_estado]);
          }
        }
      }
      else {
        if($anterior!=$actualizar[$i]->cod_costo_modelo)
        {
          $act=CostoModelo::where('cod_costo_modelo',$anterior)->select('total_materiales')->get();
          if(count($act)==1)
          {
            $total_materiales=number_format($act[0]->total_materiales+$total_material, 6, '.', '');
            $modelo=DB::table('costo_modelo')
            ->join('serie_modelo','costo_modelo.modelo_serie','=','serie_modelo.codigo')
            ->where('costo_modelo.cod_costo_modelo','=',$anterior)
            ->select('serie_modelo.estado AS serie_estado','costo_modelo.estado AS modelo_estado')
            ->get();
            if($modelo[0]->modelo_estado==7)
            {
              $act_costo=CostoModelo::where('cod_costo_modelo',$anterior)
              ->update(['total_materiales'=>$total_materiales,'estado'=>$modelo[0]->serie_estado]);
            }
          }
          $anterior=$actualizar[$i]->cod_costo_modelo;
          $total_material=0;
        }
        $nuevo_total=0;
        $anterior_total=$actualizar[$i]->total;
        $consumo=number_format($actualizar[$i]->consumo_por_par, 6, '.', '');
        $precio=number_format($actualizar[$i]->costo_sin_igv_material, 6, '.', '');
        $factor=number_format($actualizar[$i]->factor_equivalencia, 6, '.', '');
        $nuevo_total=number_format(($precio*$consumo)/$factor, 6, '.', '');
        $total_material=$total_material+($nuevo_total-$anterior_total);
        $act=DetalleCostoModeloMaterial::where('cod_detalle_costo_material',$actualizar[$i]->cod_detalle_costo_material)
        ->update(['total'=>$nuevo_total,'estado_material_costos'=>1]);
      }
    }
    $total_material=0;
    $anterior=0;
    if (count($obra)>=1) {
      $anterior=$obra[0]->cod_costo_modelo;
    }
    $empresa=Auth::user()->RUC_empresa;
    $empresas=DB::table('empresa')
    ->where('RUC_empresa','=',$empresa)
    ->select('cod_regimen_laboral')
    ->get();
    $beneficios=DB::table('escala_salarial')
    ->join('trabajador','escala_salarial.cod_trabajador','=','trabajador.DNI_trabajador')
    ->where('escala_salarial.RUC_empresa','=',$empresa)
    ->where('escala_salarial.cod_regimen_laboral','=',$empresas[0]->cod_regimen_laboral)
    ->where('trabajador.estado_trabajador','=',1)
    ->where('escala_salarial.estado','=',1)
    ->avg('tasa_beneficio');
    if($beneficios=="")
    {
      $beneficios=0;
    }
    else {
      $beneficios=$beneficios/100;
    }
    $produccion_promedio=DB::Table('datos_generales')
    ->where('RUC_empresa','=',$empresa)
    ->sum('produccion_promedio');
    if($produccion_promedio=="")
    {
      $produccion_promedio=0;
    }
    //
    for ($i=0; $i <= count($obra); $i++) {
      if ($i==count($obra)) {
        $act=CostoModelo::where('cod_costo_modelo',$anterior)->select('total_obra')->get();
        if(count($act)==1)
        {
          $total_materiales=number_format($act[0]->total_obra+$total_material, 6, '.', '');
          $modelo=DB::table('costo_modelo')
          ->join('serie_modelo','costo_modelo.modelo_serie','=','serie_modelo.codigo')
          ->where('costo_modelo.cod_costo_modelo','=',$anterior)
          ->select('serie_modelo.estado AS serie_estado','costo_modelo.estado AS modelo_estado')
          ->get();
          if($modelo[0]->modelo_estado==7)
          {

            $act_costo=CostoModelo::where('cod_costo_modelo',$anterior)
            ->update(['total_obra'=>$total_materiales,'estado'=>$modelo[0]->serie_estado]);
          }
        }
      }
      else {
        if($anterior!=$obra[$i]->cod_costo_modelo)
        {
          $act=CostoModelo::where('cod_costo_modelo',$anterior)->select('total_obra')->get();
          if(count($act)==1)
          {
            $total_materiales=number_format($act[0]->total_obra+$total_material, 6, '.', '');
            $modelo=DB::table('costo_modelo')
            ->join('serie_modelo','costo_modelo.modelo_serie','=','serie_modelo.codigo')
            ->where('costo_modelo.cod_costo_modelo','=',$anterior)
            ->select('serie_modelo.estado AS serie_estado','costo_modelo.estado AS modelo_estado')
            ->get();
            if($modelo[0]->modelo_estado==7)
            {

              $act_costo=CostoModelo::where('cod_costo_modelo',$anterior)
              ->update(['total_obra'=>$total_materiales,'estado'=>$modelo[0]->serie_estado]);
            }
          }
          $anterior=$obra[$i]->cod_costo_modelo;
          $total_material=0;
        }
        $nuevo_total=0;
        $tipo_de_trabajador=$obra[$i]->cod_tipo_trabajador;
        $anterior_total=$obra[$i]->costo_por_par;
        $costo=number_format($obra[$i]->costo, 6, '.', '');
        $otro_costo=number_format($obra[$i]->otros_costos, 6, '.', '');
        $nuevo_total=0;
        $beneficio=0;
        switch ($tipo_de_trabajador) {
          case 1:
            $beneficio=$costo*$beneficios;
            $nuevo_total=number_format(($beneficio+$costo+$otro_costo)/$produccion_promedio, 6, '.', '');
          break;
          case 2:
            $nuevo_total=number_format(($costo+$otro_costo)/($produccion_promedio/4), 6, '.', '');
          break;
          case 3:
            $nuevo_total=number_format(($costo+$otro_costo)/12, 6, '.', '');
          break;
          default:
          break;
        }
        $total_material=$total_material+($nuevo_total-$anterior_total);
        $act=DetalleCostoModeloMano::where('cod_detalle_costo_mano',$obra[$i]->cod_detalle_costo_mano)
        ->update(['costo_por_par'=>$nuevo_total,'estado_trabajador'=>1,'beneficio'=>$beneficio]);
      }
    }

    return "S";
  }
}
