<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\OrdenPedidoModel;
use erpCite\DetalleOrdenPedidoModel;
use DB;

class OrdenPedidoController extends Controller
{
  public function __construct()
  {
    $this->middleware('jefe');
  }
  public function index()
  {
      $empresa=  $idempresa=Auth::user()->RUC_empresa;
      $ordenes=DB::table('orden_pedido')
      ->join('cliente','orden_pedido.codigo_cliente','=','cliente.codigo')
      ->where('orden_pedido.RUC_empresa','=',$empresa)
      ->select('orden_pedido.codigo_pedido','cliente.nombre','orden_pedido.fecha','orden_pedido.deuda','orden_pedido.total_pedido')
      ->get();
      return view('Ventas.pedido.index',['ordenes'=>$ordenes]);
  }
  public function create()
  {
    $empresa=  $idempresa=Auth::user()->RUC_empresa;
    $datosempresa=DB::table('empresa')
    ->where('RUC_empresa','=',$empresa)
    ->get();
    $cliente=DB::table('cliente')
    ->where('RUC_empresa','=',$empresa)
    ->where('estado','=','1')
    ->get();
    $articulos=DB::table('serie_modelo')
    ->join('modelo','serie_modelo.codigo_modelo','=','modelo.cod_modelo')
    ->join('serie','codigo_serie','=','serie.cod_serie')
    ->join('catalogo_precios','serie_modelo.codigo','=','catalogo_precios.cod_articulo')
    ->where('serie_modelo.RUC_empresa','=',Auth::user()->RUC_empresa)
    ->where(function($query){
      $query->orWhere('serie_modelo.estado','=','3');
    })
    ->orderBy('modelo.cod_modelo','asc')
    ->get();
    return view('Ventas.pedido.create',['datosempresa'=>$datosempresa,'articulos'=>$articulos,'cliente'=>$cliente]);
  }
  public function store()
  {
    $total_op=Input::Get('totales');
    echo $total_op;
    if ($total_op!="" && $total_op!="NaN") {
      $empresa=$idempresa=Auth::user()->RUC_empresa;
      $dni_cliente=Input::Get('cliente');
      $fecha_tentativa=Input::get('fecha_tentativa');
      $articulos=Input::get('articulos');
      $costo_par=Input::get('costo_par');
      $adelanto=Input::get('adelanto');
      $urgencia=Input::get('urgencia');
      $deuda=0;
      if($adelanto<$total_op)
      {
        $deuda=$total_op-$adelanto;
      }
      $ultima_op=DB::table('orden_pedido')
      ->where('orden_pedido.RUC_empresa','=',$empresa)
      ->select('orden_pedido.codigo_pedido')
      ->orderBy('orden_pedido.fecha','desc')
      ->get();
      $datosempresa=DB::table('empresa')
      ->where('RUC_empresa','=',$empresa)
      ->select('empresa.siglas')
      ->get();
      $numeracion="";

      if (count($ultima_op)==0) {
        $numeracion="0001";
      }
      else {
        $dat=explode('-',$ultima_op[0]->codigo_pedido);
        $numero=$dat[2];
        $numero++;
        $numero_string=(string)$numero;
        switch (strlen($numero_string)) {
          case 1:
            $numeracion="000".$numero_string;
          break;
          case 2:
            $numeracion="00".$numero_string;
          break;
          case 3:
            $numeracion="0".$numero_string;
          break;
          case 4:
            $numeracion=$numero_string;
          break;
          default:
          break;
        }
      }
      $codigo_op=$datosempresa[0]->siglas."-".date("Y")."-".$numeracion;
      //Guarda la CABECERA DE OP
      $orden_pedido=new OrdenPedidoModel;
      $orden_pedido->codigo_pedido=$codigo_op;
      $orden_pedido->codigo_cliente=$dni_cliente;
      $orden_pedido->fecha_entrega=$fecha_tentativa;
      $orden_pedido->deuda=$deuda;
      $orden_pedido->RUC_empresa=$empresa;
      $orden_pedido->total_pedido=$total_op;
      $orden_pedido->estado_orden_pedido=1;
      $orden_pedido->save();
      //GUARDA EL DETALLE DE LA OP
      for ($i=0; $i < count($articulos); $i++) {
        $tallas=Input::get($articulos[$i]."tallas");
        $tallas_string="";
        for ($j=0; $j < count($tallas); $j++) {
          $tallas_string=$tallas_string.$tallas[$j].",";
        }
        $tallas_string=substr($tallas_string,"0",strlen($tallas_string)-1);
        $detalle_orden_pedido=new DetalleOrdenPedidoModel;
        $detalle_orden_pedido->codigo_orden_pedido=$codigo_op;
        $detalle_orden_pedido->codigo_serie_articulo=$articulos[$i];
        $detalle_orden_pedido->cantidades=$tallas_string;
        $detalle_orden_pedido->costo_total=$costo_par[$i];
        $detalle_orden_pedido->tipo_urgencia=$urgencia[$i];
        $detalle_orden_pedido->save();
      }
      session()->flash('success','Orden de Pedido registrado');
      return Redirect::to('Ventas/pedidos/create');
    }
    else {
      session()->flash('error','No se registro ninguna ORDEN DE PEDIDO');
      return Redirect::to('Ventas/pedidos/create');
    }
  }
  public function obtener_detalle($var)
  {
    $resultado=DB::table('orden_pedido')
    ->join('detalle_orden_pedido','orden_pedido.codigo_pedido','=','detalle_orden_pedido.codigo_orden_pedido')
    ->join('serie_modelo','detalle_orden_pedido.codigo_serie_articulo','=','serie_modelo.codigo')
    ->join('modelo','serie_modelo.codigo_modelo','modelo.cod_modelo')
    ->join('serie','serie_modelo.codigo_serie','=','serie.cod_serie')
    ->where('orden_pedido.codigo_pedido','=',$var)
    ->select('modelo.cod_modelo','detalle_orden_pedido.cantidades','cantidades_ord_prod','serie.tallaInicial','serie.tallaFinal')
    ->get();
    return $resultado;
    
  }
}
