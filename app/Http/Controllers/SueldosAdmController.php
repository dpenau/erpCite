<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use erpCite\Sueldos;
use erpCite\Empresa;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;


class SueldosAdmController extends Controller
{
  public function __construct()
{
  $this->middleware('jefe');
}
public function index(Request $request)
{

if ($request) {
    $sueldos=DB::table('gasto_sueldos')
    ->where('estado','=','1')->where('RUC_empresa',Auth::user()->RUC_empresa)
    ->paginate(10);

    $total=DB::table('gasto_sueldos')
    ->selectRaw('sum(gasto_mensual) as sum')
    ->where('estado','=','1')->where('RUC_empresa',Auth::user()->RUC_empresa)

    ->get();
        return view('costos_indirectos.SueldosAdm.index',["sueldos"=>$sueldos,"total"=>$total]);

  }


}
public function create(Request $request)
{
  if($request)
  {
    $area=DB::table('area')->get();
    $puesto=DB::table('puesto')->get();
    $unidad_medida=DB::table('unidad_medida')->get();
    return view("costos_indirectos.SueldosAdm.create",["area"=>$area,"puesto"=>$puesto,'unidad_medida'=>$unidad_medida]);
  }

}
public function store()
{

  $identificador=rand(100000,999999);
  $idempresa=Auth::user()->RUC_empresa;
  $articulo=new Sueldos;
  $articulo->id_gastos_sueldos=$identificador;
  $articulo->area=Input::get('area');
  $articulo->puesto=Input::get('puesto');
  $articulo->dni_trabajador=Input::get('dni_trabajador');
  $articulo->nombre_trabajador=Input::get('nombre_trabajador');
  $articulo->sueldo_mensual=Input::get('sueldo_mensual');
  $articulo->beneficios=Input::get('beneficios');
  $articulo->otros=Input::get('otros');
  $articulo->gasto_mensual=Input::get('gasto_mensual');

  $articulo->estado=1;
  $articulo->RUC_empresa=$idempresa;
  $articulo->save();
  session()->flash('success','Gasto ingresado satisfactoriamente');
  return Redirect::to('costos_indirectos/SueldosAdm');


}
public function show()
{
 /* return view('logistica.clasificacion.index',["clasificacion"=>$clasi]);*/
}
public function edit($id)
{
 /* return Redirect::to('logistica/clasificacion');*/
}
public function update($data)
  {



  }
public function destroy()
{

 $email=Input::get('email');
  $estado=Input::get('estado');
  if($estado==0){$mensaje="Eliminado";}
  $act=Sueldos::where('id_gastos_sueldos',$email)
  ->update(['estado'=>$estado]);
    session()->flash('success','Gasto'.$mensaje);
  return Redirect::to('costos_indirectos/SueldosAdm');
}
}
