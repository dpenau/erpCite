<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\Http\Requests\ClasificacionFormRequest;
use DB;
use erpCite\ClienteModel;
class ClientesController extends Controller
{
  public function __construct()
  {
    $this->middleware('jefe');
  }
public function index(Request $request)
  {
    if ($request) {
      $cliente=DB::table('cliente')
      ->get();
      return view('Ventas.cliente.index',["cliente"=>$cliente]);
    }
  }
  public function create(Request $request)
  {
    if($request)
    {
      return view("Ventas.cliente.create");
    }

  }
  public function store()
  {
    $empresa=Auth::user()->RUC_empresa;
    $sigla=DB::table('empresa')->where('RUC_empresa',$empresa)->get();
    $identificador=rand(10000,99999);
    $siglax = $sigla[0]->siglas;
    $res=$siglax.'-'.$identificador;

    $cliente=new ClienteModel;
    $cliente->codigo=$res;
    $cliente->documento=Input::get('documento');
    $cliente->nombre=Input::get('nombre');
    $cliente->destino=Input::get('destino');
    $cliente->telefono=Input::get('telefono');
    $cliente->direccion=Input::get('direccion');
    $cliente->RUC_empresa=$empresa;
    $cliente->save();
    session()->flash('success','Cliente Registrado');
    return Redirect::to('Ventas/clientes');
  }
  public function update()
  {
    $cod=Input::get('codigo');
    $documento=Input::get('documento');
    $nombre=Input::get('nombre');
    $destino=Input::get('destino');
    $telefono=Input::get('telefono');
    $direccion=Input::get('direccion');
    $act=ClienteModel::where('codigo',$cod)
    ->update(['documento'=>$documento,
  'nombre'=>$nombre,
'destino'=>$destino,
'telefono'=>$telefono,
'direccion'=>$direccion]);
    session()->flash('success','Datos de Cliente Actualizado');
    return Redirect::to('Ventas/clientes');
  }
  public function destroy()
  {
    $cod=Input::get('codigo');
    $estado=Input::get('estado');
    if($estado==0)
    {
      $mensaje="Desactivado";
    }
    else{$mensaje="Activado";}
    $act=ClienteModel::where('codigo',$cod)
    ->update(['estado'=>$estado]);
    session()->flash('success','Cliente '.$mensaje);
    return Redirect::to('Ventas/clientes');
  }
  public function configuracion(){
    $empresa=Auth::user()->RUC_empresa;
    $cliente=DB::Table('cliente')
    ->where('RUC_empresa','=',$empresa)
    ->select('codigo','nombre')
    ->orderby('nombre','asc')
    ->get();
    return view('Ventas.configuracion.index',["cliente"=>$cliente]);
  }
  public function obt_descuentos($var){
    $empresa=Auth::user()->RUC_empresa;
    $desc_materiales=DB::table('desc_materiales')
    ->join('cliente','desc_materiales.codigo_cliente','=','cliente.codigo')
    ->join('material','desc_materiales.codigo_material','=','material.cod_material')
    ->where('desc_materiales.codigo_cliente','=',$var)
    ->where('desc_materiales.RUC_empresa','=',$empresa)
    ->where('desc_materiales.estado','=',1)
    ->select('material.cod_material','material.descrip_material','desc_materiales.codigo_cliente','cliente.nombre','desc_materiales.porcentaje_desc')
    ->get();
    $desc_modelo=DB::table('desc_modelo')
    ->join('cliente','desc_modelo.codigo_cliente','=','cliente.codigo')
    ->join('serie_modelo','desc_modelo.codigo_articulo','=','serie_modelo.codigo')
    ->join('serie','serie_modelo.codigo_serie','=','serie.cod_serie')
    ->where('desc_modelo.codigo_cliente','=',$var)
    ->where('desc_modelo.RUC_empresa','=',$empresa)
    ->where('desc_modelo.estado','=',1)
    ->select('serie_modelo.codigo','serie_modelo.codigo_modelo','serie.nombre_serie'
    ,'desc_modelo.codigo_cliente','cliente.nombre','desc_modelo.porcentaje_desc')
    ->get();
    $datos=["materiales"=>$desc_materiales,"modelo"=>$desc_modelo];
    return $datos;
  }
  public function nuevo($var)
  {
    $cliente=DB::Table('cliente')
    ->where('codigo','=',$var)
    ->select('codigo','nombre')
    ->orderby('nombre','asc')
    ->get();
    return view("Ventas.configuracion.create",["cliente"=>$cliente]);
  }
  public function todos_descuentos($var){
    $empresa=Auth::user()->RUC_empresa;
    if($var==2)
    {
      $datos=DB::Table('catalogo_precios')
      ->join('serie_modelo','catalogo_precios.cod_articulo','=','serie_modelo.codigo')
      ->join('serie','serie_modelo.codigo_serie','=','serie.cod_serie')
      ->select('cod_articulo','costo_venta','utilidad','precio','serie_modelo.codigo_modelo','serie.nombre_serie')
      ->where('catalogo_precios.RUC_empresa','=',$empresa)
      ->orderBy('cod_articulo','asc')
      ->get();
    }
    else
    {
      $datos=DB::table('material')
      ->join('subcategoria','material.cod_subcategoria','=','subcategoria.cod_subcategoria')
      ->select('material.descrip_material','material.cod_material','material.costo_sin_igv_material','material.cod_subcategoria')
      ->where('material.RUC_empresa','=',$empresa)
      ->where('material.estado_material','=',1)
      ->orderBy('descrip_material','asc')
      ->get();
    }
    return $datos;
  }
}
