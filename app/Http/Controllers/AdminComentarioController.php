<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;

use erpCite\comentario;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\Http\Requests\ClasificacionFormRequest;
use DB;
class AdminComentarioController extends Controller
{
    public function __construct()
  {
    $this->middleware('admin');
  }
  public function index()
  {
    $comentario=DB::table('comentarios')
    ->join('empresa','comentarios.RUC_empresa','=','empresa.RUC_empresa')
    ->where('estado_comentario','=','1')
    ->get();
    return view('Mantenimiento.comentarios.index',["comentario"=>$comentario]);
  }
  public function create()
  {
    
  }
}
