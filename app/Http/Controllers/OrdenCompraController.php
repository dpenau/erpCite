<?php

namespace erpCite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use erpCite\MaterialSuministroModel;
use erpCite\OrdenCompra;
use erpCite\OrdenCompras;
use erpCite\DetalleOrdenCompra;
use erpCite\DetalleCompraTemp;
use erpCite\DetalleCompraTalla;
use erpCite\DetalleCompraTallaMaterial;
use erpCite\DetalleCompraMaterialTalla;
use erpCite\HistorialDetalleCompra;
use erpCite\Sueldos;
use erpCite\kardex_cab;
use erpCite\kardex_cab_talla;
use erpCite\kardex_cab_talla_tallas;
use erpCite\kardex;
use erpCite\kardex_talla;
use erpCite\kardex_talla_tallas;
use Carbon\Carbon;
use erpCite\KardexDetalle;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use erpCite\Http\Requests\MaterialRequest;
use erpCite\CategoriaModel;
use erpCite\Material;
use erpCite\Proveedor;
use Exception;
use Illuminate\Support\Facades\DB;

class OrdenCompraController extends Controller
{
    var $list;

    public function __construct()
    {
        $this->middleware('logistica');
    }

    public function index(Request $request)
    {
        if ($request) {
            $empresa = Auth::user()->RUC_empresa;
            $orden_compra = OrdenCompras::where('orden_compras.RUC_empresa', '=', $empresa)
                ->where('orden_compras.estado_ordcomp', '=', 1)
                ->orderBy('fecha', 'asc')
                ->get();

            $detalle_temp = DetalleCompraTemp::join('material', 'detalle_orden_compra_temp.cod_material', '=', 'material.cod_material')
                ->join('unidad_compra', 'material.unidad_compra', '=', 'unidad_compra.cod_unidad_medida')
                ->get();

            $detalle_talla = DetalleCompraTalla::join('material', 'detalle_orden_compra_talla.cod_material', '=', 'material.cod_material')
                ->join('serie', 'material.cod_serie', '=', 'serie.cod_serie')
                ->join('unidad_compra', 'material.unidad_compra', '=', 'unidad_compra.cod_unidad_medida')
                ->get();

            $detalle_talla_material = DetalleCompraMaterialTalla::join('material', 'detalle_material_talla.cod_material', '=', 'material.cod_material')
                ->join('serie', 'material.cod_serie', '=', 'serie.cod_serie')
                ->get();
            return view('logistica.orden_compras.index', ["orden_compra" => $orden_compra, "detalle_temp" => $detalle_temp, "detalle_talla" => $detalle_talla, "detalle_talla_material" => $detalle_talla_material]);
        }
    }

    public function create(Request $request)
    {
        $empresa = Auth::user()->RUC_empresa;
        $proveedores = DB::table('proveedor')->where('proveedor.RUC_empresa', '=', $empresa)->orderBy('nom_proveedor', 'asc')->get();

        $materiales = Material::where('material.RUC_empresa', '=', $empresa)
            ->where('material.estado_material', '=', '1')
            //En base de datos ya se guardaba el codigo unidad de compra como "cod_unidad_medida"
            ->join('unidad_compra', 'material.unidad_compra', '=', 'unidad_compra.cod_unidad_medida')
            ->get();

        $categorias = CategoriaModel::get();
        $categoria = 1;
        $index = 1;


        $now = Carbon::now();
        $date = $now->format('Y-m-d');

        $ordenCompraN = "OCN" . "-" . $date . "-" . $index;
        $ordencompras = DB::table('orden_compras')
            ->where('orden_compras.RUC_empresa', '=', $empresa)
            //->where('orden_compras.fecha',"=", $date)
            ->latest()
            ->get();


        if (sizeof($ordencompras) != 0) {

            $fecha = explode(" ", $ordencompras[0]->fecha);
            //dd($fecha[0]);
            if ($fecha[0] != $date) {
                return view('logistica.orden_compras.create', ["proveedores" => $proveedores, "materiales" => $materiales, "categorias" => $categorias, "categoria" => $categoria, "fechaActual" => $now, "ordenCompraN" => $ordenCompraN]);
            } else {
                $linea = $ordencompras[0]->codigo_orden;
                $datos = explode("-", $linea);
                $ordenCompraN = "OCN" . "-" . $date . "-" . ($datos[4] + 1);
                return view('logistica.orden_compras.create', ["proveedores" => $proveedores, "materiales" => $materiales, "categorias" => $categorias, "categoria" => $categoria, "fechaActual" => $now, "ordenCompraN" => $ordenCompraN]);

                /*$linea = $ordencompras[0]->codigo_orden;
                $datos=explode("-",$linea);

                if($datos[4]>=1 && $fecha[0] == $date){
                  $index = $datos[4]+1;
                  $ordenCompraN = "OCN" ."-". $date ."-". $index;
                  return view('logistica.orden_compras.create',["proveedores"=>$proveedores,"materiales"=>$materiales,"categorias"=>$categorias,"categoria"=>$categoria,"fechaActual"=>$now,"ordenCompraN"=>$ordenCompraN]);
                } else {
                  return view('logistica.orden_compras.create',["proveedores"=>$proveedores,"materiales"=>$materiales,"categorias"=>$categorias,"categoria"=>$categoria,"fechaActual"=>$now,"ordenCompraN"=>$ordenCompraN]);

                }*/
            }
        }
        return view('logistica.orden_compras.create', ["proveedores" => $proveedores, "materiales" => $materiales, "categorias" => $categorias, "categoria" => $categoria, "fechaActual" => $now, "ordenCompraN" => $ordenCompraN]);
    }

    public function show(Request $request)
    {

        $empresa = Auth::user()->RUC_empresa;
        $proveedores = DB::table('proveedor')
            ->where('proveedor.RUC_empresa', '=', $empresa)
            ->orderBy('nom_proveedor','asc')
            ->get();

        $materiales = DB::table('material')
            ->where('material.RUC_empresa', '=', $empresa)
            ->where('material.estado_material', '=', '1')
            ->join('serie', 'material.cod_serie', '=', 'serie.cod_serie')
            //En base de datos ya se guardaba el codigo unidad de compra como "cod_unidad_medida"
            ->join('unidad_compra', 'material.unidad_compra', '=', 'unidad_compra.cod_unidad_medida')
            ->get();

        $categorias = DB::table('categoria')
            ->get();
        $categoria = 1;
        $index = 1;

        $now = Carbon::now();
        $date = $now->format('Y-m-d');
        $ordenCompraT = "OCT" . "-" . $date . "-" . $index;
        $ordenCompraN = "OCT" . "-" . $date . "-" . $index;
        $ordencompras = DB::table('orden_compras')
            ->where('orden_compras.RUC_empresa', '=', $empresa)
            //->where('orden_compras.fecha',"=", $date)
            ->latest()
            ->get();

        if (sizeof($ordencompras) != 0) {

            $fecha = explode(" ", $ordencompras[0]->fecha);
            $linea = $ordencompras[0]->codigo_orden;
            $datos = explode("-", $linea);

            if ($datos[4] >= 1 && $fecha[0] == $date) {
                $index = $datos[4] + 1;
                $ordenCompraN = "OCT" . "-" . $date . "-" . $index;
                return view('logistica.orden_compras.create_talla', ["proveedores" => $proveedores, "materiales" => $materiales, "categorias" => $categorias, "categoria" => $categoria, "fechaActual" => $now, "ordenCompraN" => $ordenCompraN]);
            } else {
                return view('logistica.orden_compras.create_talla', ["proveedores" => $proveedores, "materiales" => $materiales, "categorias" => $categorias, "categoria" => $categoria, "fechaActual" => $now, "ordenCompraN" => $ordenCompraN]);

            }
        }
        return view('logistica.orden_compras.create_talla', ["proveedores" => $proveedores, "materiales" => $materiales, "categorias" => $categorias, "categoria" => $categoria, "fechaActual" => $now, "ordenCompraN" => $ordenCompraN]);
    }

    public function store(Request $request)
    {
        // 0 --> talla
        // 1 --> normal
        // dd($request);


        $valorTalla = Input::get('tipoDetalle');
        $now = Carbon::now();
        $date = $now->format('Y-m-d h-m-s');
        //dd($date);
        $codigoordenfinal = Input::get('codigo_orden');
        $orden_compra = OrdenCompras::where('codigo_orden', $codigoordenfinal)->first();

        //dd(Input::get('fecha2'));

        if ($orden_compra) {

        }

        if ($valorTalla == 0) {
            $idempresa = Auth::user()->RUC_empresa;
            $ordencompra = new OrdenCompras;
            $ordencompra->codigo_orden = Input::get('codigo_orden');
            $ordencompra->fecha = $date;
            $ordencompra->RUC_proveedor = Input::get('proveedor');
            $ordencompra->RUC_empresa = $idempresa;
            $ordencompra->tipo_compra = Input::get('tipo_compra');
            $ordencompra->dias_credito = Input::get('dias_credito');
            $ordencompra->observaciones = Input::get('observaciones');
            $ordencompra->subtotal = Input::get('subTotalval');
            $ordencompra->total = Input::get('idTotal');
            $ordencompra->IGV = Input::get('igvSubtotal');
            $ordencompra->cod_material = Input::get('rep_cod_material');
            $ordencompra->estado_ordcomp = 1;
            $ordencompra->tipo_orden = 0;
            $ordencompra->cantidad_total = Input::get('cantidad_total_controller');
            $ordencompra->cantidad_faltante_total = Input::get('cantidad_total_controller');
            $ordencompra->save();
            $cod_material = $request->input('nombre');
            $cantidad = $request->input('cantidad_talla');
            $costo_total = $request->input('costo_total');
            $costo_sin_igv = $request->input('costo_sin_igv');
            $cantidad_total_cab = 0;
            $contador = 0;

            foreach ($cod_material as $item) {
                $materialdetalle = new DetalleCompraTalla;
                $materialdetalle->subvalor_total = $costo_sin_igv[$contador];
                $materialdetalle->cantidad = $cantidad[$contador];
                $materialdetalle->cod_material = $item;
                $materialdetalle->costo_total = $costo_total[$contador];
                $materialdetalle->cantidad_faltante = $cantidad[$contador];
                $materialdetalle->cod_cab_ordencompra = $ordencompra->id;
                $materialdetalle->save();
                $tallasfinal = 'tallasFinal' . $item;
                $tallasfinales = Input::get($tallasfinal);

                //KARDEX CAB_TALLA
                $karde_cab = new kardex_cab_talla;
                $karde_cab->cod_kardex = $item . "-" . $now;
                $karde_cab->movimiento = 1;
                $karde_cab->cod_material = $item;
                $karde_cab->cantidad = 0;
                $karde_cab->costo_total_sin_igv = 0;
                $karde_cab->costo_total_con_igv = 0;
                $karde_cab->igv = 0;
                $karde_cab->fecha = $date;
                $karde_cab->RUC_empresa = $idempresa;
                $karde_cab->save();

                //KARDEX_TALLA
                $kardexs = DB::table('kardex_talla')
                    ->where('kardex_talla.cod_material', '=', $item)
                    ->get();
                if (!(sizeof($kardexs) != 0)) {
                    $kardex = new kardex_talla;
                    $kardex->id_kardex = $item . "-" . $now;
                    $kardex->cod_material = $item;
                    $kardex->stock_actual = 0;
                    $kardex->costo_total_sin_igv = 0;
                    $kardex->costo_total_con_igv = 0;
                    $kardex->igv = 0;
                    $kardex->RUC_empresa = $idempresa;
                    $kardex->save();
                }

                foreach ($tallasfinales as $tfinal) {
                    $aux = explode("-", $tfinal);
                    $material = new DetalleCompraMaterialTalla;
                    $material->cantidad = $aux[0];
                    $material->cod_material = $item;
                    $material->cantidad_faltante = $aux[0];
                    $material->talla = $aux[1];
                    $material->cod_cab_detalle_talla = $materialdetalle->cod_temp_mat;
                    $material->save();
                    //KARDEX_CAB_TALLA_TALLAS
                    $talla = new kardex_cab_talla_tallas;
                    $talla->talla = $aux[1];
                    $talla->cantidad = 0;
                    $talla->cod_kardex = $item . "-" . $now;
                    $talla->save();
                    //KARDEX_TALLA_TALLAS
                    $tallak = new kardex_talla_tallas;
                    $tallak->talla = $aux[1];
                    $tallak->cantidad = 0;
                    $tallak->cod_kardex = $item . "-" . $now;
                    $tallak->save();
                }


                $cantidad_total_cab = $cantidad_total_cab + $cantidad[$contador];
                $contador = $contador + 1;
            }
            session()->flash('success', 'Orden de compra registrada satisfactoriamente');
            return Redirect::to('logistica/orden_compra');
        } //creacion normal
        else {
            //  dd($request->fecha2);
            $idempresa = Auth::user()->RUC_empresa;
            $ordencompra = new OrdenCompras;
            $codigoordenfinal = Input::get('codigo_orden');
            $ordencompra->codigo_orden = Input::get('codigo_orden');
            $ordencompra->fecha = $date;
            $ordencompra->RUC_proveedor = Input::get('proveedor');
            $ordencompra->RUC_empresa = $idempresa;
            $ordencompra->tipo_compra = Input::get('tipo_compra');
            $ordencompra->dias_credito = Input::get('dias_credito');
            $ordencompra->observaciones = Input::get('observaciones');
            $ordencompra->subtotal = Input::get('subTotalval');
            $ordencompra->total = Input::get('idTotal');
            $ordencompra->IGV = Input::get('igvSubtotal');
            $ordencompra->cod_material = Input::get('cod_material');
            $ordencompra->estado_ordcomp = 1;
            $ordencompra->tipo_orden = 1;
            $ordencompra->cantidad_total = Input::get('cantidad_total_controller');
            $ordencompra->cantidad_faltante_total = Input::get('cantidad_total_controller');
            $ordencompra->save();
            //$ordencompra->subtotal = Input::get('subtotal');
            //$ordencompra->total = Input::get('total');
            //$ordencompra->IGV = Input::get('IGV');
            //$descripcion = $request->get('descripcion');
            //namesas =  Input::get('nombre');
            //$descripcion = $_POST['descripcion'];
            //$namesas = $request->input('nombre');
            $cod_material = $request->input('nombre');
            $cantidad = $request->input('cantidad_algo');
            $costo_total = $request->input('costo_total');
            $costo_sin_igv = $request->input('costo_sin_igv');
            $cantidad_total_cab = 0;
            $contador = 0;
            foreach ($cod_material as $item) {
                $material = new DetalleCompraTemp;
                $material->subvalor_total = $costo_sin_igv[$contador];
                $material->cantidad = $cantidad[$contador];
                $material->cod_material = $item;
                $material->costo_total = $costo_total[$contador];
                $material->cantidad_faltante = $cantidad[$contador];
                $material->cod_cab_ordencompra = $ordencompra->id;
                $material->save();
                $cantidad_total_cab = $cantidad_total_cab + $cantidad[$contador];
                //KARDEX CAB
                $karde_cab = new kardex_cab;
                $karde_cab->movimiento = 1;
                $karde_cab->cod_material = $item;
                $karde_cab->cantidad = 0;
                $karde_cab->costo_total_sin_igv = 0;
                $karde_cab->costo_total_con_igv = 0;
                $karde_cab->fecha = $date;
                $karde_cab->igv = 0;
                $karde_cab->RUC_empresa = $idempresa;
                $karde_cab->save();
                //KARDEX
                $kardexs = DB::table('kardex')
                    ->where('kardex.cod_material', '=', $item)
                    ->get();
                if (!(sizeof($kardexs) != 0)) {
                    $kardex = new kardex;
                    $kardex->cod_material = $item;


                    $kardex->stock_actual = 0;
                    $kardex->costo_total_sin_igv = 0;
                    $kardex->costo_total_con_igv = 0;
                    $kardex->igv = 0;
                    $kardex->RUC_empresa = $idempresa;
                    $kardex->save();


                }
                $contador = $contador + 1;
            }
            //movimiento 0=devoluciones 1=ingreso 2=salidas
            session()->flash('success', 'Orden de compra registrada satisfactoriamente');
            return Redirect::to('logistica/orden_compra');
        }
    }


    public function eliminar($var)
    {
        $mensaje = "Eliminación exitosa";
        $estado = 0;
        $act = OrdenCompras::where('id', $var)
            ->update(['estado_ordcomp' => $estado]);
        return $act;
    }

    public function destroy(Request $request)
    {
        $mensaje = "Eliminación exitosa";
        $estado = 0;
        $var = Input::get('idOrdenCompra');
        $act = OrdenCompras::where('id', $var)
            ->update(['estado_ordcomp' => $estado]);
        return Redirect::to('logistica/orden_compra');
    }

    public function obtdatosSubCat($var)
    {
        $dato = $var;
        $empresa = Auth::user()->RUC_empresa;
        $subcategoria = DB::table('subcategoria')
            ->join('proveedor_subcategoria', 'subcategoria.cod_subcategoria', '=', 'proveedor_subcategoria.cod_subcategoria')
            ->where('proveedor_subcategoria.RUC_proveedor', '=', $dato)
            ->where('proveedor_subcategoria.estado', '=', '1')
            ->orderBy('nom_subcategoria','asc')
            ->get();

        return $subcategoria;

    }

    //OBTENER MATERIALES NORMAL
    public function obtdatosMateriales($var)
    {
        $dato = $var;
        $empresa = Auth::user()->RUC_empresa;
        $materiales = DB::table('material')
            ->where('material.RUC_empresa', '=', $empresa)
            ->where('material.cod_subcategoria', '=', $dato)
            ->where('material.estado_material', '=', 1)
            //SERIE TALLA
            ->where('material.cod_serie', '=', null)
            ->orderBy('descrip_material','asc')
            ->get();
        return $materiales;
    }

    //OBTENER MATERIALES TALLA
    public function obtdatosMaterialesTalla($var)
    {
        $dato = $var;
        $empresa = Auth::user()->RUC_empresa;
        $materiales = DB::table('material')
            ->where('material.RUC_empresa', '=', $empresa)
            ->where('material.cod_subcategoria', '=', $dato)
            ->where('material.cod_serie', '!=', null)
            ->where('material.estado_material', '=', 1)
            //SERIE TALLA
            // ->where('material.cod_serie','=',null)
            ->orderBy('descrip_material','asc')
            ->get();
        return $materiales;
    }

    public function obtdatosCat($var)
    {
        //CODIGO DE CATEGORIA DEL PROVEEDOR
        $dato = $var;
        $empresa = Auth::user()->RUC_empresa;
        $categoriaP = DB::table('categoria')
            ->where('categoria.cod_categoria', '=', $dato)
            ->where('categoria.estado_categoria', '=', '1')
            ->get();
        return $categoriaP;
    }

    public function dateActual()
    {
        $now = Carbon::now();
        $date = $now->format('Y-m-d');
        $ordenCompraN = "OCN" . $date;
        $ordenCompraT = "OCT" . $date;
        return $ordenCompraN;
    }

    public function getRecepcion($var)
    {
        $dato = $var;
        $detalle_temp = DB::table('detalle_orden_compra_temp')
            //->where('detalle_orden_compra_temp.cod_cab_ordencompra','=',$dato)
            ->get();
        return $detalle_temp;
    }

    public function recepcionarTodo($var)
    {
        $datos = explode("-", $var);

        foreach ($datos as $item) {

            if ($item != null && $item != "") {
                $empresa = Auth::user()->RUC_empresa;
                $detalle = DetalleCompraTemp::where('cod_temp_mat', $item)->get();

                $materialAux = DB::table('material')
                    ->where('cod_material', '=', $detalle[0]->cod_material)
                    ->get();
                $now = Carbon::now();
                $date = $now->format('Y-m-d');
                //KARDEX CAB
                //0-DEVOLUCION   1-INGRESO   2-SALIDA
                $karde_cab = new kardex_cab;
                $karde_cab->movimiento = 1;
                $karde_cab->cod_material = $detalle[0]->cod_material;
                $karde_cab->cantidad = $detalle[0]->cantidad_faltante;
                $karde_cab->costo_total_sin_igv = $detalle[0]->cantidad_faltante * $materialAux[0]->costo_sin_igv_material;
                $karde_cab->costo_total_con_igv = $detalle[0]->cantidad_faltante * $materialAux[0]->costo_con_igv_material;
                $karde_cab->fecha = $date;
                $karde_cab->igv = ($detalle[0]->cantidad_faltante * $materialAux[0]->costo_con_igv_material) - ($detalle[0]->cantidad_faltante * $materialAux[0]->costo_sin_igv_material);
                $karde_cab->RUC_empresa = $empresa;
                $karde_cab->save();

                $kardexs = DB::table('kardex')
                    ->where('cod_material', '=', $detalle[0]->cod_material)
                    ->get();
                //KARDEX

                $kardex = kardex::where('cod_material', $detalle[0]->cod_material)
                    ->update([
                        'stock_actual' => $kardexs[0]->stock_actual + $detalle[0]->cantidad_faltante,
                        'costo_total_sin_igv' => $kardexs[0]->costo_total_sin_igv + ($detalle[0]->cantidad_faltante * $materialAux[0]->costo_sin_igv_material),
                        'costo_total_con_igv' => $kardexs[0]->costo_total_con_igv + ($detalle[0]->cantidad_faltante * $materialAux[0]->costo_con_igv_material),
                        'igv' => $kardexs[0]->igv + ($detalle[0]->cantidad_faltante * $materialAux[0]->costo_con_igv_material) - ($detalle[0]->cantidad_faltante * $materialAux[0]->costo_sin_igv_material)
                    ]);

                $detalle = DetalleCompraTemp::where('cod_temp_mat', $item)
                    ->update([
                        'cantidad_faltante' => 0
                    ]);
                $detallechange = DB::table('detalle_orden_compra_temp')
                    ->where('cod_temp_mat', $item)
                    ->get();
                $faltante_total = OrdenCompras::where('id', $detallechange[0]->cod_cab_ordencompra)
                    ->update([
                        'cantidad_faltante_total' => 0
                    ]);
            }
        }
        return route('logistica/orden_compra');
    }

    public function recepcionarTodoTalla($var)
    {
        $datos = explode("_", $var);
        foreach ($datos as $item) {
            $datos2 = explode("-", $item);
            if ($datos2[0] != null && $datos2[0] != "") {
                $empresa = Auth::user()->RUC_empresa;
                $detalle = DetalleCompraMaterialTalla::where('cod_cab_detalle_talla', $datos2[1])->get();
                $detalleTalla = DetalleCompraTalla::where('cod_temp_mat', $datos2[1])->get();

                $now = Carbon::now();
                $date = $now->format('Y-m-d');
                $materialAux = DB::table('material')
                    ->where('cod_material', '=', $detalle[0]->cod_material)
                    ->get();
                $cod_kardex_now = $item . "-" . $now;

                //KARDEX_CAB_TALLA
                $karde_cab = new kardex_cab_talla;
                $karde_cab->cod_kardex = $cod_kardex_now;
                $karde_cab->movimiento = 1;
                $karde_cab->cod_material = $detalleTalla[0]->cod_material;
                $karde_cab->cantidad = $detalleTalla[0]->cantidad_faltante;
                $karde_cab->costo_total_sin_igv = $detalleTalla[0]->cantidad_faltante * $materialAux[0]->costo_sin_igv_material;
                $karde_cab->costo_total_con_igv = $detalleTalla[0]->cantidad_faltante * $materialAux[0]->costo_con_igv_material;
                $karde_cab->fecha = $date;
                $karde_cab->igv = ($detalleTalla[0]->cantidad_faltante * $materialAux[0]->costo_con_igv_material) - ($detalleTalla[0]->cantidad_faltante * $materialAux[0]->costo_sin_igv_material);
                $karde_cab->RUC_empresa = $empresa;
                $karde_cab->save();


                $kardexs = DB::table('kardex_talla')
                    ->where('kardex_talla.cod_material', '=', $detalleTalla[0]->cod_material)
                    ->get();
                //KARDEX_TALLA
                $kardex = kardex_talla::where('cod_material', $detalleTalla[0]->cod_material)
                    ->update([
                        'stock_actual' => $kardexs[0]->stock_actual + $detalleTalla[0]->cantidad_faltante,
                        'costo_total_sin_igv' => $kardexs[0]->costo_total_sin_igv + ($detalleTalla[0]->cantidad_faltante * $materialAux[0]->costo_sin_igv_material),
                        'costo_total_con_igv' => $kardexs[0]->costo_total_con_igv + ($detalleTalla[0]->cantidad_faltante * $materialAux[0]->costo_con_igv_material),
                        'igv' => $kardexs[0]->igv + ($detalleTalla[0]->cantidad_faltante * $materialAux[0]->costo_con_igv_material) - ($detalleTalla[0]->cantidad_faltante * $materialAux[0]->costo_sin_igv_material)
                    ]);

                $kardexAux = kardex_talla::where('cod_material', $datos2[0])->get();
                $kardexTallas = DB::table('kardex_talla_tallas')
                    ->where('cod_kardex', $kardexAux[0]->id_kardex)
                    ->get();

                $contadorStock = 0;

                foreach ($detalle as $kardexTalla) {
                    $tfinal = $kardexTalla->talla;

                    //KARDEX_CAB_TALLA_TALLAS
                    $talla = new kardex_cab_talla_tallas;
                    $talla->talla = $tfinal;
                    $talla->cantidad = $kardexTalla->cantidad_faltante;
                    $talla->cod_kardex = $cod_kardex_now;
                    $talla->save();




                    //KARDEX_TALLA_TALLAS

                    $kardexs_tallas = DB::table('kardex_talla_tallas')
                        ->join('kardex_talla', 'kardex_talla_tallas.cod_kardex', '=', 'kardex_talla.id_kardex')
                        ->where('cod_material', '=', $datos2[0])
                        ->where('talla', '=', $tfinal)
                        ->get();
                    //KARDEX_TALLA

                    $kardexT1 = DB::table('kardex_talla_tallas')
                        ->where('cod_kardex', '=', $kardexs_tallas[0]->cod_kardex)
                        ->where('talla', '=', $tfinal)
                        ->update([
                            'cantidad' => $kardexs_tallas[0]->cantidad + $kardexTalla->cantidad_faltante
                        ]);


                    $contadorStock = $contadorStock + 1;

                }

                $detalle = DetalleCompraMaterialTalla::where('cod_cab_detalle_talla', $datos2[1])
                    ->update([
                        'cantidad_faltante' => 0
                    ]);
                $detalleTalla = DetalleCompraTalla::where('cod_temp_mat', $datos2[1])
                    ->update([
                        'cantidad_faltante' => 0
                    ]);
                $detallechange = DB::table('detalle_orden_compra_talla')
                    ->where('cod_temp_mat', $datos2[1])
                    ->get();
                $faltante_total = OrdenCompras::where('id', $detallechange[0]->cod_cab_ordencompra)
                    ->update([
                        'cantidad_faltante_total' => 0
                    ]);
            }
        }
        return route('logistica/orden_compra');
    }

    public function update(Request $request)
    {
        $valorTalla = Input::get('tipoDetalle');
        $empresa = Auth::user()->RUC_empresa;
        //1:NORMAL   0:TALLA
        if ($valorTalla == 1) {
            $cantidad_faltante_total_val = 0;
            $cantidad_faltante = Input::get('cantidad_faltante');
            $costo_unitario = Input::get('costo_unitario');
            $cod_material = Input::get('rep_cod_material');
            $orden_compra = Input::get('orden_compra');
            $fecha = Input::get('fecha_emision');
            $contador = 0;
            foreach ($cantidad_faltante as $item) {
                $datos = explode("-", $item);
                $costos = explode("-", $costo_unitario[$contador]);
                $cod_mat_temp = $datos[1];
                $cantidadf = $datos[0];
                $cantidadt = $datos[2];
                $cantidad_faltante_total_val = $cantidad_faltante_total_val + $cantidadf;
                $detalle_temp = DetalleCompraTemp::where('cod_temp_mat', $cod_mat_temp)
                    ->update([
                        'cantidad_faltante' => $cantidadf
                    ]);
                $historial = new HistorialDetalleCompra;
                $historial->cod_temp_mat = $cod_mat_temp;
                $historial->cantidad_faltante = $cantidadf;
                $historial->fecha = $fecha;
                $historial->save();
                //KARDEX CAB
                //0-DEVOLUCION   1-INGRESO   2-SALIDA
                $karde_cab = new kardex_cab;
                $karde_cab->movimiento = 1;
                $karde_cab->cod_material = $cod_material[$contador];
                $karde_cab->cantidad = $cantidadt;
                $karde_cab->costo_total_sin_igv = $cantidadt * $costos[0];
                $karde_cab->costo_total_con_igv = $cantidadt * $costos[1];
                $karde_cab->fecha = $fecha;
                $karde_cab->igv = ($cantidadt * $costos[1]) - ($cantidadt * $costos[0]);
                $karde_cab->RUC_empresa = $empresa;
                $karde_cab->save();
                $kardexs = DB::table('kardex')
                    ->where('kardex.cod_material', '=', $cod_material[$contador])
                    ->get();
                //KARDEX
                $kardex = kardex::where('cod_material', $cod_material[$contador])
                    ->update([
                        'stock_actual' => $kardexs[0]->stock_actual + $cantidadt,
                        'costo_total_sin_igv' => $kardexs[0]->costo_total_sin_igv + ($cantidadt * $costos[0]),
                        'costo_total_con_igv' => $kardexs[0]->costo_total_con_igv + ($cantidadt * $costos[1]),
                        'igv' => $kardexs[0]->igv + ($cantidadt * $costos[1]) - ($cantidadt * $costos[0])
                    ]);
                $contador++;
            }
            $faltante_total = OrdenCompras::where('codigo_orden', $orden_compra)
                ->update([
                    'cantidad_faltante_total' => $cantidad_faltante_total_val
                ]);
            session()->flash('success', 'Recepción realizada satisfactoriamente');
            return Redirect::to('logistica/orden_compra');
        } else {


            $now = Carbon::now();
            $cod_material = $request->input('nombre');
            $orden_compra = Input::get('orden_compra_talla');
            $costo_unitario = Input::get('costo_unitario');
            //valeria
            $cantidad = Input::get('cantidad_recepcion');

            $fecha = Input::get('fecha_emision');
            $cantidad_faltante_total_val = 0;
            $cantidad_total_cab = 0;
            $contador = 0;
            foreach ($cod_material as $item) {
                $datos = explode("-", $item);
                $costos = explode("-", $costo_unitario[$contador]);
                $cod_mat_temp = $datos[1];
                $materialCode = $datos[0];
                $tallasfinal = 'tallasFinal' . $materialCode;
                $tallasfinal2 = 'tallasFinal2' . $materialCode;

                $tallasfinales = Input::get($tallasfinal);
                $tallasfinales2 = Input::get($tallasfinal2);
                $cantTotalTemp = 0;
                $cod_kardex_now = $item . "-" . $now;

                //KARDEX_CAB_TALLA
                $karde_cab = new kardex_cab_talla;
                $karde_cab->cod_kardex = $cod_kardex_now;
                $karde_cab->movimiento = 1;
                $karde_cab->cod_material = $materialCode;
                $karde_cab->cantidad = $cantidad[$contador];
                $karde_cab->costo_total_sin_igv = $cantidad[$contador] * $costos[0];
                $karde_cab->costo_total_con_igv = $cantidad[$contador] * $costos[1];
                $karde_cab->fecha = $fecha;
                $karde_cab->igv = ($cantidad[$contador] * $costos[1]) - ($cantidad[$contador] * $costos[0]);
                $karde_cab->RUC_empresa = $empresa;
                $karde_cab->save();
                $kardexs = DB::table('kardex_talla')
                    ->where('kardex_talla.cod_material', '=', $materialCode)
                    ->get();
                //KARDEX_TALLA
                $kardex = kardex_talla::where('cod_material', $materialCode)
                    ->update([
                        'stock_actual' => $kardexs[0]->stock_actual + $cantidad[$contador],
                        'costo_total_sin_igv' => $kardexs[0]->costo_total_sin_igv + ($cantidad[$contador] * $costos[0]),
                        'costo_total_con_igv' => $kardexs[0]->costo_total_con_igv + ($cantidad[$contador] * $costos[1]),
                        'igv' => $kardexs[0]->igv + ($cantidad[$contador] * $costos[1]) - ($cantidad[$contador] * $costos[0])
                    ]);

                $contadorStock = 0;

                foreach ($tallasfinales as $tfinal) {

                    $material2 = DB::table('detalle_material_talla')
                        ->where('detalle_material_talla.cod_cab_detalle_talla', '=', $cod_mat_temp)
                        ->where('cod_material', $materialCode)
                        ->where('talla', $tfinal)
                        ->get();
                    $material = DetalleCompraMaterialTalla::where('cod_cab_detalle_talla', $cod_mat_temp)
                        ->where('cod_material', $materialCode)
                        ->where('talla', $tfinal)
                        ->update([
                            'cantidad_faltante' => (($material2[0]->cantidad_faltante) - $tallasfinales2[$contadorStock]),
                        ]);
                    $detalle_temp2 = DB::table('detalle_orden_compra_talla')
                        ->where('detalle_orden_compra_talla.cod_temp_mat', '=', $cod_mat_temp)
                        ->where('cod_material', $materialCode)
                        ->get();
                    $detalle_temp = DetalleCompraTalla::where('cod_temp_mat', $cod_mat_temp)
                        ->update([
                            'cantidad_faltante' => ($detalle_temp2[0]->cantidad_faltante) - $tallasfinales2[$contadorStock],
                        ]);
                    $faltante_total2 = DB::table('orden_compras')
                        ->where('orden_compras.codigo_orden', '=', $orden_compra)
                        ->get();
                    $faltante_total = OrdenCompras::where('codigo_orden', $orden_compra)
                        ->update([
                            'cantidad_faltante_total' => ($faltante_total2[0]->cantidad_faltante_total) - $tallasfinales2[$contadorStock],
                        ]);
                    if ($tallasfinales2[$contadorStock] == null) {
                        //KARDEX_CAB_TALLA_TALLAS
                        $talla = new kardex_cab_talla_tallas;
                        $talla->talla = $tfinal;
                        $talla->cantidad = 0;
                        $talla->cod_kardex = $cod_kardex_now;
                        $talla->save();
                    } else {
                        //KARDEX_CAB_TALLA_TALLAS
                        $talla = new kardex_cab_talla_tallas;
                        $talla->talla = $tfinal;
                        $talla->cantidad = $tallasfinales2[$contadorStock];
                        $talla->cod_kardex = $cod_kardex_now;
                        $talla->save();
                    }

                    //KARDEX_TALLA_TALLAS

                    $kardexs_tallas = DB::table('kardex_talla_tallas')
                        ->join('kardex_talla', 'kardex_talla_tallas.cod_kardex', '=', 'kardex_talla.id_kardex')
                        ->where('cod_material', '=', $materialCode)
                        ->where('talla', '=', $tfinal)
                        ->get();
                    //KARDEX_TALLA

                    $kardex = kardex_talla_tallas::where('cod_kardex', $kardexs_tallas[0]->cod_kardex)->where('talla', '=', $tfinal)
                        ->update([
                            'cantidad' => $kardexs_tallas[0]->cantidad + $tallasfinales2[$contadorStock]
                        ]);
                    $contadorStock = $contadorStock + 1;
                }
                $contador = $contador + 1;
            }
            session()->flash('success', 'Recepción realizada satisfactoriamente');
            return Redirect::to('logistica/orden_compra');
        }
    }


    public function obt_subcategoria($var)
    {
        $RUC_proveedor = $var;
        $proveedores_subcategoria = DB::table('proveedor_subcategoria')
            ->where('proveedor_subcategoria.RUC_proveedor', '=', $RUC_proveedor)
            ->where('proveedor_subcategoria.estado', '=', 1)
            ->get();
        return $proveedores_subcategoria;
    }

    public function cancelar_orden(Request $request)
    {
        $codigo = $request["orden"];
        echo $codigo;
        $actualizar = OrdenCompra::where('cod_orden_compra', '=', $codigo)
            ->update(['estado_orden_compra' => 0]);
        session()->flash('success', 'Se cerro la orden de compra ' . $codigo);
    }

    public function obtlistMat($var)
    {
        $list = $var;
        return $list;
    }

    public function obtdatos($var)
    {
        $datos = explode("+", $var);
        $empresa = Auth::user()->RUC_empresa;
        $materiales = DB::table('material')
            ->join('unidad_medida', 'material.unidad_compra', '=', 'unidad_medida.cod_unidad_medida')
            ->join('subcategoria', 'material.cod_subcategoria', '=', 'subcategoria.cod_subcategoria')
            ->where('material.RUC_empresa', '=', $empresa)
            ->where('material.estado_material', '=', '1')
            ->where('material.t_moneda', '=', $datos[1])
            ->where('material.t_compra', '=', $datos[2])
            ->where('material.cod_subcategoria', '=', $datos[0])
            ->orderBy('material.descrip_material', "asc")
            ->get();
        return $materiales;
    }

    public function obtmateriales_kardex($var)
    {
        $datos = explode("+", $var);
        $empresa = Auth::user()->RUC_empresa;
        $materiales = DB::table('kardex_material')
            ->join('material', 'kardex_material.cod_material', '=', 'material.cod_material')
            ->join('unidad_medida', 'material.unidad_compra', '=', 'unidad_medida.cod_unidad_medida')
            ->join('subcategoria', 'material.cod_subcategoria', '=', 'subcategoria.cod_subcategoria')
            ->where('kardex_material.RUC_empresa', '=', $empresa)
            ->where('kardex_material.stock_total', '>', '0')
            ->where('material.t_compra', '=', $datos[1])
            ->where('material.cod_subcategoria', '=', $datos[0])
            ->orderBy('material.descrip_material', "asc")
            ->get();
        return $materiales;
    }

    public function obt_kardex()
    {
        $empresa = Auth::user()->RUC_empresa;
        $materiales = DB::table('kardex_cab')
            ->join('material', 'kardex_cab.cod_material', '=', 'material.cod_material')
            ->where('kardex_material.RUC_empresa', '=', $empresa)
            ->get();
        return $materiales;
    }

    public function obt_kardex_tienda()
    {
        $empresa = Auth::user()->RUC_empresa;
        $materiales = DB::table('kardex_tienda')
            ->join('material', 'kardex_tienda.codigo_material', '=', 'material.cod_material')
            ->join('unidad_medida', 'material.unidad_compra', '=', 'unidad_medida.cod_unidad_medida')
            ->join('subcategoria', 'material.cod_subcategoria', '=', 'subcategoria.cod_subcategoria')
            ->join('almacen', 'kardex_tienda.codigo_almacen', '=', 'almacen.cod_almacen')
            ->where('kardex_tienda.RUC_empresa', '=', $empresa)
            ->get();
        return $materiales;
    }

    public function obt_rordendes()
    {
        $empresa = Auth::user()->RUC_empresa;
        $ordenes = DB::table('orden_compra')
            ->join('proveedor', 'orden_compra.RUC_proveedor', '=', 'proveedor.RUC_proveedor')
            ->where('orden_compra.RUC_empresa', '=', Auth::user()->RUC_empresa)
            ->orderBy('orden_compra.fecha_orden_compra', 'desc')
            ->select('proveedor.nom_proveedor', 'orden_compra.cod_orden_compra', 'orden_compra.fecha_orden_compra', 'orden_compra.estado_orden_compra')
            ->get();
        return $ordenes;
    }

    public function obt_suministro($var)
    {
        $empresa = Auth::user()->RUC_empresa;
        $materiales = DB::table('material')
            ->join('unidad_medida', 'material.unidad_compra', '=', 'unidad_medida.cod_unidad_medida')
            ->where('material.RUC_empresa', '=', $empresa)
            ->where('material.cod_subcategoria', '=', $var)
            ->orderBy('material.descrip_material', "asc")
            ->get();
        return $materiales;
    }

    public function mat_indirecto()
    {
        $empresa = Auth::user()->RUC_empresa;
        $actualizar = DB::table('gasto_suministro')
            ->join('material', 'gasto_suministro.cod_material', '=', 'material.cod_material')
            ->where('gasto_suministro.RUC_empresa', '=', $empresa)
            ->where('gasto_suministro.estado_suministro', '=', '3')
            ->select('gasto_suministro.codigo_suministro', 'gasto_suministro.consumo'
                , 'material.t_moneda', 'material.costo_sin_igv_material',
                'gasto_suministro.gasto_mensual_suministro', 'material.factor_equivalencia',
                'gasto_suministro.meses_duracion')
            ->get();
        $sueldo = DB::table('gasto_sueldos')
            ->where('gasto_sueldos.RUC_empresa', '=', $empresa)
            ->where('gasto_sueldos.estado', '=', '2')
            ->select('gasto_sueldos.id_gastos_sueldos', 'gasto_sueldos.sueldo_mensual'
                , 'gasto_sueldos.beneficios', 'gasto_sueldos.otros')
            ->get();
        $conf = DB::Table('configuracion_valores')
            ->where('nombre', '=', 'Dolar')
            ->get();
        if (count($actualizar) != 0) {
            for ($i = 0; $i < count($actualizar); $i++) {
                $costo = 0;
                $precio = $actualizar[$i]->costo_sin_igv_material;
                if ($actualizar[$i]->t_moneda == 1) {
                    $precio = $precio * $conf[0]->valor;
                }
                $costo = ($precio * $actualizar[$i]->consumo) / $actualizar[$i]->meses_duracion;
                $act = MaterialSuministroModel::where('codigo_suministro', $actualizar[$i]->codigo_suministro)
                    ->update(['gasto_mensual_suministro' => $costo, 'estado_suministro' => 1]);
            }
        }
        if (count($sueldo) != 0) {
            $empresas = DB::table('empresa')
                ->where('RUC_empresa', '=', $empresa)
                ->select('cod_regimen_laboral')
                ->get();
            $beneficios = DB::table('escala_salarial')
                ->join('trabajador', 'escala_salarial.cod_trabajador', '=', 'trabajador.DNI_trabajador')
                ->where('escala_salarial.RUC_empresa', '=', $empresa)
                ->where('escala_salarial.cod_regimen_laboral', '=', $empresas[0]->cod_regimen_laboral)
                ->where('trabajador.estado_trabajador', '=', 1)
                ->where('escala_salarial.estado', '=', 1)
                ->avg('tasa_beneficio');
            if ($beneficios == "") {
                $beneficios = 0;
            } else {
                $beneficios = $beneficios / 100;
            }
            $produccion_promedio = DB::Table('datos_generales')
                ->where('RUC_empresa', '=', $empresa)
                ->sum('produccion_promedio');
            if ($produccion_promedio == "") {
                $produccion_promedio = 0;
            }
            for ($i = 0; $i < count($sueldo); $i++) {
                $bene = $sueldo[$i]->sueldo_mensual * $beneficios;
                $nuevo_total = $sueldo[$i]->sueldo_mensual + $bene + $sueldo[$i]->otros;
                $act = Sueldos::where('id_gastos_sueldos', $sueldo[$i]->id_gastos_sueldos)
                    ->update(['estado' => 1, 'beneficios' => $bene, 'gasto_mensual' => $nuevo_total]);
            }
        }
        return "S";
    }

    public function corregir_material()
    {
        $codigo_orden = Input::get('orden');
        $lista_materiales = DB::table('detalle_orden_compra')
            ->join('material', 'detalle_orden_compra.cod_material', '=', 'material.cod_material')
            ->where('detalle_orden_compra.cod_orden_compra', '=', $codigo_orden)
            ->select('material.cod_material')
            ->get();

        $contador = 0;
        if (count($lista_materiales) != 0) {
            for ($i = 0; $i < count($lista_materiales); $i++) {
                $existe_cabecera = DB::Table('kardex_material')
                    ->where('kardex_material.cod_kardex_material', '=', $lista_materiales[$i]->cod_material)
                    ->select('kardex_material.cod_kardex_material')
                    ->get();
                $existe_detalle = DB::Table('detalle_kardex_material')
                    ->where('detalle_kardex_material.cod_kardex_material', '=', $lista_materiales[$i]->cod_material)
                    ->select('detalle_kardex_material.cod_kardex_material')
                    ->get();
                if (count($existe_cabecera) == 1 && count($existe_detalle) == 0) {
                    $borrar_cabecera = DB::Table('kardex_material')->where('cod_kardex_material', '=', $lista_materiales[$i]->cod_material)->delete();
                    $contador++;
                }
            }
        }
        session()->flash('success', 'Se corriguieron ' . $contador . ' materiales, reintente su ingreso a kardex');
        return Redirect::to('logistica/recoger/' . $codigo_orden);
    }
}
