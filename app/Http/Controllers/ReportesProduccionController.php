<?php

namespace erpCite\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use erpCite\OrdenProduccion;
use erpCite\TallasPares;
use erpCite\DatosGeneralesModel;
use erpCite\Linea;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;


class ReportesProduccionController extends Controller
{
  public function __construct()
{
  $this->middleware('jefe');
}
public function index(Request $request)
{
if ($request) {
  $año_actual=date("Y");
  $linea=DB::table('linea')
    ->where('RUC_empresa',Auth::user()->RUC_empresa)
    ->where('estado_linea','=','1')
    ->orderBy('cod_linea')
    ->get();
$ordenes_totales=DB::table('orden_produccion')
    ->where('estado_op','=','1')
    ->where('RUC_empresa',Auth::user()->RUC_empresa)
    ->whereYear('ingreso_prod', '=', $año_actual)
    ->orderBy('ingreso_prod', 'asc')
    ->get();
$ordenes=DB::table('orden_produccion')
    ->where('estado_op','=','1')
    ->where('RUC_empresa',Auth::user()->RUC_empresa)
    ->orderBy('ingreso_prod', 'asc')
    ->get();
$mesbase=0;
$c=0;
for($i=0;$i<count($ordenes);$i++)
{
  $mes_orden=$ordenes[$i]->ingreso_prod;
  $d = date_parse_from_format("Y-n-d", $mes_orden);
  if($mesbase!=$d["month"])
  {
    $mesbase=$d["month"];
    $c++;
  }
}
$primera_vez=0;
if($c<=1)
{
$primera_vez=1;
}

$mes_actual=date("n");
$añoprevio="";
if($mes_actual<4)
{
  $añoprevio=DB::table('orden_produccion')
      ->where('estado_op','=','1')
      ->where('RUC_empresa',Auth::user()->RUC_empresa)
      ->whereYear('ingreso_prod', '=', $año_actual-1)
      ->orderBy('ingreso_prod', 'asc')
      ->get();
  if(count($añoprevio)==0)
  {
    $añoprevio="";
  }
  else {
    switch ($mes_actual) {
      case 1:
          $base=10;
        break;
      case 2:
          $base=11;
      break;
      case 3:
          $base=12;
      break;
      default:
        break;
    }
    for($i=0;$i<count($linea);$i++)
    {
      $mes=$base;
    $previo_octubre_array[$i]=0;
    $previo_noviembre_array[$i]=0;
    $previo_diciembre_array[$i]=0;
    $totales[$i]=0;
      $total=0;
      for($j=0;$j<count($añoprevio);$j++)
      {
        if($linea[$i]->cod_linea==$añoprevio[$j]->cod_linea)
        {

            $mes_orden=$añoprevio[$j]->ingreso_prod;
            $d = date_parse_from_format("Y-n-d", $mes_orden);
            if($mes!=$d["month"] && $d["month"]>=$base)
            {
              $mes=$d["month"];
              $total=0;
            }
            if($d["month"]>=$base)
            {
              $total=$total+$añoprevio[$j]->total_pares;
            }
            switch ($mes) {
              case 10:
                $previo_octubre_array[$i]=$total;
              break;
              case 11:
                $previo_noviembre_array[$i]=$total;
                break;
              case 12:
                $previo_diciembre_array[$i]=$total;
              break;
              default:
                // code...
                break;
            }
        }
      }
      $totales[$i]=$previo_octubre_array[$i]+$previo_noviembre_array[$i]+$previo_diciembre_array[$i];
    }

  }
}
else {
  $añoprevio="";
}
for($i=0;$i<count($linea);$i++)
{
  $mes=1;
  $enero_array[$i]=0;
$febrero_array[$i]=0;
$marzo_array[$i]=0;
$abril_array[$i]=0;
$mayo_array[$i]=0;
$junio_array[$i]=0;
$julio_array[$i]=0;
$agosto_array[$i]=0;
$setiembre_array[$i]=0;
$octubre_array[$i]=0;
$noviembre_array[$i]=0;
$diciembre_array[$i]=0;
$total_actual[$i]=0;
  $total=0;
  for($j=0;$j<count($ordenes_totales);$j++)
  {
    if($linea[$i]->cod_linea==$ordenes_totales[$j]->cod_linea)
    {
        $mes_orden=$ordenes_totales[$j]->ingreso_prod;
        $d = date_parse_from_format("Y-n-d", $mes_orden);
        if($mes!=$d["month"])
        {
          $mes=$d["month"];
          $total=0;
        }
        $total=$total+$ordenes_totales[$j]->total_pares;
        switch ($mes) {
          case 1:
            $enero_array[$i]=$total;
            break;
          case 2:
            $febrero_array[$i]=$total;
          break;
          case 3:
            $marzo_array[$i]=$total;
            break;
          case 4:
            $abril_array[$i]=$total;
          break;
          case 5:
            $mayo_array[$i]=$total;
            break;
          case 6:
            $junio_array[$i]=$total;
          break;
          case 7:
            $julio_array[$i]=$total;
            break;
          case 8:
            $agosto_array[$i]=$total;
          break;
          case 9:
            $setiembre_array[$i]=$total;
            break;
          case 10:
            $octubre_array[$i]=$total;
          break;
          case 11:
            $noviembre_array[$i]=$total;
            break;
          case 12:
            $diciembre_array[$i]=$total;
          break;
          default:
            // code...
            break;
        }
    }
  }
  $total_actual[$i]=$enero_array[$i]+$febrero_array[$i]+$marzo_array[$i]+$abril_array[$i]+$mayo_array[$i]+$junio_array[$i]+$julio_array[$i]+$agosto_array[$i]+$setiembre_array[$i]+$octubre_array[$i]+$noviembre_array[$i]+$diciembre_array[$i];
}
if($añoprevio!="")
{
  for ($i=0;$i < count($total_actual) ; $i++) {
    $total_actual[$i]=$total_actual[$i]+$totales[$i];
  }
}
$totalaso=0;
for($i=0;$i<count($total_actual);$i++)
{
  $totalaso=$totalaso+$total_actual[$i];
}
$totaldoc=0;
for($i=0;$i<count($total_actual);$i++)
{
  $docenas[$i]=$total_actual[$i]/12;
  $docenas[$i]=number_format($docenas[$i],0);
  $totaldoc=$docenas[$i]+$totaldoc;
  $porcentaje[$i]=number_format(($total_actual[$i]/$totalaso)*100,2);
}

if ($añoprevio!="") {
  return view('Produccion.reportes.index',["linea"=>$linea,
  "primera_vez"=>$primera_vez,
  "añoprevio"=>$añoprevio,
  "previo_octubre"=>$previo_octubre_array,
  "previo_noviembre"=>$previo_noviembre_array,
  "previo_diciembre"=>$previo_diciembre_array,
  "enero"=>$enero_array,
  "febrero"=>$febrero_array,
  "marzo"=>$marzo_array,
  "abril"=>$abril_array,
  "mayo"=>$mayo_array,
  "junio"=>$junio_array,
  "julio"=>$julio_array,
  "agosto"=>$agosto_array,
  "setiembre"=>$setiembre_array,
  "octubre"=>$octubre_array,
  "noviembre"=>$noviembre_array,
  "diciembre"=>$diciembre_array,
  "totalpares"=>$total_actual,
  "doc"=>$docenas,
  "porcentaje"=>$porcentaje,
  "totalaso"=>$totalaso,
  "totaldoc"=>$totaldoc]);
  }
  else
  {
    return view('Produccion.reportes.index',["linea"=>$linea,
    "primera_vez"=>$primera_vez,
    "añoprevio"=>$añoprevio,
    "enero"=>$enero_array,
    "febrero"=>$febrero_array,
    "marzo"=>$marzo_array,
    "abril"=>$abril_array,
    "mayo"=>$mayo_array,
    "junio"=>$junio_array,
    "julio"=>$julio_array,
    "agosto"=>$agosto_array,
    "setiembre"=>$setiembre_array,
    "octubre"=>$octubre_array,
    "noviembre"=>$noviembre_array,
    "diciembre"=>$diciembre_array,
    "totalpares"=>$total_actual,
    "doc"=>$docenas,
    "porcentaje"=>$porcentaje,
    "totalaso"=>$totalaso,
    "totaldoc"=>$totaldoc]);
  }

  }
}
public function create(Request $request)
{


}
public function store()
{
  $empresa=Auth::user()->RUC_empresa;
  $promedio=Input::Get("promedio");
    $datosgenerales=DB::Table("datos_generales")
    ->where("RUC_empresa","=",$empresa)
    ->get();
    if(count($datosgenerales)>0)
    {
      $dato=DatosGeneralesModel::where("RUC_empresa",$empresa)
      ->update(["produccion_promedio"=>$promedio]);
      session()->flash('success','Produccion Promedio Actualizadas');
    }
    else {
      $dato=new DatosGeneralesModel;
      $dato->RUC_empresa=$empresa;
      $dato->produccion_promedio=$promedio;
      $dato->save();
      session()->flash('success','Produccion Promedio Actualizada');
    }

    return Redirect::to('Produccion/reportes');

}
public function show()
{
 /* return view('logistica.clasificacion.index',["clasificacion"=>$clasi]);*/
}
public function edit($id)
{
 /* return Redirect::to('logistica/clasificacion');*/
}
public function update()
{

 /* return Redirect::to('logistica/articulo');*/
}
public function destroy()
{

}
/*
public function ajax()
  {
    $ordenesProd=DB::table('orden_produccion')
  ->join('linea','orden_produccion.cod_linea','=','linea.cod_linea')
    ->where('estado_op','=','1')
    ->where('orden_produccion.RUC_empresa',Auth::user()->RUC_empresa)->paginate(10);

    return view('Produccion.reportes.index',["ordenesProd"=>$ordenesProd]);

 $identificador=rand(10000,99999);
$serie=DB::table('serie')->where('estado_serie','=','1')->where('RUC_empresa','=',Auth::user()->RUC_empresa)->get();

    $empresa=DB::table('empresa')->where('RUC_empresa','=',Auth::user()->RUC_empresa)->get();

        $states = DB::table('linea')->where('estado_linea','=','1')->where('RUC_empresa',Auth::user()->RUC_empresa)->paginate(10);

    return view('Produccion.reportes.index',["states"=>$states,"empresa"=>$empresa,"identificador"=>$identificador,"serie"=>$serie]);
  }

  public function ajaxAct($id,$id2)
  {

    $ordenesProd=DB::table('orden_produccion')
  ->join('linea','orden_produccion.cod_linea','=','linea.cod_linea')
    ->where('estado_op','=','1')
    ->where('orden_produccion.RUC_empresa',Auth::user()->RUC_empresa)->paginate(10);

    return view('Produccion.reportes.index',["ordenesProd"=>$ordenesProd]);
if($id2==1){
       $cities=DB::table('orden_produccion')
   ->selectRaw('sum(orden_produccion.total_pares) as sum')

   ->whereYear('ingreso_prod', '=', $id)
       ->pluck('sum','sum');

       return json_encode($cities);
    }

  }
*/



}
