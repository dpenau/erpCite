<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DetalleCompraTemp extends Model
{
    protected $table='detalle_orden_compra_temp';

    protected $primaryKey='cod_temp_mat';
  
    public $timestamps=false;
  
    protected $fillable=['subvalor_total','cantidad','costo_total','cod_material','cod_cab_ordencompra','cantidad_faltante','tipo_orden'];
  
    protected $guarded=[];
}
