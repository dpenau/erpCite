<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DesarrolloProductoModel extends Model
{
  protected $table='gasto_desarrollo_producto';

  protected $primaryKey='cod_gasto_desarrollo_producto';

  public $timestamps=false;


  protected $fillable=['cod_area','descrip_gasto','cantidad_mensual','gasto_total','importe','RUC_empresa','tipo_gasto','cod_tipo_desarrollo','estado'];

  protected $guarded=[];
}
