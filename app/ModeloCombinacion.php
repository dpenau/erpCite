<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class ModeloCombinacion extends Model
{
    protected $table='combinacion_modelo';

    protected $primaryKey="cod_combinacion";

    protected $keyType = "string";

    public $timestamps=false;

    protected $fillable=['descripcion','imagen','cod_modelo','RUC_empresa','estado_modelo','codigo_comb','aprobado','costo_mano_directa','costo_material_directo','modelo_base'];

    protected $guarded=[];
}
