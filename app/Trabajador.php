<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
  protected $table='trabajador';

  protected $primaryKey="DNI_trabajador";

  protected $keyType = "string";

  public $timestamps=false;


  protected $fillable=['nombres','correo', 'apellido_paterno', 'apellido_materno', 'direccion','cod_distrito','experiencia_laboral','telefono', 'sexo', 'fecha_nacimiento', 'puesto', 'foto','estado_trabajador', 'cod_tipo_trabajador', 'cod_area','g_instruccion', 'RUC_empresa','usuario', 'especialidad', 'eficiencia'];

  protected $guarded=[];
}
