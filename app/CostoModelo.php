<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class CostoModelo extends Model
{
  protected $table='costo_modelo';

protected $primaryKey="cod_costo_modelo";
protected $keyType = "string";

public $timestamps=false;


protected $fillable=['RUC_empresa','total_materiales','total_obra','estado','modelo_serie'];

protected $guarded=[];
}
