<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DetalleOrdenCompra extends Model
{
  protected $table='detalle_orden_compra';

  protected $primaryKey="id_detalle_oc";

  public $timestamps=false;


  protected $fillable=['fecha_deposito','cod_orden_compra','cod_material','cantidad','costo_unitario','costo_total','material_recibido','pago_contado','pago_credito','fecha_pago','cantidad_recibida','cantidad_restante'];

  protected $guarded=[];
}
