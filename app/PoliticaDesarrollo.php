<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class PoliticaDesarrollo extends Model
{
    protected $table='politica_desarrollo';

  protected $primaryKey="id";

  public $timestamps=false;


  protected $fillable=['produccion_promedio','producto','hormas','troqueles'];

  protected $guarded=[];
}
