<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
  protected $table='modelo';

  protected $primaryKey="cod_modelo";

  protected $keyType = "string";

  public $timestamps=false;

  protected $fillable=['nombre','descripcion','RUC_empresa','estado_modelo','cod_linea','cod_capellada','cod_forro',
'cod_piso','cod_plantilla','cod_coleccion','cod_horma','genero_modelo','cod_serie','num_combinacion','codigo','comb_proceso','comb_aprobado'];

  protected $guarded=[];
}
