<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class kardex_cab_talla_tallas extends Model
{
    protected $table='kardex_cab_talla_tallas';
    protected $primaryKey='id';
    public $timestamps=false;
    protected $fillable=['id', 'talla','cantidad', 'cod_kardex'];
    protected $guarded=[];
}
