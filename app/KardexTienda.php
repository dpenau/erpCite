<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class KardexTienda extends Model
{
    protected $table='kardex_tienda';

    protected $primaryKey='cod_kardex_tienda';

    protected $keyType="string";

    public $timestamps=false;

    protected $fillable=['codigo_material','codigo_almacen','lugar_tienda','stock_total_tienda','RUC_empresa'];

    protected $guarded=[];
}
