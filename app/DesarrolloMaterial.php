<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DesarrolloMaterial extends Model
{
    protected $table='desarrollo_material';

    protected $primaryKey="id";

    public $timestamps=false;

    protected $fillable=['proceso_id',
                        'RUC_empresa',
                        'numero_prototipo',
                        'descripcion_material',
                        'costo_total',
                        'importe',
                        'estado_registro'];

    protected $guarded=[];
    public function proceso()
    {
        return $this->belongsTo(Proceso::class,'proceso_id','cod_proceso');
    }
}
