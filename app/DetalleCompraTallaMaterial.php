<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class DetalleCompraTallaMaterial extends Model
{
    protected $table='detalle_material_talla';

    protected $primaryKey='cod_temp_mat';
  
    public $timestamps=false;
  
    protected $fillable=['cantidad','cod_material','cod_cab_detalle_talla','cantidad_faltante','talla'];
  
    protected $guarded=[];
}
