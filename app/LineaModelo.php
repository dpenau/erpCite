<?php

namespace erpCite;

use Illuminate\Database\Eloquent\Model;

class LineaModelo extends Model
{
  protected $table='linea_modelo';

public $timestamps=false;

	
protected $fillable=['cod_linea','cod_modelo','RUC_empresa'];

protected $guarded=[];
}
